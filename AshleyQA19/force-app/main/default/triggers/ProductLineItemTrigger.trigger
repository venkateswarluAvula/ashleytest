trigger ProductLineItemTrigger on ProductLineItem__c (after Update) {
    if(trigger.isAfter && trigger.isUpdate){
      ProductLineItemTriggerHandler.handleAfterUpdate(Trigger.newMap, Trigger.oldMap);  
    }
}