trigger OpportunityTrigger on Opportunity(before insert, before update, after insert, after update ) {

    if(trigger.isBefore){
         
         if (trigger.isInsert || trigger.isUpdate){
             
             OpportunityHandlerController.ADSZipandMrktRegnUpdate(Trigger.new);
         }
    }  
}