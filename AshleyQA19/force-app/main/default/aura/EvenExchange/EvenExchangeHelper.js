({
	sendValues : function(component, event, helper) {        
        var action2 = component.get("c.sendEvenexchangeValue");  
        var dDate = component.get("v.selectedDateToDelivery");
        var selectedPR = component.get("v.selectedPR");
        var checkvalue = component.get("v.starFlags");  
        var vipdesc = component.get("v.vipDescription");
        var hccdesc = component.get("v.hccDescription");     
        var selectedPRv = component.get("v.selectedPRv");
        var selectedlitem = component.get("v.selectedLineItem");
        var selpro = component.get("v.productlineItems");
        var finalpli = component.get("v.selproductlineItems");
        
        for (var j =0; j < selectedlitem.length;j++){
            for(var z = 0; z < selpro.length; z++){
                if(selpro[z].Id == selectedlitem[j]){
                    finalpli.push(selpro[z]);
                }
            }
        }
        
        for (var i = 0; i < selectedPR.length; i++) {
            if(!$A.util.isEmpty(checkvalue)){
                for(var j = 0; j < checkvalue.length; j++){
                    if(checkvalue[j].ItemID == selectedPR[i]){
                        var vsdes = selectedPR[i] + ":" + checkvalue[j].ItemDescription;
                        selectedPRv.push(vsdes);
                    }
                }
            }            
        }
        action2.setParams({
            "deliveryDate": dDate,
            "salesordercomment" : component.get("v.salesOrdercomment"),
            "caselists" : component.get("v.caseAddress"),
            "lineItems" : component.get("v.orderAddress"),
            "selproductlineitems" : finalpli,
            "salesorder" : component.get("v.salesOrder"),
            "AddressType" : component.get("v.AddressValue"),            
            "Phone1" : component.get("v.caseAddress[0].Account.Phone"),
            "Phone2" : component.get("v.caseAddress[0].Contact.Phone_2__c"),
            "Phone3" : component.get("v.caseAddress[0].Contact.Phone_3__c"),
            "emailinfo" : component.get("v.caseAddress[0].ContactEmail"),
            "caseId": component.get("v.recordId"),
            "vipdes" : component.get("v.vipDescription"),
            "hccdes" : component.get("v.hccDescription"),
            "addStars" : selectedPRv            
        });
        action2.setCallback(this, function(response) {
            if (component.isValid() && response.getState() == "SUCCESS") {                
                console.log(response.getReturnValue());
                var callResponse = response.getReturnValue(); 
				if(callResponse.isSuccess == true){
					if (callResponse.message != null){						
						//Display call response message
						var warningToast = $A.get("e.force:showToast");
                        var returnv = callResponse.message;
                        var res = returnv.split(" ");
                        if (res.length > 0){
                            for(var j = 0; j < res.length; j++){
                               if(!isNaN(res[j])){                                    
                                    component.set("v.newsalesOrder", res[j]);
                                    break;
                                } 
                            }                                                        
                        }
                        component.set("{!v.showSpinner}", false);
						warningToast.setParams({"message": callResponse.message, "type":"warning"});
						warningToast.fire();
					}
				} else {
                    component.set("{!v.showSpinner}", false);
					var errorToast = $A.get("e.force:showToast");
					errorToast.setParams({"message": callResponse.message, "type":"error", "mode":"sticky"});
					errorToast.fire();
				}
            } else {
                component.set("{!v.showSpinner}", false);
                component.set("v.openModal", false);                
                console.log("Failed with state: " + JSON.stringify(response.getError()));                
            }
        });
        $A.enqueueAction(action2);		
	}
})