({
	myAction : function(component, event, helper) {
		
	},
    dissociateOrder : function(component, event, helper) {
		console.log("recordLoader--->"+component.get("v.recordLoader"));
        var getId = component.get("v.recordId");
        console.log("getId--->"+component.get("v.recordId"));
        var saveRecord = component.get("c.getCase");
        component.set("v.openModal", false);
        saveRecord.setParams({
            caseId : getId,
        });
        saveRecord.setCallback(this,function(res){
            console.log('selectedItems::::res.getReturnValue-->',+res.getReturnValue());
            var result = res.getReturnValue();
            console.log('isSavemySN--->',+result);
            if(result== '1')
        	{
                $A.get('e.force:refreshView').fire();
                var toastEvent = $A.get("e.force:showToast");
                    	toastEvent.setParams({
                    		title: "Success!",
                    		message: "Successfully Dissociate Order.",
                    		type: "success"
                    	});
                toastEvent.fire();
            }
            else
            {
              console.log('Saved--->',+result);
              var toastEvent2 = $A.get("e.force:showToast");
                    	toastEvent2.setParams({
                    		title: "Error!",
                    		message: "No Sales Order Found.",
                    		type: "error"
                    	});
                toastEvent2.fire();
            }
         });
        $A.enqueueAction(saveRecord);
    },
    openModal : function(component, event, helper) { 
        component.set("v.openModal",true);
    },
    closeModel: function(component, event, helper) {
      component.set("v.openModal", false);
   },
})