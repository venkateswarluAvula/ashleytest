({
    // object and set the corresponding aura attribute.
    getvalesfromUI : function(component,event,helper) {
        var action = component.get("c.SalesOrderHotStatus");
        var startDateField = component.find("StartDate");
        var startDateFieldValue = startDateField.get("v.value");
        var strtdat = new Date(startDateFieldValue);
        if (!isNaN(strtdat.getTime())) {
            // Months use 0 index.
            var startdatevalupd =  strtdat.getMonth() + 1 + '/' + strtdat.getDate() + '/' + strtdat.getFullYear();
            component.set('v.updatedstartdate', startdatevalupd);
        }
        var endDateField = component.find("EndDate");
        var endDateFieldValue = endDateField.get("v.value");
        var enddate = new Date(endDateFieldValue);
        if (!isNaN(enddate.getTime())) {
            // Months use 0 index.
            var enddatevalupd =  enddate.getMonth() + 1 + '/' + enddate.getDate() + '/' + enddate.getFullYear();
            component.set('v.updatedenddate', enddatevalupd);
        }
        var marketField = component.find("MarketAccount");
        var marketFieldValue = marketField.get("v.value");
        action.setParams({
            'startdate':component.get('v.updatedstartdate'),
            'enddate':component.get('v.updatedenddate'),
            'market':marketFieldValue
        });
        action.setCallback(this, function(response) {
            console.log('res'  + response.getReturnValue());
            response = response.getReturnValue();
            var json = JSON.parse(response);
            console.log('values'+json);
            component.set('v.myData',json);
            if(json.length > 0){
                var totalRecordsList = json;
                var totalLength = totalRecordsList.length ;
                component.set("v.isExport",false);
                component.set("v.totalRecordsCount", totalLength);
                component.set("v.bNoRecordsFound" , false);
            }else{
                // if there is no records then display message
                component.set("v.bNoRecordsFound" , true);
            }
        });
        $A.enqueueAction(action);
    },
    convertArrayOfObjectsToCSV : function(component,objectRecords){
        // declare variables
        var csvStringResult, counter, keys, columnDivider, lineDivider;
       
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
         }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
 
        // in the keys valirable store fields API Names as a key 
        // this labels use in CSV file header  
        keys = ['OrderNumber','Market','AccountShipTo','DeliveryDate','WindowBegin','WindowEnd','Type','DateMarkedHot'];
        
        csvStringResult = '';
        csvStringResult += keys.join(columnDivider);
        csvStringResult += lineDivider;
 
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;
           
             for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
 
              // add , [comma] after every String value,. [except first]
                  if(counter > 0){ 
                      csvStringResult += columnDivider; 
                   }   
               
               csvStringResult += '"'+ objectRecords[i][skey]+'"'; 
               
               counter++;
 
            } // inner for loop close 
             csvStringResult += lineDivider;
          }// outer main for loop close 
       
       // return the CSV formate String 
        return csvStringResult;        
    },
})