({
    initSO : function(component, event, helper) {
        
        var actioncaseDetails = component.get("c.getCaseinfo");
        actioncaseDetails.setParams({
            "caseId": component.get("v.recordId")
        });
        actioncaseDetails.setCallback(this, function(caseResponse){
            if (component.isValid() && caseResponse.getState() === "SUCCESS") {
                if (caseResponse.getReturnValue() != null) {
                    component.set("v.caseAccShipTo", caseResponse.getReturnValue().Legacy_Account_Ship_To__c);
                    console.log('caseShipTo--' + component.get("v.caseAccShipTo")); 
                }
            } else {
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": caseResponse.getError()[0].message, "type":"error"});
                errorToast.fire();
            }
        })
        $A.enqueueAction(actioncaseDetails);
        
        var actionSalesOrderList = component.get("c.getSalesOrders");
        actionSalesOrderList.setParams({
            "caseId": component.get("v.recordId")
        });
        actionSalesOrderList.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                component.set("v.salesOrders" , response.getReturnValue());
                console.log('line: '+JSON.stringify(response.getReturnValue()));
            } else {
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": response.getError()[0].message, "type":"error"});
                errorToast.fire();
            }
        })
        $A.enqueueAction(actionSalesOrderList);
    },
    
    updateSalesOrder:function(component, event, helper) {
        var caseId = component.get("v.recordId");
        var selectedOption = component.find("mySelect").get("v.value");
        var optionVals = selectedOption.split("||");
        var caseAccShipto = component.get("v.caseAccShipTo");
        console.log('caseAccShipto--'+caseAccShipto);
        var soLegacyAccShipto = optionVals[3];
        console.log('caseId: ' + caseId + ' :ExternalId: ' + optionVals[0] + ' :caseAccShipto: ' + caseAccShipto + ' :StoreNameStoreNumberPC: ' + optionVals[1] + ' :solegacyAccshipto: ' + soLegacyAccShipto);
        if(optionVals[0] != '' ) {
            
            if ((caseAccShipto != null) && (caseAccShipto != soLegacyAccShipto)){
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": "The Order you are trying to associate to this case is from a different market than " + caseAccShipto + ". Please create a new Case for this Order.", "type":"error"});
                errorToast.fire();
            } else {
                var actionUpdate = component.get('c.updateSalesOrderRecord');
                actionUpdate.setParams({
                    "caseId": caseId,
                    "soId": optionVals[0],
                    "storeName": optionVals[2],
                    "NPS":optionVals[4],
                    "accShipto": soLegacyAccShipto
                });
                actionUpdate.setCallback(this, function(response) {
                    if(component.isValid() && response.getState() === "SUCCESS") {
                        console.log("response: " + response.getReturnValue());
                        
                        if(response.getReturnValue() == 'success') {
                            var navEvt = $A.get("e.force:navigateToSObject");
                            navEvt.setParams({ "recordId": caseId , "slideDevName": "detail" });
                            navEvt.fire();
                            
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                title: "Success!",
                                message: "Successfully submitted Sales Orders!",
                                type: "success"
                            });
                            toastEvent.fire();
                            
                            $A.get('e.force:refreshView').fire();
                        } else {
                            var errorToast = $A.get("e.force:showToast");
                            errorToast.setParams({"message": "Error while updating case details, please contact admin.", "type":"error"});
                            errorToast.fire();
                        }
                    }
                })
                $A.enqueueAction(actionUpdate);
            }
        } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Error",
                message: "Please choose the Sales Order.",
                type: "Error"
            });
            toastEvent.fire();
        }
    }
})