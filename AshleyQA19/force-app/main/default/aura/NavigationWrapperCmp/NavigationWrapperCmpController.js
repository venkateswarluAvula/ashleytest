({
    Init : function(component, event, helper) {
        /* var pageReference = component.get("v.pageReference");
         alert(JSON.stringify(pageReference.state));
          var objkeys = Object.keys(pageReference.state); 
        
        alert(pageReference.state[objkeys[0]]);
        var parameterWithoutCUnderscore = {};
        var val = pageReference.state;
        for(var j in val){
            var sub_key = j;        
            if(j!=='c__subCmpName'){
                var new_sub_key = sub_key.split('c__').join('');
                parameterWithoutCUnderscore[new_sub_key]=val[j];
            }
        }
        alert(pageReference.state.c__subCmpName);
        var cmpNameString = pageReference.state.c__subCmpName;
         var cmpNameList = cmpNameString.split(',');
         alert(JSON.stringify(cmpNameList));
       
         alert(JSON.stringify(parameterWithoutCUnderscore));
        */
        var pageReference = component.get("v.pageReference");
        
        
        var receivedParams = pageReference.state.c__TargetParameters;
        
        //Read parameters from pageReference state
        var cmpNameString = receivedParams.subCmpName;
        var cmpNameList = cmpNameString.split(',');
        var parameterWithoutCUnderscore = {};
        var val = receivedParams;
        
        for(var j in val){
            var sub_key = j;        
            if(j!=='subCmpName'){
                parameterWithoutCUnderscore[j]=val[j];
            }
        }        
        cmpNameList.forEach(function(currentValue, index, arr){
            var cmpName = "c:"+currentValue;
            $A.createComponent(cmpName,  parameterWithoutCUnderscore, 
                               function(newCmp) {
                                   // newCmp is a reference to another component
                                   var body = component.get("v.center");
                                   body.push(newCmp);
                                   component.set("v.center", body);
                               }); 
        });
        
        
    }
})