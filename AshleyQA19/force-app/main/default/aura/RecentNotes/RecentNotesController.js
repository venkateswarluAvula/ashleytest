({
    doInit : function(component, event, helper) {
        var action = component.get('c.searchNotes');
        var currentRecId = component.get("v.recordId");
        console.log('SuceessDataRecordID-->'+currentRecId);
        action.setParams({
            parentRecordId : currentRecId
        });
        action.setCallback(this, function(actionResult) {
            var successData = actionResult.getReturnValue();
            console.log('SuceessData-->'+successData);
            successData.forEach(function(record){
                record.linkName = '/'+record.Id;
            });
            component.set("v.myColumns", successData);
        });
        $A.enqueueAction(action);
    },
    
    editRecord : function (component, event, helper){
        
        var action = component.get('c.getNotes');
        var selectedItem = event.currentTarget;
        var idstr = selectedItem.getAttribute("data-recId");
        var currentRecId = idstr;
        action.setParams({
            recordId : currentRecId
        });
        action.setCallback(this, function(actionResult) {
            var successData = actionResult.getReturnValue();
            console.log('SuceessData-->'+successData);
            component.set("v.NotesToBeEdit", successData);
            
        });
        $A.enqueueAction(action);
        component.set("v.bShowModal",true); 
       /* 
        var rectarget = event.currentTarget;
        var idstr = rectarget.getAttribute("data-recId");
        console.log('index-->'+idstr);
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": idstr
        });
        editRecordEvent.fire(); */
    },
    
    OpenAddNote : function(component, event, helper) {
        var clickedBtn = event.getSource().getLocalId();
        if(clickedBtn == "Add")
        {
            component.set("v.flag",true);
            var elements = document.getElementsByClassName("allnotes");
            elements[0].style.display = 'none';
        }
    },
    
    UpdateNote: function (component, event, helper) {
        
        var title = component.find("titletext").get("v.value");
        console.log('title-->'+title);
        var content = component.find("bodytext").get("v.value");
        content = content.replace(/<\/?[^>]+(>|$)/g, "");
        console.log('aftercontent-->'+content);
        var recId = component.get("v.NotesToBeEdit[0].Id");
        console.log('Id-->'+recId);
        var parentId = component.get("v.recordId");
        console.log('Id-->'+parentId);
        var action = component.get("c.UpdateNotes");
        action.setParams({
            "title" : title,
            "content" : content,
            "Id" : recId,
            "ParentId" : parentId
        
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            $A.get('e.force:refreshView').fire();
            
        });
        $A.enqueueAction(action);
    },
    
    OpenRelatedList : function(component, event, helper) {
        var relatedListEvent = $A.get("e.force:navigateToRelatedList");
        relatedListEvent.setParams({
            "relatedListId": "AttachedContentNotes",
            "parentRecordId": component.get("v.recordId")
        });
        relatedListEvent.fire();
    },
    
    Create : function(component, event, helper) {
        //getting the candidate information
        var notes = component.get("v.note");
        //Calling the Apex Function
        var action = component.get("c.createRecord");
        //Setting the Apex Parameter
        action.setParams({
            cn : notes,
            ParentId : component.get("v.recordId")
        });
        //Setting the Callback
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            //check if result is successfull
            if(state == "SUCCESS"){
                //Reset Form
                var notes = {'sobjectType': 'ContentNote',
                             'Title': '',
                             'Content': ''
                            };
                //resetting the Values in the form
                component.set("v.note",notes);
                var toast = $A.get("e.force:showToast");
                toast.setParams({
                    title : 'Successful',
                    message: 'New Note Added Successfully.',
                    duration: '300',
                    type: 'success' 
                });
                toast.fire();
                $A.get('e.force:refreshView').fire();
                
            } else if(state == "ERROR"){
                alert('Error in calling server side action');
            }
            
        });
        var clickedBtn = event.getSource().getLocalId();
        if(clickedBtn == "create")
        {
            component.set("v.flag",true);
            var elements = document.getElementsByClassName("allnotes");
            elements[0].style.display = 'none';
        }
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    },
    
    Cancel : function(component,event,helper){
        var clickedBtn = event.getSource().getLocalId();
        if(clickedBtn == "close")
        {
            component.set("v.flag",true);
            var elements = document.getElementsByClassName("allnotes");
            elements[0].style.display = 'none';
        }
        $A.get('e.force:refreshView').fire();
    },
})