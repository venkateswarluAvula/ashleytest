({
    validate : function(component) {

        var validateData = true;
        try{
            var address1 = component.find("address1");
            var address1upd = address1.get("v.value");
            if ( $A.util.isEmpty(address1upd) || address1upd == '--SELECT--' || address1upd == undefined ) {
                validateData = false;
                address1.set("v.errors", [{message:"Address fields are mandatory"}]);
            } else {
                address1.set("v.errors", null);
            }

            var city = component.find("city");
            var cityup = city.get("v.value");
            if ( $A.util.isEmpty(cityup) || cityup == '--SELECT--' || cityup == undefined ) {
                validateData = false;
                city.set("v.errors", [{message:"City is required"}]);
            } else {
                city.set("v.errors", null);
            }

            var state = component.find("state");
            var stateup = state.get("v.value");
            if ( $A.util.isEmpty(stateup) || stateup == '--SELECT--' || stateup == undefined ) {
                validateData = false;
                state.set("v.errors", [{message:"State is required"}]);
            } else if (stateup.length != 2) {
                validateData = false;
                state.set("v.errors", [{message:"State should be of 2 character length"}]);
            } else {
            	console.log('length: ' + stateup.length);
                state.set("v.errors", null);
            }

            var zip = component.find("zip");
            var zipup = zip.get("v.value");
            if ( $A.util.isEmpty(zipup) || zipup == '--SELECT--' || zipup == undefined ) {
                validateData = false;
                zip.set("v.errors", [{message:"Zip is required"}]);
            }  else {
                zip.set("v.errors", null);
            }

            var phone1 = component.find("phone1");
            var phoneValue1= phone1.get("v.value");
            if (!$A.util.isEmpty(phoneValue1)) {
                var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                if(!phoneValue1.match(phoneno)) {
                    phone1.set("v.errors", [{message:"entry is an invalid phone number."}]);
                } else {
                    phone1.set("v.errors", null);
                }
            }

            var phone2 = component.find("phone2");
            var phoneValue2= phone2.get("v.value");
            if (!$A.util.isEmpty(phoneValue2)) {
                var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                if(!phoneValue2.match(phoneno)) {
                    phone2.set("v.errors", [{message:"entry is an invalid phone number."}]);
                } else {
                    phone2.set("v.errors", null);
                }
            }

            var email1 = component.find("email1");
            var emailValue1= email1.get("v.value");
            if (!$A.util.isEmpty(emailValue1)) {
                var phoneno = /(\w+)\@(\w+)\.[a-zA-Z]/g;
                if(!emailValue1.match(phoneno)) {
                    validateData = false;
                    email1.set("v.errors", [{message:"entry is an invalid email"}]);
                } else if (emailValue1.length > 40) {
					validateData = false;
                	email1.set("v.errors", [{message:"email length should not be greater than 40"}]);
                } else {
                    email1.set("v.errors", null);
                }
            }

            var email2 = component.find("email2");
            var emailValue2= email2.get("v.value");
            if (!$A.util.isEmpty(emailValue2)) {
                var phoneno = /(\w+)\@(\w+)\.[a-zA-Z]/g;
                if(!emailValue2.match(phoneno)) {
                    email2.set("v.errors", [{message:"entry is an invalid email"}]);
                } else {
                    email2.set("v.errors", null);
                }
            }

            var company = component.find("myCompany");
            var companyValue = company.get("v.value");
            if ( $A.util.isEmpty(companyValue) || companyValue == '--SELECT--' || companyValue == undefined ) {
                validateData = false;
                company.set("v.errors", [{message:"Company is required" }]);
            } else {
                company.set("v.errors", null);
            }

            var tech = component.find("technician");
            var techValue1= tech.get("v.value");
            if ( $A.util.isEmpty(techValue1) || techValue1 == '--SELECT--' || techValue1 == undefined ) {
                validateData = false;
                tech.set("v.errors", [{message:"Technician is required"}]);
            } else {
                tech.set("v.errors", null);
            }

            var timestop = component.find("myTimeEstimated");
            var meetingRoomValue1 = timestop.get("v.value");
            if ( $A.util.isEmpty(meetingRoomValue1) || meetingRoomValue1 == '--SELECT--' || meetingRoomValue1 == undefined ) {
                validateData = false;
                timestop.set("v.errors", [{message:"Estimated time is required"}]);
            } else {
	            if(parseInt(meetingRoomValue1) > 24 ) {
	                validateData = false;
	                timestop.set("v.errors", [{message:"The Hours for Estimated Stop should not be greater than 24."}]);
	            } else {
	                timestop.set("v.errors", null);
	            }
            }

            var techdate = component.find("techscheduleddate");
            var techdateup = techdate.get("v.value");
            if ( $A.util.isEmpty(techdateup) || techdateup == undefined ) {
                validateData = false;
                techdate.set("v.errors", [{message:"Technician date is required"}]);
            } else {
                techdate.set("v.errors", null);
            }

			//validate pli and pli serial nos
			var liwrap = component.get("v.ScheduleLIWrapObj");
			if(liwrap.pliMissing) {
                validateData = false;
                var errorToast = $A.get("e.force:showToast");
            	errorToast.setParams({"mode": "sticky", "message": "Scheduling a Technician requires at least 1 PLI on the case. Please add one by clicking the 'Click Here to Schedule for Items' button", "type":"error"});
            	errorToast.fire();
            }

			if(liwrap.serialNoMissing) {
                validateData = false;
                var errorToast = $A.get("e.force:showToast");
            	errorToast.setParams({"mode": "sticky", "message": "To schedule a technician every PLI on this case must have a serial number. It cannot be blank. Please populate the serial number before continuing", "type":"error"});
            	errorToast.fire();
			}

            return(validateData);  
        } catch(e) {
            console.log("issue" ,e)
        }
    },

    getValidationOnValues :function(component) {
        var validateData = true;
        try{
        	var minVal = component.find("minValTechSch");
            var minDateValid = minVal.get("v.value");
            console.log(minDateValid);
            if ( $A.util.isEmpty(minDateValid) || minDateValid == '' || minDateValid == undefined ) {
                validateData = false;
                var errorToast = $A.get("e.force:showToast");
            	errorToast.setParams({"message": "Minimum Value is required", "type":"error"});
            	errorToast.fire();
            }

	        var maxVal = component.find("customer_po1");
	        var maxDateValid = maxVal.get("v.value");
	        console.log(maxDateValid);
			if ( $A.util.isEmpty(minDateValid) || minDateValid == '' || minDateValid == undefined ) {
			}
			else{
				if(parseInt(minDateValid) > parseInt(maxDateValid))
		        {
		        	validateData = false;
		            var errorToast = $A.get("e.force:showToast");
		            errorToast.setParams({"message": "Maximum Value should be greater than Minimum", "type":"error"});
		            errorToast.fire();
		        }
			}
        } catch(e) {
            console.log("issue" ,e)
        }

        return validateData;
    },

    getAPIResponse: function(component) {
    	//component.set("v.Spinner", true);
    	if (this.isNullOrEmpty(component.get("v.zipcode"))) {
        	var errorToast = $A.get("e.force:showToast");
        	errorToast.setParams({"message": "There is no Zip Code on the address associated with this Case. Please update the address to include the proper Zip Code.", "type":"error"});
        	errorToast.fire();
    	} else {
	        var fulfillerId = component.get("v.fulfillID");
	        var zipcode = component.get("v.zipcode").split("-");
	        console.log('getApiResponse: ' + zipcode + ':' + fulfillerId);
	        var actionHTTPReq = component.get("c.getApiResponse");
	        actionHTTPReq.setParams({
	            "fulfillerId": fulfillerId,
	            "zipcode" : zipcode[0]
	        });
	        actionHTTPReq.setCallback(this, function(response) {
	            var state = response.getState();
	            if (component.isValid() && state === "SUCCESS") {
	                console.log('getApiResponse1: ' + JSON.stringify(response.getReturnValue()));
	                if(response.getReturnValue().respStatusCode == 200) {
	                	component.set("v.ListOfCompanies", response.getReturnValue().respList);
	                } else if(response.getReturnValue().respStatusCode == 404) {
	                	component.set("v.ListOfCompanies", response.getReturnValue().respList);
	                	var errorToast = $A.get("e.force:showToast");
	                	errorToast.setParams({"mode": "sticky", "message": response.getReturnValue().errorStr, "type":"error"});
	                	errorToast.fire();
	                } else if(response.getReturnValue().respStatusCode == 500) {
	                	component.set("v.ListOfCompanies", response.getReturnValue().respList);
	                	var errorToast = $A.get("e.force:showToast");
	                	errorToast.setParams({"mode": "sticky", "message": response.getReturnValue().errorStr, "type":"error"});
	                	errorToast.fire();
	                }
	            }
	            else {
	                console.log("Failed with state: " + state );
	                var errors = response.getError();
	                if (errors) {
	                    if (errors[0] && errors[0].message) {
	                        console.log("Error message: " + errors[0].message);
	                    }
	                } else {
	                    console.log("Unknown error");
	                }
	            }
	            //component.set("v.Spinner", false);
	        });
	        $A.enqueueAction(actionHTTPReq);
		}
    },

    saveChanges: function(component, draftValues) {
    	//component.set("v.Spinner", true);
        component.set('v.draftItems', draftValues);

		console.log('B Draft Items: ' + JSON.stringify(component.get('v.draftItems')));
		console.log('B Confirm Items: ' + JSON.stringify(component.get('v.confirmItems')));

		//logic starts to remove newly entered serial number reference from confirm items array
        //upon saving, remove saved item from confirm items [example if the user re-enter serial number then again they can confirm with new serial no]
        //if the draft unique id present in the confirm item then delete it from confirm item variable
        var newConfirmItems = [];
        var confirmItems = component.get('v.confirmItems');

		for(var uci=0; uci<confirmItems.length; uci++){
			for(var di=0; di<draftValues.length; di++){
	            if(confirmItems[uci].keyId != draftValues[di].keyId){
	                newConfirmItems.push(confirmItems[uci]);
	            }
	        }
		}
		component.set('v.confirmItems', newConfirmItems);
		//logic ends to remove newly entered serial number reference from confirm items array

		console.log('A Confirm Items: ' + JSON.stringify(component.get('v.confirmItems')));

        var rerendSch = component.get('c.openLineItem');
		$A.enqueueAction(rerendSch);
		//component.set("v.Spinner", false);
    },

    selectRowValues : function(component, event) {
        var row = event.getParam('row');

        var jsondata= JSON.stringify(row);
        //alert('jsondata: '+jsondata);
        //alert('selected sku: ' + row.sku);
        //var draftValues = component.find("AssociateItems").get("v.draftValues");

        component.set("v.dterror", "");
        //alert(JSON.stringify(component.get('v.draftItems')));

        if(row.serialNo == ''){
            component.set("v.dterror", "Enter and Save Serial Number to confirm");
            //As per Alan request if the serialno is null then setting 99999 by default
            //row.serialNo = '999999';
        }
        else{
            var actionConfHttpReq = component.get("c.confirmProductLineItem");
            actionConfHttpReq.setParams({
                "sku" : row.sku,
                "serialNo" : row.serialNo
            });
            //alert('selected sku: ' + row.sku);
            actionConfHttpReq.setCallback(this, function(response) {
                //alert(response.getState());
                if(component.isValid() && response.getState() === "SUCCESS"){
                    //alert(response.getReturnValue());
                    if(response.getReturnValue() == true){
                        row.isSelected = true;

                        //var selItem = component.get('v.selectedAssociateItems');
                        //selItem.push(row);

						console.log('Row Items: ' + JSON.stringify(row));
						console.log('B Associate Items: ' + JSON.stringify(component.get('v.selectedAssociateItems')));

						//logic starts to remove newly entered serial number reference from selected associate items array
				        //upon confirm, remove saved item from selected associate items [example if the user re-enter serial number then again they can confirm with new serial no]
				        //if the confirmed row present in the selected associate item then delete it from selected associate item variable
				        var newAssItems = [];
				        var selAssItems = component.get('v.selectedAssociateItems');
				
						for(var sai=0; sai<selAssItems.length; sai++){
				            if(selAssItems[sai].keyId != row.keyId){
				                newAssItems.push(selAssItems[sai]);
				            }
				        }
				        newAssItems.push(row);
						component.set('v.selectedAssociateItems', newAssItems);
						//logic ends to remove newly entered serial number reference from selected associate items array

						console.log('A Associate Items: ' + JSON.stringify(component.get('v.selectedAssociateItems')));

                        var confirmItems = component.get('v.confirmItems');
                        confirmItems.push({'keyId': row.keyId, 'serialNo': row.serialNo});

                        var rerendSch = component.get('c.openLineItem');
                        $A.enqueueAction(rerendSch);
                    }
                    else{
                        component.set("v.dterror","Serial number is not valid");
                    }
                }
                else{
                    console.log("Failed with state: " + JSON.stringify(response.getError()));
                    //component.set("v.errors", "Failed with state: " + JSON.stringify(response.getError()));
                }
            });
            $A.enqueueAction(actionConfHttpReq);
        }
    },

    saveChangesSku: function(component, draftValues) {
    	//component.set("v.Spinner", true);
        component.set('v.draftItemsSku', draftValues);

		console.log('B Draft Items Sku: ' + JSON.stringify(component.get('v.draftItemsSku')));
		console.log('B Confirm Items Sku: ' + JSON.stringify(component.get('v.confirmItemsSku')));

		//logic starts to remove newly entered serial number reference from confirm items array
        //upon saving, remove saved item from confirm items [example if the user re-enter serial number then again they can confirm with new serial no]
        //if the draft unique id present in the confirm item then delete it from confirm item variable
        var newConfirmItems = [];
        var confirmItems = component.get('v.confirmItemsSku');

		for(var uci=0; uci<confirmItems.length; uci++){
			for(var di=0; di<draftValues.length; di++){
	            if(confirmItems[uci].keyId != draftValues[di].keyId){
	                newConfirmItems.push(confirmItems[uci]);
	            }
	        }
		}
		component.set('v.confirmItemsSku', newConfirmItems);
		//logic ends to remove newly entered serial number reference from confirm items array

		console.log('A Confirm Items Sku: ' + JSON.stringify(component.get('v.confirmItemsSku')));

        var rerendSch = component.get('c.openLineItem');
		$A.enqueueAction(rerendSch);
		//component.set("v.Spinner", false);
    },

    selectRowValuesSku: function(component, event) {
        var row = event.getParam('row');

        var jsondata= JSON.stringify(row);
        //alert('jsondata: '+jsondata);
        //alert('selected sku: ' + row.sku);
        //var draftValues = component.find("AssociateItems").get("v.draftValues");

        component.set("v.dterrorSku", "");
        //alert(JSON.stringify(component.get('v.draftItems')));

        //serial number is not required for SKU item, just to pass the validation setting the serial no on 4th July 2019 by Sekar
        row.serialNo = '999999';
        if(row.serialNo == ''){
            component.set("v.dterrorSku", "Enter and Save Serial Number to confirm");
            //As per Alan request if the serialno is null then setting 99999 by default
            //row.serialNo = '999999';
        }
        else{
            var actionConfHttpReq = component.get("c.confirmProductLineItem");
            actionConfHttpReq.setParams({
                "sku" : row.sku,
                "serialNo" : row.serialNo
            });
            //alert('selected sku: ' + row.sku);
            actionConfHttpReq.setCallback(this, function(response) {
                //alert(response.getState());
                if(component.isValid() && response.getState() === "SUCCESS"){
                    //alert(response.getReturnValue());
                    if(response.getReturnValue() == true){
                        row.isSelected = true;

                        //var selItem = component.get('v.selectedAssociateItems');
                        //selItem.push(row);

						console.log('Row Items Sku: ' + JSON.stringify(row));
						console.log('B Associate Items Sku: ' + JSON.stringify(component.get('v.selectedAssociateItemsSku')));

						//logic starts to remove newly entered serial number reference from selected associate items array
				        //upon confirm, remove saved item from selected associate items [example if the user re-enter serial number then again they can confirm with new serial no]
				        //if the confirmed row present in the selected associate item then delete it from selected associate item variable
				        var newAssItems = [];
				        var selAssItems = component.get('v.selectedAssociateItemsSku');

						for(var sai=0; sai<selAssItems.length; sai++){
				            if(selAssItems[sai].keyId != row.keyId){
				                newAssItems.push(selAssItems[sai]);
				            }
				        }
				        newAssItems.push(row);
						component.set('v.selectedAssociateItemsSku', newAssItems);
						//logic ends to remove newly entered serial number reference from selected associate items array

						console.log('A Associate Items Sku: ' + JSON.stringify(component.get('v.selectedAssociateItemsSku')));

                        var confirmItems = component.get('v.confirmItemsSku');
                        confirmItems.push({'keyId': row.keyId, 'serialNo': row.serialNo});

                        var rerendSch = component.get('c.openLineItem');
                        $A.enqueueAction(rerendSch);
                    }
                    else{
                        component.set("v.dterrorSku","Serial number is not valid");
                    }
                }
                else{
                    console.log("Failed with state: " + JSON.stringify(response.getError()));
                    //component.set("v.errors", "Failed with state: " + JSON.stringify(response.getError()));
                }
            });
            $A.enqueueAction(actionConfHttpReq);
        }
    },

    techCalendarLoad : function(component) {
        var availableDates = [];
        var technicianAvail = [];
        var technicianPartAvail = [];
        var technicianNotAvail = [];
        var workingHours = [];
        var repairStops = [];
        var PiecesRepair = [];

		var WeeklyScheduleVar = component.get("v.weeklyScheduleVar");
		var leaveDates = component.get("v.cLeaveDates");

        var fulfillerId = component.get("v.fulfillID");
        var employeeTechId = component.find("technician").get("v.value");
        var tmth = component.get("v.cMonth");
        var tYr = component.get("v.cYear");
		var tmthinc = component.get("v.cMonthInc");

		var estimateTime = component.get("v.estimatedtimeforstopwithminutes").replace("Hour",".");

        console.log('C estimateTime: ' + estimateTime);

        console.log('C fulfillerId: ' + fulfillerId);
        console.log('C employeeTechId: ' + employeeTechId);
        console.log('C tYr: ' + tYr);
        console.log('C tmth: ' + tmth);
        console.log('C tmthinc: ' + tmthinc);
		console.log('C leaveDates: ' + leaveDates);
		console.log('C WeeklyScheduleVar: ' + WeeklyScheduleVar);

		//calculate line item count based on selected line item and case product line item
		var sliwrap = component.get("v.ScheduleLIWrapObj");
		console.log('C sliwrap: ' + JSON.stringify(sliwrap));
		var totalPli = 0;
		if (sliwrap != null) {
			totalPli = sliwrap.TotalLI;
		}

		console.log('C totalPli: ' + totalPli + ' time: ' + estimateTime);

        var actionCalHTTPReq = component.get("c.getAvailableDates");
        actionCalHTTPReq.setParams({
            "fulfillerId"   : fulfillerId,
            "resourceId" : employeeTechId,
            "cYear"      : tYr,
            "cMonth"     : tmth
        });
        //var selectedCompany = component.find("myCompany").get("v.value").trim();
        actionCalHTTPReq.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {

                response.getReturnValue().forEach(function(responseObj){
                    console.log('responsetime' + JSON.stringify(response.getReturnValue()));

					WeeklyScheduleVar = responseObj.WeeklySchedule;
					var curHours = Number(responseObj.CurrentWorkingHours) + Number(estimateTime);
					var curRepair = Number(responseObj.CurrentRepairStops) + 1;
					var curPieces = Number(responseObj.CurrentPieces) + Number(totalPli);

					console.log('responseObj.CurrentWorkingHours: ' + responseObj.CurrentWorkingHours);
					console.log('curHours: ' + curHours);
					console.log('responseObj.CurrentRepairStops: ' + responseObj.CurrentRepairStops);
					console.log('curRepair: ' + curRepair);
					console.log('responseObj.CurrentPieces: ' + responseObj.CurrentPieces);
					console.log('curPieces: ' + curPieces);

                    //available dates
                    availableDates.push($A.localizationService.formatDate(responseObj.ScheduleDate, "YYYY-MM-DD"));

                    if((curHours > responseObj.MaxHoursPerDay) || (curRepair > responseObj.MaxRepairPerDay) || (curPieces > responseObj.MaxPiecesPerDay)){
                    	//technician is not available for the day
                    	technicianNotAvail.push($A.localizationService.formatDate(responseObj.ScheduleDate, "YYYY-MM-DD"));

                    	if(curHours > responseObj.MaxHoursPerDay){
                    		workingHours.push($A.localizationService.formatDate(responseObj.ScheduleDate, "YYYY-MM-DD"));
                    	}

						if(curRepair > responseObj.MaxRepairPerDay){
							repairStops.push($A.localizationService.formatDate(responseObj.ScheduleDate, "YYYY-MM-DD"));
						}

						if(curPieces > responseObj.MaxPiecesPerDay){
							PiecesRepair.push($A.localizationService.formatDate(responseObj.ScheduleDate, "YYYY-MM-DD"));
						}
                    }
                    else{
                    	//technician is available for the day
                    	if((curHours == 0) && (curRepair == 0) && (curPieces == 0)){
                    		//technician is fully available for the day
                    		technicianAvail.push($A.localizationService.formatDate(responseObj.ScheduleDate, "YYYY-MM-DD"));
                    	}
                    	else{
                    		//technician is partially available for the day
                    		technicianPartAvail.push($A.localizationService.formatDate(responseObj.ScheduleDate, "YYYY-MM-DD"));
                    	}
                    }
                });

				console.log('C WeeklyScheduleVar1: ' + WeeklyScheduleVar);
				console.log('C availableDates: ' + availableDates);
				console.log('C technicianAvail: ' + technicianAvail);
				console.log('C technicianPartAvail: ' + technicianPartAvail);
				console.log('C technicianNotAvail: ' + technicianNotAvail);
				console.log('C workingHours: ' + workingHours);
				console.log('C repairStops: ' + repairStops);
				console.log('C PiecesRepair: ' + PiecesRepair);

                $("#datepicker").datepicker("destroy");
                jQuery("#datepicker").datepicker({
                    minDate: 0,
                    inline:true,
                    nextText: "",
                    prevText: "",
                    hideIfNoPrevNext: true,
                    defaultDate : '+' + tmthinc + 'm',
                    beforeShowDay: function(date){
	                   	//console.log('date: ' + date);
                    	//console.log('YYYY-MM-DD ' + jQuery.datepicker.formatDate('YYYY-MM-DD', date));
                    	//console.log('yy-mm-dd ' + jQuery.datepicker.formatDate('yy-mm-dd', date));

                        var dateArray = WeeklyScheduleVar;
                        var showdaysArray = [];
                        for (var i = 0; i < dateArray.length; i++) {
                            if(dateArray[i]=='Y'){
                                showdaysArray.push(i);
                            }
                        }
                        //console.log('showdaysArray: ' + showdaysArray);
                        var showdays = date.getDay();

                        var show = false;
                        var css_class = '';
                        var tooltip = '';
                        //console.log('showdays: ' + showdays);
                        if(showdaysArray.indexOf(showdays) != -1 ){
                        	//based on weekly schedule
                            if(leaveDates.indexOf(jQuery.datepicker.formatDate('yy-mm-dd', date)) != -1){
                            	//if the tech is on leave
                                show = false;
                                console.log('leavedates' + leaveDates);

                                css_class = 'tech-leave';
                                //set tooltip
								tooltip = 'Technician is on leave';
                            }else{
                            	//if the tech is not on leave
                                show = true;
                            }
                        }

						/*if(availableDates.indexOf(jQuery.datepicker.formatDate('yy-mm-dd', date)) != -1){
							//if the tech is scheduled for this date
							show = true;
						}*/
						if ((component.get('v.maxHours') == 0) && (component.get('v.maxRepair') == 0) && (component.get('v.maxPieces') == 0)){
							// if the work load of the selected technician is zero then gray out all the date in the calendar - work item #268440
							show = false;
						}

						if(show === true){
							if(technicianNotAvail.indexOf(jQuery.datepicker.formatDate('yy-mm-dd', date)) != -1){
								//Technician is not available for the day
								css_class = 'tech-not-avail '+jQuery.datepicker.formatDate('yy-mm-dd', date);

								//set tooltip
								if(workingHours.indexOf(jQuery.datepicker.formatDate('yy-mm-dd', date)) != -1){
									//Working Hours is not available for this tech
									tooltip = 'Technician Working Hours was Scheduled more than 100% for the selected date, kindly selected another date';
								}
	
								if(repairStops.indexOf(jQuery.datepicker.formatDate('yy-mm-dd', date)) != -1){
									//Repair Stops is not available for this tech
									tooltip = 'Technician Repair Stops was Scheduled more than 100% for the selected date, kindly selected another date';
								}
	
								if(PiecesRepair.indexOf(jQuery.datepicker.formatDate('yy-mm-dd', date)) != -1){
									//Pieces Repair is not available for this tech
									tooltip = 'Technician Pieces Repair was Scheduled more than 100% for the selected date, kindly selected another date';
								}
							} else if(technicianPartAvail.indexOf(jQuery.datepicker.formatDate('yy-mm-dd', date)) != -1){
								//Technician is partially available for the day
								css_class = 'tech-partially-avail '+jQuery.datepicker.formatDate('yy-mm-dd', date);
								//set tooltip
								tooltip = 'Technician is partially available for the day';
							} else {
								//Technician is fully available for the day
								css_class = 'tech-avail '+jQuery.datepicker.formatDate('yy-mm-dd', date);
								//set tooltip
								tooltip = 'Technician is fully available for the day';
							}
						}

                        //console.log('show: ' + show);
                        return [show , css_class, tooltip];
                    },
                    
                    onSelect: function(date) {
                        var selectedDate =   $('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();

                        var actionProgValue = component.get("c.getProgressValue");
                        actionProgValue.setParams({
                            "fulfillerId"  : fulfillerId,
                            "resourceId"   : component.get("v.caseObj.Tech_Scheduled_Company__c"),
                            "selecteddate" : selectedDate
                        });
                        actionProgValue.setCallback(this, function(response) {
                            if (component.isValid() && response.getState() === "SUCCESS") {
                                component.set('v.progressMap', response.getReturnValue());
                                component.set('v.MaxWorkingHours', component.get('v.progressMap').MaxWorkingHours);
                                component.set('v.MaxRepairStops', component.get('v.progressMap').MaxRepairStops);
                                component.set('v.MaxPiecesDay', component.get('v.progressMap').MaxPiecesDay);

								var curProgressHours = Number(component.get('v.progressMap').CurrentWorkingHours) + Number(estimateTime);
								var curProgressRepair = Number(component.get('v.progressMap').CurrentRepairStops) + 1;
								var curProgressPieces = Number(component.get('v.progressMap').CurrentPieces) + Number(totalPli);

								component.set('v.CurrentWorkingHours', curProgressHours);
								component.set('v.CurrentRepairStops', curProgressRepair);
								component.set('v.CurrentPieces', curProgressPieces);
								component.set("v.workingHoursprogvalue", curProgressHours + '/' + component.get('v.progressMap').MaxWorkingHours);
								component.set("v.RepairStopsprogvalue", curProgressRepair + '/' +  component.get('v.progressMap').MaxRepairStops);
								component.set("v.PiecesRepairprogvalue", curProgressPieces +'/'+component.get('v.progressMap').MaxPiecesDay);
								var cwhpv = ((parseInt(curProgressHours) / parseInt(component.get('v.progressMap').MaxWorkingHours)) * 100);
								var crspv = ((parseInt(curProgressRepair) / parseInt(component.get('v.progressMap').MaxRepairStops)) * 100);
								var cprpv = ((parseInt(curProgressPieces) / parseInt(component.get('v.progressMap').MaxPiecesDay)) * 100);

                                component.set("v.CurrentWorkingHoursProgValue", cwhpv);
                                component.set("v.CurrentRepairStopsprogvalue", crspv);
                                component.set("v.CurrentPiecesRepairprogvalue", cprpv);
                                var errorMsg;
                                if(cwhpv > 100){
                                    errorMsg = 'Technician Working Hours was Scheduled more than 100% for the selected date, kindly selected another date';
                                }
                                else if(crspv > 100){
                                    errorMsg = 'Technician Repair Stops was Scheduled more than 100% for the selected date, kindly selected another date';
                                }
                                else if(cprpv > 100){
                                    errorMsg = 'Technician Pieces Repair was Scheduled more than 100% for the selected date, kindly selected another date';
                                }
                                if(errorMsg != null){
                                	component.set("v.cSelectedDate", "");
                                    $("#datepicker").val("");
                                    var errorToast = $A.get("e.force:showToast");
                                    errorToast.setParams({"message": errorMsg, "type":"error"});
                                    errorToast.fire();
                            	}
                            	else{
                            		//if there is no error then set the selected date
                            		component.set("v.cSelectedDate", selectedDate);
                            	}
                            }
                            else {
                                var errorToast = $A.get("e.force:showToast");
                                errorToast.setParams({"message": response.getError()[0].message, "type":"error"});
                                errorToast.fire();
                            }
                        });
                        $A.enqueueAction(actionProgValue);
                    },

                    onChangeMonthYear : function(ntYr, ntmth, inst) {
						console.log('Year: ' + ntYr + ' Month: ' + ntmth + ' Instance: ' + inst);
						// before i set the month and year, i have to know whether the user hit previous button or next button

						if(ntYr == tYr){
							//same year increment or decrement based on month
							if(tmth < ntmth){
								++tmthinc;
							}
							else{
								--tmthinc;
							}
						}
						else{
							//either the year is increased or decreased
							if(tYr < ntYr){
								//increased
								++tmthinc;
							}
							else{
								//decreased
								--tmthinc;
							}
						}

						component.set("v.cMonth", ntmth);
						component.set("v.cYear", ntYr);
						component.set("v.cMonthInc", tmthinc);

				        var rerendMon = component.get('c.onMonthChange');
						$A.enqueueAction(rerendMon);
					}
                });
            }
            else {
            	this.responseError(component, response);

                //var errors = response.getError();
                //if (errors) {
                //    if (errors[0] && errors[0].message) {
                //        console.log('Error Message: ' + errors[0].message);
                //   }
                //}

                //var errorToast = $A.get("e.force:showToast");
                //errorToast.setParams({"message": 'There might be some issue with your selection \n OR an issue with the system. Please contact your administrator.', "type":"error"});
                //errorToast.fire();
            }
        });
        $A.enqueueAction(actionCalHTTPReq);
    },

    resetScheduleAttr : function(component) {
        component.set("v.maxHours", '');
        component.set("v.maxRepair",'');
        component.set("v.maxPieces",'');
        component.set("v.upHostery",'');
        component.set("v.casegoods",'');
        component.set("v.leather",'');
        component.set("v.weeklyScheduleVar",'');
        component.set("v.cLeaveDates",'');
        component.set("v.cMonth", '');
        component.set("v.cYear",'');
        component.set("v.cMonthInc",'0');
    },

    resetCalendarAttr : function(component) {
        component.set("v.workingHoursprogvalue", 0);
        component.set("v.RepairStopsprogvalue", 0);
        component.set("v.PiecesRepairprogvalue", 0);
        component.set("v.CurrentWorkingHoursProgValue", 0);
        component.set("v.CurrentRepairStopsprogvalue", 0);
        component.set("v.CurrentPiecesRepairprogvalue", 0);
        component.set("v.cSelectedDate", "");
        component.set("v.datevalue", "");
        $("#datepicker").val('');
        $( "#datepicker" ).datepicker( "destroy" );
    },

    processLineItem : function(component) {

        var selItem = component.get('v.selectedAssociateItems');
        console.log('selectedlineitesm..' + JSON.stringify(selItem));

        var ItemsDtovar = [];
        for(var si=0;si<selItem.length;si++){
            var ItemsLineDtovar = {};
            ItemsLineDtovar.IsNewItemRequest = "1";
            if(selItem[si].salesorder_number != undefined){
                ItemsLineDtovar.ItemSaleNumber = selItem[si].salesorder_number;
            }
            else{
                ItemsLineDtovar.ItemSaleNumber = "";
            }
            
            if(selItem[si].seqNumber != undefined){
                ItemsLineDtovar.ItemSaleSequenceNumber = selItem[si].seqNumber;
            }
            else{
                ItemsLineDtovar.ItemSaleSequenceNumber = "";
            }
            
            if(selItem[si].sku != undefined){
                ItemsLineDtovar.ItemSKUNumber = selItem[si].sku;
            }
            else{
                ItemsLineDtovar.ItemSKUNumber = "";
            }
            
            if(selItem[si].serialNo != undefined){
                ItemsLineDtovar.ItemSerialNumber = selItem[si].serialNo;
            }
            else{
                ItemsLineDtovar.ItemSerialNumber = "";
            }

            ItemsLineDtovar.ItemInvoiceNumber = "";
            ItemsLineDtovar.ItemDefect = "";
            ItemsLineDtovar.PartOrderTrackNumber = "";
            ItemsLineDtovar.OrderNumber = "";
            ItemsLineDtovar.PieceExchangedFixed = "";
            ItemsLineDtovar.PartNumber = "";
            
            if(selItem[si].desc != undefined){
                ItemsLineDtovar.PartDesc = selItem[si].desc;
            }
            else{
                ItemsLineDtovar.PartDesc = "";
            }

            if(selItem[si].deldate != undefined){
                ItemsLineDtovar.DeliveryDate = selItem[si].deldate;
            }
            else{
                ItemsLineDtovar.DeliveryDate = "";
            }

            ItemsLineDtovar.SignedBy = "";
            ItemsLineDtovar.RowState = "A";
            ItemsLineDtovar.SelectV = 0;

            if(selItem[si].uniqueid != undefined){
                ItemsLineDtovar.ItemQuniqueId = selItem[si].uniqueid;
            }
            else{
                ItemsLineDtovar.ItemQuniqueId = 0;
            }

            if(selItem[si].orderstatus != undefined){
                ItemsLineDtovar.OrderStatus = selItem[si].orderstatus;
            }
            else{
                ItemsLineDtovar.OrderStatus = "";
            }

            if(selItem[si].wardate != undefined){
                ItemsLineDtovar.WarrantyEndDate = selItem[si].wardate;
            }
            else{
                ItemsLineDtovar.WarrantyEndDate = "";
            }

            ItemsLineDtovar.OrderShipInformation = "";
            ItemsLineDtovar.ShipDate = "";
            ItemsLineDtovar.Qty = selItem[si].Quantity;
            ItemsLineDtovar.IsPLI = true;
            ItemsLineDtovar.ItemUniqueId = 0;

            if(selItem[si].storeid != undefined){
                ItemsLineDtovar.StoreID = selItem[si].storeid;
            }
            else{
                ItemsLineDtovar.StoreID = "";
            }
            if(selItem[si].erpcustomerid != undefined){
                ItemsLineDtovar.ERPCustomerId = selItem[si].erpcustomerid;
            }
            else{
                ItemsLineDtovar.ERPCustomerId = "";
            }
            if(selItem[si].erpaccountshipto != undefined){
                ItemsLineDtovar.ERPAccountShipTo = selItem[si].erpaccountshipto;
            }
            else{
                ItemsLineDtovar.ERPAccountShipTo = "";
            }
            if(selItem[si].customerid != undefined){
                ItemsLineDtovar.CustomerID = selItem[si].customerid;
            }
            else{
                ItemsLineDtovar.CustomerID = "";
            }
            if(selItem[si].accountshipto != undefined){
                ItemsLineDtovar.FulfillerID = selItem[si].accountshipto;
            }
            else{
                ItemsLineDtovar.FulfillerID = "";
            }

            ItemsDtovar.push(ItemsLineDtovar);
        }

		console.log('Helper ItemsDtovar 1: ' + JSON.stringify(ItemsDtovar));

        var selItemSku = component.get('v.selectedAssociateItemsSku');
        console.log('selectedlineitesm Sku..' + JSON.stringify(selItemSku));

        for(var sis=0;sis<selItemSku.length;sis++){
            var ItemsLineDtovar = {};
            ItemsLineDtovar.IsNewItemRequest = "1";
            if(selItemSku[sis].salesorder_number != undefined){
                ItemsLineDtovar.ItemSaleNumber = selItemSku[sis].salesorder_number;
            }
            else{
                ItemsLineDtovar.ItemSaleNumber = "";
            }
            
            if(selItemSku[sis].seqNumber != undefined){
                ItemsLineDtovar.ItemSaleSequenceNumber = selItemSku[sis].seqNumber;
            }
            else{
                ItemsLineDtovar.ItemSaleSequenceNumber = "";
            }
            
            if(selItemSku[sis].sku != undefined){
                ItemsLineDtovar.ItemSKUNumber = selItemSku[sis].sku;
            }
            else{
                ItemsLineDtovar.ItemSKUNumber = "";
            }
            
            if(selItemSku[sis].serialNo != undefined){
                ItemsLineDtovar.ItemSerialNumber = selItemSku[sis].serialNo;
            }
            else{
                ItemsLineDtovar.ItemSerialNumber = "";
            }

            ItemsLineDtovar.ItemInvoiceNumber = "";
            ItemsLineDtovar.ItemDefect = "";
            ItemsLineDtovar.PartOrderTrackNumber = "";
            ItemsLineDtovar.OrderNumber = "";
            ItemsLineDtovar.PieceExchangedFixed = "";
            ItemsLineDtovar.PartNumber = "";
            
            if(selItemSku[sis].desc != undefined){
                ItemsLineDtovar.PartDesc = selItemSku[sis].desc;
            }
            else{
                ItemsLineDtovar.PartDesc = "";
            }

            if(selItemSku[sis].deldate != undefined){
                ItemsLineDtovar.DeliveryDate = selItemSku[sis].deldate;
            }
            else{
                ItemsLineDtovar.DeliveryDate = "";
            }

            ItemsLineDtovar.SignedBy = "";
            ItemsLineDtovar.RowState = "A";
            ItemsLineDtovar.SelectV = 0;

            if(selItemSku[sis].uniqueid != undefined){
                ItemsLineDtovar.ItemQuniqueId = selItemSku[sis].uniqueid;
            }
            else{
                ItemsLineDtovar.ItemQuniqueId = 0;
            }

            if(selItemSku[sis].orderstatus != undefined){
                ItemsLineDtovar.OrderStatus = selItemSku[sis].orderstatus;
            }
            else{
                ItemsLineDtovar.OrderStatus = "";
            }

            if(selItemSku[sis].wardate != undefined){
                ItemsLineDtovar.WarrantyEndDate = selItemSku[sis].wardate;
            }
            else{
                ItemsLineDtovar.WarrantyEndDate = "";
            }

            ItemsLineDtovar.OrderShipInformation = "";
            ItemsLineDtovar.ShipDate = "";
            ItemsLineDtovar.Qty = selItemSku[sis].Quantity;
            ItemsLineDtovar.IsPLI = true;
            ItemsLineDtovar.ItemUniqueId = 0;

            if(selItemSku[sis].storeid != undefined){
                ItemsLineDtovar.StoreID = selItemSku[sis].storeid;
            }
            else{
                ItemsLineDtovar.StoreID = "";
            }
            if(selItemSku[sis].erpcustomerid != undefined){
                ItemsLineDtovar.ERPCustomerId = selItemSku[sis].erpcustomerid;
            }
            else{
                ItemsLineDtovar.ERPCustomerId = "";
            }
            if(selItemSku[sis].erpaccountshipto != undefined){
                ItemsLineDtovar.ERPAccountShipTo = selItemSku[sis].erpaccountshipto;
            }
            else{
                ItemsLineDtovar.ERPAccountShipTo = "";
            }
            if(selItemSku[sis].customerid != undefined){
                ItemsLineDtovar.CustomerID = selItemSku[sis].customerid;
            }
            else{
                ItemsLineDtovar.CustomerID = "";
            }
            if(selItemSku[sis].accountshipto != undefined){
                ItemsLineDtovar.FulfillerID = selItemSku[sis].accountshipto;
            }
            else{
                ItemsLineDtovar.FulfillerID = "";
            }

            ItemsDtovar.push(ItemsLineDtovar);
        }

		console.log('Helper ItemsDtovar 2: ' + JSON.stringify(ItemsDtovar));

        var actionHTTPReq = component.get("c.getLineItemstoHomes");
        actionHTTPReq.setParams({
            "caseObj": component.get("v.caseObj"),
            "jsonItems" : JSON.stringify(ItemsDtovar)
        });
        actionHTTPReq.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log('getLineItemstoHomes response: ' + JSON.stringify(response.getReturnValue()));
                component.set("v.ScheduleLIWrapObj", response.getReturnValue());

				if(!$A.util.isEmpty(component.get("v.weeklyScheduleVar")) && !$A.util.isEmpty(component.get("v.estimatedtimeforstopwithminutes"))){
			        //have to reset the Tech Scheduled Date and reset calendar
			        console.log('Calendar trigger after selecting associate line item');
			        this.resetCalendarAttr(component);

					var dt = new Date();
					var tYr = dt.getFullYear();
					var tmth = dt.getMonth() + 1;

					component.set("v.cYear", tYr);
					component.set("v.cMonth", tmth);
					component.set("v.cMonthInc", 0);

					this.techCalendarLoad(component);
				}

            }
            else {
                console.log("Failed with state: " + state );
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(actionHTTPReq);
    },

    responseError : function(component, res) {
        var state = res.getState();
        if (component.isValid() && state === "ERROR") {
            var errors = res.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    console.log("Error message: " + errors[0].message);
                }
            } else {
                console.log("Unknown error");
            }

	        var errorToast = $A.get("e.force:showToast");
	        errorToast.setParams({"message": 'There might be some issue with your selection \n OR an issue with the system. Please contact your administrator.', "type":"error"});
	        errorToast.fire();
		}
    },

    controlZipDetails: function(component, flag) {
    	component.find("myCompany").set('v.disabled', flag);
    	component.find("technician").set('v.disabled', flag);
    	component.find("myTimeEstimated").set('v.disabled', flag);
    	component.find("techscheduleddate").set('v.disabled', flag);
    	component.find("scheduleBtn").set('v.disabled', flag);
    },

    redirectToCase: function(component) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.caseId"),
            "slideDevName": "related"
        });
        navEvt.fire();

		//while cancelling close the sub tab
		var workspaceAPI = component.find("workspace");
		console.log('workspaceAPI: ' + workspaceAPI);
		if(workspaceAPI != undefined){
	        workspaceAPI.getFocusedTabInfo().then(function(response) {
		        console.log('response: ' + JSON.stringify(response));
	            var focusedTabId = response.tabId;
                console.log('focusedTabId: ' + focusedTabId);
	            if(response.isSubtab == true){
	            	workspaceAPI.refreshTab({
						tabId: response.parentTabId,
						includeAllSubtabs: true
					});
	            }
	            workspaceAPI.closeTab({tabId: focusedTabId});
	        })
	        .catch(function(error) {
	            console.log(error);
	        });
		}
    },

    //EDQ settings added by praneeth

      // EDQ added Validation logic
    getSettings : function() {
        return {
            // Default country will be taken if auraId for the country is not specified
            defaultCountry : 'usa',
            // Country's auraId and the field name of the object containing the value
            country : {
                auraId : null,
                fieldName : null
            },
            // Street's auraId and the field name of the object containing the value
            street : {
                auraId : 'address1',
                fieldName : 'Address_Line_1__c'
            },
            // Second street's auraId and the field name of the object containing the value
            apartmentNumber : {
                auraId : 'address2',
                fieldName : 'Address_Line_2__c'
            },
            // City's auraId and the field name of the object containing the value
            city : {
                auraId : 'city',
                fieldName : 'City__c'
            },
            // State's auraId and the field name of the object containing the value
            state : {
                auraId : 'state',
                fieldName : 'StateList__c'
            },
            // Zip's auraId and the field name of the object containing the value
            zip : {
                auraId : 'zip',
                fieldName : 'Zip_Code__c'
            },
            // Validation status's auraId and the field name of the object containing the value
            status : {
                auraId : 'shippingAddrValStatus',
                fieldName : 'Address_Validation_Status__c'
            },

            // Validation response mapping. The keys of this object represent the keys returned by the validation
            // service's response. If the value of a key in this mapping is null, the result from the validation will
            // not be reflected. If the value of a key in this mapping is provided it must reflect a key from the above
            // settings (country, street, city, etc.) and then the value from the validation response will be reflected
            addressMapping : {
                "country" : null,
                "addressLine1" : 'street',
                "subBuilding1" : 'apartmentNumber',
                "locality" : 'city',
                "province" : 'state',
                "postalCode" : 'zip',
            },
            // Id of the spinner
            loaderId : 'loader',

            // The property name of the object containing the address
            addressObjectPropertyName : 'newShipping',
            // The property name of the object containing the address snapshot
            addressObjectSnapshotPropertyName : 'ShippingAddressObjectSnapshot',

            // Apex method name for address search
            searchAddressActionName : 'SearchAddress',
            // Apex method name for detailed address search
            formatAddressActionName : 'FormatAddress',

            // The property name of the hasSuggestions boolean
            hasSuggestionsVariableName : 'hasSuggestions',
            // The property name of the object containing the suggestions
            suggestionsVariableName : 'suggestions',
            // Whether would we display errors or not
            displayErrors: false,
            // The property name of the object containing the timeout identifier's value
            timeoutIdentifierVariableName: 'timeoutIdentifier',

            // Partial value of the suggestion index html id
            suggestionIndexClassPrefix : '-suggestion-index-',
            MaxSuggestionsToTake : 10,

            // Value of the status if validation is successful
            verifiedByServiceStatus : 'Verified by Experian!',
            // Value of the status if validation has passed but the user changed some value
            userPreferredStatus : 'User Preferred!',
            // Value of the status if validation has failed
            unknownStatus : 'Unknown!'
        };
    },
    // EDQ added Validation logic
   
    // EDQ added Validation logic
    handleSearchChange : function(component, event, settings) {
        var countryValue = this.getCountry(component, settings);

        var keyCode = event.which;
        if(keyCode == 13) { // Enter
            this.acceptFirstSuggestion(component, settings);
        } else if(keyCode == 40) { //Arrow down
            this.selectNextSuggestion(component, null, settings);
        } else if(keyCode == 38) { //Arrow up
            this.hideSuggestions(component, settings);
        } else {
            this.getSuggestions(component, settings);
        }
    },
    // EDQ added Validation logic
    onSuggestionKeyUp : function(component, event, settings) {
        var keyCode = event.which;
        if(keyCode == 40) { // Arrow down
            this.selectNextSuggestion(component, event.target, settings);
        } else if(keyCode == 38) { // Arrow up
            this.selectPreviousSuggestion(component, event.target, settings);
        } else if(keyCode > 40) { // User is typing a character, set the focus on the text field and add the character there.
            this.appendKeyCodeToEndOfInput(component, keyCode, settings);
        }
    },
    // EDQ added Validation logic
    handleResultSelect: function(component, event, settings) {
        var addressFormatId = event.target.title;
        this.getFullAddress(component, addressFormatId, settings);
    },
    // EDQ added Validation logic
    getSuggestions : function(component, settings) {
        var searchTermValue = this.getElementValue(component, settings.street.auraId);

        this.hideAndRemoveSuggestions(component, settings);

        if("undefined" === typeof searchTermValue || searchTermValue.length < 2 ){return;}

        var country = this.getCountry(component, settings);

        var action = component.get("c." + settings.searchAddressActionName);
        action.setParams({
            searchTerm : searchTermValue,
            country : country,
            take : settings.MaxSuggestionsToTake
        });

        action.setCallback(this, function(a) {
            this.hideLoader(component, settings);

            if (a.getState() === "SUCCESS") {
                try {
                    var resultAsString = a.getReturnValue();
                    var retJSON = JSON.parse(resultAsString);
                    var requestHasFailed = retJSON.hasOwnProperty('Message');
                    if (requestHasFailed) {
                        if (settings.displayErrors) {
                            this.showToast("error", 'Validation failed', component, retJSON['Message']);
                        }
                        return;
                    }
                    var results = retJSON.results;
                    for(var i = 0; i < results.length; i++) {
                        results[i].index = i;
                    }

                    if (results.length > 0) {
                        this.setVariableValue(component, settings.hasSuggestionsVariableName, true);
                        this.setVariableValue(component, settings.suggestionsVariableName, results);
                    }
                } catch(error) {
                    this.log(error);
                }
            } else {
                this.log(a);
            }
        });

        var timeoutIdentifier = this.getVariableValue(component, settings.timeoutIdentifierVariableName);
        if(timeoutIdentifier != null || timeoutIdentifier != undefined)
            clearTimeout(timeoutIdentifier);

        var helper = this;
        timeoutIdentifier = window.setTimeout($A.getCallback(function() {
            helper.showLoader(component, settings);
            $A.enqueueAction(action);
        })
        , 1000);

        this.setVariableValue(component, settings.timeoutIdentifierVariableName, timeoutIdentifier);
    },
    // EDQ added Validation logic
    getFullAddress : function(component, formatAddressUrl, settings) {
        var _this = this;
        this.hideAndRemoveSuggestions(component, settings);
        this.cleanupAddressFields(component, settings);
        var action = component.get("c." + settings.formatAddressActionName);
        action.setParams({formatUrl : formatAddressUrl});

        this.showLoader(component, settings);
        action.setCallback(this, function(a) {
            this.hideLoader(component, settings);

            if (a.getState() === "SUCCESS") {
                try {
                    var resultAsString = a.getReturnValue();
                    var retJSON = JSON.parse(resultAsString);
                    var requestHasFailed = retJSON.hasOwnProperty('Message');
                    if (requestHasFailed) {
                        if (settings.displayErrors) {
                            this.showToast("error", 'Validation failed', component, retJSON['Message']);
                        }
                        return;
                    }
                    var mapping = settings.addressMapping;
                    var parsedResult = this.parseResult(retJSON);
                    var validatedAddressSnapshot = {};
                    Object.keys(parsedResult).forEach(function(addressKeyFromService) {
						console.log('addressKeyFromService: ' + parsedResult[addressKeyFromService]);
                        if(mapping.hasOwnProperty(addressKeyFromService) && null !== mapping[addressKeyFromService]) {
                            var addressComponentAuraId = settings[mapping[addressKeyFromService]].auraId;
                            var addressComponentFieldName = settings[mapping[addressKeyFromService]].fieldName;
							console.log('addressComponentAuraId: ' + addressComponentAuraId);
							console.log('parsedResult[addressKeyFromService]: ' + parsedResult[addressKeyFromService]);
							//added by sekar to remove last 4 digit
							if(addressComponentAuraId === 'zip') {
								var zipcode = parsedResult[addressKeyFromService].split("-");
								parsedResult[addressKeyFromService] = zipcode[0];
								console.log('parsedResult[addressKeyFromService]: ' + parsedResult[addressKeyFromService]);
							}
                            _this.setElementValue(component, addressComponentAuraId, parsedResult[addressKeyFromService]);
                            validatedAddressSnapshot[addressComponentFieldName] = parsedResult[addressKeyFromService];
                       
                        }
                    });
					
					this.changeValidationStatus(component, settings.status, settings['verifiedByServiceStatus']);
                     
                    this.saveSnapshot(component, settings, validatedAddressSnapshot);
                } catch(error) {
                    this.log(error);
                }
            } else {
                this.log(a);
            }
        });

        $A.enqueueAction(action);
    },
    // EDQ added Validation logic
    parseResult : function(resultJSON) {
        var parsedResult = {};
        for(var i = 0; i < resultJSON.address.length; i++) {
            var line = resultJSON.address[i];
            for(var key in line) {
                if(!line.hasOwnProperty(key)) { continue; }

                var value = line[key];
                parsedResult[key] = value;
            }
        }

        for(var i = 0; i < resultJSON.components.length; i++) {
            var line = resultJSON.components[i];
            for(var key in line) {
                if(!line.hasOwnProperty(key)) { continue; }

                var value = line[key];
                parsedResult[key] = value;
            }
        }
        return parsedResult;
    },
    // EDQ added Validation logic
    acceptFirstSuggestion : function(component, settings) {
        var suggestions =  this.getVariableValue(component, settings.suggestionsVariableName);
        if(this.isNull(suggestions) || suggestions.length == 0)
            return;

        this.getFullAddress(component, suggestions[0].format, settings);
    },
    // EDQ added Validation logic
    selectNextSuggestion : function(component, selectedSuggestion, settings) {
        var suggestions =  this.getVariableValue(component, settings.suggestionsVariableName);
        if(this.isNull(suggestions) || suggestions.length == 0)
            return;

        var selectedSuggestionIndex = this.getSelectedSuggestionIndex(component, selectedSuggestion, settings);
        if(selectedSuggestionIndex >= suggestions.length - 1) {
            selectedSuggestionIndex = -1;
        }

        selectedSuggestionIndex++;
        this.selectSuggestionById(component, selectedSuggestionIndex, settings);
    },
    // EDQ added Validation logic
    selectPreviousSuggestion : function(component, selectedSuggestion, settings) {
        var suggestions =  this.getVariableValue(component, settings.suggestionsVariableName);
        if(this.isNull(suggestions) || suggestions.length == 0)
            return;

        var selectedSuggestionIndex = this.getSelectedSuggestionIndex(component, selectedSuggestion, settings);
        if(selectedSuggestionIndex == -1)
            return;

        if(selectedSuggestionIndex == 0) {
            this.focusStreet(component, settings);
        } else {
            selectedSuggestionIndex--;
            this.selectSuggestionById(component, selectedSuggestionIndex, settings);
        }
    },
    // EDQ added Validation logic
    selectSuggestionById : function(component, suggestionId, settings) {
        var id = component.getGlobalId() + settings.suggestionIndexClassPrefix + suggestionId;
        document.getElementById(id).focus();
    },
    // EDQ added Validation logic
    getSelectedSuggestionIndex : function(component, selectedSuggestion, settings) {
        if(this.isNull(selectedSuggestion))
            return -1;

        var idPrefix = component.getGlobalId() + settings.suggestionIndexClassPrefix;
        var id = selectedSuggestion.id;
        var indexOfSuggestionIndexClassPrefix = id.indexOf(idPrefix);
        if(indexOfSuggestionIndexClassPrefix == 0) {
            var selectedIndexAsString = id.substring(indexOfSuggestionIndexClassPrefix + idPrefix.length);

            return parseInt(selectedIndexAsString);
        } else {
            return -1;
        }
    },
    // EDQ added Validation logic
    hideSuggestions : function(component, settings) {
        this.setVariableValue(component, settings.hasSuggestionsVariableName, false);
    },
    // EDQ added Validation logic
    hideAndRemoveSuggestions :  function(component, settings) {
        this.setVariableValue(component, settings.hasSuggestionsVariableName, false);
        this.setVariableValue(component, settings.suggestionsVariableName, []);
    },
    // EDQ added Validation logic
    focusStreet : function(component, settings) {
        component.find(settings.street.auraId).focus();
    },
    // EDQ added Validation logic
    appendKeyCodeToEndOfInput : function(component, keyCode, settings) {
        try {
            this.focusStreet(component, settings);

            var oldValue = this.getElementValue(component, settings.street.auraId);
            this.setElementValue(component, settings.street.auraId, oldValue + String.fromCharCode(keyCode).toLowerCase());
            this.getSuggestions(component, settings);
        }
        catch(error) { this.log(error); }
    },
    // EDQ added Validation logic
    getCountry : function(component, settings) {
        if(this.isNullOrEmpty(settings.country.auraId)) {
            return settings.defaultCountry;
        } else {
            return this.getElementValue(component, settings.country.auraId);
        }
    },
    // EDQ added Validation logic
    handleValidationStatus : function(component, settings) {
        // unlock this once persistence of validation status is decided on
        return;
        var hasBeenValidated = null !== this.getVariableValue(component, settings.addressObjectSnapshotPropertyName);
        if (hasBeenValidated) {
            if (this.addressIsDifferentThanTheValidatedOne(component, settings)) {
                 
                this.changeValidationStatus(component, settings.status, settings['userPreferredStatus']);
              
            } else {
                this.changeValidationStatus(component, settings.status, settings['verifiedByServiceStatus']);

            }
             
          
        }
     
    },
    // EDQ added Validation logic
    changeValidationStatus : function(component, statusConfig, value) {
        // unlock this once persistence of validation status is decided on
        return;
        if (null === statusConfig.auraId) {
            return;
        }

        this.setElementValue(component, statusConfig.auraId, value);
        
    },
    // EDQ added Validation logic
    saveSnapshot : function(component, settings, validatedAddressSnapshot) {
        this.setVariableValue(component, settings.addressObjectSnapshotPropertyName, validatedAddressSnapshot);
    },
    // EDQ added Validation logic
    addressIsDifferentThanTheValidatedOne : function(component, settings) {
        var addressHasBeenChanged;
        var addressKeys = this.getMappedAddressFieldNames(settings);
        var currentAddress =  this.getVariableValue(component, settings.addressObjectPropertyName);
        var validatedAddress =  this.getVariableValue(component, settings.addressObjectSnapshotPropertyName);

        for (var index in addressKeys) {
           var key = addressKeys[index];
           if (currentAddress[key] === validatedAddress[key]) {
               addressHasBeenChanged = false;
           }  else {
               addressHasBeenChanged = true;
               break;
           }
        };

        return addressHasBeenChanged;
    },
    // EDQ added Validation logic
    getMappedAddressFieldNames : function(settings) {
        var mappedAddressKeys = [];
        Object.keys(settings.addressMapping).forEach(function(key){
            var mappedKey = settings.addressMapping[key];
            var hasMappedKey = null !== mappedKey;
            if (hasMappedKey) {
                mappedAddressKeys.push(settings[mappedKey].fieldName);
            }
        });

        return mappedAddressKeys;
    },
    //  <--- Utility functions --->
    // EDQ added Validation logic
    showLoader : function(component, settings) {
        var loader = component.find(settings.loaderId);
        $A.util.removeClass(loader, 'invisible');
        $A.util.addClass(loader, 'visible');
    },
    // EDQ added Validation logic
    hideLoader : function(component, settings) {
        var loader = component.find(settings.loaderId);
        $A.util.removeClass(loader, 'visible');
        $A.util.addClass(loader, 'invisible');
    },
    // EDQ added Validation logic
    getVariableValue : function(component, attributeName) {
        return component.get('v.' + attributeName);
    },
    // EDQ added Validation logic
    setVariableValue : function(component, attributeName, value) {
        component.set('v.' + attributeName, value);
    },
    // EDQ added Validation logic
    getElementValue : function(component, auraId) {
        return component.find(auraId).get('v.value');
    },
    // EDQ added Validation logic
    setElementValue : function(component, auraId, value) {
        component.find(auraId).set('v.value', value);
    },
    // EDQ added Validation logic
    isNullOrEmpty : function(value) {
        return this.isNull(value) || value == '';
    },
    // EDQ added Validation logic
    isNull : function(value) {
        return value == undefined || value == null;
    },
    // EDQ added Validation logic
    log : function(error) {
        if(this.isNull(window.console)) return;

        console.log(error);
    },
    // EDQ added Validation logic
    cleanupAddressFields : function(component, settings) {
        for(var addressMap in settings.addressMapping) {
            if(!settings.addressMapping.hasOwnProperty(addressMap)) continue;
            var settingsProperty = settings.addressMapping[addressMap];
            if(settings.hasOwnProperty(settingsProperty) && settings[settingsProperty] && settings[settingsProperty].auraId) {
                component.find(settings[settingsProperty].auraId).set('v.value', "");
            }
    	}
    }
})