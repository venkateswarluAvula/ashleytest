({
    myAction : function(component, event, helper) {

        var caseId = component.get("v.caseId");

        //get case details starts
        var actionCaseObj = component.get("c.getCaseObj");
        actionCaseObj.setParams({
            "recordId":caseId
        });
        actionCaseObj.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	console.log('case: ' + JSON.stringify(response.getReturnValue()));
                component.set("v.caseObj", response.getReturnValue());
                component.set("v.salesOrder", response.getReturnValue().Sales_Order__c);
                component.set("v.accId", response.getReturnValue().AccountId);
                component.set("v.fulfillID", response.getReturnValue().Legacy_Account_Ship_To__c);

				//set ship to address if the case address is not null
				if(response.getReturnValue().Address__c != null){
					component.set("v.shipToAdd1", response.getReturnValue().Address__r.Address_Line_1__c);
					component.set("v.shipToAdd2", response.getReturnValue().Address__r.Address_Line_2__c);
					component.set("v.shipToCity", response.getReturnValue().Address__r.City__c);
					component.set("v.shipToState", response.getReturnValue().Address__r.StateList__c);
					component.set("v.shipToZip", response.getReturnValue().Address__r.Zip_Code__c);
				}

                // Call Process line item method
                helper.processLineItem(component);

		        //get sales order fullfiller detail starts
		        var actionSalesOrderObj = component.get("c.getSalesOrderInfo");
		        actionSalesOrderObj.setParams({
		            "soId" : component.get("v.salesOrder")
		        });
		        actionSalesOrderObj.setCallback(this, function(response1) {
		            if (component.isValid() && response1.getState() === "SUCCESS") {
						console.log('salesorder: ' + JSON.stringify(response1.getReturnValue()));
						var ffid = '';
						var customerid = '';
		                component.set("v.salesOrderObj", response1.getReturnValue());
						if((response1.getReturnValue().fulfillerID__c != null) && (response1.getReturnValue().fulfillerID__c != '')){
							ffid = response1.getReturnValue().fulfillerID__c;
							customerid = response1.getReturnValue().phhCustomerID__c;
						} else {
							ffid = response1.getReturnValue().phhERPAccounShipTo__c;
							customerid = response1.getReturnValue().phhERPCustomerID__c;
						}
						component.set("v.fulfillID", ffid);
						component.set("v.CustId", customerid);

				        //get service technicians schedule duration detail starts
				        var actionScheduleDurationObj = component.get("c.getTechScheduleDuration");
				        actionScheduleDurationObj.setParams({
				            "fulfillerId" : ffid
				        });
				        actionScheduleDurationObj.setCallback(this, function(response2) {
				            if (component.isValid() && response2.getState() === "SUCCESS") {
				            	console.log('tech duration: ' + JSON.stringify(response2.getReturnValue()));
				            	if (response2.getReturnValue().error) {
					                var errorToast = $A.get("e.force:showToast");
					                errorToast.setParams({"mode": "sticky", "message": "Fulfiller Id is blank", "type":"error"});
					                errorToast.fire();
				            	} else {
					                component.set("v.techScheduleHours", response2.getReturnValue().hours);
					                component.set("v.techScheduleMins", response2.getReturnValue().mins);
					                console.log(component.get("v.techScheduleHours"));
					                console.log(component.get("v.techScheduleMins"));
				            	}
				            }
				            else {
				                var errorToast = $A.get("e.force:showToast");
				                errorToast.setParams({"message": response2.getError()[0].message, "type":"error"});
				                errorToast.fire();
				            }
				        });
				        $A.enqueueAction(actionScheduleDurationObj);
				        //get service technicians schedule duration detail ends

		                //get salesorder item zip code starts
		                var actionZipcode = component.get("c.getZipCode");
		                actionZipcode.setParams({
		                    "extId" : component.get("v.salesOrder")
		                });
		                actionZipcode.setCallback(this, function(response3) {
		                    if (component.isValid() && response3.getState() === "SUCCESS") {
		                    	console.log('soi zip code: ' + JSON.stringify(response3.getReturnValue()));
		                        if(component.get("v.caseObj").Address__c == null){
		                            component.set("v.zipcode", response3.getReturnValue());
		                        }
		                        else{
		                            component.set("v.zipcode", component.get("v.shipToZip"));
		                        }

		                        // Call API Response
		                        helper.getAPIResponse(component);
		                    }
		                    else {
		                        var errorToast = $A.get("e.force:showToast");
		                        errorToast.setParams({"message": response3.getError()[0].message, "type":"error"});
		                        errorToast.fire();
		                    }
		                });
		                $A.enqueueAction(actionZipcode);
		                //get salesorder item zip code ends
		            }
		            else {
		                var errorToast = $A.get("e.force:showToast");
		                errorToast.setParams({"message": response1.getError()[0].message, "type":"error"});
		                errorToast.fire();
		            }
		        });
		        $A.enqueueAction(actionSalesOrderObj);
		        //get sales order fullfiller detail ends

				//get sales order item detail starts
		        var actionSalesOrderObj1 = component.get("c.getSalesOrderLineInfo");
		        actionSalesOrderObj1.setParams({
		            "soId" : component.get("v.salesOrder")
		        });
		        actionSalesOrderObj1.setCallback(this, function(response4) {
		            if (component.isValid() && response4.getState() === "SUCCESS") {
		            	console.log('so line: ' + JSON.stringify(response4.getReturnValue()));
		                component.set("v.saleslineitem", response4.getReturnValue());

						//set ship to address if the case address is null
						if((component.get("v.caseObj").Address__c == null) || (component.get("v.caseObj").Address__c == "")){
							component.set("v.shipToAdd1", response4.getReturnValue().phdShipAddress1__c);
							component.set("v.shipToAdd2", response4.getReturnValue().phdShipAddress2__c);
							component.set("v.shipToCity", response4.getReturnValue().phdShipCity__c);
							component.set("v.shipToState", response4.getReturnValue().phdShipState__c);
							component.set("v.shipToZip", response4.getReturnValue().phdShipZip__c);
						}
						//component.set("v.shipToState", response4.getReturnValue().phdShipState__c);
		            }
		            else {
		                var errorToast = $A.get("e.force:showToast");
		                errorToast.setParams({"message": response4.getError()[0].message, "type":"error"});
		                errorToast.fire();
		            }
		        });
		        $A.enqueueAction(actionSalesOrderObj1);
		        //get sales order item detail ends

            }
            else {
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": response.getError()[0].message, "type":"error"});
                errorToast.fire();
            }
        });
        $A.enqueueAction(actionCaseObj);
        //get case details ends

        //get case line item detail starts
        /*
        var actionCaseLineItem = component.get("c.getLineItemcase");
        actionCaseLineItem.setParams({
            "recordId":caseId
        });
        actionCaseLineItem.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                component.set("v.caseItemObj", response.getReturnValue());
                console.log('lineitem..' + JSON.stringify(response.getReturnValue()));
            }
            else {
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": response.getError()[0].message, "type":"error"});
                errorToast.fire();
            }
        });
        $A.enqueueAction(actionCaseLineItem);
        */
		//get case line item detail ends

		//get state picklist value starts
		var actionStatePl = component.get("c.getStateVal");
        actionStatePl.setCallback(this, function(response6) {
            if (component.isValid() && response6.getState() === "SUCCESS") {
                console.log('Picklist: ' + JSON.stringify(response6.getReturnValue()));
                var opts = [];
                opts.push({ class: "optionClass", label: "State", value: "State", disabled: "true" });
                response6.getReturnValue().forEach(function(key){
                	if((component.get("v.shipToState") != null) && (component.get("v.shipToState") == key)) {
	                    opts.push({ class: "optionClass", label: key, value: key, selected: "true" });
                	} else {
	                    opts.push({ class: "optionClass", label: key, value: key });
                	}
                });
                console.log('opts: ' + JSON.stringify(opts));

		        component.find("state").set("v.options", opts);
            }
            else {
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": response6.getError()[0].message, "type":"error"});
                errorToast.fire();
            }
        });
        $A.enqueueAction(actionStatePl);
        //get state picklist value ends
    },

    refreshAPI: function(component, event, helper) {
    	//called when shipping address is changed
    	helper.controlZipDetails(component, false);

    	if (helper.isNullOrEmpty(component.find("zip").get("v.value"))) {
        	var errorToast = $A.get("e.force:showToast");
        	errorToast.setParams({"mode": "sticky", "message": "There is no Zip Code on the address associated with this Case. Please update the address to include the proper Zip Code.", "type":"error"});
        	errorToast.fire();
		} else {
			var zipVar = component.find("zip").get("v.value").trim();
			component.set("v.zipcode", zipVar);
	        component.set("v.selectedCompany", '');
	        component.set("v.ListOfCompanies", '');
	        component.set("v.selectedEmployee", '');
	        component.set("v.ListOfEmployees", null);
	        component.set("v.selectedTechnician", '');

	        //call helper method to reset tech schedule common attributes
	    	helper.resetScheduleAttr(component);
	    	helper.resetCalendarAttr(component);

    		var schTechName = component.get("v.caseObj").TechnicianNameScheduled__c;
    		if ((schTechName != null) && (!component.get("v.isTechAdrChanged"))) {
    			//rescheduling technician for the same case

		        var isAddressChanged = false;
		        if(component.find("billAddress1").get("v.value") != component.find("address1").get("v.value")){
		            isAddressChanged = true;
		        }
		        if(component.find("billAddress2").get("v.value") != component.find("address2").get("v.value")){
		            isAddressChanged = true;
		        }
		        if(component.find("billCity").get("v.value") != component.find("city").get("v.value")){
		            isAddressChanged = true;
		        }
		        if(component.find("billState").get("v.value") != component.find("state").get("v.value")){
		            isAddressChanged = true;
		        }
		        if(component.find("billZipCode").get("v.value") != component.find("zip").get("v.value")){
		            isAddressChanged = true;
		        }

				if (isAddressChanged == true) {
			        var fulfillerId = component.get("v.fulfillID");
		    	    var schTechCompany = component.get("v.caseObj").Technician_Company__c;
		    	    var adr = component.get("v.caseObj").Technician_Address__c;
		    	    var adrSplit = adr.split(",");
		    	    var oldZip = adrSplit[adrSplit.length - 1];
		    	    var zipcode = component.get("v.shipToZip").split("-");
					var techSchDate = component.get("v.caseObj").Technician_Schedule_Date__c;
					var schTechId = component.get("v.caseObj").Technician_Id__c;

					console.log('Change Ship Adr: ' + fulfillerId + ' | ' + oldZip + ' | ' + schTechCompany + ' | ' + schTechId + ' | ' + zipcode + ' | ' + techSchDate);
					var actionTechAva = component.get("c.validateTechnicianAvailablity");
			        actionTechAva.setParams({
			            "fulfillerId" : fulfillerId,
			            "companyValue" : schTechCompany,
			            "techId" : schTechId,
			            "oldZipCode" : oldZip,
			            "newZipCode" : zipcode[0],
			            "techDate" : techSchDate
			        });
		        	actionTechAva.setCallback(this, function(response) {
		            	if (component.isValid() && response.getState() === "SUCCESS") {
		                	console.log('response: ' + JSON.stringify(response.getReturnValue()));
		                	var callAvaResponse = response.getReturnValue();
		                	if(callAvaResponse == 'Tech Available') {
		                		// technician available
		                		//Same technician is available for new address, do you want to update the address for the existing scheduled technician?
		                		component.set("v.isTechUpdateConfirm", true);
		                	} else if(callAvaResponse == 'Tech Reschedule') {
		                		//case is closed and address update is not allowed
		                		component.set("v.isTechAddressChange", true);
		                	} else {
		                		// technician not available
		                		//The previously scheduled technician is not available for the updated address, you will need to schedule a different technician.
		                		component.set("v.isTechChangeConfirm", true);
		                	}
			            } else {
		                	var errorToast = $A.get("e.force:showToast");
		                	errorToast.setParams({"mode": "sticky", "message": response.getError()[0].message, "type":"error"});
		                	errorToast.fire();
		            	}
		        	});
		        	$A.enqueueAction(actionTechAva);
				} else {
			        // Call API Response
		    	    helper.getAPIResponse(component);
				}
	    	} else {
	    		component.set("v.isTechAdrChanged", false);
		        // Call API Response
	    	    helper.getAPIResponse(component);
			}
		}
    },

    // experian code added by praneeth on 12/11
      handleSearchChange : function(component, event, helper) {
        var settings = helper.getSettings();
        helper.handleSearchChange(component, event, settings);
        helper.handleValidationStatus(component, settings);
    },
    // EDQ added Validation logic
    handleSuggestionNavigation :  function(component, event, helper) {
        var settings = helper.getSettings();
        var keyCode = event.which;
        if(keyCode == 13) { // Enter
            helper.acceptFirstSuggestion(component, settings);
        } else if(keyCode == 40) { //Arrow down
            helper.selectNextSuggestion(component, null, settings);
        } else if(keyCode == 38) { //Arrow up
            helper.hideAndRemoveSuggestions(component, settings);
        }
    },
    // EDQ event listener
    onSuggestionKeyUp : function(component, event, helper) {
        var settings = helper.getSettings();
        helper.onSuggestionKeyUp(component, event, settings);
    },
    // EDQ event listener
    handleResultSelect : function(component, event, helper) {
        var settings = helper.getSettings();
        helper.handleResultSelect(component, event, settings);
    },
    // EDQ event listener
    onAddressChanged : function(component, event, helper) {
		helper.controlZipDetails(component, true);
        var settings = helper.getSettings();
        helper.handleValidationStatus(component, settings);
    },
    // EDQ event listener
    onElementFocusedOut : function (component, event, helper) {
        var settings = helper.getSettings();

        var useHasNotSelectedASuggestion = false;
        if (helper.isNull(event.relatedTarget)) {
            useHasNotSelectedASuggestion = true;
        } else if (helper.isNull(event.relatedTarget.id)) {
            useHasNotSelectedASuggestion = true;
        } else if (event.relatedTarget.id.indexOf(settings.suggestionIndexClassPrefix) === -1) {
            useHasNotSelectedASuggestion = true;
        }

        if (useHasNotSelectedASuggestion) {
            helper.hideAndRemoveSuggestions(component, settings);
        }
    },
    // experian code ends
    onCompanyChange : function (component, event, helper) {
        component.set("v.caseObj.Tech_Scheduled_Company__c",'');
        component.set("v.selectedTechnician", '');
        component.set("v.ListOfEmployees", null);
        //call helper method to reset tech schedule common attributes
    	helper.resetScheduleAttr(component);
    	helper.resetCalendarAttr(component);

        var fulfillerId = component.get("v.fulfillID");
        var zipcode = component.get("v.zipcode").split("-");
        var selectedCompany = component.find("myCompany").get("v.value").trim();
        console.log('fulfillerId: ' + fulfillerId + ' zipcode: ' + zipcode + ' selectedCompany: ' + selectedCompany);

        if(selectedCompany != '') {
	        var actionHTTPReq = component.get("c.getTechnicians");
	        actionHTTPReq.setParams({
	            "fulfillerId"  : fulfillerId,
	            "companyvalue" : selectedCompany,
	            "zipcode"      : zipcode[0]
	        });
	        actionHTTPReq.setCallback(this, function(response) {
	            if (component.isValid() && response.getState() === "SUCCESS") {
	                console.log('onCompanyChange: ' + JSON.stringify(response.getReturnValue()));
	                if(response.getReturnValue().respStatusCode == 200) {
		                var employeeList = [];
		                (response.getReturnValue().respList).forEach(function(key){
		                    employeeList.push(key);
		                });
		                component.set("v.ListOfEmployees", employeeList);
	                } else if(response.getReturnValue().respStatusCode == 404) {
	                	var errorToast = $A.get("e.force:showToast");
	                	errorToast.setParams({"mode": "sticky", "message": response.getReturnValue().errorStr, "type":"error"});
	                	errorToast.fire();
	                }
	            }
	            else {
	                console.log('Tech');
	            }
	        });
	        $A.enqueueAction(actionHTTPReq);
        }
    },

    onEmployeeChange : function (component, event, helper) {
    	//call helper method to reset tech schedule common attributes
    	helper.resetScheduleAttr(component);
    	helper.resetCalendarAttr(component);

        //component.set("v.Spinner", true);

        var fulfillerId = component.get("v.fulfillID");
        var employeeTechId = component.find("technician").get("v.value");
        var maxHours  = 0;
        var maxRepair = 0;
        var maxPieces = 0;
        var upHostery = 0;
        var casegoods = 0;
        var leather   = 0;
        var workingHoursprogvalue = 0;
        var RepairStopsprogvalue  = 0;
        var PiecesRepairprogvalue = 0;
        var CurrentWorkingHoursProgValue = 0;
        var CurrentRepairStopsprogvalue  = 0;
        var CurrentPiecesRepairprogvalue = 0;
        var WeeklyScheduleVar  = '';
        var leaveDates = [];

        var actionHttpWorkLoad = component.get("c.getWorkloadInfo");
        actionHttpWorkLoad.setParams({
            "fulfillerId" : fulfillerId,
            "resourceId"  : employeeTechId
        });
        actionHttpWorkLoad.setCallback(this, function(response) {
            console.log(actionHttpWorkLoad);
            if (component.isValid() && response.getState() === "SUCCESS") {
                console.log('workloaddetails' + JSON.stringify(response.getReturnValue()));

                response.getReturnValue().forEach(function(key){
                    if(key.TechId == employeeTechId){
                        maxHours = key.MaxHoursPerDay;
                        maxRepair = key.MaxRepairPerDay;
                        maxPieces = key.MaxPiecesPerDay;
                        upHostery = key.UpholsterySkillLevel;
                        casegoods = key.CasegoodsSkillLevel;
                        leather = key.LeatherSkillLevel;
                        WeeklyScheduleVar = key.WeeklySchedule;

                        if (key.LeaveDates != undefined) {
	                        key.LeaveDates.forEach(function(key1){
						        leaveDates.push(key1.LeaveDate.split('T')[0]);
	                        });
                        }
                    }
                });
                console.log('leaveDates: ' + leaveDates);

                component.set("v.cLeaveDates", leaveDates);
                component.set("v.maxHours", maxHours);
                component.set("v.maxRepair",maxRepair);
                component.set("v.maxPieces",maxPieces);
                component.set("v.upHostery",upHostery);
                component.set("v.casegoods",casegoods);
                component.set("v.leather",leather);
                component.set("v.weeklyScheduleVar",WeeklyScheduleVar);
                component.set("v.workingHoursprogvalue",workingHoursprogvalue);
                component.set("v.RepairStopsprogvalue",RepairStopsprogvalue);
                component.set("v.PiecesRepairprogvalue",PiecesRepairprogvalue);
                component.set("v.CurrentWorkingHoursProgValue", CurrentWorkingHoursProgValue);
                component.set("v.CurrentRepairStopsprogvalue", CurrentRepairStopsprogvalue);
                component.set("v.CurrentPiecesRepairprogvalue", CurrentPiecesRepairprogvalue);

		        if(!$A.util.isEmpty(component.get("v.estimatedtimeforstopwithminutes"))){
		        	console.log('Calendar trigger after employee change');
			        helper.resetCalendarAttr(component);

		    		var dt = new Date();
					var tYr = dt.getFullYear();
					var tmth = dt.getMonth() + 1;

					component.set("v.cYear", tYr);
					component.set("v.cMonth", tmth);
					component.set("v.cMonthInc", 0);

					helper.techCalendarLoad(component);
				}
            }
            else {
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": 'There might be some issue with your selection \n OR an issue with the system. Please contact your administrator.', "type":"error"});
                errorToast.fire();
            }
        });
		$A.enqueueAction(actionHttpWorkLoad);
    },

    onMonthChange : function(component, event , helper) {
        helper.techCalendarLoad(component);
    },

    GobackToRecord : function (component, event, helper) {
    	helper.redirectToCase(component);
    },

    openLineItem : function(component , event, helper) {
        component.set("v.isOpenForLine",true);
        component.set("v.conferror", '');

        //component.set("v.Spinner", true);
        var actionlinevalue = component.get("c.getApiResponse1");
        actionlinevalue.setParams({
            "recordId" : component.get("v.caseObj").AccountId
        });
        actionlinevalue.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var callResponse = response.getReturnValue();
                console.log('getApiResponse1: ' + callResponse.length + ':' + callResponse);
                component.set("v.orders", callResponse); 
                component.set("v.totalSize", callResponse.length);
                var descripvals = [];
                var sonumber = [];
                var deldate = [];
                var rows = [];
                var errorrows = [];
                var isSelectedFlag;
                var serialnumber;

                if(callResponse.length == 0 ){
                    component.set("v.isOpenForLine",false);

                    var errorToast = $A.get("e.force:showToast");
                    errorToast.setParams({"message": 'No Line items found for this Customer', "type":"error"});
                    errorToast.fire();
                } else {

					console.log('O Draft Items: ' + JSON.stringify(component.get('v.draftItems')));
					console.log('O Confirm Items: ' + JSON.stringify(component.get('v.confirmItems')));
					console.log('O Selected Associate Items: ' + JSON.stringify(component.get('v.selectedAssociateItems')));

					var jsonData = JSON.parse(response.getReturnValue());
                    for(var i=0;i<jsonData.value.length;i++){
                        var snNumber = jsonData.value[i].SalesOrderNumber;

                        for(var j=0;j<jsonData.value[i].TechSchedulingItems.length;j++){
                        	if(jsonData.value[i].TechSchedulingItems[j].Quantity >= 0){
                            	var deliverdate = jsonData.value[i].TechSchedulingItems[j].DeliveredDate;

								var keyVal = snNumber + '|' + jsonData.value[i].TechSchedulingItems[j].ItemSKU + '|' + jsonData.value[i].TechSchedulingItems[j].ItemSequence;

	                            isSelectedFlag = false;
	                            serialnumber = '';
	                            //populating serial number & selected values based on the values stored in selectedAssociateItems attribute
	                            var selAssociateItems = component.get('v.selectedAssociateItems');
	                            for(var ci=0; ci<selAssociateItems.length; ci++){
	                                if(selAssociateItems[ci].keyId == keyVal){
	                                    isSelectedFlag = true;
	                                    serialnumber = selAssociateItems[ci].serialNo;
	                                }
	                            }

	                            //populating serial number based on the values stored in draftItems attribute
	                            var draftItem = component.get('v.draftItems');
	                            //alert('draftItem controller: ' + JSON.stringify(draftItem));
	                            for(var di=0; di<draftItem.length; di++){
	                                if(draftItem[di].keyId == keyVal){
	                                    serialnumber = draftItem[di].serialNo;
	                                }
	                            }

								console.log('Associate Items: salesorder_number: ' + snNumber + ' sku: ' + jsonData.value[i].TechSchedulingItems[j].ItemSKU + ' seqNumber: ' + jsonData.value[i].TechSchedulingItems[j].ItemSequence + ' UniqueID: ' + jsonData.value[i].TechSchedulingItems[j].UniqueID + ' Key: ' + keyVal);

	                            if(deliverdate != null  && deliverdate == undefined) {
	                                var eachRow  = {
										isSelected:isSelectedFlag,
										PersonAccountID:false,
										UniqueID:false,
										salesorder_number:snNumber,
										serialNo:serialnumber,
										seqNumber:jsonData.value[i].TechSchedulingItems[j].ItemSequence,
										desc:jsonData.value[i].TechSchedulingItems[j].ItemDescription2,
										deldate:jsonData.value[i].TechSchedulingItems[j].DeliveredDate.substring(0,10),
										wardate:jsonData.value[i].TechSchedulingItems[j].WarrantyEndDate.substring(0,10),
										sku:jsonData.value[i].TechSchedulingItems[j].ItemSKU,
										qty:jsonData.value[i].TechSchedulingItems[j].Quantity,
										uniqueid:jsonData.value[i].TechSchedulingItems[j].UniqueID,
										orderstatus:jsonData.value[i].TechSchedulingItems[j].SalesOrderStatus,
										storeid:jsonData.value[i].TechSchedulingItems[j].StoreID,
										erpcustomerid:jsonData.value[i].TechSchedulingItems[j].ERPCustomerId,
										erpaccountshipto:jsonData.value[i].TechSchedulingItems[j].ERPAccounShipTo,
										customerid:jsonData.value[i].TechSchedulingItems[j].CustomerID,
										accountshipto:jsonData.value[i].TechSchedulingItems[j].FulfillerID,
										keyId:keyVal
	                                }
								} else {
                                    var eachRow  = {
                                        isSelected:isSelectedFlag,
                                        salesorder_number:snNumber,
                                        serialNo:serialnumber,
                                        seqNumber :jsonData.value[i].TechSchedulingItems[j].ItemSequence,
                                        desc:jsonData.value[i].TechSchedulingItems[j].ItemDescription2,
                                        deldate:jsonData.value[i].TechSchedulingItems[j].DeliveredDate,
                                        wardate:jsonData.value[i].TechSchedulingItems[j].WarrantyEndDate.substring(0,10),
                                        sku:jsonData.value[i].TechSchedulingItems[j].ItemSKU,
                                        qty:jsonData.value[i].TechSchedulingItems[j].Quantity,
                                        uniqueid:jsonData.value[i].TechSchedulingItems[j].UniqueID,
                                        orderstatus:jsonData.value[i].TechSchedulingItems[j].SalesOrderStatus,
										storeid:jsonData.value[i].TechSchedulingItems[j].StoreID,
										erpcustomerid:jsonData.value[i].TechSchedulingItems[j].ERPCustomerId,
										erpaccountshipto:jsonData.value[i].TechSchedulingItems[j].ERPAccounShipTo,
										customerid:jsonData.value[i].TechSchedulingItems[j].CustomerID,
										accountshipto:jsonData.value[i].TechSchedulingItems[j].FulfillerID,
                                    	keyId:keyVal
                                    }
								};

	                            var rowjson = JSON.stringify(eachRow);
	                            rows.push(eachRow);
	                            //console.log('sekar: '+rowjson);
	                            var itDesc = jsonData.value[i].TechSchedulingItems[j].ItemDescription2;
	                            var itdeldate = jsonData.value[i].TechSchedulingItems[j].DeliveredDate;
	                            descripvals.push(itDesc);
	                            sonumber.push(snNumber);
	                            deldate.push(itdeldate);
                            }
                        }
                    }
                }
                component.set('v.allrows',rows);
                component.set('v.democolumns', [
                    {
                        fieldName: 'isSelected', type: "boolean", initialWidth: 50,
                        cellAttributes: {
                            iconName: { fieldName: "isSelected_chk" }
                        }},
                    {label: 'Seq NUMBER', fieldName: 'seqNumber', type: 'number', initialWidth: 50},
                    {label: 'SALESORDER NUMBER', fieldName: 'salesorder_number', type: 'text', initialWidth: 100},
                    {label: 'DESCRIPTION', fieldName: 'desc', type: 'text', initialWidth: 150},
                    {label: 'DELIVERY DATE', fieldName: 'deldate', type: 'text', initialWidth: 100},
                    {label: 'WARRANTY DATE', fieldName: 'wardate', type: 'text', initialWidth: 100},
                    {label: 'SKU', fieldName: 'sku', type: 'text', initialWidth: 100},
                    {label: 'Status', fieldName: 'orderstatus', type: 'text', initialWidth: 100},
                    {label: 'Quantity', fieldName: 'qty', type: 'number', initialWidth: 50},
                    {label: 'Serial Number', fieldName: 'serialNo',editable:'true', type: 'text', initialWidth: 100},
                    {type: 'button', initialWidth: 100, typeAttributes: {label: 'Confirm', name:'confirmRecord', class: 'btn_next'} }
                    
                ]);

                component.set('v.datajsonstringify',eachRow);

				//STAR SKU STARTS
        		var actionsku = component.get("c.getApiResponseforlinesku");
		        actionsku.setParams({
		            "recordId" : component.get("v.caseObj").AccountId
		        });
		        actionsku.setCallback(this,function(skuresponse)	{
		            if (component.isValid() && skuresponse.getState() === "SUCCESS") {
		                var skucallResponse = skuresponse.getReturnValue();
		                console.log('length: ' + skucallResponse.length);
		                component.set("v.orderssku", skucallResponse);
		                component.set("v.totalSizesku", skucallResponse.length);

		                var descripvalssku = [];
		                var sonumbersku = [];
		                var deldatesku = [];
		                var rowssku = [];
		                var errorrowssku = [];
		                var isSelectedFlagsku;
		                var serialnumbersku;
		                if(skucallResponse.length == 0){
		                    //component.set("v.isOpenForLine",false);
		                    var errorToast = $A.get("e.force:showToast");
		                    errorToast.setParams({"message": 'No Star Sku Items Found for this Customer', "type":"error"});
		                    errorToast.fire();
		                } else {

		                	var techSchSkuLabel = ($A.get("{!$Label.c.Tech_Schedule_Star_SKU}")).toLowerCase();

		                	var jsonDatasku = JSON.parse(skuresponse.getReturnValue());
		                    for(var si=0;si<jsonDatasku.value.length;si++){

		                        var snSkuNumber = jsonDatasku.value[si].SalesOrderNumber;
		                        for(var sj=0;sj<jsonDatasku.value[si].TechSchedulingItems.length;sj++){

		                            var deliverdatesku = jsonDatasku.value[si].TechSchedulingItems[sj].DeliveredDate;

		                            var keyVal1 = snSkuNumber + '|' + jsonDatasku.value[si].TechSchedulingItems[sj].ItemSKU + '|' + jsonDatasku.value[si].TechSchedulingItems[sj].ItemSequence;

		                            isSelectedFlagsku = false;
		                            serialnumbersku = '';
		                            //populating serial number & selected values based on the values stored in selectedAssociateItemsSku attribute
		                            var selAssociateItemsSku = component.get('v.selectedAssociateItemsSku');
		                            for(var ci1=0; ci1<selAssociateItemsSku.length; ci1++){
		                            	if(selAssociateItemsSku[ci1].keyId == keyVal1){
		                            		isSelectedFlagsku = true;
		                            		serialnumbersku = selAssociateItemsSku[ci1].serialNo;
		                            	}
		                            }

		                            //populating serial number based on the values stored in draftItems attribute
		                            var draftItemSku = component.get('v.draftItemsSku');
		                            //alert('draftItem controller: ' + JSON.stringify(draftItem));
		                            for(var di1=0; di1<draftItemSku.length; di1++){
		                            	if(draftItemSku[di1].keyId == keyVal1){
		                            		serialnumbersku = draftItemSku[di1].serialNo;
		                            	}
		                            }

		                            console.log('Star SKU Items: salesorder_number: ' + snSkuNumber + ' sku: ' + jsonDatasku.value[si].TechSchedulingItems[sj].ItemSKU + ' seqNumber: ' + jsonDatasku.value[si].TechSchedulingItems[sj].ItemSequence + ' UniqueID: ' + jsonDatasku.value[si].TechSchedulingItems[sj].UniqueID + ' Key: ' + keyVal1);

		                            var skuConfirmBtnDisable = true;
		                            if (techSchSkuLabel.includes((jsonDatasku.value[si].TechSchedulingItems[sj].ItemSKU).toLowerCase())) {
		                            	skuConfirmBtnDisable = false;
		                            }

		                            if(deliverdatesku != null  && deliverdatesku == undefined) {
		                                var eachRow  = {
		                                    isSelected:isSelectedFlagsku,
		                                    PersonAccountID:false,
		                                    UniqueID:false,
		                                    salesorder_number:snSkuNumber,
		                                    serialNo:serialnumbersku,
		                                    seqNumber:jsonDatasku.value[si].TechSchedulingItems[sj].ItemSequence,
		                                    desc:jsonDatasku.value[si].TechSchedulingItems[sj].ItemDescription2,
		                                    deldate:jsonDatasku.value[si].TechSchedulingItems[sj].DeliveredDate.substring(0,10),
		                                    wardate:jsonDatasku.value[si].TechSchedulingItems[sj].WarrantyEndDate.substring(0,10),
		                                    sku:jsonDatasku.value[si].TechSchedulingItems[sj].ItemSKU,
		                                    qty:jsonDatasku.value[si].TechSchedulingItems[sj].Quantity,
		                                    uniqueid:jsonDatasku.value[si].TechSchedulingItems[sj].UniqueID,
		                                    orderstatus:jsonDatasku.value[si].TechSchedulingItems[sj].SalesOrderStatus,
											storeid:jsonDatasku.value[si].TechSchedulingItems[sj].StoreID,
											erpcustomerid:jsonDatasku.value[si].TechSchedulingItems[sj].ERPCustomerId,
											erpaccountshipto:jsonDatasku.value[si].TechSchedulingItems[sj].ERPAccounShipTo,
											customerid:jsonDatasku.value[si].TechSchedulingItems[sj].CustomerID,
											accountshipto:jsonDatasku.value[si].TechSchedulingItems[sj].FulfillerID,
		                                    keyId:keyVal1,
		                                    actionDisabled:skuConfirmBtnDisable
		                                }
		                            } else {
		                                var eachRow  = {
		                                    isSelected:isSelectedFlagsku,
		                                    salesorder_number:snSkuNumber,
		                                    serialNo:serialnumbersku,
		                                    seqNumber:jsonDatasku.value[si].TechSchedulingItems[sj].ItemSequence,
		                                    desc:jsonDatasku.value[si].TechSchedulingItems[sj].ItemDescription2,
		                                    deldate:jsonDatasku.value[si].TechSchedulingItems[sj].DeliveredDate,
		                                    wardate:jsonDatasku.value[si].TechSchedulingItems[sj].WarrantyEndDate.substring(0,10),
		                                    sku:jsonDatasku.value[si].TechSchedulingItems[sj].ItemSKU,
		                                    qty:jsonDatasku.value[si].TechSchedulingItems[sj].Quantity,
		                                    uniqueid:jsonDatasku.value[si].TechSchedulingItems[sj].UniqueID,
		                                    orderstatus:jsonDatasku.value[si].TechSchedulingItems[sj].SalesOrderStatus,
											storeid:jsonDatasku.value[si].TechSchedulingItems[sj].StoreID,
											erpcustomerid:jsonDatasku.value[si].TechSchedulingItems[sj].ERPCustomerId,
											erpaccountshipto:jsonDatasku.value[si].TechSchedulingItems[sj].ERPAccounShipTo,
											customerid:jsonDatasku.value[si].TechSchedulingItems[sj].CustomerID,
											accountshipto:jsonDatasku.value[si].TechSchedulingItems[sj].FulfillerID,
		                                    keyId:keyVal1,
		                                    actionDisabled:skuConfirmBtnDisable
		                            	}
		                            };
		                            var rowjson = JSON.stringify(eachRow);
		                            rowssku.push(eachRow);

		                            var itSkuDesc = jsonDatasku.value[si].TechSchedulingItems[sj].ItemDescription2;
		                            var itSkudeldate = jsonDatasku.value[si].TechSchedulingItems[sj].DeliveredDate;
		                            descripvalssku.push(itSkuDesc);
		                            sonumbersku.push(snSkuNumber);
		                            deldatesku.push(itSkudeldate);
		                        }
		                    }
		                }
		                component.set('v.allrowssku',rowssku);
		                component.set('v.democolumnssku', [
		                    {
		                        fieldName: 'isSelected', type: "boolean", initialWidth: 50,
		                        cellAttributes: {
		                            iconName: { fieldName: "isSelected_chk" }
		                        }},
		                    {label: 'Seq NUMBER', fieldName: 'seqNumber', type: 'number', initialWidth: 100},
		                    {label: 'SALESORDER NUMBER', fieldName: 'salesorder_number', type: 'text', initialWidth: 100},
		                    {label: 'DESCRIPTION', fieldName: 'desc', type: 'text', initialWidth: 200},
		                    {label: 'DELIVERY DATE', fieldName: 'deldate', type: 'text', initialWidth: 100},
		                    {label: 'WARRANTY DATE', fieldName: 'wardate', type: 'text', initialWidth: 100},
		                    {label: 'SKU', fieldName: 'sku', type: 'text', initialWidth: 100},
		                    {label: 'Status', fieldName: 'orderstatus', type: 'text', initialWidth: 100},
		                    {label: 'Quantity', fieldName: 'qty', type: 'number', initialWidth: 100},
		                    {type: 'button', initialWidth: 100, typeAttributes: {label: 'Confirm', name:'confirmRecord', disabled: {fieldName: 'actionDisabled'}, class: 'btn_next'} }
		                ]);

		                component.set('v.datajsonstringify',eachRow); 
		            } else {
		                console.log('erros' + skuresponse.getState());
		            }
		        });
		        $A.enqueueAction(actionsku);
		        //STAR SKU ENDS

            } else {
                console.log('erros' + response.getState());
            }
        });
        $A.enqueueAction(actionlinevalue);
    },

    handleRowAction: function (component, event, helper) {
        //var action = event.getParam('button');
        var row = event.getParam('row');

		console.log('C Draft Items: ' + JSON.stringify(component.get('v.draftItems')));
		console.log('C Confirm Items: ' + JSON.stringify(component.get('v.confirmItems')));
		console.log('C Selected Associate Items: ' + JSON.stringify(component.get('v.selectedAssociateItems')));

        //validate if this item is already confirmed
        var confirmed = false;
        var confirmedItems = component.get('v.confirmItems');
        for(var z=0; z<confirmedItems.length; z++){
            if(confirmedItems[z].keyId == row.keyId){
                confirmed = true;
            }
        }

        //if the confirm is not triggered then call the helper method
        if(confirmed == false){
            //alert('Call replacement API');
            helper.selectRowValues(component, event);
        }
    },

    handleSaveEdition: function (cmp, event, helper){
        var draftValues = event.getParam('draftValues');
        //alert(JSON.stringify(draftValues));
        helper.saveChanges(cmp, draftValues);
    },

    handleRowActionSku: function (component, event, helper) {
        //var action = event.getParam('button');
        var row = event.getParam('row');

		console.log('C Draft Items Sku: ' + JSON.stringify(component.get('v.draftItemsSku')));
		console.log('C Confirm Items Sku: ' + JSON.stringify(component.get('v.confirmItemsSku')));
		console.log('C Selected Associate Items Sku: ' + JSON.stringify(component.get('v.selectedAssociateItemsSku')));

        //validate if this item is already confirmed
        var confirmed = false;
        var confirmedItems = component.get('v.confirmItemsSku');
        for(var z=0; z<confirmedItems.length; z++){
            if(confirmedItems[z].keyId == row.keyId){
                confirmed = true;
            }
        }

        //if the confirm is not triggered then call the helper method
        if(confirmed == false){
            //alert('Call replacement API');
            helper.selectRowValuesSku(component, event);
        }
    },

    handleSaveEditionSku: function (cmp, event, helper){
        var draftValues = event.getParam('draftValues');
        //alert(JSON.stringify(draftValues));
        helper.saveChangesSku(cmp, draftValues);
    },

    closeLineItem: function(component, event, helper) {
        component.set("v.isOpenForLine",false);

        // Call Process line item method
        helper.processLineItem(component);
    },
    
    openCommentSection : function(component, event,helper) {
        component.set("v.isOpenForComments",true);
    },
    
    closeCommentSection : function(component,event,helper) {
        component.set("v.isOpenForComments",false);
    },
    
    openModel: function(component, event, helper) {
        component.set("v.isOpen", true);
    },
    
    closeModel: function(component, event, helper) { 
        component.set("v.isOpen", false);
    },
    
    saveMeetingTime: function(component, event, helper) {
    	//component.set("v.Spinner", true);
        if(helper.getValidationOnValues(component)) {
	        var incrementalconversion;
	        if( component.get("v.incrementValue") == null || component.get("v.incrementValue") == undefined)  {
	            incrementalconversion = 0;
	        }
	        if (component.get("v.incrementValue") ==5){
	            incrementalconversion = 8;
	        }
	        if (component.get("v.incrementValue") ==10){
	            incrementalconversion = 17;
	        }
	        if (component.get("v.incrementValue") ==15){
	            incrementalconversion = 25;
	        }
	        if (component.get("v.incrementValue") ==20){
	            incrementalconversion = 33;
	        }
	        if (component.get("v.incrementValue") ==25){
	            incrementalconversion = 42;
	        }
	        if (component.get("v.incrementValue") ==30){
	            incrementalconversion = 50;
	        }
	        if (component.get("v.incrementValue") ==35){
	            incrementalconversion = 58;
	        }
	        if (component.get("v.incrementValue") ==40){
	            incrementalconversion = 67;
	        }
	        if (component.get("v.incrementValue") ==45){
	            incrementalconversion = 75;
	        }
	        if (component.get("v.incrementValue") ==50){
	            incrementalconversion = 83;
	        }
	        if (component.get("v.incrementValue") ==55){
	            incrementalconversion = 92;
	        }

	        var finalincrementalmeetingval;
	        if (incrementalconversion == undefined) {
	            finalincrementalmeetingval = component.get("v.minValue") +  'Hour0';
	        } else if (incrementalconversion == 8) {
	            finalincrementalmeetingval = component.get("v.minValue") +  'Hour08';
	        } else {
	            finalincrementalmeetingval = component.get("v.minValue") +  'Hour'  + incrementalconversion; 
	        }

	        var meetingValue = component.get("v.minValue") +  'Hour'  + component.get("v.incrementValue") ;
	        component.set("v.estimatedtimeforstopwithminutes" , finalincrementalmeetingval);
	        component.set("v.meetingRoomValue", meetingValue);

	        console.log('estimatedtimeforstopwithminutes: ' + component.get("v.estimatedtimeforstopwithminutes"));
	        console.log('meetingRoomValue: ' + component.get("v.meetingRoomValue"));

        	var hrsList = component.get("v.techScheduleHours");
        	var techMaxHrs = hrsList[hrsList.length-1];
        	var estimateTime = component.get("v.estimatedtimeforstopwithminutes").replace("Hour",".");
        	if(Number(estimateTime) > Number(techMaxHrs)){
        		var errorToast = $A.get("e.force:showToast");
            	errorToast.setParams({"message": "Maximum Value for Estimated Time should not be greater than " + techMaxHrs + " hours", "type":"error"});
            	errorToast.fire();
        	} else {
		        component.set("v.isOpen", false);

		        //have to call the calendar here instead of company change method
	    	    if(!$A.util.isEmpty(component.get("v.weeklyScheduleVar"))){
	    	    	console.log('Calendar trigger after estimate time to stop');
		    	    helper.resetCalendarAttr(component);

	    			var dt = new Date();
					var tYr = dt.getFullYear();
					var tmth = dt.getMonth() + 1;

					component.set("v.cYear", tYr);
					component.set("v.cMonth", tmth);
					component.set("v.cMonthInc", 0);

					helper.techCalendarLoad(component);
				}
			}
        }
        //component.set("v.Spinner", false);
    },
    
    onDatePickerValue: function(component, event, helper) {
    	//component.set("v.Spinner", true);
        console.log('Date save selected date: ' + component.get("v.cSelectedDate"));
        console.log('datepicker date: ' + $( "#datepicker" ).val());

        if(component.get("v.cSelectedDate") == "") {
            //alert("no date selected");
            var errorToast = $A.get("e.force:showToast");
        	errorToast.setParams({"message": "Select a date to schedule a Technician", "type":"error"});
        	errorToast.fire();
        } else{
            var datePick1 = $( "#datepicker" ).val();
            var date = new Date(datePick1);
            var newdate = new Date(date);
            newdate.setDate(newdate.getDate() + 1);
            
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1;
            var y = newdate.getFullYear();
            
            var someFormattedDate = mm + '/' + dd + '/' + y;
            var datescheduledvalue = $( "#datepicker" ).val().split("/").reverse().join("-");
            var datePick = moment(datePick1).format('dddd MMMM D Y');
            component.set("v.datevalue", datePick);
            component.set("v.isopenfortech", false);
        }
        //component.set("v.Spinner", false);
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true);
    },
    
    hideSpinner : function(component,event,helper){   
        component.set("v.Spinner", false);
    },
    
    validate : function(component, event, helper) {
        
        var nameField = component.find("testCmp");//.get("v.value");
        var nameValue = nameField.get("v.value");
        
        if($A.util.isEmpty(nameValue) || $A.util.isUndefined(nameValue)){
            nameField.set("v.errors", [{message:"Enter a Name"}]);
        }         
        else {
            nameField.set("v.errors",null);
        }
    },

    openModal : function(component,event,helper) {
		//component.set("v.Spinner", true);
        var timestop = component.find("myTimeEstimated");
        var meetingRoomValue1= timestop.get("v.value");
        if( component.get("v.selectedCompany") === undefined || component.get("v.caseObj.Tech_Scheduled_Company__c") === undefined || component.get("v.selectedCompany").length === 0 ||  component.get("v.caseObj.Tech_Scheduled_Company__c").length === 0 || $A.util.isEmpty(meetingRoomValue1) || meetingRoomValue1 == '--SELECT--' || meetingRoomValue1 == undefined ){
            //alert("You must Select Company and Technician before scheduling a Technician");
            var errorToast = $A.get("e.force:showToast");
        	errorToast.setParams({"message": "You must Select Company, Technician and Estimated time for stop before scheduling a Technician", "type":"error"});
        	errorToast.fire();
        } else {
            component.set("v.isopenfortech",true);
        }
        //component.set("v.Spinner", false);
    },

    closeModal : function(component,event,helper) {
        component.set("v.isopenfortech",false);
    },

    techAdrUpdateConfirmYes: function(component, event, helper) {
		component.set("v.isTechUpdateConfirm", false);
		//call address update api then redirect user to case detail page.
        var fulfillerId = component.get("v.fulfillID");
	    var techReqId = component.get("v.caseObj").Technician_ServiceReqId__c;

		var add1 = component.find("address1").get("v.value");
		var add2 = component.find("address2").get("v.value");
		if(component.get("v.salesOrderObj").phhCustomerType__c == 'AFC'){
			add1 = component.get("v.salesOrderObj").phhShipToName__c;
			add2 = component.find("address1").get("v.value");
		}
		
		if (add2 == null) {
			add2 = '';
		}

		var tmpZip = component.find("zip").get("v.value").split("-");
		var selectedZip = tmpZip[0];

		var techAddr;
        if((component.find("address2").get("v.value") != undefined) && (component.find("address2").get("v.value") != '')) {
            techAddr = component.find("address1").get("v.value") +','+ component.find("address2").get("v.value") + ',' + component.find("city").get("v.value") +','+
                component.find("state").get("v.value") + ',' + selectedZip;
        } else {
            techAddr = component.find("address1").get("v.value") + ',' + component.find("city").get("v.value") +','+
                component.find("state").get("v.value") + ',' + selectedZip;
        }

        var jsonAdrvar = {
            AddressLine1 : add1,
            AddressLine2 : add2,
            City : component.find("city").get("v.value"),
            State : component.find("state").get("v.value"),
            ZipCode : selectedZip
        };

		console.log('Ship Adr Update: ' + fulfillerId + ' | ' + techReqId + ' | ' + JSON.stringify(jsonAdrvar));
		var actionAdrUpd = component.get("c.updateTechnicianAddress");
        actionAdrUpd.setParams({
            "fulfillerId" : fulfillerId,
            "reqId" : techReqId,
            "jsonData" : JSON.stringify(jsonAdrvar),
            "adrUpd" : techAddr,
            "caseId" : component.get("v.caseObj").Id,
            "sAdd1" : component.find("address1").get("v.value"),
            "sAdd2" : component.find("address2").get("v.value"),
            "sCity" : component.find("city").get("v.value"),
            "sState" : component.find("state").get("v.value"),
            "sZip" : selectedZip
        });
    	actionAdrUpd.setCallback(this, function(response) {
        	if (component.isValid() && response.getState() === "SUCCESS") {
        		console.log('res: ' + res);
				var res = response.getReturnValue();

                var errorToast = $A.get("e.force:showToast");
            	if(res == 'success') {
            		errorToast.setParams({"title": "Success!", "message": 'Ship to address updated successfully for this Service Request.', "type":"success"});
            	} else {
            		errorToast.setParams({"title": "Error!", "message": res, "mode": "sticky", "type":"error"});
            	}
                errorToast.fire();

            	helper.redirectToCase(component);
            } else {
            	var errorToast = $A.get("e.force:showToast");
            	errorToast.setParams({"message": response.getError()[0].message, "type":"error"});
            	errorToast.fire();
        	}
    	});
    	$A.enqueueAction(actionAdrUpd);
    },

    techAdrUpdateConfirmNo: function(component, event, helper) { 
		component.set("v.isTechUpdateConfirm", false);
        //continue with normal tech scheduling process
        helper.getAPIResponse(component);
    },

    techChangeConfirmOk: function(component, event, helper) { 
        component.set("v.isTechChangeConfirm", false);
        component.set("v.isTechAddressChange", false);
        //continue with normal tech scheduling process
        helper.getAPIResponse(component);
    },

    techChangeConfirmCancel: function(component, event, helper) {
     	component.set("v.isTechChangeConfirm", false);
     	//address change is not allowed
     	component.set("v.isTechAddressChange", false);
     	//take back to case page
     	helper.redirectToCase(component);
    },

    openAdrConfirmation: function(component, event, helper) {
    	if(helper.validate(component)) {
			//make a call to apex controller to get the list of address associated to this customer and sales order line item address

    		console.log('Tech Adr: ' + component.find("address1").get("v.value") +'|'+ component.find("address2").get("v.value") +'|'+ component.find("city").get("v.value") +'|'+ component.find("state").get("v.value") +'|'+ component.find("zip").get("v.value"));

			//get customer addresses starts
	        var actionCustAdrObj = component.get("c.getAddressOption");
	        actionCustAdrObj.setParams({
	        	"custId" : component.get("v.accId"),
	            "soId" : component.get("v.salesOrder"),
	            "techAdrLine1": component.find("address1").get("v.value"),
	            "techAdrLine2": component.find("address2").get("v.value"),
	            "techAdrCity": component.find("city").get("v.value"),
	            "techAdrState": component.find("state").get("v.value"),
	            "techAdrZip": component.find("zip").get("v.value")
	        });
	        actionCustAdrObj.setCallback(this, function(response) {
	        	console.log(JSON.stringify(actionCustAdrObj.getParams()));
	            if (component.isValid() && response.getState() === "SUCCESS") {
	            	console.log('Customer addresses: ' + JSON.stringify(response.getReturnValue()));
	            	component.set("v.custAddressWrapObj", response.getReturnValue());
	            } else {
	                var errorToast = $A.get("e.force:showToast");
	                errorToast.setParams({"message": response.getError()[0].message, "type":"error"});
	                errorToast.fire();
	            }
	        });
	        $A.enqueueAction(actionCustAdrObj);
	        //get customer addresses ends

        	component.set("v.isAdrConfirm", true);
    	}
    },

    closeAdrConfirmation: function(component, event, helper) { 
        component.set("v.isAdrConfirm", false);
    },

    selectAdr: function(component, event, helper) {
    	console.log('Id: ' + event.currentTarget.id);
    	console.log('Type: ' + event.currentTarget.dataset.adrtype);
    	console.log('Zip: ' + event.currentTarget.dataset.adrzip);
    	component.set("v.techAdrId", event.currentTarget.id);
    	component.set("v.techAdrType", event.currentTarget.dataset.adrtype);
    	component.set("v.techAdr1", event.currentTarget.dataset.adr1);
    	component.set("v.techAdr2", event.currentTarget.dataset.adr2);
    	component.set("v.techAdrCity", event.currentTarget.dataset.adrcity);
    	component.set("v.techAdrState", event.currentTarget.dataset.adrstate);
    	component.set("v.techAdrZip", event.currentTarget.dataset.adrzip);
    },

    confirmAdr: function(component, event, helper) {
    	console.log('confirmAdr techAdrId: ' + component.get("v.techAdrId"));
    	console.log('confirmAdr techAdrType: ' + component.get("v.techAdrType"));
    	if ((component.get("v.techAdrId") != undefined) && (component.get("v.techAdrType") != undefined) && (component.get("v.techAdrType") != 'selected_address')) {
    		//based on the selected address check the technician availability and schedule
    		var fulfillerId = component.get("v.fulfillID");
    		var schTechCompany = component.find("myCompany").get("v.value").trim();
    		//var schTechCompany = component.find("myCompany").get("v.value");
    		var schTechId = component.find("technician").get("v.value");
			var tmpZip = component.find("zip").get("v.value").split("-");
			var oldZip = tmpZip[0];
    	    var zipcode = component.get("v.techAdrZip");
    	    var datescheduledvalue = $( "#datepicker" ).val().split("/");
    	    var techSchDate = datescheduledvalue[2] + '-' + datescheduledvalue[0] + '-' + datescheduledvalue[1];

    	    console.log('Change Ship Adr: ' + fulfillerId + ' | ' + oldZip + ' | ' + schTechCompany + ' | ' + schTechId + ' | ' + zipcode + ' | ' + techSchDate);

			var actionTechAva = component.get("c.validateTechnicianAvailablity");
	        actionTechAva.setParams({
	            "fulfillerId" : fulfillerId,
	            "companyValue" : schTechCompany,
	            "techId" : schTechId,
	            "oldZipCode" : oldZip,
	            "newZipCode" : zipcode,
	            "techDate" : techSchDate
	        });
        	actionTechAva.setCallback(this, function(response) {
            	if (component.isValid() && response.getState() === "SUCCESS") {
                	console.log('response: ' + JSON.stringify(response.getReturnValue()));
                	var callAvaResponse = response.getReturnValue();
                	if(callAvaResponse == 'Tech Available') {
                		// technician available
                		//Same technician is available for new address, do you want to update the address for the existing scheduled technician?
                		component.set("v.techAdrSch", true);
                	} else {
                		// technician not available
                		//The previously scheduled technician is not available for the updated address, you will need to schedule a different technician.
                		component.set("v.techAdrChange", true);
					}
	            } else {
                	var errorToast = $A.get("e.force:showToast");
                	errorToast.setParams({"message": response.getError()[0].message, "type":"error"});
                	errorToast.fire();
            	}
        	});
        	$A.enqueueAction(actionTechAva);

    	} else {
    		//call generate post call method
    		var a = component.get('c.generatePostCall');
    		$A.enqueueAction(a);
    	}
    },

    technicianAdrChangeSch: function(component, event, helper) {
    	component.find("address1").set("v.value", component.get("v.techAdr1"));
    	console.log('Adr2: ' + component.get("v.techAdr2"));
    	if (component.get("v.techAdr2") != undefined) {
    		component.find("address2").set("v.value", component.get("v.techAdr2"));
    	} else {
    		component.find("address2").set("v.value", "");
    	}
    	component.find("city").set("v.value", component.get("v.techAdrCity"));
    	component.find("state").set("v.value", component.get("v.techAdrState"));
    	component.find("zip").set("v.value", component.get("v.techAdrZip"));
    	//disable picklist options as the zipcode is changed
    	helper.controlZipDetails(component, true);
    	//close tech schedule confirm popup
    	component.set("v.techAdrSch", false);
		//call generate post call js controller method
		var a = component.get('c.generatePostCall');
		$A.enqueueAction(a);
    },

    technicianAdrChange: function(component, event, helper) {
    	component.find("address1").set("v.value", component.get("v.techAdr1"));
    	console.log('Adr2: ' + component.get("v.techAdr2"));
    	if (component.get("v.techAdr2") != undefined) {
    		component.find("address2").set("v.value", component.get("v.techAdr2"));
    	} else {
    		component.find("address2").set("v.value", "");
    	}
    	component.find("city").set("v.value", component.get("v.techAdrCity"));
    	component.find("state").set("v.value", component.get("v.techAdrState"));
    	component.find("zip").set("v.value", component.get("v.techAdrZip"));
    	//disable picklist options as the zipcode is changed
    	helper.controlZipDetails(component, true);
    	//close tech schedule address change popup
    	component.set("v.techAdrChange", false);
    	//close schedule address confirm popup
    	component.set("v.isAdrConfirm", false);
		//call refresh API js controller method
		component.set("v.isTechAdrChanged", true);
		var a = component.get('c.refreshAPI');
		$A.enqueueAction(a);
    },

    closeTechAdrSch: function(component, event, helper) {
        component.set("v.techAdrSch", false);
    },

    closeTechAdrChange: function(component, event, helper) {
        component.set("v.techAdrChange", false);
    },

    generatePostCall : function(component, event, helper) {
    	component.set("v.isAdrConfirm",false);
		var tmpZip = component.find("zip").get("v.value").split("-");
		var selectedZip = tmpZip[0];

        /*var isAddressChanged = false;
        if(component.find("billAddress1").get("v.value") != component.find("address1").get("v.value")){
            isAddressChanged = true;
        }

        if(component.find("billAddress2").get("v.value") != component.find("address2").get("v.value")){
            isAddressChanged = true;
        }

        if(component.find("billCity").get("v.value") != component.find("city").get("v.value")){
            isAddressChanged = true;
        }

        if(component.find("billState").get("v.value") != component.find("state").get("v.value")){
            isAddressChanged = true;
        }

        if(component.find("billZipCode").get("v.value") != component.find("zip").get("v.value")){
            isAddressChanged = true;
        }*/

            //component validation starts
            if(helper.validate(component)) {

                var SelectedAssigneeName = ' ';
                var isTechResource = 'Y';
                var RequestStatusText = 'TechScheduled';  
                var techName = ' ';
                var RequestActivityFlag = 'Y';
                var createdTime = ' ';
                var lastTime = ' ';
                var lastUserId = ' ';
                var ReasonCodetext = ' ';
                var TechnicianName = ' ';

				//var addressConfirmed = true;

                /*if(isAddressChanged === true){
                    if(window.confirm("Are you sure you want to update Shipping Address"))
                    {
                        
                    }
					else {
                    	addressConfirmed = false;
                    }
                } else {
                    console.log('shipp');
                }*/

				//address change confirmed starts
				//if(addressConfirmed === true){
	                component.get("v.ListOfEmployees").forEach(function(key){
	                    if( key.ResourceId == component.find("technician").get("v.value") ){
	                        TechnicianName = key.Name;
	                    }
	                });

	                if(component.find("address2").get("v.value")!=undefined) {
	                    var techAddr  = component.find("address1").get("v.value") +','+ component.find("address2").get("v.value") + ',' + component.find("city").get("v.value") +','+
	                        component.find("state").get("v.value") + ',' + selectedZip;
	                } else {
	                    var techAddr  = component.find("address1").get("v.value") + ',' + component.find("city").get("v.value") +','+
	                        component.find("state").get("v.value") + ',' + selectedZip;
	                }
	                component.set("v.caseObj.Technician_Address__c",  techAddr);

	                component.set("v.caseObj.Technician_Schedule_Date__c", $( "#datepicker" ).val());
	                component.set("v.caseObj.TechnicianNameScheduled__c",  TechnicianName);
	                component.set("v.caseObj.Technician_Id__c",  component.find("technician").get("v.value"));
	                component.set("v.caseObj.Technician_Company__c",  component.find("myCompany").get("v.value"));
	                component.set("v.caseObj.followup_Priority_EstimatedTime__c", component.find("myTimeEstimated").get("v.value"));

	                var caseId = component.get("v.caseId");
	                var fulfillerId = component.get("v.fulfillID");
	                var ignoreEstimatedstops = component.get("v.estimatedtimeforstopwithminutes").replace("Hour",".");

	                var datePick1 = $( "#datepicker" ).val();
	                var date = new Date(datePick1);
	                var newdate = new Date(date);
	                newdate.setDate(newdate.getDate() + 2);

	                var dd = newdate.getDate();
	                var mm = newdate.getMonth() + 1;
	                var y = newdate.getFullYear();

	                var someFormattedDate = mm + '/' + dd + '/' + y;

					var add1 = component.find("address1").get("v.value");
					var add2 = component.find("address2").get("v.value");
					if(component.get("v.salesOrderObj").phhCustomerType__c == 'AFC'){
						add1 = component.get("v.salesOrderObj").phhShipToName__c;
						add2 = component.find("address1").get("v.value");
					}

	                var ServiceTechDtovar = {
	                    ScheduledDate           : $( "#datepicker" ).val(),
	                    OpenDate                : component.find("dateopened").get("v.value"),
	                    ReopenDate              : component.find("dateopened").get("v.value"),
	                    createdTime             : component.find("dateopened").get("v.value"),
	                    lastTime                : component.find("dateopened").get("v.value"),
	                    ReopenDate              : component.find("dateopened").get("v.value"),
	                    FollowUpDate            : someFormattedDate,
	                    RequestActivityFlag     : RequestActivityFlag,
	                    lastUserId              : lastUserId,
	                    VendId                  : component.find("myCompany").get("v.value").trim(),
	                    RequestSalesOrderNumber : component.find("Salesorder").get("v.value"),
	                    Address1                : add1,
	                    Address2                : add2,
	                    CityName                : component.find("city").get("v.value"),
	                    StateCode               : component.find("state").get("v.value"),
	                    ZipCode                 : selectedZip,
	                    CreatedUserId           : '0',
	                    ServiceTechId           : component.find("technician").get("v.value"),
	                    TechName                : TechnicianName,
	                    VendName                : '',
	                    VpcName                 : '',
	                    PcKey                   : '',
	                    CustomerPhone1          : component.find("phone1").get("v.value"),
	                    CustomerPhone2          : component.find("phone2").get("v.value"),
	                    CustomerPhone3          : '',
	                    CustomerEmail           : component.find("email1").get("v.value"),
	                    EstimatedTimeForStop    : ignoreEstimatedstops,
	                    ProfitCenterCode        : component.get("v.salesOrderObj").phhProfitcenter__c,
	                    CustomerId              : component.get("v.CustId")
	                };

	                var jsonvar = {
	                    ServiceTechDto : ServiceTechDtovar,
	                    InsLineItem : component.get("v.ScheduleLIWrapObj").InsLineItemWrapList,
	                    UpdLineItem : component.get("v.ScheduleLIWrapObj").UpdLineItemWrapList,
	                    TotalLineItem : component.get("v.ScheduleLIWrapObj").TotalLI,
	                    CLIUniqueIdStr : component.get("v.ScheduleLIWrapObj").CLIUnqStr
	                };

	                console.log('jsonvar ',JSON.stringify(jsonvar));

	                var actionHTTPReq = component.get("c.POSTCallout");
	                actionHTTPReq.setParams({
	                    "recordId"   : caseId,
	                    "jsonData"   : JSON.stringify(jsonvar),
	                    "caseIs"     : component.get("v.caseObj"),
	                    "fulfillerId": fulfillerId,
	                    "sAdd1"		 : component.get("v.shipToAdd1"), 
	                    "sAdd2"		 : component.get("v.shipToAdd2"), 
	                    "sCity"		 : component.get("v.shipToCity"), 
	                    "sState"	 : component.find("state").get("v.value"), 
	                    "sZip"		 : component.get("v.shipToZip") 
	                });
	                actionHTTPReq.setCallback(this, function(response) {
	                    if (component.isValid() && response.getState() === "SUCCESS") {
	                    	console.log('response: ' + response.getReturnValue());
	                    	var res = response.getReturnValue();

	                        var errorToast = $A.get("e.force:showToast");
		                	if(res.includes("success")) {
		                		var reqNoArray = res.split('|');
		                		component.set('v.message', reqNoArray[1]);
		                		errorToast.setParams({"title": "Success!", "message": 'Technician is scheduled for this case and the Service Request: ' + reqNoArray[1], "type":"success"});
		                	} else {
		                		errorToast.setParams({"title": "Error!", "message": res, "mode": "sticky", "type":"error"});
		                	}
	                        errorToast.fire();

	                        var navEvt = $A.get("e.force:navigateToSObject");
	                        navEvt.setParams({
	                            "recordId": component.get("v.caseId"),
	                            "slideDevName": "detail"
	                        });
	                        navEvt.fire();

							//after successfull schedule close tech schedule console sub tab
							var workspaceAPI = component.find("workspace");
							console.log('workspaceAPI: ' + workspaceAPI);
							if(workspaceAPI != undefined){
						        workspaceAPI.getFocusedTabInfo().then(function(response) {
							        console.log('response: ' + JSON.stringify(response));
						            var focusedTabId = response.tabId;
						            if(response.isSubtab == true){
						            	workspaceAPI.refreshTab({
											tabId: response.parentTabId,
											includeAllSubtabs: true
										});
						            }
						            workspaceAPI.closeTab({tabId: focusedTabId});
						        })
						        .catch(function(error) {
						            console.log(error);
						        });
							}
	                    } else {
	                        var errorToast = $A.get("e.force:showToast");
	                        errorToast.setParams({"message": response.getError()[0].message, "type":"error"});
	                        errorToast.fire();
	                    }
	                });
	                $A.enqueueAction(actionHTTPReq);
				//}
				//address change confirmed  ends
            }
            //component validation ends
    },
    
    scriptsLoaded : function(component, event, helper) {
    },
    
    init : function(component, event, helper) {
        var today = new Date();
        component.set('v.today', currDate.getFullYear() + "-" + (currDate.getMonth() + 1) + "-" + currDate.getDate());
    },
    
    Next : function(component, event, helper) {
        var today = new Date(component.get('v.today'));
    },
    
    changeState : function changeState (component){ 
        component.set('v.isexpanded',!component.get('v.isexpanded'));
    },
    
    toggleShippingAddress: function(component, event, helper) {
        var checkCmp = component.find("checkbox");
        component.set("v.displayShipAddress", checkCmp.get("v.value"));
    },
    
    nullify : function(comp, ev, hel) {
        var dp = comp.find('dateopened');
        dp.set('v.value', '');
    },
    
    ONWorkingHours : function(component,event,helper) {
        var workingHours = component.get("v.workingHoursprogvalue") + '/' + component.get("v.maxHours");
        component.set("v.workingHoursProgValueNew" , workingHours);
    },

    getProgressVal :function(component,event,helper) {
        var progressVal = 0;
        component.get("v.responseObj").forEach(function(key){
            if(key.TechId == employeeTechId){
                progressVal = key.MaxHoursPerDay;
            }
        });
        component.set("v.progressValue", progressVal);
    },

    onPicklistChange: function(component, event, helper) {
        var selectedPicklist = component.find("InputAccountIndustry");
    },

    dateUpdate : function(component, event, helper) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd < 10){
            dd = '0' + dd;
        }
        if(mm < 10){
            mm = '0' + mm;
        }

        var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        if(component.get("v.myDate") != '' && component.get("v.myDate") < todayFormattedDate){
            component.set("v.dateValidationError" , true);
        }else{
            component.set("v.dateValidationError" , false);
        }
    },
    
    onChecklines: function(component,event,helper) {
        
        var selectedRec = event.getSource().get("v.value");
        var getSelectedNumber = component.get("v.selectedCount");
        if (selectedRec == true) {
            getSelectedNumber++;
        } else {
            getSelectedNumber--;
        }
        component.set("v.selectedCount", getSelectedNumber);
        
    },

    validateval  : function(component, event, helper) {
        var inp = component.get('v.minValue');
        if(inp.length > 2)
        {
            component.set('v.minValue', parseFloat(inp.substring(0, 2)));
        }
    }, 
    
    handleError: function(cmp,event,helper){
        var comp = event.getSource();
        $A.util.addClass(comp, "error");   
    },
    
    handleClearError: function(cmp,event,helper){
        var comp = event.getSource();
        $A.util.removeClass(comp, "error");   
    },

    visibleToUser : function(component, event, helper) {
        var elem = component.get("getLineItemcase");
        var selected = elem.textContent;
        resultCmp = component.find("visibleT");        
        resultCmp.set("v.value", selected);
    }
})