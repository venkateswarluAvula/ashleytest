({
	myAction : function(component, event, helper) {
		console.log('POI id -->'+component.get('v.POIIdee'));
	},
    getStatus : function(component, event, helper) {
		console.log("id-->"+component.get("v.POIIdee"));
	    var getId = component.get("v.POIIdee");
        var result = '1';
	    var isError = false;
        //----TRACKING NUMBER----START----//
        var isTracking = component.get('c.getTrackingNumber');
        isTracking.setParams({
            mynum : getId,
            FulfillerId : component.get('v.fulfillerID'),
            PoNum : component.get('v.POnumber')
        });
        isTracking.setCallback(this,function(res){
            console.log('selectedItems::::res.getReturnValue-->',+res.getReturnValue());
            result = res.getReturnValue();
            console.log('isSavemySN--->',+result);
            
            if(result== 1)
            {
                component.set("v.isUTSuccess", true);
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.isUTSuccess", false);
                    }), 3000
                );
              //  $A.get('e.force:refreshView').fire();
                location.reload();
                console.log('CHANGES MADE');
                
            }
            else
            {
                component.set("v.isUTFail", true);
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.isUTFail", false);
                    }), 3000
                );
                console.log('----NO CHANGE----');
            }
            var retValue = getId;
            console.log("Record-->"+retValue);
        }); 
        
        $A.enqueueAction(isTracking);
       
        //----TRACKING NUMBER----END----//   
     }
})