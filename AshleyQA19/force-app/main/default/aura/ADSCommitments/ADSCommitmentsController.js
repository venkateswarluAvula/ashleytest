({ 
    doInit: function(component, event, helper) { 
        //console.log('doInit');
        var recId = component.get("v.recordId");
        //var recId = 'a0p4F000001qraxQAA';
        if (recId) { 
            component.set("v.modalContext", "Edit");
            component.find('forceEditRecordSegment').reloadRecord(true);   
            
            window.setTimeout(
                $A.getCallback(function() {
                    helper.CommitmentCalculate(component, event, helper);
                    
                    var statusvalue = component.find("Status").get("v.value");
        			
                    if(statusvalue == "Complete"){
                        component.set("v.IsEnable", true);
                    }
                    
                }), 1500
            );
        }
        
        component.find("Status").set("v.value", 'Working');
        
        
        
    }, 
    
    handleOnChange: function(component, event, helper) {
        var originVal = component.get("v.NewCommitment.Origin_Market__c");
        var DestinationVal = component.get("v.NewCommitment.Destination_Market__c");
        
        if(originVal != '' && DestinationVal != ''){
            helper.CommitmentCalculate(component, event, helper);
            
            // validation rules for on-changes
            if(originVal == DestinationVal){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({"message": 'Origin and Destination should not be same.',"type": "Error"});
                toastEvent.fire();
                return; 
            }
        }
        
    },    
    
    CommitmentCreation : function(component, event, helper) {  
        
        var trps = component.find("trips").get("v.value");
        var oppamt = component.find("oppamt").get("v.value");
        var mode = component.get("v.modalContext");
        
        if (mode == 'New') {   
            var NewCommitment = component.get("v.NewCommitment");
            
            //Validation
            
            if($A.util.isEmpty(NewCommitment.Origin_Market__c) || $A.util.isUndefined(NewCommitment.Origin_Market__c)){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Origin can not be empty.',
                    "type": "Error"
                });
                toastEvent.fire();
                return;
            }
            if($A.util.isEmpty(NewCommitment.Destination_Market__c) || $A.util.isUndefined(NewCommitment.Destination_Market__c)){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Destination can not be empty.',
                    "type": "Error"
                });
                toastEvent.fire();
                return; 
            }
            
            if(NewCommitment.Origin_Market__c == NewCommitment.Destination_Market__c){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Origin and Destination should not be same.',
                    "type": "Error"
                });
                toastEvent.fire();
                return; 
            }
            
            if($A.util.isEmpty(NewCommitment.Number_Trips_Required__c) || $A.util.isUndefined(NewCommitment.Number_Trips_Required__c)){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Trips can not be empty.',
                    "type": "Error"
                });
                toastEvent.fire();
                return; 
            }
            if(trps < 0){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({"message": 'Trips can not be Negative values.', "type": "Error"});
                toastEvent.fire();
                return; 
            }
            if(oppamt < 0){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({"message": 'Amount can not be Negative values.', "type": "Error"});
                toastEvent.fire();
                return; 
            }
            var Gdate = component.find("GoalDate").get("v.value");
            if( Gdate < $A.localizationService.formatDate(new Date(), "YYYY-MM-DD")){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({"message": 'Date must be future date.', "type": "Error"});
                toastEvent.fire();
                return; 
            }
            
            // -----action for the creating commitment records-------
            var action = component.get("c.createRecord");  
            action.setParams({
                NewCommitment : NewCommitment
            });
            
            action.setCallback(this,function(response){  
                
                var state = response.getState();
                if(state === "SUCCESS"){
                    var recId = response.getReturnValue();
                    //alert('response sucess message-'+response.successMessages);
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recId.Id, 
                        "slideDevName": "detail"
                    });
                    navEvt.fire(); 
                    $A.get('e.force:refreshView').fire();
                    
                    var successToast = $A.get("e.force:showToast");
                    successToast.setParams({"message": "The record is created successfully.", "type":"success", "mode":"dismissible", "duration":5000});
                    successToast.fire();
                    
                } else if(state == "ERROR"){
                    
                    //alert('Error in calling server side action'); 
                }
            });
            // -------- action end --------------------------
            
            
            
        }else if(mode == 'Edit') {
            
            console.log('mode---edit-'+mode);
            var NewCommitment = component.get("v.NewCommitment");
            console.log('NewCommitment---edit-'+NewCommitment);
            
            //Validation rules while editing
            
            /*var Gdate = component.find("GoalDate").get("v.value");
            if( Gdate < $A.localizationService.formatDate(new Date(), "YYYY-MM-DD")){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({"message": 'Date must be future date.', "type": "Error"});
                toastEvent.fire();
                return; 
            }*/
            if($A.util.isEmpty(NewCommitment.Origin_Market__c) || $A.util.isUndefined(NewCommitment.Origin_Market__c)){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Origin can not be empty.',
                    "type": "Error"
                });
                toastEvent.fire();
                return;
            }  
            
            if($A.util.isEmpty(NewCommitment.Destination_Market__c) || $A.util.isUndefined(NewCommitment.Destination_Market__c)){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Destination can not be empty.',
                    "type": "Error"
                });
                toastEvent.fire();
                return; 
            }
            
            if(NewCommitment.Destination_Market__c == NewCommitment.Origin_Market__c){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Origin and Destination should not be same.',
                    "type": "Error"
                });
                toastEvent.fire();
                return; 
            }
            if($A.util.isEmpty(NewCommitment.Number_Trips_Required__c) || $A.util.isUndefined(NewCommitment.Number_Trips_Required__c)){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Trips can not be empty.',
                    "type": "Error"
                });
                toastEvent.fire();
                return; 
            }
            if(trps < 0){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({"message": 'Trips can not be Negative values.', "type": "Error"});
                toastEvent.fire();
                return; 
            }
            if(oppamt < 0){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({"message": 'Amount can not be Negative values.', "type": "Error"});
                toastEvent.fire();
                return; 
            }
            
            //alert('new---edit-alert-'+JSON.stringify(NewCommitment));
            
            
            var segUpd = JSON.stringify(NewCommitment); 
            var action = component.get("c.updtRecord");
            action.setParams({updCommitment : segUpd});   
            
            action.setCallback(this,function(response){
                var state = response.getState();
                console.log('update-resp---'+JSON.stringify(response.getReturnValue().Status__c));
                if(state === "SUCCESS"){
                    var recrdId = response.getReturnValue();
                    //alert('recrdId-----'+recrdId.Id);   
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recrdId.Id,
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
                    $A.get('e.force:refreshView').fire();
                    
                    var successToast = $A.get("e.force:showToast");
                    successToast.setParams({"message": "The record has been updated successfully.", "type":"success", "mode":"dismissible", "duration":5000});
                    successToast.fire();
                    
                } else if(state == "ERROR"){
                    //alert('Error in calling server side action');
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({"message": 'Error in calling server side action.', "type": "Error"});
                    toastEvent.fire();
                    return; 
                }
                
            }); 
            
        }
        
        $A.enqueueAction(action);
        
        // -----creating records for commitment opportunity-------
        helper.Commopp(component, event, helper);
        
        
    },
    cancelSegment : function(component, event, helper) { 
        var recId = component.get("v.recordId");
        var mode = component.get("v.modalContext");
        var urlEvent = $A.get("e.force:navigateToURL");
        
        if(mode == 'New') {
            urlEvent.setParams({
                "url": "/lightning/o/Commitment__c/list?filterName=All"
            });
        } else if(mode == 'Edit') {
            urlEvent.setParams({
                "url": "/lightning/r/"+recId+"/view"
            });
        }
        
        urlEvent.fire(); 
        
    },
    
    CommitmentCalculate: function(component, event, helper) { 
        helper.CommitmentCalculate(component, event, helper);
        
    }
    
    
})