({
	CommitmentCalculate : function(component, event, helper) {
		//alert('adsc helper');
        var NewCommitment = component.get("v.NewCommitment");
        console.log('NewCommitment helper--'+JSON.stringify(NewCommitment));
        
        var originVal = component.get("v.NewCommitment.Origin_Market__c");
        var DestinationVal = component.get("v.NewCommitment.Destination_Market__c");
        console.log('originValhelper--'+originVal);
        //console.log('originVal0--'+component.find("addTypes").get("v.value"));
        console.log('DsestinationVal--'+DestinationVal);
        
        var action = component.get("c.CalculateValues");
        action.setParams({originVal : originVal,DestinationVal:DestinationVal });   
        
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log('state---'+state);
            if(state === "SUCCESS"){
                var callResponse = response.getReturnValue();
                console.log('callResponse--'+JSON.stringify(callResponse));
                //component.set("v.data", callResponse);
                
                var compl = 0;
                var wonrows = [];
                var rows = [];
            	var eachRow ;
            	var rowjson;
                var getoppcount = component.get("v.oppCount");
                var getwoncount = component.get("v.wonCount");
                
                //---------looping values for won & others-----------
                if(callResponse.length != '0'){
                    for(var i=0;i<callResponse.length;i++){
                        var AccName = callResponse[i].AccountName;
                        var opid = callResponse[i].OppId;
                        var lneName = callResponse[i].laneNames;
                        var oppnam = callResponse[i].oppname;
                        var oppstg = callResponse[i].oppstage;
                        var trips = callResponse[i].trps;
                        var count = callResponse[i].cnt;
                        
                        eachRow = {
                            AccountName:AccName,
                            OppId:opid,
                            laneNames:lneName,
                            oppname:oppnam,
                            oppstage:oppstg,
                            trps:trips,
                            cnt:count
                        }
                        rowjson = JSON.stringify(eachRow);
                        
                        if(callResponse[i].oppstage == 'Closed Won'){
                            wonrows.push(eachRow);
                            component.set('v.wondata',wonrows);
                            getwoncount++
                            //console.log('length--'+getwoncount);
                            //-------- trip count calculation -------
                            var tripcount = callResponse[i].trps;
                            if($A.util.isUndefined(tripcount)){
                                tripcount = 0;
                            }
                            compl = compl + tripcount;
                            
                        }else{
                            rows.push(eachRow);
                            component.set('v.data',rows);
                            getoppcount ++;
                            console.log('length2--'+getoppcount);
                            //-------- trip count calculation -------
                            if($A.util.isUndefined(tripcount)){
                                tripcount = 0;
                            }
                            compl = compl + tripcount;
                        }
                        console.log('oppcount--'+getoppcount);
                        console.log('won1--'+rowjson);
                        console.log('getwoncount--'+getwoncount);
                        
                        component.find("InOpp").set("v.value", getoppcount);
                    	component.find("NoOpp").set("v.value", getwoncount);
                    }
                    
                }
                
                console.log('compl---'+compl);
                
                var TripsVal = component.get("v.NewCommitment.Number_Trips_Required__c");
                var CalculateComplete = (compl/TripsVal) *100;
                //alert('CalculateComplete---'+CalculateComplete);
                console.log('complete---'+component.find("complete").get("v.value"));
                console.log('trips---'+component.find("trips").get("v.value"));
				//component.find("complete").set("v.value", CalculateComplete);                
                if($A.util.isUndefined(component.find("trips").get("v.value")) || $A.util.isEmpty(component.find("trips").get("v.value")) || component.find("trips").get("v.value") == 0 ){
                	component.find("complete").set("v.value", 0);
                } else{
                    component.find("complete").set("v.value", CalculateComplete);
                }
                
                //var resplength = callResponse.length;
                //var op = component.find("NoOpp").set("v.value", resplength);
                //console.log('op---'+resplength);
                
                
            } else if(state == "ERROR"){
                //alert('Error in calling server side action');
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({"message": 'Error in calling server side action.', "type": "Error"});
                toastEvent.fire();
                return; 
            }
            
        });
        $A.enqueueAction(action);
                
	},
    
    Commopp : function(component, event, helper) {
        //var NewCommitment = component.get("v.NewCommitment");
        var originVal = component.get("v.NewCommitment.Origin_Market__c");
        var DestinationVal = component.get("v.NewCommitment.Destination_Market__c");
        console.log('originValsave--'+originVal);
        var recId = component.get("v.recordId");
        //alert('save recId--'+recId);
        var action = component.get("c.CalculateValues");
        action.setParams({originVal : originVal,DestinationVal:DestinationVal, recId:recId }); 
        
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log('state---'+state);
            var callResponse1 = response.getReturnValue();
            console.log('callResponse1--'+callResponse1.length);
            
            if(state === "SUCCESS" && callResponse1.length > 0 ){
                var successToast = $A.get("e.force:showToast");
                successToast.setParams({"message": "The Commitment opportunity has been updated successfully.", "type":"success", "mode":"dismissible", "duration":5000});
                successToast.fire();
            } else if(state == "ERROR"){
                //alert('Error in calling server side action');
                var successToast = $A.get("e.force:showToast");
                successToast.setParams({"message": "Error in calling server side action.", "type":"Error", "mode":"dismissible", "duration":5000});
                successToast.fire();
            }
            
        });
        $A.enqueueAction(action);
    }
    
    
        
})