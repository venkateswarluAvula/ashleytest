({
    doInit : function(component, event, helper) {
        
        var Recid = component.get("v.recordId");
        var action = component.get("c.getComOpp");
        action.setParams({
            "COId" : Recid
        });
        
        action.setCallback(this,function(response) {
            component.find("Id_spinner").set("v.class" , 'slds-hide');
            var state = response.getState();
            if (state === "SUCCESS") {
                var succesData = response.getReturnValue();
                console.log('succesData--rel--'+JSON.stringify(succesData));
                //
                var wonrows = [];
                var rows = [];
                var eachRow ;
                
                for(var i=0;i<succesData.length;i++){
                    //console.log('succesData--rel--'+JSON.stringify(succesData));
                    
                    var RId = succesData[i].Id;
                    var nam = succesData[i].Name;
                    console.log('nam--'+nam);
                    var opp = JSON.stringify(succesData[i].Opportunity__r.Name);
                    opp = opp.replace('\"', '');
                    opp = opp.replace('\"', '');
                    var acc = succesData[i].Account__c;
                    var status = succesData[i].Status__c;
                    var trp = succesData[i].Trips_Per_Week__c;
                    console.log('opp--'+opp);
                    
                    eachRow = {
                        Id:RId,
                        Name:nam,
                        Opportunity__c:opp,
                        Account__c:acc,
                        Status__c:status,
                        Trips_Per_Week__c:trp
                    }
                    
                    if(succesData[i].Status__c == 'Closed Won'){
                        wonrows.push(eachRow);
                        component.set('v.wondata',wonrows);
                        component.set("v.datalenght1",wonrows.length);
                        console.log('wonrows--'+wonrows);
                    }else{
                        rows.push(eachRow);
                        component.set("v.data", rows);
                        component.set("v.datalenght2",rows.length);
                    }
                    
                }
                component.set("v.alldata", succesData);
                
                
            }
        }); 
        $A.enqueueAction(action);
        
    },
    
    openRelatedList : function(component, event, helper) {
        var relatedListEvent = $A.get("e.force:navigateToRelatedList");
        //var recid = "5000n00000637qbAAA";
        var recid = component.get("v.recordId");
        //alert('recid2--'+component.get("v.recordId"));
        relatedListEvent.setParams({
            "relatedListId": "Commitment_Opportunities__r",
            "parentRecordId": recid
        });
        relatedListEvent.fire();
    },
    
    closePop : function(component, event, helper) {
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
        // var selectedItem = event.currentTarget;
        // var Id = selectedItem.dataset.record;
        
    },
    
    openPop : function(component, event, helper) {
        
        var cmpTarget = component.find('pop');
        
        $A.util.addClass(cmpTarget, 'slds-show');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;
        console.log('id--'+id_str)
        component.set('v.PopupsoId',id_str);   
        var actiondata = component.get("c.getComOppVal");
        
        actiondata.setParams({
            "recid" : id_str
        });
        
        actiondata.setCallback(this, function(response){    
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var callResponse = response.getReturnValue();
                console.log('callResponse'+JSON.stringify(callResponse));
                component.set("v.COData", callResponse);
            }
            else{
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": response.getError()[0].message, "type":"error", "mode":"dismissible", "duration":4000});
                errorToast.fire();
            }
        });
        
        $A.enqueueAction(actiondata);
        
    },
})