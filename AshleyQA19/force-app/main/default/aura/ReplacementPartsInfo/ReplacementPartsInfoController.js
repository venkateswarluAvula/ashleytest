({
    myAction : function(component, event, helper) {
        console.log('record id -->'+component.get('v.recordId'));
        var action = component.get('c.getPOI');
        action.setParams({
            "PLIid" : component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.POIList", response.getReturnValue());
                component.set("v.isPOI", true);
                if(response.getReturnValue() != null && response.getReturnValue() != ''){
                    console.log('POIList-->'+JSON.stringify(component.get('v.POIList')));
                    var isRP = JSON.stringify(component.get('v.POIList[0].Replacement_Part_Item_Number__c'));
                    console.log('isRP-->'+isRP);
                    if(isRP != '' && isRP != undefined){
                        component.set("v.isRPNum", true);
                        console.log('isRPNum value-->'+(component.get('v.isRPNum')));
                        component.set("v.isPartNum", false);
                    }
                }
                else{
                    component.set("v.isRPNum", true);
                console.log('isRPNum value-->'+(component.get('v.isRPNum')));
                component.set("v.isPartNum", false);
                }
            } else {
                component.set("v.isRPNum", true);
                console.log('isRPNum value-->'+(component.get('v.isRPNum')));
                component.set("v.isPartNum", false);
                console.log('failed');
            }
        });
        $A.enqueueAction(action);
        
    },
    getStatus : function(component, event, helper) {
		console.log("id-->"+component.find("UpdatePI").get("v.value"));
	    var getId = component.find("UpdatePI").get("v.value");
        var result = '1';
	    var isError = false;
        //----TRACKING NUMBER----START----//
        var isTracking = component.get('c.getTrackingNumber');
        isTracking.setParams({
            mynum : getId,
            FulfillerId : component.find('fulfillerID').get('v.value'),
            PoNum : component.find('POnumber').get('v.value'),
        });
        isTracking.setCallback(this,function(res){
            console.log('selectedItems::::res.getReturnValue-->',+res.getReturnValue());
            result = res.getReturnValue();
            console.log('isSavemySN--->',+result);
            if(result== '1')
            {
                console.log('CHANGES MADE');
            }
            else
            {
                console.log('----NO CHANGE----');
            }
            var retValue = getId;
            console.log("Record-->"+retValue);
        }); 
        
        $A.enqueueAction(isTracking);
       
        //----TRACKING NUMBER----END----//   
     },
    callSaveComp : function(component, event, helper){
        var evt = $A.get("e.force:navigateToComponent");
        console.log('Event '+evt);
        var recID = component.get("v.recordId");
        evt.setParams({
            componentDef  : "c:PartInfo" ,
            componentAttributes : {
                PLIId : recID
            }
        });
        evt.fire();
    },
    NewPart : function(component,event,helper){
        var recId = component.get('v.recordId');
        var action = component.get('c.NewPartInsert');
        action.setParams({
            "PLIid" : recId
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var result = response.getReturnValue();
                console.log('Inserted');
                component.set("v.new_part", response.getReturnValue());
                if(result== '1')
                {
                    console.log('CHANGES MADE');
                    component.set("v.isSuccess", true);
                    window.setTimeout(
                        $A.getCallback(function() {
                            component.set("v.isSuccess", false);
                        }), 3000
                    );
                }
			} else {
                console.log('failed');
            }
         	window.location.reload();
        });
        $A.enqueueAction(action);
    },
    TrackingUrl : function(component,event,helper){
        var selectedItem = event.currentTarget; // Get the target object
        console.log('selectedItem-->'+selectedItem);
        var index = selectedItem.dataset.index; // Get its value i.e. the index
        console.log('index-->'+index);
        var trackingId = component.get("v.POIList")[index].Part_Order_Tracking_Number__c; 
        console.log('trackingId-->'+trackingId);
        console.log('trackingId-->'+component.get("v.POIList")[index].Part_Order_Tracking_Number__c);
        var valueLink;
        if(trackingId.startsWith('1Z')){
            valueLink = $A.get("$Label.c.UPSurl")+trackingId+'&requester=UPSHome&agreeTerms=yes/trackdetails';
        } else if(trackingId.startsWith('927')){
            valueLink = $A.get("$Label.c.USPSurl")+trackingId+'%2C';
        } else if(trackingId.startsWith('4')){
            valueLink = $A.get("$Label.c.FedExURL")+trackingId+'&locale=en_US&cntry_code=us';
        } else{
            valueLink = $A.get("$Label.c.UPSurl")+trackingId+'&requester=UPSHome&agreeTerms=yes/trackdetails';
        }
		console.log('valueLink-->'+valueLink);
        window.open(valueLink,'_blank');
           
    },
    AshleyDirectLink : function(component,event,helper){
        var rec = component.get('v.recordId');
        var action = component.get('c.OpenAshleyDirect');
        action.setParams({
            "ProductLineItemId" : rec
        });
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                console.log('AshleyDirect Link-->' + JSON.stringify(response.getReturnValue()));
                var valueLink = response.getReturnValue();
                console.log('Link-->'+valueLink);
                window.open(valueLink,'_blank');
            } else {
                console.log('failed');
            }
        });
        $A.enqueueAction(action);
    }
})