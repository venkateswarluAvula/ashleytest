({
    doInit: function (component, event, helper) {
        var pageRef = component.get("v.pageReference");
        console.log('pageRef: ' + JSON.stringify(pageRef));
        var parentId;
        if (pageRef != null) {
            var base64Context = pageRef.state.inContextOfRef;
            console.log('base64Context: ' + base64Context);
            if (base64Context != undefined) {
                if (base64Context.startsWith("1\.")) {
                    base64Context = base64Context.substring(2);
                }
                var addressableContext = JSON.parse(window.atob(base64Context));
                console.log('addressableContext: ' + JSON.stringify(addressableContext));
                if (addressableContext != null) {
                    parentId = addressableContext.attributes.recordId;
                }
            }
        }
        console.log('parentId: ' + parentId);
        component.set("v.parentId", parentId);
        
        var recordid = component.get("v.recordId");
        console.log('recordid: ' + recordid);
        if (recordid != null) {
            component.set("v.isEdit", true);
        }
        
        //get address detail starts
        var actionAdr = component.get("c.getAddress");
        actionAdr.setParams({
            "adrId" : recordid
        });
        actionAdr.setCallback(this, function(responseAdr) {
            if (component.isValid() && responseAdr.getState() === "SUCCESS") {
                console.log('adr detail: ' + JSON.stringify(responseAdr.getReturnValue()));
                if (responseAdr.getReturnValue() != null) {
                    component.set("v.newAddress", responseAdr.getReturnValue());
                    console.log(responseAdr.getReturnValue().AccountId__c);
                    parentId = responseAdr.getReturnValue().AccountId__c;
                    
                    var oldPrefer = responseAdr.getReturnValue().Preferred__c;
                    console.log('preferredstatus'+oldPrefer);
                    component.set("v.oldPreferred", oldPrefer);
                    
                    component.set("v.parentId", parentId);
                    helper.getAccountInfo(component);
                }
            }
            else {
                var errorToast = $A.get("e.force:showToast");
                errorToast.setParams({"message": responseAdr.getError()[0].message, "type":"error"});
                errorToast.fire();
            }
        });
        $A.enqueueAction(actionAdr);
        //get address detail ends
        
        helper.getAccountInfo(component);
    },
    changeAcc: function (component, event, helper) {
        component.set("v.hideAccLookup", false);
    },
    doCancel: function (component, event, helper) {
        var cmpTargetForEdit = component.find('modalIdForAddressCreation');
        var cmpBackDropForEdit = component.find('backdropIdForCreationAddress');
        $A.util.removeClass(cmpBackDropForEdit, 'slds-backdrop--open');
        $A.util.removeClass(cmpTargetForEdit, 'slds-fade-in-open');
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function (response) {
            var focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        })
        .catch(function (error) {
            console.log(error);
        });
    },
    insertAdr: function (component, event, helper) {
        var acc;
        if (!helper.isNullOrEmpty(component.get("v.newAddress.AccountId__c"))) {
            acc = component.find("account-lookup").get("v.value");
        } else if (!helper.isNullOrEmpty(component.get("v.parentId"))) {
            acc = component.get("v.parentId");
        }
        var newAddress = component.get("v.newAddress");
        console.log('Save Address: ' + JSON.stringify(newAddress));
        helper.saveAdr(component, newAddress, acc, 'insert', null, null, null, null);
    },
    insertAdrSchTech: function (component, event, helper) {
        var acc;
        if (!helper.isNullOrEmpty(component.get("v.newAddress.AccountId__c"))) {
            acc = component.find("account-lookup").get("v.value");
        } else if (!helper.isNullOrEmpty(component.get("v.parentId"))) {
            acc = component.get("v.parentId");
        }
        var newAddress = component.get("v.newAddress");
        var caseDetail = component.get("v.caseList");
        console.log('Sch Address: ' + JSON.stringify(newAddress));
        console.log('Sch Case: ' + JSON.stringify(caseDetail));
        console.log('Sekar: ' + JSON.stringify(caseDetail[0]));
        helper.saveAdr(component, newAddress, acc, 'insert', 'schTech', caseDetail[0].Id, caseDetail[0].Sales_Order__c, caseDetail[0].Legacy_Service_Request_ID__c);
    },
    insertAdrUnSchTech: function (component, event, helper) {
        var acc;
        if (!helper.isNullOrEmpty(component.get("v.newAddress.AccountId__c"))) {
            acc = component.find("account-lookup").get("v.value");
        } else if (!helper.isNullOrEmpty(component.get("v.parentId"))) {
            acc = component.get("v.parentId");
        }
        var newAddress = component.get("v.newAddress");
        var caseDetail = component.get("v.caseList");
        console.log('UnSch Address: ' + JSON.stringify(newAddress));
        console.log('UnSch Case: ' + JSON.stringify(caseDetail));
        helper.saveAdr(component, newAddress, acc, 'insert', 'unSchTech', caseDetail[0].Id, caseDetail[0].Sales_Order__c, caseDetail[0].Legacy_Service_Request_ID__c);
    },
    clickCreate: function (component, event, helper) {
        //account-lookup, addType, addLine1, addLine2, addCity, addState, addZip
        //addValStatus, addPreffer, addGeo
        var newPrefer = component.get("v.newAddress.Preferred__c");
        var oldPrefer = component.get("v.oldPreferred");
        console.log('oldPrefer-->'+oldPrefer +' newPrefer' + newPrefer);

        var acc;
        if (!helper.isNullOrEmpty(component.get("v.newAddress.AccountId__c"))) {
            acc = component.find("account-lookup").get("v.value");
        } else if (!helper.isNullOrEmpty(component.get("v.parentId"))) {
            acc = component.get("v.parentId");
        }

        console.log('alookup: ' + component.get("v.newAddress.AccountId__c"));
        console.log('acc: ' + acc);
        console.log('type: ' + component.get("v.newAddress.Address_Type__c"));
        console.log('line1: ' + component.get("v.newAddress.Address_Line_1__c"));
        console.log('city: ' + component.get("v.newAddress.City__c"));
        console.log('state: ' + component.get("v.newAddress.StateList__c"));
        console.log('zip: ' + component.get("v.newAddress.Zip_Code__c"));
        console.log('vstatus: ' + component.get("v.newAddress.Address_Validation_Status__c"));
        
        if(
            helper.isNullOrEmpty(acc) ||
            helper.isNullOrEmpty(component.get("v.newAddress.Address_Type__c")) ||
            helper.isNullOrEmpty(component.get("v.newAddress.Address_Line_1__c")) ||
            helper.isNullOrEmpty(component.get("v.newAddress.City__c")) ||
            helper.isNullOrEmpty(component.get("v.newAddress.StateList__c")) ||
            helper.isNullOrEmpty(component.get("v.newAddress.Zip_Code__c"))
        ) {
            var errorToast = $A.get("e.force:showToast");
            errorToast.setParams({"message": "Please enter the values in all mandatory fields.", "type":"error"});
            errorToast.fire();
        } else if ((oldPrefer == true) && (newPrefer == false)) {
            var errorToast = $A.get("e.force:showToast");
            errorToast.setParams({"message": "Every Person Account Must have a Preferred Address. To uncheck this address as preferred, please mark a different existing address as preferred or create a new address and mark that address as preferred", "type":"error"});
            errorToast.fire();
        } else {
            var newAddress = component.get("v.newAddress");
            console.log("Create Address: " + JSON.stringify(newAddress));
                
            //save address detail starts
            var actionSaveAdr = component.get("c.validateAddress");
            actionSaveAdr.setParams({
                "Address": newAddress,
                "accId": acc
            });
            actionSaveAdr.setCallback(this, function(responseSaveAdr) {
                console.log(responseSaveAdr.getState());
                if (component.isValid() && responseSaveAdr.getState() === "SUCCESS") {
                    console.log('response: ' + JSON.stringify(responseSaveAdr.getReturnValue()));

                    var vResp = responseSaveAdr.getReturnValue();
                    if (vResp != null) {
                        if (vResp.vCaseList != null) {
                            component.set("v.caseList", vResp.vCaseList);
                        }
                            
                        component.set("v.techAvailable", false);
                        component.set("v.techNotAvailable", false);
                        component.set("v.moreSchedule", false);
                        if (vResp.vMessage != null) {
                            if (vResp.vMessage == 'duplicate_address') {
                                var errorToast = $A.get("e.force:showToast");
                                errorToast.setParams({"message": "Address is available for this customer, please try with another address", "type":"error"});
                                errorToast.fire();
                            } else if (vResp.vMessage == 'tech_available') {
                                component.set("v.techAvailable", true);
                            } else if (vResp.vMessage == 'tech_not_available') {
                                component.set("v.techNotAvailable", true);
                            } else if (vResp.vMessage == 'more_schedule') {
                                component.set("v.moreSchedule", true);
                            } else if (vResp.vMessage == 'other_cases') {
                                //insert address
                                helper.saveAdr(component, newAddress, acc, 'insert', null, null, null, null);
                            }
                        } else {
                            var dmlTpye;
                            if (component.get("v.isEdit")) {
                                //update
                                dmlTpye = 'update';
                            } else {
                                //insert
                                dmlTpye = 'insert';
                            }

                            helper.saveAdr(component, newAddress, acc, dmlTpye, null, null, null, null);
                        }
                    }
                        
                    /*
               		if (res != null) {
	               		var reqNoArray = res.split('|');

						if (reqNoArray[0] == 'error') {

	                		if (reqNoArray[1] == 'duplicate_address') {
	                			var errorToast = $A.get("e.force:showToast");
	                			errorToast.setParams({"message": "Address is available for this customer, please try with another address", "type":"error"});
	                			errorToast.fire();
	                		} else if (reqNoArray[1] == 'tech_available') {
	                			// confirmation box
	                		} else if (reqNoArray[1] == 'tech_not_available') {
	                		} else if (reqNoArray[1] == 'more_schedule') {
	                		}

						} else {
							// call saveAddress method
						}

						if(res.includes("success")) {
		                    var toastEvent = $A.get("e.force:showToast");
		                    toastEvent.setParams({
		                        "title": "Success!",
		                        "type": "success",
		                        "message": "Address record has been successfully saved."
		                        
		                    });
		                    toastEvent.fire();
                    
		                    var workspaceAPI = component.find("workspace");
		                    workspaceAPI.getFocusedTabInfo().then(function (response) {
		                        var focusedTabId = response.tabId;
		                        workspaceAPI.closeTab({tabId: focusedTabId});
		                    })
		                    .catch(function (error) {
		                        console.log(error);
		                    });

							var refreshId = component.get("v.newAddress.AccountId__c");
							if (reqNoArray[1] != null) {
								refreshId = reqNoArray[1];
							}
		                    var navEvt = $A.get("e.force:navigateToSObject");
		                    navEvt.setParams({
		                        "recordId": refreshId,
		                        "slideDevName": "detail"
		                    });
		                    navEvt.fire();
						}
					} else {
		                var errorToast = $A.get("e.force:showToast");
		                errorToast.setParams({"message": "Error while saving address, please contact administrator", "type":"error"});
	    	            errorToast.fire();
					}
					*/
                } else {
                    var errorToast = $A.get("e.force:showToast");
                    errorToast.setParams({"message": responseSaveAdr.getError()[0].message, "type":"error"});
                    errorToast.fire();
                }
            });
            $A.enqueueAction(actionSaveAdr);
            //save address detail ends

            /*
            var actiondata = component.get("c.saveAddress");
            actiondata.setParams({
                "Address": newAddress
            });
            actiondata.setCallback(this, function (response) {
                var resState = response.getState();
                //alert('resState---- '+ resState);
                if (resState === "SUCCESS") {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Address record has been successfully added."
                        
                    });
                    toastEvent.fire();
                    
                    var workspaceAPI = component.find("workspace");
                    workspaceAPI.getFocusedTabInfo().then(function (response) {
                        var focusedTabId = response.tabId;
                        workspaceAPI.closeTab({tabId: focusedTabId});
                    })
                    .catch(function (error) {
                        console.log(error);
                    });

                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": component.get("v.newAddress.AccountId__c"),
                        "slideDevName": "related"
                    });
                    navEvt.fire();
                }
                else{
                    var toastParams = {
                        title: "Error",
                        message: "Unknown error", // Default error message
                        type: "error"
                    };
                    var errors=response.getError();
                    if (errors && Array.isArray(errors) && errors.length > 0) {
                        toastParams.message = 'Address Already Exists For This Customer';
                    }
                    // Fire error toast
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                } 
            });
            $A.enqueueAction(actiondata);
            */
        }
    },
    // EDQ added Validation logic
    handleSearchChange: function (component, event, helper) {
        var settings = helper.getSettings();        
        helper.handleSearchChange(component, event, settings);
        helper.handleValidationStatus(component, settings);       
    },
    // EDQ added Validation logic
    handleSuggestionNavigation: function (component, event, helper) {
        var settings = helper.getSettings();
        var keyCode = event.which;
        if (keyCode == 13) { // Enter
            helper.acceptFirstSuggestion(component, settings);
        } else if (keyCode == 40) { //Arrow down
            helper.selectNextSuggestion(component, null, settings);
        } else if (keyCode == 38) { //Arrow up
            helper.hideAndRemoveSuggestions(component, settings);
        }
    },
    // EDQ event listener
    onSuggestionKeyUp: function (component, event, helper) {
        var settings = helper.getSettings();
        helper.onSuggestionKeyUp(component, event, settings);
    },
    // EDQ event listener
    handleResultSelect: function (component, event, helper) {
        var settings = helper.getSettings();
        helper.handleResultSelect(component, event, settings);
    },
    // EDQ event listener
    onAddressChanged: function (component, event, helper) {
        var settings = helper.getSettings();
        helper.handleValidationStatus(component, settings);
    },
    // EDQ event listener
    onElementFocusedOut: function (component, event, helper) {
        var settings = helper.getSettings();
        
        var useHasNotSelectedASuggestion = false;
        if (helper.isNull(event.relatedTarget)) {
            useHasNotSelectedASuggestion = true;
        } else if (helper.isNull(event.relatedTarget.id)) {
            useHasNotSelectedASuggestion = true;
        } else if (event.relatedTarget.id.indexOf(settings.suggestionIndexClassPrefix) === -1) {
            useHasNotSelectedASuggestion = true;
        }
        
        if (useHasNotSelectedASuggestion) {
            helper.hideAndRemoveSuggestions(component, settings);
        }
    }
})