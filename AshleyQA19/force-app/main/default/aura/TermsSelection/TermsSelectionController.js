({
	doInit : function(component, event, helper) {
        helper.getPaymentTermOptions(component, helper);       
	},
    closeDialog : function(component, helper) {
        component.getEvent("NotifyParentCloseModal").fire();
    },
    // finance API defect fix
    setThirdPartyTermsCode : function(component, helper) {
    	var fTerms = component.get("v.termOptions");
    	for(var i=0; i<fTerms.length; i++){
    		if(fTerms[i].Code == component.get("v.paymentInfo.FINterms")){
    			component.set("v.paymentInfo.FINThirdPartyTerms", fTerms[i].ThirdPartyTermCode);
    		}
    	}
    }
})