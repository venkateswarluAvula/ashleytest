({ 
    doInit: function(component, event, helper) { 
        
        var recId = component.get("v.recordId");
        
        if (recId) { 
            component.set("v.modalContext", "Edit");
            component.find('forceEditRecordSegment').reloadRecord(true);
        }
        
        component.find("seq").set("v.value", 0);
    }, 
    
    SegmentCreation : function(component, event, helper) {  
        
        var mode = component.get("v.modalContext");
        if (mode == 'New') {   
            var newSegment = component.get("v.newSegment");
            
            //Validation --- Start For New
            if($A.util.isEmpty(newSegment.Origin__c) || $A.util.isUndefined(newSegment.Origin__c)){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Origin can not be empty.',
                    "type": "Error"
                });
                toastEvent.fire();
                return;
            }            
            if($A.util.isEmpty(newSegment.Destination__c) || $A.util.isUndefined(newSegment.Destination__c)){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Destination can not be empty.',
                    "type": "Error"
                });
                toastEvent.fire();
                return; 
            }
            if($A.util.isEmpty(newSegment.Stop__c) || $A.util.isUndefined(newSegment.Stop__c)){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Stop can not be empty.',
                    "type": "Error"
                });
                toastEvent.fire();
                return;
            }
            
            if((newSegment.Origin__c.toUpperCase() == newSegment.Stop__c.toUpperCase()) || (newSegment.Stop__c.toUpperCase() == newSegment.Destination__c.toUpperCase())){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Stop can not be same as Origin or Destination',
                    "type": "Error"
                });
                toastEvent.fire();
                return;
            }
            if(newSegment.Origin__c == newSegment.Destination__c){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Origin and Destination should not be same',
                    "type": "Error"
                });
                toastEvent.fire();
                return;
            }
          //Validation --- End For New
             
            var action = component.get("c.createRecord");  
            action.setParams({
                newSegment : newSegment
            });
            
            action.setCallback(this,function(response){  
                
                var state = response.getState();
                if(state === "SUCCESS"){
                     
                    var recId = response.getReturnValue();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "message": "Record created Successfully.",
                         "type": "success"
                                                
                    });
                	toastEvent.fire();
                    
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recId.Id, 
                        "slideDevName": "detail"
                    });
                    navEvt.fire();            
                } else if(state == "ERROR"){
                    
                    //alert('Error in calling server side action');
                    var toastEvent = $A.get("e.force:showToast");
                	toastEvent.setParams({
                    	"message": 'Error in calling server side action',
                    	"type": "Error"
                    });
                    toastEvent.fire(); 
                }
            });
            
        }else if(mode == 'Edit') {  
            console.log('mode---edit-'+mode);
            
            var newSegment = component.get("v.newSegment");
            
            //alert('new---edit-alert-'+JSON.stringify(newSegment));
            //Validation --- Start For Edit
            if($A.util.isEmpty(newSegment.Origin__c) || $A.util.isUndefined(newSegment.Origin__c)){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Origin can not be empty.',
                    "type": "Error"
                });
                toastEvent.fire();
                return;
            }            
            if($A.util.isEmpty(newSegment.Destination__c) || $A.util.isUndefined(newSegment.Destination__c)){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Destination can not be empty.',
                    "type": "Error"
                });
                toastEvent.fire();
                return; 
            }
            if($A.util.isEmpty(newSegment.Stop__c) || $A.util.isUndefined(newSegment.Stop__c)){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Stop can not be empty.',
                    "type": "Error"
                });
                toastEvent.fire();
                return;
            }            
            if((newSegment.Origin__c.toUpperCase() == newSegment.Stop__c.toUpperCase()) || (newSegment.Stop__c.toUpperCase() == newSegment.Destination__c.toUpperCase())){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Stop can not be same as Origin or Destination',
                    "type": "Error"
                });
                toastEvent.fire();
                return;
            }
            if(newSegment.Origin__c == newSegment.Destination__c){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": 'Origin and Destination should not be same',
                    "type": "Error"
                });
                toastEvent.fire();
                return;
            }
            
          //Validation --- End For Edit
          
            var segUpd = JSON.stringify(newSegment); 
            var action = component.get("c.updtRecord");
            action.setParams({updSegment : segUpd});   
            
            action.setCallback(this,function(response){
                
                var state = response.getState(); 
				if(state === "SUCCESS"){
                    var recrdId = response.getReturnValue();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "message": "The record has been updated successfully.",
                         "type": "success"
                                                
                    });
                	toastEvent.fire();
                    
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recrdId.Id,
                        "slideDevName": "detail"
                         
                    });
                    navEvt.fire();
                    
                } else if(state == "ERROR"){
                    //alert('Error in calling server side action');
                    var toastEvent = $A.get("e.force:showToast");
                	toastEvent.setParams({
                    	"message": 'Error in calling server side action',
                    	"type": "Error"
                    });
                    toastEvent.fire();
                }
                
            }); 
            
        }
        $A.enqueueAction(action); 
    },
    cancelSegment : function(component, event, helper) { 
        var recId = component.get("v.recordId");
        var mode = component.get("v.modalContext");
        var urlEvent = $A.get("e.force:navigateToURL");
        if(mode == 'New') {
            urlEvent.setParams({
                "url": "/lightning/o/Segment__c/list?filterName=All"
            });
        } else if(mode == 'Edit') {
            urlEvent.setParams({
                "url": "/lightning/r/"+recId+"/view"
            });
        }
        
        urlEvent.fire(); 
        /*
            var action = component.get("c.getListViews");
            action.setCallback(this, function(response){
                var state = response.getState();
                
                if (state === "SUCCESS") {
                    
                    var listviews = response.getReturnValue();
                   alert('alert---ListViews-'+JSON.stringify(listviews));
                    var navEvent = $A.get("e.force:navigateToList");
                    navEvent.setParams({
                        "listViewId": listviews.Id
                        
                    });
                    navEvent.fire();
                }
            });
            $A.enqueueAction(action);
      */
    },
    
})