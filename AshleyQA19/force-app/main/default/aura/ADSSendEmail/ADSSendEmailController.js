({
	init : function(component, event, helper) {
		component.set("v.isOpen", true);
        var recordId = component.get("v.recordId");
        //alert('recordId--'+recordId);
        var action = component.get("c.getrecords");
        action.setParams({ RecId : recordId });
        action.setCallback(this, function(response){
            var state = response.getState();
            //alert(state);
            if (state === "SUCCESS") {
                var Responsedata = response.getReturnValue(); 
                //alert('Responsedata--'+JSON.stringify(Responsedata));
                component.set("v.recval", response.getReturnValue());
            }
            
        });
        
        $A.enqueueAction(action);
	},
    
    sendMail:function(component, event, helper) {
        console.log('here');
		var recordId = component.get("v.recordId");
        var action = component.get("c.sendEmail");
        var myText = component.get("v.TextAreaVal");
        //alert('myText--'+myText);
        action.setParams({
            "Id": recordId,
            "TextVal": myText
        });
        action.setCallback(this,function(response){
            if (component.isValid() && response.getState() === "SUCCESS") {
                $A.get("e.force:closeQuickAction").fire();
       		 var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Email has been successfully sent."
                    });
                    toastEvent.fire();
             
                console.log('succ' + JSON.stringify(response.getReturnValue()));
            }
        });
         $A.enqueueAction(action);
	},
    
    GobackToRecord:function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
     
})