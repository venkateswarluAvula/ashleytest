@isTest
public class EDQServiceTest {

    static testmethod void getValidationToken() {
        EDQService.staticResourcesResult.add(new StaticResource(Name = 'EDQSessionToken', Body = Blob.valueOf('{"SessionToken":"Test","AV2Token":"Test123"}')));

        String result = EDQService.GetJSONFromStaticResource('EDQSessionToken');
        String expected = '{"SessionToken":"Test","AV2Token":"Test123"}';
        System.assertEquals(expected, result);

        String token = EDQService.GetValidationToken();
        System.assertEquals('Test123', token);
    }

    static testmethod void getCountryAliases() {
        EDQService.staticResourcesResult.add(new StaticResource(Name = 'EDQGlobalSettings', Body = Blob.valueOf('edqGlobalSettings={"AddressSettings":{"ValidationEndPoint":"https://sfv5.online.qas.com/SalesforceV5Api/api/address/search","GlobalIntuitiveValdiationEndpoint":"https://api.edq.com/capture/address/v2/search?query=","RefineEndPoint":"https://sfv5.online.qas.com/SalesforceV5Api/api/address/refine","ConcatenationSeparator":",","GlobalIntuitiveNumberOfSuggestions":7,"IsCountryShownAbove":false,"ValidationTimeout":10,"MaxPicklistItems":100,"RefineIntuitiveSearchAfter":1,"SearchIntensity":"Close","StartInitialIntuitiveSearchAfter":5,"Treshold":25,"UponFailureDisableInteractiveFor":5,"DataSetAliases":{"AW":"ABW","ABW":"ABW","AF":"AFG","AFG":"AFG","AO":"AGO","AGO":"AGO","AL":"ALB","ALB":"ALB"}}}')));

        Map<String, String> result = EDQService.getCountryAliases();
        Map<String, String> expected = new Map<String, String>{'ABW'=>'ABW', 'AF'=>'AFG', 'AFG'=>'AFG', 'AGO'=>'AGO', 'AL'=>'ALB', 'ALB'=>'ALB','AO'=>'AGO', 'AW'=>'ABW'};
        System.assertEquals(expected, result);
    }

    static testmethod void transformToISO3 (){
        EDQService.staticResourcesResult.add(new StaticResource(Name = 'EDQGlobalSettings', Body = Blob.valueOf('edqGlobalSettings={"AddressSettings":{"ValidationEndPoint":"https://sfv5.online.qas.com/SalesforceV5Api/api/address/search","GlobalIntuitiveValdiationEndpoint":"https://api.edq.com/capture/address/v2/search?query=","RefineEndPoint":"https://sfv5.online.qas.com/SalesforceV5Api/api/address/refine","ConcatenationSeparator":",","GlobalIntuitiveNumberOfSuggestions":7,"IsCountryShownAbove":false,"ValidationTimeout":10,"MaxPicklistItems":100,"RefineIntuitiveSearchAfter":1,"SearchIntensity":"Close","StartInitialIntuitiveSearchAfter":5,"Treshold":25,"UponFailureDisableInteractiveFor":5,"DataSetAliases":{"AW":"ABW","ABW":"ABW","AF":"AFG","AFG":"AFG","AO":"AGO","AGO":"AGO","AL":"ALB","ALB":"ALB"}}}')));

        String result = EDQService.transformToISO3('ABW');
        System.assertEquals('ABW', result);

        String result2 = EDQService.transformToISO3('AL');
        System.assertEquals('ALB', result2);

        String result3 = EDQService.transformToISO3('');
        System.assertEquals('', result3);
    }

    static testmethod void FormatAddress(){
        Test.setMock(HttpCalloutMock.class, new EDQHttpResponseMock(200));
        String result = EDQService.FormatAddress('https://api.edq.com/capture/address/v2/format?country=AUS&id=700AUS-NOAUSHAHgBwAAAAAIAwEAAAABV3R_gAAAAAAAAAAA..9kAAAAAP....8AAAAAAAAAAAAAAAAASGFtaWx0b24A');
        String expected = '{"address":[{"addressLine1":"38 Lane Cove Rd"},{"addressLine2":"38 Lane Cove Road"},{"addressLine3":""},{"locality":"RYDE"},{"province":"NSW"},{"postalCode":"2112"},{"country":"AUSTRALIA"}],"components":[{"deliveryPointId1":"76077169"},{"streetNumber1":"38"},{"street1":"Lane Cove Rd"},{"locality1":"RYDE"},{"province1":"New South Wales"},{"provinceCode1":"NSW"},{"postalCode1":"2112"},{"country1":"AUSTRALIA"},{"countryISO1":"AUS"}],"metadata":{}}';
        System.assertEquals(expected, result);
    }

    static testmethod void SearchAddress(){
        Test.setMock(HttpCalloutMock.class, new EDQHttpResponseMock(200));
        EDQService.staticResourcesResult.add(new StaticResource(Name = 'EDQSessionToken', Body = Blob.valueOf('{"SessionToken":"Test","AV2Token":"Test123"}')));

        String result = EDQService.SearchAddress('Akron', 'US', 7);
        String expected ='{"count":7,"results":[{"suggestion":"PO Box 10099, Akron OH 44310","matched":[[7,12],[0,6]],"format":"https://api.edq.com/capture/address/v2/format?country=USA&id=95889123-1cea-48cf-8633-da5f46b74a58ql12"},{"suggestion":"PO Box 10099, Albany NY 12201","matched":[[7,12],[0,6]],"format":"https://api.edq.com/capture/address/v2/format?country=USA&id=fdfa85d0-e324-4dad-a001-a02eafde0219ql12"},{"suggestion":"PO Box 10099, Albuquerque NM 87184","matched":[[7,12],[0,6]],"format":"https://api.edq.com/capture/address/v2/format?country=USA&id=e2d741c8-db56-438d-987e-7385f34b842eql12"},{"suggestion":"PO Box 10099, Alexandria VA 22310","matched":[[7,12],[0,6]],"format":"https://api.edq.com/capture/address/v2/format?country=USA&id=17285670-e81a-4062-b130-919de99338acql12"},{"suggestion":"PO Box 10099, Amarillo TX 79116","matched":[[7,12],[0,6]],"format":"https://api.edq.com/capture/address/v2/format?country=USA&id=3e7b0be2-0c9c-45fa-b7ba-4188f58e4087ql12"},{"suggestion":"PO Box 10099, American Canyon CA 94503","matched":[[7,12],[0,6]],"format":"https://api.edq.com/capture/address/v2/format?country=USA&id=cffcdb43-dc15-4ab4-8b1d-ac7be13dbfb1ql12"},{"suggestion":"PO Box 10099, Anaheim CA 92812","matched":[[7,12],[0,6]],"format":"https://api.edq.com/capture/address/v2/format?country=USA&id=08fea279-976c-43ac-8dbf-d7f7f25eb159ql12"}]}';

        System.assertEquals(expected, result);
    }

}