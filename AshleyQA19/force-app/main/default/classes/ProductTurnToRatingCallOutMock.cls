/* *******************************************************************************************************************
* Class Name   : ProductTurnToRatingCallOutMock
* Description  : Http Call Out Mock test class for Asheley Product Reviews and Rating API .
* Author       : Shiva PAILA
* Created On   : 20/03/19	
* Modification Log:  
* --------------------------------------------------------------------------------------------------------------------------------------
* Developer                          Date                   Modification ID      Description 
* ---------------------------------------------------------------------------------------------------------------------------------------
*Shiva paila						20/03/19										Replacing Bazar voice with TurnTo
*************************************************************************************************************************************///
@isTest
global class ProductTurnToRatingCallOutMock implements HttpCalloutMock {
    private String fetchType;
    /**
    * @description <Constructor for apiType> 
    **/
    global ProductTurnToRatingCallOutMock(String apiType){
        this.fetchType = apiType;
    }
    
    /**
    * @description <Implement the interface HttpCalloutMock's respond method to return mock response>
    * @return <return is the Mocked JSON string or throw a CalloutException>
    **/
	global HTTPResponse respond(HTTPRequest req) {
        if(fetchType == 'CalloutExceptionTest'){
            CalloutException e = (CalloutException)CalloutException.class.newInstance();
            e.setMessage('Throw CalloutException Test. Unauthorized endpoint, please check Setup->Security->Remote site settings.');
            throw e;
        }else{
            system.debug(req.getEndpoint());
            System.assertEquals('GET', req.getMethod());
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            
            ProductReviewsWrapper productReviews;
            
            if(fetchType == 'ReviewsTest'){
              productReviews = prepareProductReviews();
                String bodyContent = JSON.serialize(productReviews);
                res.setBody(bodyContent);
            }
            res.setStatusCode(200);
            return res;
        }
    }


     /**
    * @description <Simulate Product Reviews info>                                                       
    * @return <Asserts return is the Mocked ProductRatingsWrapper object>
    **/    
    public static ProductReviewsWrapper prepareProductReviews(){
        ProductReviewsWrapper prsW = new ProductReviewsWrapper();
        ProductReviewWrapper productReview = new ProductReviewWrapper();
       	ProductReviewcatalogIt prdtusr= new ProductReviewcatalogIt();
		prdtusr.reviewCount =1; 
        prdtusr.averageRating =2.4;
        productReview.catalogItems = new List<ProductReviewcatalogIt>{prdtusr};
        ProductUserReviewWrap prdur = new ProductUserReviewWrap();
        prdur.nickName = 'test';
       	productReview.id= 1200014; 
       	productReview.ProductId= '1200014';        
        productReview.rating=5;
        productReview.text= '1200014';
        prsW.reviews = new List<ProductReviewWrapper>{productReview};
            
        return prsW;
    }
    
}