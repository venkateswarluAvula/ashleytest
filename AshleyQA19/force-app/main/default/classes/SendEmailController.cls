public class SendEmailController {
    @AuraEnabled
    public static list<EmailTemplate> getTemplates(){ 
        list<EmailTemplate> emailTemp = new list<EmailTemplate>();
        emailTemp = [SELECT Id,Name,Subject,TemplateType FROM EmailTemplate WHERE TemplateType IN ('custom','text','HTML')];
        return emailTemp;
    }
    @AuraEnabled 
    public static EmailTemplate getTemplateDetails(string templteId){
        
        EmailTemplate emailTemp = new EmailTemplate();
        list<EmailTemplate> emailTempLst = new list<EmailTemplate>();
        emailTempLst = [SELECT Id,Name,Subject,TemplateType,body FROM EmailTemplate WHERE ID=: templteId];
        
        emailTemp = emailTempLst.size()>0 ? emailTempLst[0] : emailTemp;
        return emailTemp;
        
    }
    
    @AuraEnabled 
    public static void sendMailMethod(String mMail ,String mSubject ,String mbody,string recid,string templateId,string fmail){
        //Inserting the record id in Email subject
        system.debug('fromAddress-->'+fmail);
        system.debug('testxte-->'+mbody);
        system.debug('templatetestxte-->'+templateId);
        Case mycase = [SELECT CaseNumber,Id,ContactId,ContactEmail FROM Case WHERE Id =:recid];
        string subrec= '('+ mycase.CaseNumber +')';  
        // For storing Relation Id
        string opcid = mycase.ContactId;
        OrgWideEmailAddress owea = new OrgWideEmailAddress();
        try{
           owea = [SELECT Id,Address,DisplayName FROM OrgWideEmailAddress Where Address=:fmail]; 
        }catch(Exception e){
            
        }
        //Email Sending Start
        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();  
        //Create a new Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        if(string.isNotBlank(templateId)){
        email = Messaging.renderStoredEmailTemplate(templateId, opcid, mycase.Id);
        mbody = email.getHtmlBody();
        }
        List<String> sendTo = mMail.split(',');
        String addrUrl= System.Label.Email_Addresses_for_Email_to_others;
        mail.setToAddresses(sendTo);
        mail.setSubject(mSubject + subrec );
        mail.setReplyTo(addrUrl);
        system.debug('test email---'+UserInfo.getUserEmail());
        if(fmail.equals(UserInfo.getUserEmail())){
            if(string.isNotBlank(templateId)){
                system.debug('templatetestxte22-->'+templateId);
                mail.setSenderDisplayName(UserInfo.getName());
                mail.setHtmlBody(email.getHtmlBody());
                mail.setPlainTextBody(email.getPlainTextBody());
                
            }else if(string.isNotBlank(mbody)){
                mail.setSenderDisplayName(UserInfo.getName());
                mail.setHtmlBody(mbody);
                mail.setPlainTextBody(mbody);
            }
            
        }else if(fmail.equals(owea.Address)){
            if(string.isNotBlank(templateId)){
                mail.setOrgWideEmailAddressId(owea.Id);
                mail.setHtmlBody(email.getHtmlBody());
                mail.setPlainTextBody(email.getPlainTextBody());
                
            }else{
                mail.setOrgWideEmailAddressId(owea.Id);
                mail.setHtmlBody(mbody);
                mail.setPlainTextBody(mbody);
            }
        }
        
        // Add your email to the master list
        mails.add(mail);        
        try {
            //Add Inbound Email Message for contact
            EmailMessage caseEmailMessage = new EmailMessage();
            caseEmailMessage.ToAddress = mMail;
            caseEmailMessage.Subject = mSubject;
            caseEmailMessage.Incoming= false;
            caseEmailMessage.status = '3';
            if(string.isNotBlank(templateId)){
                caseEmailMessage.TextBody = email.getPlainTextBody();
                caseEmailMessage.HtmlBody = email.getHtmlBody(); 
            }else if(string.isNotBlank(mbody)){
                caseEmailMessage.TextBody = mbody;
                caseEmailMessage.HtmlBody = mbody; 
            }
            
            caseEmailMessage.ParentId = mycase.Id;
            if (fmail.equals(UserInfo.getUserEmail())){
                caseEmailMessage.ValidatedFromAddress= UserInfo.getUserEmail();
                caseEmailMessage.FromName = UserInfo.getName();
            }else if(fmail.equals(owea.Address)){
                caseEmailMessage.ValidatedFromAddress = owea.Address;
                caseEmailMessage.FromName = owea.DisplayName;
            }
            insert caseEmailMessage;  
            // Add Email Message Relation for id of the sender
            EmailMessageRelation emr = new EmailMessageRelation();
            emr.EmailMessageId = caseEmailMessage.Id;
            emr.RelationType = 'FromAddress';
            //Case Email Message Relation Id
            emr.RelationId = opcid;
            if(fmail.equals(UserInfo.getUserEmail())){
                emr.RelationAddress = UserInfo.getUserEmail();
            }else if(fmail.equals(owea.Address)){
                emr.RelationAddress = owea.Address; 
            }
            insert emr;     
            // Send all emails in the master list
            Messaging.sendEmail(mails);
        }
        catch(Exception e){
            System.debug('Query Issue: ' + e);
        }
    }
}