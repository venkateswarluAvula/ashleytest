/****** v1 | Description: Test methods for estimated delivery date controller class | 1/16/2018 | L OMeara */
@isTest
public class EstimatedDeliveryDateController_Test {
    /* test query to get delivery date */
    @isTest
    static void testGetDeliveryDate() {
        String sampleResponse = '{"deliveryWindows": [';
        for(Integer i=0; i < 5; i++){
            sampleResponse += '{';
            sampleResponse += '"start": "' + API_ATCClient.formatDateToISO8601(Date.today().addDays(i)) + '",';
            sampleResponse += '"end": "' + API_ATCClient.formatDateToISO8601(Date.today().addDays(i)) + '",';
            sampleResponse += '"sortIndex": ' + String.valueOf(i);
            sampleResponse += '}';
            if(i != 4){
                sampleResponse += ',';
            }
        }      
        sampleResponse += ']}';
        
		prepareCustomSetting();

        //Create user with required onesource values. FIXME: Put in test data factory
        User u = new user();
        u.LastName = 'Test Code';
        u.Email = 'eddc@test.com';
        u.Alias = 'eddc';
        u.Username = 'testeddc@test.com';
        u.CommunityNickname = 'test12';
        u.LocaleSidKey = 'en_US';
        u.TimeZoneSidKey = 'GMT';
        u.ProfileID = Utilities_SalesAndService.getProfileIdByName('RSA');
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.FulfillerID__c = '8888000-130';
        u.Store_Zip__c = '33547';
        u.Last_Checked_Store_Info__c = Datetime.now();
        u.AcctNo_ShipTo_ProfitCtr__c = '8888300-298-34-KWF-GA';
        u.RDC__c = '164';
        u.LegacyStoreID__c = '133';
        insert u;

        Test.startTest();
        API_ATCClient.ATCResponseWrapper resp;

        system.runAs(u){
            Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'OK', sampleResponse,new Map<String, String>()));
            String retVal = EstimatedDeliveryDateController.getDatesFromZip('33547', '1200020', 1);
            system.debug('response ' + retVal);
            resp = (API_ATCClient.ATCResponseWrapper)JSON.deserialize(retVal, API_ATCClient.ATCResponseWrapper.class);
        
		      Test.stopTest();
          }
        
        system.assert(resp.isSuccess);
        system.assert(resp.deliveryWindows.size() > 0);
    }
    
    public static void prepareCustomSetting(){
        Integration_Settings__c sandboxSetting = new Integration_Settings__c(Name = API_ATCClient.INTEGRATION_SETTING_KEY_SANDBOX, API_Key__c = 'test key');
        Integration_Settings__c productionSetting = new Integration_Settings__c(Name = API_ATCClient.INTEGRATION_SETTING_KEY_PRODUCTION, API_Key__c = 'test key');
        insert new List<Integration_Settings__c>{sandboxSetting, productionSetting};
            
        OneSourceSettings__c oneSourceSetting = new OneSourceSettings__c();
        oneSourceSetting.Name = StoreInfo.ONE_SOURCE_API_CONFIG_NAME;
        oneSourceSetting.One_Source_API_Path__c = 'https://apigw.ashleyfurniture.com';
        oneSourceSetting.One_Source_API_Key__c = 'iuvg18wbwA3LwIyy4aj3wEPRTaQGGGYN';
        oneSourceSetting.One_Source_API_Refresh_Minutes__c = 1.0;
        insert oneSourceSetting;

        MarketSettings__c  marketSettings = new MarketSettings__c();
        marketSettings.Name = '8888000-130';
        marketSettings.Market_Code__c = 'SWF';
        insert marketSettings;
    }
}