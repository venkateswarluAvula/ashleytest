/* This is a test class for CreateCaseCtrl.*/
@isTest
public class CreateCaseCtrlTest {
    @testSetup
    static void setup() {
        List<Account> accList = new List<Account>();
        accList = TestDataFactory.initializeAccounts(1);
        insert accList;

        List<Contact> contList = new List<Contact>();
        contList = TestDataFactory.initializeContacts(accList[0].Id, 1);
        insert contList;

        // Insert Case
        Case caseObj = new Case(Sales_Order__c = '17331400:001q000000raDkvAAE', Status = 'New', Origin = 'Phone',
                            Type = 'Open Order Inquiry', Subject = 'Test',
                            Description = 'Test', AccountId = accList[0].Id, ContactId = contList[0].Id);
        insert caseObj;
        system.debug(caseObj);
    }

    @isTest static void getSalesOrderInfoTest() {
        Case caseObj = [Select Id, AccountId from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
        system.debug(caseObj);
        Test.startTest();
        CreateCaseCtrl.getSalesOrderInfo(caseObj.Id);
        CreateCaseCtrl.getAccDetail(caseObj.AccountId);
        CreateCaseCtrl.getSoLineItemsBySoExternalId('17331400:001q000000raDkvAAE');
        Test.stopTest();
    }

    @isTest static void createCaseTest() {
        Case caseObj = [Select Id, AccountId from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
        Test.startTest();
        CreateCaseCtrl.create_case(caseObj, '17331400:001q000000raDkvAAE', true, 'Yes', caseObj.AccountId, 'type', 'casesubtype', 'subject', 'marketAccount', 'serviceProvider','storeNameStoreNumberPC', 'shipAddress1', 'shipAddress2', 'shipCity', 'shipState', 'shipZip', '', 'Refusal', 'New','88888300-164');
        caseObj.id = null;
        CreateCaseCtrl.create_case(caseObj, '17331400:001q000000raDkvAAE', true, 'Yes', caseObj.AccountId, 'type', 'casesubtype', 'subject', 'marketAccount', 'serviceProvider','storeNameStoreNumberPC', 'shipAddress1', 'shipAddress2', 'shipCity', 'shipState', 'shipZip', 'Phone', 'Refusal', 'New','88888300-164');
        Test.stopTest();
    }

    @isTest static void newProductLineItemRecordTest() {
        Case caseObj = [Select Id, AccountId from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
        Test.startTest();
		List<String> ProductId = new List<String>();
    	CreateCaseCtrl.newProductLineItemRecord(ProductId, caseObj.Id, '17331400:001q000000raDkvAAE');
        Test.stopTest();
    }
}