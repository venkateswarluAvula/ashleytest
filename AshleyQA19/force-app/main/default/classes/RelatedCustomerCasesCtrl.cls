/* *******************************************************************************************************************
* Class Name   : RelatedCustomerCasesCtrl
* Description  : Controller class for RelatedCustomerCases component.
* Author       : Theyagarajan Sadhasivam (Perficient, Inc.)
* Created On   : 12/29/2017
* Modification Log:
* --------------------------------------------------------------------------------------------------------------------------------------
* Developer                                   Date                   Modification ID      Description 
* ---------------------------------------------------------------------------------------------------------------------------------------
*
**************************************************************************************************************************************/
public class RelatedCustomerCasesCtrl {

    @AuraEnabled
    public static List<Case> getCases(String accountId) {
    	list<string> closeValList = CaseHelper.getCaseClosedValues();
    	List<Case> caseLst=[SELECT Id, CaseNumber, Status, Account.id, Subject FROM Case WHERE Account.id = :accountId AND Status NOT IN :closeValList];
        if (!caseLst.isEmpty()) {
           return caseLst;
        }
        return null;
    }
}