public class CommitmentOppRelatedCon {
	
    @AuraEnabled
    public static List<Commitment_Opportunities__c> getComOpp(Id COId){
        
       List < Commitment_Opportunities__c > lstOfComOpp = [SELECT Id,Name,Account__c,Commitment__c,Status__c,Trips_Per_Week__c,
                                                           Opportunity__c,Opportunity__r.Name
                                                           From Commitment_Opportunities__c where Commitment__c=: COId];
        system.debug('lstOfComOpp---'+lstOfComOpp);
        return lstOfComOpp;
        
    }
    
    @AuraEnabled
    public static Commitment_Opportunities__c getComOppVal(Id recid){
        system.debug('recid---'+recid);
       Commitment_Opportunities__c ComOppVal = [SELECT Id,Name,Account__c,Commitment__c,Status__c,Trips_Per_Week__c,
                                                           Opportunity__c,Opportunity__r.Name
                                                           From Commitment_Opportunities__c where Id=: recid];
        system.debug('ComOppVal---'+ComOppVal);
        return ComOppVal;
        
    }
    
}