public class SalesOrderVIPControlWrapper {
    @AuraEnabled
    public string SFContactID { get;set; }
    @AuraEnabled
    public string OrderNumber { get;set; }
    @AuraEnabled
    public string Market { get;set; }
    @AuraEnabled
    public string AccountShipTo { get;set; }
    @AuraEnabled
    public string DeliveryDate { get;set; }
    @AuraEnabled
    public string Type { get;set; }
    @AuraEnabled
    public string VIP { get;set; }
    @AuraEnabled
    public string LIB { get;set; }
    @AuraEnabled
    public string SKU { get;set; }
    @AuraEnabled
    public string CaseNumber { get;set; }
    @AuraEnabled
    public string CaseId { get;set; }
}