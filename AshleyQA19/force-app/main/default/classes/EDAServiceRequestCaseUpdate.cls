@RestResource (urlMapping='/ServiceRequests-CaseUpdate/*')
global class EDAServiceRequestCaseUpdate {    
    Public Class CaseLineItemWrap{
        Public String SFDCAccountId  {get;set;}
        public String ItemInvoiceDate  {get;set;}
        public String ItemSerialNumber  {get;set;}
        public String ProfitCenter  {get;set;}
        public String ItemUniqueID  {get;set;}
        public String ItemSaleNumber  {get;set;}
        public String ItemDescription  {get;set;}
        public String ItemSKUNumber  {get;set;}
        public String ItemInvoiceNumber  {get;set;}
        public String ItemDefect  {get;set;}
        public String DefectDesc  {get;set;}
        public String PartOrderTrackNumber  {get;set;}
        public String OrderNumber  {get;set;}
        public String OrderStatus  {get;set;}
        public String OrderShipToInfo  {get;set;}
        public String OrderShipDate  {get;set;}
        public String OrderShipDateAsChar  {get;set;}
        public String PieceExchangedFixed  {get;set;}
        public String CreatedTime  {get;set;}
        public String CreatedUserID  {get;set;}
        public String LastTime  {get;set;}
        public String LastUserID  {get;set;}
        public String PartNumber  {get;set;}
        public String PartDescription  {get;set;}
        public String DeliveryDate  {get;set;}
        public String DeliveryDateAsChar {get;set;}
        public String SignedBy {get;set;}
        public String ItemSaleSeqNumber {get;set;}
        public String AccountShipto {get;set;}
        public String RequestID {get;set;}
        public String CustomerType {get;set;}
        public String CustomerID {get;set;}
        public String ShipToAddress1 {get;set;}
        public String ShipToAddress2 {get;set;}
        public String ShipToCityName {get;set;}
        public String ShipToStateCode {get;set;}
        public String ShipToZipCode {get;set;}
        public Boolean IsDeleted {get;set;}
        public string LastModifiedUserName {get; set;}
        public string MarketAccount { get; set; }
        public string StoreNameStoreNumberPC { get; set; }
    }
    global class testWrap {
        List<CaseLineItemWrap> testwrapper;
        testWrap(){
            testwrapper = new List<CaseLineItemWrap>();
        }
    }
    @HttpPost
    global static String doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json');
        String JSONString = req.requestBody.toString();
        Map<String, Object> Someval = (Map<String, Object>)JSON.deserializeUntyped(JSONString);
        List<Map<String, Object>> data = new List<Map<String, Object>>();
        Set<String> reqId = new Set<String>();
        Set<String> AccId = new Set<String>();
        Set<String> uniqid = new set<String>();
        Set<string> AccShipto = new Set<string>();
       	List<Case> myCase = new List<Case>();
        Map<Id, List<Address__c>> custAddressMap = new Map<Id, List<Address__c>>();
        String mycaseId;
        List<Case_Line_Item__c> CLIreq = new List<Case_Line_Item__c>();
        List<Contact> Con = new List<Contact>();
        List<Account> Acc = new List<Account>();
        Case_Line_Item__c CLIreqinsert = new Case_Line_Item__c();
        for (Object instance : (List<Object>)Someval.get('CsrLineItems')){
            data.add((Map<String, Object>)instance);
            system.debug('data-->'+data);
        }
        Boolean flag = false;
        Boolean InsertLineitemFlag = false;
        Boolean DeleteFlag = false;
        Boolean updateLineitemFlag = false;
        Boolean InsertCaseFlag = false;
        List<CaseLineItemWrap> CLIList = new List<CaseLineItemWrap>();
        for(integer i=0; i<data.size(); i++){
            String Someval3 = JSON.serialize(data[i]);
            CaseLineItemWrap Someval4 = (CaseLineItemWrap)JSON.deserialize(Someval3,CaseLineItemWrap.class);
            system.debug('data val-->'+Someval4.SFDCAccountId);
            AccId.add(Someval4.SFDCAccountId);
            reqId.add(Someval4.RequestID);
            AccShipto.add(Someval4.AccountShipto);
            uniqid.add(Someval4.ItemUniqueID);
            system.debug('uniquedata-->'+uniqid);
            CLIList.add(Someval4);
        }
        if(data.size()>0){
            myCase = [SELECT Id, AccountId, Legacy_Service_Request_ID__c, Market__c, Profit_Center__c, Address__c, Legacy_Account_Ship_To__c, Sales_Order__c,
                      Type_of_Resolution__c, Resolution_Notes__c, Status
                      FROM Case WHERE Legacy_Account_Ship_To__c IN :AccShipto AND Legacy_Service_Request_ID__c IN :reqId];
            CLIreq = [SELECT Case__c,Id,Name,Legacy_Service_Request_ID__c,Legacy_Service_Request_Item_ID__c 
                      FROM Case_Line_Item__c WHERE Case__c IN :myCase AND Legacy_Service_Request_ID__c IN :reqId
                      AND Legacy_Service_Request_Item_ID__c IN :uniqid];  
            custAddressMap = AddressHelper.getAddresses(AccId);
            con = [SELECT AccountId,Id FROM Contact WHERE AccountId IN :AccId];
            Acc = [SELECT Id FROM Account WHERE ID IN :AccId];
            
            system.debug('myCase--'+myCase);
            system.debug('CLIreq--'+CLIreq);
            system.debug('custAddressMap--'+custAddressMap);
            system.debug('con--'+con);
            system.debug('Acc--'+Acc);
        }
        System.debug('listof'+CLIList.size());
        for(CaseLineItemWrap CLIW : CLIList){
            Boolean isValidAcc = false;
            boolean isexistingCLI = false;
            String cwReqId = CLIW.RequestID;
            String cwAccId = CLIW.SFDCAccountId;
            string cwAccShipto = CLIW.AccountShipto;
            ID existingCaseId = null;
            ID existingCLIiD = null;
            Boolean isExistingCase = false;
            System.debug('Acc size-->'+Acc.size());
            System.debug('Acc -->'+Acc);
            if(Acc.size() > 0){
                for(Account a : Acc){
                    System.debug('a-->'+a.id);
                    if(Id.valueOf(CLIW.SFDCAccountId) == a.Id){
                        isValidAcc = true;
                        //break;
                    }
                }
            }
            System.debug('CLIW-->'+CLIW);
            if((isValidAcc == true) && (cwAccShipto != null)){
                System.debug('myCase-->'+myCase);
                case existingCaseInfo = new case();
                for(integer i = 0; i < myCase.size(); i++){
                    if((myCase[i].Legacy_Account_Ship_To__c == cwAccShipto) && (myCase[i].Legacy_Service_Request_ID__c== CLIW.RequestID)){
                        isExistingCase = true;
                        existingCaseId = myCase[i].ID;
                        existingCaseInfo = myCase[i];
                        System.debug('existingCaseId'+existingCaseId);
                        break;
                    }
                }
                System.debug('isExistingCase-->'+isExistingCase);
                List<Address__c> myAddress = new List<Address__c>();		
				if ((custAddressMap.size() > 0) && (custAddressMap.get(cwAccId) != null)) {		
					myAddress = custAddressMap.get(cwAccId);		
				}
                if (isExistingCase == false){
                    //Insert case and CLI
                    //create case;
                    Id acID = CLIW.SFDCAccountId;
                    Case newCase = createNewCase(acID, CLIW, con, myAddress);
                    InsertCaseFlag = true;
                    system.debug('Case Inserted-->'+newCase);
                    //have to add newly created case to mycase list
                    myCase.add(newCase); // added by sekar to fix the issue# 288254
                    //get case number from the created case
                    //create new line items
                    CLIreqinsert = getCaseLineItem(newCase.Id,isExistingCase,CLIW,CLIreq);
                    CLIreqinsert.Case__c = newCase.Id;
                    insert CLIreqinsert;
                    system.debug('Inserted-->'+CLIreqinsert);
                    InsertLineitemFlag = true;
                }
                else{
                    System.debug('CLIreq-->'+CLIreq);
                    integer isexistingCLIIndex = null;
                    for(integer i = 0; i < CLIreq.size(); i++)
                    {
                        if((CLIreq[i].Legacy_Service_Request_Item_ID__c == CLIW.ItemUniqueID) && 
                           (CLIreq[i].Legacy_Service_Request_ID__c== CLIW.RequestID) && (CLIreq[i].Case__c == existingCaseId))
                        {
                               isexistingCLI = true;
                               isexistingCLIIndex = i;
                               existingCLIiD = CLIreq[i].ID;
                               break;
                        }
                    }
                    System.debug('isexistingCLI-->'+isexistingCLI);
                    if (isexistingCLI == true)
                    {   //Update or delete CLI
                        //Check ISDELETE 
                        if(CLIW.IsDeleted == false)
                        {
                            //update the line item record
                            CLIreqinsert = getCaseLineItem(existingCaseId,isexistingCLI,CLIW,CLIreq);
                            System.debug('before'+CLIreqinsert);
                            Update CLIreqinsert;
                            system.debug('Updated-->'+CLIreqinsert);
                            updateLineitemFlag = true;
                        }
                        else if(CLIW.IsDeleted == true)
                        {
                            //Delete the CLI
                            Delete CLIreq[isexistingCLIIndex];
                            system.debug('Deleted-->'+CLIreqinsert);
                            DeleteFlag = true;
                        }
                        
                    }
                    else
                    {
                        //Insert the line item record
                        CLIreqinsert = getCaseLineItem(existingCaseId,isexistingCLI,CLIW,CLIreq);
                        CLIreqinsert.Case__c = existingCaseId ;
                        insert CLIreqinsert;
                        system.debug('Inserted-->'+CLIreqinsert);
                        InsertLineitemFlag = true;
                    }

                    if ((existingCaseInfo != null) && ((existingCaseInfo.Sales_Order__c == null) || (existingCaseInfo.Market__c == null) || (existingCaseInfo.Profit_Center__c == null) || (existingCaseInfo.Address__c == null))) {
                   	//if Market__c, Profit_Center__c or Address__c is null then update case	
                   		case updateCase = new case();
                        updateCase.Id = existingCaseInfo.Id;	
                        if (existingCaseInfo.Market__c == null) {		
                            updateCase.Market__c = CLIW.MarketAccount;		
                        }		
                        if (existingCaseInfo.Profit_Center__c == null) {		
                            updateCase.Profit_Center__c = CLIW.StoreNameStoreNumberPC;		
                        }		
                        if (existingCaseInfo.Address__c == null) {		
                            //i will write the logic	
                        updateCase.Address__c = AddressHelper.getCustomerEDAAddress(CLIW.SFDCAccountId, CLIW.ShipToAddress1, CLIW.ShipToAddress2, CLIW.ShipToCityName, CLIW.ShipToStateCode, CLIW.ShipToZipCode, myAddress, 'Line Item - Case Update');
                       
                        }	
                   	 	if ((existingCaseInfo.Sales_Order__c == null) && string.isNotBlank(CLIW.ItemSaleNumber)) {
                            string SOexternalID = CLIW.ItemSaleNumber + ':' + CLIW.SFDCAccountId;
                            updateCase.Sales_Order__c = SOexternalID;
                        }

                        /*if (string.isNotBlank(existingCaseInfo.Status) && CaseHelper.caseIsClosed(existingCaseInfo.Status)) {
                            if (string.isBlank(existingCaseInfo.Type_of_Resolution__c)) {
                                existingCaseInfo.Type_of_Resolution__c = 'Closed in Legacy System';
                            }
                            if (string.isBlank(existingCaseInfo.Resolution_Notes__c)) {
                                existingCaseInfo.Resolution_Notes__c = 'This Case was closed in HOMES';
                            }
                        }*/
                   
                        update updateCase;
					}		
                }
                 
            }
        }
        if(InsertLineitemFlag == true)
        {
            system.debug('InsertCaseFlag--'+InsertCaseFlag);
            system.debug('updateLineitemFlag--'+updateLineitemFlag);
            system.debug('DeleteFlag--'+DeleteFlag);
            if(InsertCaseFlag == true && updateLineitemFlag == true && DeleteFlag == true )
            {
                return 'Lineitem inserted ; Case and Lineitem inserted ; Lineitem updated ; Lineitem deleted';
            }else if(InsertCaseFlag == true && updateLineitemFlag == true && DeleteFlag == false){
                return 'Lineitem inserted ; Case and Lineitem inserted ; Lineitem updated';
            }else if(InsertCaseFlag == true && updateLineitemFlag == false && DeleteFlag == true){
                return 'Lineitem inserted ; Case and Lineitem inserted ; Lineitem deleted';
            }else if(InsertCaseFlag == true && updateLineitemFlag == false && DeleteFlag == false){
                return 'Lineitem inserted ; Case and Lineitem inserted ';
            }
            else
            { 
                if(InsertCaseFlag == false && updateLineitemFlag == false && DeleteFlag == false){
                    return 'Lineitem inserted';
                }else if(InsertCaseFlag == false && updateLineitemFlag == true && DeleteFlag == false){
                    return 'Lineitem inserted ; Lineitem updated';
                }else if(InsertCaseFlag == false && updateLineitemFlag == false && DeleteFlag == true){
                    return 'Lineitem inserted ; Lineitem deleted';
                }
                
            }
        }
        else
        {
            if (InsertCaseFlag == true && updateLineitemFlag == true && DeleteFlag == true)
            {
                return 'Case and Lineitem inserted ; Lineitem updated ; Lineitem deleted';
            }else if (InsertCaseFlag == true && updateLineitemFlag == false && DeleteFlag == false){
                return 'Case and Lineitem inserted';
            }else if(InsertCaseFlag == true && updateLineitemFlag == true && DeleteFlag == false){
                return 'Case and Lineitem inserted ; Lineitem updated';
            }else if(InsertCaseFlag == true && updateLineitemFlag == false && DeleteFlag == true){
                return 'Case and Lineitem inserted ; Lineitem deleted';
            }
            else
            { 
                if(InsertCaseFlag == false && updateLineitemFlag == true && DeleteFlag == true){
                    return 'Lineitem updated ; Lineitem deleted';
                }else if(InsertCaseFlag == false && updateLineitemFlag == false && DeleteFlag == true){
                    return 'Lineitem deleted';
                }else if(InsertCaseFlag == false && updateLineitemFlag == true && DeleteFlag == false){
                    return 'Lineitem updated';
                }
            }
        }
       /* if((updateLineitemFlag == true) || (InsertLineitemFlag == true) || (DeleteFlag == true)){
        return 'Insertion Success..';
        } */
       return 'Insertion Failed..';
    }
    
    public static Case_Line_Item__c getCaseLineItem(Id caseId,Boolean flag,CaseLineItemWrap Someval,List<Case_Line_Item__c> cliList){
        Case_Line_Item__c CLI = new Case_Line_Item__c();
        if(flag == true ){
            system.debug('myvals-->'+Someval);
			for(integer i = 0; i< cliList.size(); i++){
                if((cliList[i].Legacy_Service_Request_Item_ID__c == Someval.ItemUniqueID) && 
                           (cliList[i].Legacy_Service_Request_ID__c== Someval.RequestID) && (cliList[i].Case__c == caseId)){
                    CLI= cliList[i];
                    system.debug('thisCase-->'+CLI);
                    break;
                }
            }
        }
        CLI.Item_Serial_Number__c = Someval.ItemSerialNumber;
        CLI.Item_SKU__c = Someval.ItemSKUNumber;
        CLI.Item_Seq_Number__c = Someval.ItemSaleSeqNumber;
        CLI.Invoice_Number__c = Someval.ItemInvoiceNumber;
        if (Someval.ItemInvoiceDate == null || Someval.ItemInvoiceDate == ''){
            CLI.Invoice_Date__c = null;
        }else{
            CLI.Invoice_Date__c = Date.parse(Someval.ItemInvoiceDate);
        }
        if (Someval.DeliveryDate == null || Someval.DeliveryDate == ''){
            CLI.Delivery_Date__c = null;
        }else{
            CLI.Delivery_Date__c = Date.parse(Someval.DeliveryDate);
        }
        if (Someval.OrderShipDate == null || Someval.OrderShipDate == ''){
            CLI.Part_Order_Shipping_Date__c = null;
        }else{
            CLI.Part_Order_Shipping_Date__c = Date.parse(Someval.OrderShipDate);
        }
        CLI.Item_Defect__c = Someval.ItemDefect;
        CLI.Item_Description__c = Someval.ItemDescription;
        CLI.Item_Resolution__c = Someval.PieceExchangedFixed;
        CLI.Part_Order_Tracking_Number__c = Someval.PartOrderTrackNumber;
        CLI.Sales_Order_Number__c = Someval.ItemSaleNumber;
        CLI.Part_Order_Ship_To_Info__c = Someval.OrderShipToInfo;
        CLI.Part_Order_Status__c = Someval.OrderStatus;
        CLI.Part_Order_Number__c = Someval.OrderNumber;
        CLI.Customer_Number__c = Someval.CustomerID;
        CLI.Fulfiller_ID__c = Someval.AccountShipto;
        CLI.Legacy_Service_Request_ID__c = Someval.RequestID;
        CLI.Legacy_Service_Request_Item_ID__c = Someval.ItemUniqueID;
        CLI.Address_1__c = Someval.ShipToAddress1;
        CLI.Address_2__c = Someval.ShipToAddress2;
        CLI.City__c = Someval.ShipToCityName;
        CLI.State__c = Someval.ShipToStateCode;
        CLI.Zip__c = Someval.ShipToZipCode;
        CLI.Legacy_Last_Modified_User__c = Someval.LastModifiedUserName;
        system.debug('CLI--'+CLI);
        return CLI;
    }
    public static Case createNewCase(Id accId, CaseLineItemWrap Someval, List<Contact> contactList, List<Address__c> myAddress){
        Case thisCase = new Case();
        Id myContactId = null;
		for(Integer i = 0; i < contactList.size(); i++){
			if(contactList[i].AccountId == accId){
				myContactId = contactList[i].ID;
				break;
			}
		} 
        thisCase.Status = 'Open';
        thisCase.Type = 'Migration';
        thisCase.Sub_Type__c = 'Temporary Status';
        thisCase.Description = 'Temporary Status';
        thisCase.Subject ='Migration : Temporary Status';
        thisCase.Type_of_Resolution__c = 'No';
        thisCase.Resolution_Notes__c = 'A CC Agent Needs to Review - this Case came from HOMES';
        thisCase.Origin = 'Phone';
        thisCase.ContactId = myContactId;
        thisCase.AccountId = accId;
        thisCase.Legacy_Service_Request_ID__c = Someval.RequestID;
		thisCase.Market__c = Someval.MarketAccount;
		thisCase.Profit_Center__c = Someval.StoreNameStoreNumberPC;
        thisCase.Legacy_Account_Ship_To__c = Someval.AccountShipto;
        thisCase.Address__c = AddressHelper.getCustomerEDAAddress(Someval.SFDCAccountId, Someval.ShipToAddress1, Someval.ShipToAddress2, Someval.ShipToCityName, Someval.ShipToStateCode, Someval.ShipToZipCode, myAddress, 'Line Item - Case Insert');		
        if(string.isNotBlank(Someval.ItemSaleNumber)){		
            string SOexternalID = Someval.ItemSaleNumber + ':' + Someval.SFDCAccountId;		
            thisCase.Sales_Order__c = SOexternalID;		
        }		
system.debug('thisCase--'+thisCase);
        insert thisCase;
        return thisCase;
    }
}