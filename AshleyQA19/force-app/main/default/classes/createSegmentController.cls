public class createSegmentController
{
	@AuraEnabled
    Public Static Segment__c createRecord(Segment__c newSegment)
    {
        System.debug('newSegment----'+newSegment);
        if(newSegment !=null){
            insert newSegment;
        }
        return newSegment;
    } 
    
    @AuraEnabled
	public static List<ListView> getListViews() {
    	List<ListView> listviews = [SELECT Id, Name FROM ListView WHERE SobjectType = 'Segment__c' and Name='All'];
     
    return listviews;
   }

    @AuraEnabled 
    Public Static Segment__c updtRecord(Segment__c updSegment)  
    { 
           
        update updSegment;   
        return updSegment; 
    }

    @AuraEnabled
    public static List<string> getMarketValues()
    {
        List<String> marketList = new List<String>();
        for (ADS_Zip__c zip : [SELECT Market__c FROM ADS_Zip__c]) {
            if (!marketList.contains(zip.Market__c)) {
                marketList.add(zip.Market__c);
            }
        }
        return marketList;
    }
}