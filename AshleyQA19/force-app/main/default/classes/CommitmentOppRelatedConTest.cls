@iSTest
public class CommitmentOppRelatedConTest {
    static testMethod void testmethod1(){
    
        Account acc = new Account();
        //acc.name = 'Acc Name';
        acc.LastName = 'Account Name';
        acc.BillingCity = 'testcity';
        acc.BillingPostalCode = '67872';
        acc.BillingState = 'teststate';
        acc.BillingStreet = 'teststreet';
        acc.PersonEmail = 'test@test.com';
        acc.Phone = '9876543210';
        insert acc;
    
        Opportunity opp = new Opportunity();
        
        opp.AccountId = acc.Id;
        opp.StageName = 'Open';
        opp.Name = 'Testing opp';
        opp.CloseDate = System.today() + 5;
        insert opp;
    
        Commitment__c com = new Commitment__c();
        com.Complete__c = 10;
        com.Commitment_Opportunity_Amount__c = 123;
        com.Destination_Market__c = 'Minneapolis Mkt';
        com.Goal_Completion_Date__c = system.today();
        com.Goal_Empty__c = 12;
        com.Number_of_In_Progress_Opportunities__c =12;
        com.Number_Trips_Required__c=10;
        com.Number_Opportunities__c=12;
        com.Origin_Market__c='Fargo';
        com.Status__c='working';
        insert com;
        
        Commitment_Opportunities__c co =new Commitment_Opportunities__c();
        co.Lanes__c = 10;
        co.Trips_Per_Week__c=5;
        co.Commitment__c=com.id;
        co.OppLanes__c = 'Lane-00216';
        co.Opportunity__c=opp.id;
        insert co;
        
        
        Id COId = 'a0q4F000000bm79QAA';
        Id recid = 'a0q4F000000bm79QAA';
            
        CommitmentOppRelatedCon.getComOpp(co.id);
        CommitmentOppRelatedCon.getComOppVal(co.id);
    }
}