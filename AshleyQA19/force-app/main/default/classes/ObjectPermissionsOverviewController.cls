public class ObjectPermissionsOverviewController {
	public List<ObjectPermissions> oplist{get;set;}
    public List<opPfWrapper> opPflist {get;set;}
    public ObjectPermissionsOverviewController(){
        List<ObjectPermissions> ops= new List<ObjectPermissions>();
        Set<String> opts = getobjNames();
        System.debug('opts-->'+opts);
           ops = [select Id, Parent.Profile.Name, SobjectType, PermissionsCreate, PermissionsDelete, PermissionsEdit, PermissionsModifyAllRecords, PermissionsRead, PermissionsViewAllRecords from ObjectPermissions WHERE SobjectType IN :opts AND Parent.Profile.Name != null AND Parent.Profile.Name != 'System Administrator' AND ID != NULL];
        System.debug('ops-->'+ops);
        Set<String> Ids = new Set<String>();
        for(ObjectPermissions op : ops){
            System.debug('op-->'+op.Id);
          //  if(op.Id != null && op.Id != ''){
                System.debug('op----->'+op.Id);
                Ids.add(op.Id);
          //  }
        }
        System.debug('Ids-->'+Ids);
	//	this.oplist = [select Id, Parent.Profile.Name, SobjectType, PermissionsCreate, PermissionsDelete, PermissionsEdit, PermissionsModifyAllRecords, PermissionsRead, PermissionsViewAllRecords from ObjectPermissions WHERE SobjectType IN :opts AND Id != NULL AND Parent.Profile.Name != null AND Parent.Profile.Name != 'System Administrator' AND Id NOT IN :Ids LIMIT 1000];
	   this.oplist = [select Id, Parent.Profile.Name, SobjectType, PermissionsCreate, PermissionsDelete, PermissionsEdit, PermissionsModifyAllRecords, PermissionsRead, PermissionsViewAllRecords from ObjectPermissions WHERE SobjectType IN :opts AND Parent.Profile.Name != null AND Parent.Profile.Name != 'System Administrator' AND ID != NULL LIMIT 1000];
        List<String> PN = new List<String>();
        for(ObjectPermissions op : oplist){
            System.debug('op-->'+op.Id);
          //  if(op.Id != null && op.Id != ''){
                System.debug('op----->'+op.Parent.Profile.Name);
                PN.add(op.Parent.Profile.Name);
          //  }
        }
       // List<String> PNList = new List<String>(PN);
        System.debug('oplist-->'+oplist);
        Map<String,Integer> elCount = new Map<String,Integer>();
        for(String key : PN)
        {
            if(!elCount.containsKey(key)){
                elCount.put(key,0);
            }
            Integer currentInt=elCount.get(key)+1;
            elCount.put(key,currentInt);
        }
        List<opPfWrapper> opw  = new List<opPfWrapper>();
        for (String key : elCount.keySet()) {
            opPfWrapper opf = new opPfWrapper();
            opf.ProfileName = key;
            opf.NumberOfCustomObject = elCount.get(key);
            if(opf!= null){
            opw.add(opf);
            }
        }
        this.opPflist = opw;
        System.debug('opPflist-->'+opPflist);
    }
    
    public set<String> getobjNames()
    {
        List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();
        System.debug('gd-->'+gd);
        set<String> options = new set<String>();
        for(Schema.SObjectType f : gd)
        {
            if(f.getDescribe().getName().contains('__c'))
                options.add(f.getDescribe().getName());
        }
        System.debug('options-->'+options);
        return options;
    }
    public class opPfWrapper
    {
        public String ProfileName {get; set;}
        public Integer NumberOfCustomObject {get; set;}
    }
	
}