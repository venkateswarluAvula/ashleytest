public class LRRTriggerhandler{
    
    public static string approverEmail;
    
    @future(callout=true)
    public static void LRRApprovalProcess(set<Id> LRRId) {
        
        system.debug('LRRId---'+LRRId);        
        Legal_Review_Request__c lrr = [select id,Name,Approver_By__c,Status__c,Account__c,Account__r.Name from Legal_Review_Request__c where id=:LRRId ];
        system.debug('lrr---'+lrr);
        
        
        //----- verifying the approver and status from object and submitting the approval process -----
        String firstName = UserInfo.getFirstName();
        String lastName = UserInfo.getLastName();
        string FullName = lastName+' '+firstName;
        
        if(lrr.Approver_By__c == 'Operations Review' && lrr.Status__c == 'Rejected'){
            system.debug('entered if---');
            
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval by - '+FullName);
            req1.setObjectId(lrr.Id);
            
            // Submit on behalf of a specific submitter
            req1.setSubmitterId(UserInfo.getUserId()); 
            
            // Submit the record to specific process and skip the criteria evaluation
            req1.setProcessDefinitionNameOrId('Legal_process_Review_Contract_submit_V2');
            req1.setSkipEntryCriteria(true);
            
            // Submit the approval request for the Legal
            if(Test.isRunningTest()){
                
            }else{
            	Approval.ProcessResult result = Approval.process(req1);
            }
            
        }
        else if(lrr.Approver_By__c == 'Legal Review' && lrr.Status__c == 'Rejected'){
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(lrr.Id);
            
            // Submit on behalf of a specific submitter
            req1.setSubmitterId(UserInfo.getUserId()); 
            
            // Submit the record to specific process and skip the criteria evaluation
            req1.setProcessDefinitionNameOrId('Legal_process_Review_Contract_submit_V1');
            req1.setSkipEntryCriteria(true);
            
            // Submit the approval request for the Legal
            if(Test.isRunningTest()){
                
            }else{
            	Approval.ProcessResult result = Approval.process(req1);
            }
        }
           
        //----- getting the current process instance details------
        
        List<ProcessInstance> PI = [SELECT Id,TargetObjectId, 
                               (SELECT Id, ProcessNodeId,StepStatus, actorid,actor.Name,actor.Email FROM StepsAndWorkitems where StepStatus = 'pending') 
                               FROM ProcessInstance where TargetObjectId =: lrr.Id AND Status ='Pending'];
        system.debug('PI---1-'+PI);
        
        string approverEmail = '';
        Id ProcessNodeId;
        Id approverId;
        for(ProcessInstance pro : PI){
            system.debug('pro----'+pro); system.debug('pro---s-'+pro.StepsAndWorkitems);
            for(ProcessInstanceHistory stp : pro.StepsAndWorkitems){
                system.debug('stp----'+stp);
                ProcessNodeId = stp.ProcessNodeId;
                approverId = stp.actorid;
                if(String.isEmpty(approverEmail)){
                    approverEmail = stp.actor.Email;
                }else{
                    approverEmail = approverEmail+';'+stp.actor.Email;
                }
                
            }
        }
        system.debug('approverEmail----'+approverEmail);
        system.debug('ProcessNodeId----'+ProcessNodeId);
        system.debug('approverId----'+approverId);
        
        //----- updating the user record for avoiding the default approval email from salesforce -----
        List <String> userEmails = new List <String>();
        if(Test.isRunningTest()){
            userEmails.add('karthik.valakonda@d4insight.com');
        }else{
        	userEmails = approverEmail.split(';');
            system.debug('userEmails---'+userEmails);
        }
        List<user> usr = [select id,Email,UserPreferencesReceiveNoNotificationsAsApprover from User where Email=:userEmails]; 
        system.debug('usr----'+usr);
        for(user u : usr){
            u.UserPreferencesReceiveNoNotificationsAsApprover = true;
            update u;
            system.debug('u----'+u);
        }
        
        //----------------------Mailing section---------------
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List <String> addressList = new List <String>();
        
        OrgWideEmailAddress[] owea = [select Id,Address from OrgWideEmailAddress where Address =: system.label.Legal_Process_Review];
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        String replyToaddress = system.label.LPR_Inbound_Email_Address;
        mail.setReplyTo(replyToaddress);
        mail.setSubject('Email From Salesforce ADS Team'+' RecId:'+lrr.Id+' ProcessNodeId:'+ProcessNodeId);
        string loggedInUser = UserInfo.getUserEmail();
        
        if(Test.isRunningTest()){
            mail.setToAddresses(new String[] { 'karthik.valakonda@d4insight.com' });
        }else{
            if(lrr.Approver_By__c == 'Legal Review' && lrr.Status__c == 'Approved'){
                List<String> mailList = new List<String>();
                Group g = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE name =: system.label.LRR_Final_Email_Group_Members];
                for (GroupMember gm : g.groupMembers){
                    mailList.add(gm.userOrGroupId);
                }
                User[] tousr = [SELECT email FROM user WHERE id IN :mailList];
                for(User u : tousr){
                    addressList.add(u.email);
                    system.debug('addressList---'+addressList);
                    mail.setToAddresses(addressList);
                } 
            }else{
                addressList = approverEmail.split(';');
                system.debug('addressList---'+addressList);
                mail.setToAddresses(addressList);
            }
        }
        
        if(lrr.Approver_By__c == 'Legal Review' && lrr.Status__c == 'Approved'){
            mail.setHtmlBody('The contract for '+lrr.Account__r.Name+' has been accepted by Legal. Please distribute to the proper individuals.');
        }else{
            mail.setHtmlBody( 'Please review the contract attached. Reply with "Accept" or "Reject" after review. If "Reject", Please attach the new edited contract.');
        	//mail.setHtmlBody('Contract is Submitted by ' + FullName + ' and Waiting for Approval');
        }
        
        
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        
        //--- getting documets(Attachments) from Content Document ---
        //if(lrr.Status__c == 'Approved'|| lrr.Status__c == '' || lrr.Status__c == null ){
            for (ContentDocumentLink a : [SELECT ContentDocumentId,Id,LinkedEntityId,ContentDocument.title, ContentDocument.FileExtension FROM ContentDocumentLink WHERE LinkedEntityId = :lrr.Id order by ContentDocument.createdDate desc Limit 1] ){
                for (ContentVersion docVersion : [Select Id, VersionData from ContentVersion where ContentDocumentId =:a.ContentDocumentId ]) {
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                    string fileName = a.ContentDocument.title + '.' + a.ContentDocument.FileExtension;
                    efa.setFileName(fileName);
                    efa.setBody(docVersion.VersionData); 
                    fileAttachments.add(efa);
                    system.debug('CD attachment added---');
                }
            }
        //}
        //--- getting documets from Attachments ---
        if(lrr.Status__c == 'Approved' || lrr.Status__c == 'Rejected'){
            List<Attachment> attachfile = [SELECT Id, Name, Body, ContentType, ParentId From Attachment where ParentId=:lrr.Id order by CreatedDate desc limit 1];
            for(Attachment attach: attachfile){
                if(attach != null){
                    Messaging.Emailfileattachment mefa = new Messaging.Emailfileattachment();
                    mefa.setFileName(attach.Name);
                    mefa.setBody(attach.Body);
                    mefa.setContentType(attach.ContentType);
                    fileAttachments.add(mefa);
                }
            }
            
        }
        
        Integer attachsize = fileAttachments.size();
        if(attachsize > 0){
            system.debug('fileAttachments--'+ attachsize);   system.debug('lastfileAttachments--'+ fileAttachments[attachsize-1]);
            Messaging.Emailfileattachment singlefileAttachment = new Messaging.Emailfileattachment();
            singlefileAttachment = fileAttachments[attachsize-1];
            
            List<Messaging.Emailfileattachment> lastfileAttachment = new List<Messaging.Emailfileattachment>();
            lastfileAttachment.add(singlefileAttachment);
            
            mail.setFileAttachments(lastfileAttachment);
        }
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        system.debug('mail---'+mail);
        
        //-----inserting Email sent to the user from salesforce into object start-----  
        EmailMessage LRREmailMessage = new EmailMessage();
        LRREmailMessage.ToAddress =  String.join(mail.toAddresses, ',');
        system.debug('Email to Address'+mail.toAddresses);
        LRREmailMessage.FromAddress = system.label.Legal_Process_Review;
        //LRREmailMessage.Subject = ('Contract is Submitted by ' + FullName + ' and Waiting for Approval');
        LRREmailMessage.Subject = mail.subject;
        LRREmailMessage.status = '2';
        LRREmailMessage.HtmlBody = mail.htmlBody;
        LRREmailMessage.Incoming= True;
        LRREmailMessage.TextBody = mail.htmlBody;
        LRREmailMessage.RelatedToId = LRR.Id;
        
        insert LRREmailMessage;
        system.debug('LRREmailMessage---'+LRREmailMessage);
        //----------------inserting email End-------------
        
        //---- reverting the changes for user ------
        for(user u : usr){
            u.UserPreferencesReceiveNoNotificationsAsApprover = false;
            //update u;
            system.debug('u--1--'+u);
        }
        
    }
    
    
    @future(callout=true)
    public static void SendingSMS(Set<Id> LRRId){
        
        //id objId = LRRId;
        system.debug('approverEmail--'+approverEmail);
        
        Legal_Review_Request__c lrr = [select id,Name,Approver_By__c,Status__c from Legal_Review_Request__c where id=:LRRId ];
        system.debug('lrr--2-'+lrr);
        
        List<ProcessInstance> PI = [SELECT Id,TargetObjectId, 
                              (SELECT Id, ProcessNodeId,StepStatus, actorid,actor.Name,actor.Email FROM StepsAndWorkitems where StepStatus='pending') 
                              FROM ProcessInstance where TargetObjectId =: lrr.Id];
        system.debug('PI---'+PI);
        
        string approverid = '';
        for(ProcessInstance pro : PI){
            system.debug('pro----'+pro.StepsAndWorkitems);
            for(ProcessInstanceHistory stp : pro.StepsAndWorkitems){
                system.debug('stp----'+stp.actorid);
                
                if(String.isEmpty(approverid)){
                    approverid = stp.actorid;
                }else{
                    approverid = approverid+';'+stp.actorid;
                }
                
            }
        }
        system.debug('approverid---'+approverid);
        List <Id> userIds = approverid.split(';');
        system.debug('userIds---'+userIds);
        
        //user usr = [SELECT Id,Phone,MobilePhone FROM User where Id=:UserInfo.getUserId()];
        List<user> usr = [SELECT Id,Phone,MobilePhone FROM User where Id=:userIds];
        system.debug('usr---'+usr.size()+'---'+usr);
        
        User u = [select Id, Name,username from User where Id = :UserInfo.getUserId()];

        string message = 'approval is submitted by '+u.Name+', click Here '+URL.getSalesforceBaseUrl().toExternalForm()+'/lightning/r/Legal_Review_Request__c/'+LRRId+'/view  login Salesforce to view and reply.';
        
        for(user usrid : usr){
            system.debug('usrid------'+usrid);
            if(usrid.Phone != null || usrid.Phone != ''){
                TwilioSendSMSController.LRRsendTextMessageViaTwilio(lrr.Id, usrid.Phone, message);
            }else if(usrid.MobilePhone != null || usrid.MobilePhone != ''){
                TwilioSendSMSController.LRRsendTextMessageViaTwilio(lrr.Id, usrid.MobilePhone, message); 
            }
        }
        
    }
    
    
}