@isTest
public class ProductLineItemTriggerTest {
    public static testmethod void ProductLineItemTriggerTest(){
        //Create Account
        Account delacct = new Account();
       // delacct.recordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CUSTOMER_PERSON_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
        delacct.LastName = 'last' + Math.random();
        delacct.FirstName = 'first' + Math.random();
        delacct.PersonEmail = delacct.FirstName + '.' + delacct.LastName + '@test.com';
        delacct.Primary_Language__pc = 'English';
        string phone = '888' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        string phone2 = '888' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        string phone3 = '889' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        delacct.phone = phone;
        delacct.Phone_2__pc = phone2;
        delacct.Phone_3__pc = phone3;
        delacct.PersonEmail = 'pqr@mm.com';
        delacct.Email_2__pc ='pqr@mm.com';
        delacct.Strike_Counter__pc = 10;
        Insert delacct;
        
        //Create Case
        Case c = new Case();
        c.Status = 'New';
        c.Priority = 'Medium';
        c.Origin = 'Web';
        c.Subject = 'Subject ' +  Math.random();
        c.Type = 'Delivery Order inquiry';
        c.Sub_Type__c = 'Parts Issue';			
        c.AccountId = delacct.Id;
        c.Type_of_Resolution__c = 'Yes';
        c.Sales_Order__c = '200453150:001q000000raI3BAAU';
        c.Case_Email__c = 'ssaha@ashleyfurniture.com';
        Insert c;
        
        //Create PLI
        ProductLineItem__c PLI = new ProductLineItem__c();
        PLI.Case__c = c.Id;
        PLI.Part_Order_Tracking_Number__c = '';
        PLI.Defect_Location__c = 'BACK';
        PLI.Fulfiller_ID__c = '8888300-164';
        PLI.Part_Order_Number__c = 'SF001321';
        insert PLI;
        
        PLI.Part_Order_Tracking_Number__c = '34567';
        update PLI;
    }

}