public class AccountDeliveryConfirmation {
@AuraEnabled
    public Static string accDelCnf(String accID) 
    {
        AccDelAuthorization accda = new AccDelAuthorization();
        system.debug('accId------Delivery-----'+ accID);
        //SalesOrder__x so = new SalesOrder__x();
        Account acc = new Account();
        if(Test.isRunningTest())
        {
            acc = new Account(Id = '001q000000raI3DAAU');
        }
        else
        {
            acc = [Select Id from Account where Id=:accID];
        }
          String accessTkn = accda.accessTokenData();   
          String actId = acc.Id;
          String actIdEn = EncodingUtil.urlEncode(actId, 'UTF-8');	          
          String endpoint = 'SFContactID eq ' + '\'' + actIdEn + '\'';
          String replaceendpoint = endpoint.replaceAll(' ','%20');
          String endpoint1 = EncodingUtil.urlEncode(endpoint, 'UTF-8');
          String endpoint2 = endpoint1.replace('+','%20');
       
        String EPoint = System.Label.CaraSORoutingAPIEndPoint+'DeliveryConfirmation?$filter=' + endpoint2;
        EPoint=  Epoint.replace('+','%20');
            system.debug('endpoint' + EPoint);
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer '+accessTkn);
        req.setHeader('Accept' ,'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(EPoint);
        req.setTimeOut(120000);
        req.setMethod('GET');
        HttpResponse res = http.send(req);

        System.debug('apiresponse...' + res.getBody());
        
        return res.getBody();
    }

    @AuraEnabled
    public Static string soConfirmData(String accShipto, string soNumber, integer profitCenter, string trkID, string delDate, Integer rPass, string custOpen, string custClose)
    { 
        string responsebody;
        system.debug('here');
        system.debug('herevals' + accShipto);
        system.debug('herevals' + soNumber );
        system.debug('herevals' + profitCenter ); 
        system.debug('herevals' +  trkID );
        system.debug('herevals' + delDate );
        system.debug('herevals' +  rPass );
        //if(accShipto != null && soNumber !=null && profitCenter !=null && trkID !=null && delDate !=null && rPass !=null){
       
            User objUsr = [Select Id, Name, Alias,CommunityNickname from User where Id=:UserInfo.getUserId()];
            String currentUser = objUsr.CommunityNickname;//UserInfo.getLastName()+','+UserInfo.getFirstName();
        	String apikeys = System.label.AshleyApigeeApiKey;
            System.Debug('currentUser------' + currentUser);
           
             //SalesOrder__x so = [Select Id, phhSalesOrder__c,phhStoreID__c, ExternalId,phhProfitcenter__c,phhDatePromised__c from SalesOrder__x where Id=:soID];
             String EPoint = System.Label.AshleyApigeeEndpoint+accShipto+'/salesorders/'+soNumber+'/SalesOrderConfirmDelivery?profitcenter='+profitCenter+'&ConfirmedFlag='+true+'&TruckId='+trkID+'&DeliverDate='+delDate+' 00:00:00.000'+'&RoutingPass='+rPass+'&CustomerETAOpen='+custOpen+'.000&CustomerETAClose='+custClose+'.000&profitusername='+currentUser+'&apikey='+apikeys;
             //String EPoint = 'https://stageapigw.ashleyfurniture.com/homestores/8888300-164/salesorders/200462350/SalesOrderConfirmDelivery?profitcenter=23&ConfirmedFlag=true&TruckId=G03&DeliverDate=2017-05-26 00:00:00.000&RoutingPass=1&CustomerETAOpen=2017-05-26 08:45:00.000&CustomerETAClose=2017-05-26 12:45:00.000&profitusername=Ashley&apikey=bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713';
            EPoint = EPoint.replace(' ','%20');
        system.debug('EPoint'+EPoint);
            String response;
            
            Http http = new Http();
            HttpRequest httpReq = new HttpRequest();
            HttpResponse httpResp = new HttpResponse();
            httpReq.setHeader('apikey',System.label.AshleyApigeeApiKey);
            //httpReq.setHeader('Authorization', 'Bearer '+accessTkn);
            httpReq.setHeader('Content-Type', 'application/json');
            httpReq.setHeader('Accept' ,'application/json');
            httpReq.setMethod('PUT'); 
            httpReq.setTimeout(120000);
            httpReq.setBody('{}');
            httpReq.setEndpoint(EPoint);
            
            httpResp = http.send(httpReq); 
             System.debug('httpResp-------Hstry--------'+httpResp);
            if (httpResp.getStatusCode() == 200) {
                System.debug('httpResp-------Hstryenter--------'+httpResp.getBody());
                response = httpResp.getBody();
                System.debug('response--------'+response);
                if(response.contains('successfully'))
                {
                 responsebody = response;   
                } 
            }
        else if (httpResp.getStatusCode() == 404) {
            response = httpResp.getBody();
            if (response.contains('TransportationOrderId')){
                if(Test.isRunningTest()){

                }else{
            throw new AuraHandledException('TransportationOrderId not found for SalesOrderNumber:'+soNumber+' and ProfitCenter: '+profitCenter);
                }
                }
            else{
                throw new AuraHandledException('The request could not be processed');
            }
        }
        return responsebody;
    }
    
    @AuraEnabled
    public static string getSalesOrderLineItem (id salesOrderId, string salesordernum){
     string str;  
     system.debug('salesOrderId');
        List<SalesOrder__x> salesorderData= new List<SalesOrder__x>();
        Map<String, SalesOrder__x> SalesorderMap = new Map<String, SalesOrder__x>();
       
        salesorderData=[Select Id,phhSalesOrder__c From SalesOrder__x  where phhGuestID__r.id =: salesOrderId];
        system.debug('salesorderData----'+salesorderData);
       
        for(SalesOrder__x tt:salesorderData)
        {
            SalesorderMap.put(tt.phhSalesOrder__c,tt);  
        }
        if(SalesorderMap.containsKey(salesordernum)){
           str = SalesorderMap.get(salesordernum).Id;
        }                                                                  
 		return str;
     
    }
    
}