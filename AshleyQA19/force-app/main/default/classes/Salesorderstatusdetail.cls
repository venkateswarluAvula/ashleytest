public class Salesorderstatusdetail {

  @AuraEnabled
    public Static String sodetailData(String soID) {
        
        SORoutingAuthorization sora = new SORoutingAuthorization();
        system.debug('SOId------routing-----'+ soID);
        SalesOrder__x so = new SalesOrder__x();
       if(Test.isRunningTest())
        {
            so = new SalesOrder__x(fulfillerID__c = '8888300-164',
                                                         ExternalId = '17331400:001q000000raDkvAAE',  
                                                         phhProfitcenter__c = 1234567,
                                                         Phhcustomerid__c = '784584585',
                                                         phhSalesOrder__c = '200442750',
                                                         phhStoreID__c = '133'
                                                         );
        }
        else
        {
            so = [Select Id, phhSalesOrder__c,phhStoreID__c,phhCustomerID__c, ExternalId from SalesOrder__x where Id=:soID];
        }
          String accessTkn = sora.accessTokenData();   
          String SONum = so.phhSalesOrder__c;
          String SONumEn = EncodingUtil.urlEncode(SONum, 'UTF-8');
          String strId = so.phhStoreID__c;//133
          Integer storeId = Integer.ValueOf(EncodingUtil.urlEncode(strId, 'UTF-8'));
          String endpoint = 'SaleOrderNo eq'+ ' ' + '\'' +SONumEn + '\'' +' '+'and StoreID eq'+' '+ storeId;
          String replaceendpoint = endpoint.replaceAll(' ','%20');
          String endpoint1 = EncodingUtil.urlEncode(endpoint, 'UTF-8');
          String endpoint2 = endpoint1.replace('+','%20');
       
        String EPoint = System.Label.CaraSORoutingAPIEndPoint+'SOStatusDetails?$filter=' + endpoint2;
        EPoint=  Epoint.replace('+','%20');
            system.debug('endpoint' + EPoint);
  // Http Callout 
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer '+accessTkn);
        req.setHeader('Accept' ,'application/json');
        req.setHeader('Content-Type', 'application/json');
         
          req.setEndpoint(EPoint);
          req.setTimeOut(120000);
          req.setMethod('GET');
          HttpResponse res = http.send(req);

          System.debug('apiresponse...' + res.getBody());
        
          return res.getBody();//resultsmap;
    }
    
}