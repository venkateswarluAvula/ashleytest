@RestResource(urlMapping='/NpsOptSurvey/*')
global with sharing class API_NpsOptSurvey {
    
    @HttpPost
    global static NpsResponseWrapper updateCustomerInfo() {
        NpsResponseWrapper result = new NpsResponseWrapper ();
        //result.message = 'There are no records to update';
        //result.isSuccess = false;
        // to switch off the integration that sends accounts and NPS checkboxes back to CARA
       
        Savepoint sp = Database.setSavepoint();
        
        try {
            RestRequest request = RestContext.request;
            system.debug('request.requestBody--'+request.requestBody);
               List<NpsWrapper> customerList = APINpsHelper.parseNpsRequest(request.requestBody.toString());
               NpsOptController.updateAllNpsRecords(customerList); 
            if (request.requestBody.toString() != ''){ result.message = 'The records are successfully updated';  
               result.isSuccess = true;
               }
            else{
                result.message = 'There are no records to update'; result.isSuccess = false;
            }
        } catch (Exception e) {
            Database.rollback(sp);
            system.debug(e.getMessage());
            new ErrorLogController().createLog(
                new ErrorLogController.Log(
                    'API_NpsOptSurvey', 
                    'updateNPSOptText', 
                    'NPSOptText, Text Opt Exception: ' + e.getMessage() + 
                    ' Stack Trace: ' + e.getStackTraceString()
                )
            );
            
            RestContext.response.statusCode = 500;
            result.message = 'NPS list is failed: ' +e.getMessage();
            result.isSuccess = false;
        }
       
        return result;
    }
}