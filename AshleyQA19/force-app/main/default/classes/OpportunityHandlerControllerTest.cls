@isTest
public class OpportunityHandlerControllerTest{
    static testMethod void OpportunityTestData() {
        
        ADS_Zip__c adszip = new ADS_Zip__c();
        adszip.City__c = '100 PALMS';
        adszip.State_Code__c = 'CA';
        adszip.Market__c = 'Redlands Mkt';
        adszip.Region__c = 'WEST';
        adszip.Zip_Code__c = '22225';
        adszip.City_State_Code__c = '100 PALMSCA';
        Insert adszip;
        
        
        Opportunity Opp = new Opportunity();
        Opp.Name = 'Test';
        Opp.StageName = 'Prospect';
        Opp.CloseDate = system.Today();
        Opp.Origin_Zip_Code__c = adszip.Zip_Code__c;
        Opp.Origin_Market__c = adszip.Market__c;
        Opp.Origin_Region__c = adszip.Region__c;
        Opp.Origin_City__c= '100 PALMS';
        Opp.Origin_State__c= 'CA';
        
        Opp.Destination_City__c= '100 PALMS';
        Opp.Destination_State__c= 'CA';
        Opp.Destination_Zip_Code__c  = adszip.Zip_Code__c;
        Opp.Destination_Market__c = adszip.Market__c;
        Opp.Destination_Region__c = adszip.Region__c;
        
        Insert Opp;
        
        List<Opportunity> adslist = new List<Opportunity>();
        adslist.add(Opp);
        
        Test.startTest();
            OpportunityHandlerController.ADSZipandMrktRegnUpdate(adslist);
        Test.stopTest();
    }
}