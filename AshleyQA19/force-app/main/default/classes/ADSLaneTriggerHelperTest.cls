@isTest
public class ADSLaneTriggerHelperTest {
    static testMethod void ADSTestData() {
        
        ADS_Zip__c adszip = new ADS_Zip__c();
        adszip.City__c = '100 PALMS';
        adszip.State_Code__c = 'CA';
        adszip.Market__c = 'Redlands Mkt';
        adszip.Region__c = 'WEST';
        adszip.Zip_Code__c = '22225';
        Insert adszip;
        
        
        ADS_Lane__c ADS = new ADS_Lane__c();
        
        ADS.ADS_Lane_Origin_Zip__c = adszip.Zip_Code__c;
        ADS.ADS_Lane_Origin_Market__c = adszip.Market__c;
        ADS.ADS_Lane_Origin_City__c = '100 PALMS';
        ADS.ADS_Lane_Origin_State__c = 'CA';
        ADS.Origin_Region__c = adszip.Region__c;
        ADS.ADS_Lane_Destination_City__c = '100 PALMS';
        ADS.ADS_Lane_Destination_State__c = 'CA';
        ADS.ADS_Lane_Destination_Zip__c = adszip.Zip_Code__c;
        ADS.ADS_Lane_Destination_Market__c = adszip.Market__c;
        ADS.Destination_Region__c = adszip.Region__c;
        ADS.of_Trips_Per_Week__c = 10;
        Insert ADS;
        
        List<ADS_Lane__c> adslist = new List<ADS_Lane__c>();
        adslist.add(ADS);
        
        Test.startTest();
            ADSLaneTriggerHelper.LoadZipMarket(adslist);
        Test.stopTest();
    }
 
}