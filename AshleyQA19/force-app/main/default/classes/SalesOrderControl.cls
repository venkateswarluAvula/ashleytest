global class SalesOrderControl {
    @AuraEnabled
    Public static String SalesOrderHotStatus(String startdate,String enddate,String market){
        SORoutingAuthorization CARAPart= new SORoutingAuthorization();
        String accessTkn = CARAPart.accessTokenData();
        String endpoint = System.label.HotOrdersAPI +'startDate='+startdate+'&endDate='+enddate+'&marketID='+market;
        System.debug('endpoint----' + endpoint);
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer '+accessTkn);
        req.setHeader('Accept' ,'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(endpoint);
        req.setTimeOut(120000);
        req.setMethod('GET');
        HttpResponse res = http.send(req);
        System.debug('apiresponse...' + res.getBody());
        return res.getBody();//resultsmap;
    }
           
    @AuraEnabled
    public static String searchAccountsSOQL(String searchString) {
       Salesorder__x Order = new Salesorder__x();
        if(Test.isRunningTest()){}else{
       Order  = [SELECT Id,ExternalId FROM SalesOrder__x WHERE ExternalId =:searchString];
        }
       system.debug('order==>'+Order);
        return Order.Id;
    }
    
    @AuraEnabled
    public static List<HotOrders__c> marketvalues(){
        List<HotOrders__c> options = new List<HotOrders__c>();
        options = HotOrders__c.getAll().values();
        System.debug('values----' +options);
        return options;
    }
}