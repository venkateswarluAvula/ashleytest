public class CaseHistoryController {
    
    @AuraEnabled
    public static List<caseHistory> getcaseHistoryrecords(String parentRecordId){
        List<caseHistory> lstOfRec = [select id,CaseId,toLabel(Field),NewValue,OldValue,CreatedDate,CreatedBy.Name 
                                      from caseHistory where CaseId =: parentRecordId ORDER BY CreatedDate DESC];
        // ORDER BY Id is changed to CreatedDate  modified by Prasad.
        
        List<caseHistory> chList = New List<caseHistory>();
        string Nval;
        object newvalue;
        for(caseHistory ch : lstOfRec){
            if(test.isRunningTest()){
                newvalue = 'test';
            }else{
                newvalue = ch.NewValue;
            }
            if(newvalue != Null){
                Nval = string.ValueOf(newvalue);
                if(Nval.length() > 3){
                    if(Nval.substring(0, 3) != '005' && Nval.substring(0, 3) != '00G'){
                        chList.add(ch);
                    }
                } else{
                    chList.add(ch);
                }
            } else{
                chList.add(ch);
            }
        }
        lstOfRec = chList;
        return lstOfRec;
    }
}