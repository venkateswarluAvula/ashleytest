global class AddPreferredUpdBatch implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //List<String> idList= new List<String>{'0010n00000xtEhHAAU'};
        return Database.getQueryLocator([SELECT Id FROM Account]); 
    }
    global void execute(Database.BatchableContext bc, List<Account> accList) {
        //map to store address based on account id and address type
        Map<string, Map<string, List<Address__c>>> addressAccMap = new Map<string, Map<string, List<Address__c>>>();
        //temp map to store address based on address type
        Map<string, List<Address__c>> addressTypeMap;
        //temp list to store address
        List<Address__c> adrList;
        //map to store preferred address associated to the account
        Map<string, string> preferredAddressMap = new Map<string, string>();
        //get all the address associated to the account
        List<Address__c> addressList = [SELECT Id, AccountId__c, Address_Type__c, Preferred__c FROM Address__c WHERE AccountId__c IN: accList ORDER BY CreatedDate DESC];
        //based on the response arrange the address
        if (addressList.size() > 0) {
            for (Address__c ard:addressList) {
                //reset map
                addressTypeMap = new Map<string, List<Address__c>>();
                //reset list
                adrList = new List<Address__c>();
                //if there is an address associated to the account  
                if (addressAccMap.get(ard.AccountId__c) != null) {
                    //get the associated address for the account
                    addressTypeMap = addressAccMap.get(ard.AccountId__c);
                    //if there is an address for the associated address type
                    if (addressTypeMap.get(ard.Address_Type__c) != null) {
                        adrList = addressTypeMap.get(ard.Address_Type__c);
                        adrList.add(ard);
                        addressTypeMap.put(ard.Address_Type__c, adrList);
                    } else {
                        //if there is no address associated for the address type
                        addressTypeMap.put(ard.Address_Type__c, new list<Address__c>{ard});
                    }
                    addressAccMap.put(ard.AccountId__c, addressTypeMap);
                } else {
                    //if there is no address associated to the account then set the address type and add the address
                    addressTypeMap.put(ard.Address_Type__c, new list<Address__c>{ard});
                    //set the account and associated address based on address type
                    addressAccMap.put(ard.AccountId__c, addressTypeMap);
                }
                
                //get the preferred address
                if (ard.Preferred__c) {
                    preferredAddressMap.put(ard.AccountId__c, ard.Id);
                }
            }
        }
        system.debug('Address Map: ' + addressAccMap);
        system.debug('Preferred Address Map: ' + preferredAddressMap);
        
        //logic to prepare/set preferred address
        list<Address__c> updateAdrList = new list<Address__c>();
        for (Account acc:accList) {
            //reset map
            addressTypeMap = new Map<string, List<Address__c>>();
            //reset list
            adrList = new List<Address__c>();
            //if there is no preferred address for the loop over customer
            if (preferredAddressMap.get(acc.Id) == null) {
                system.debug('entered 63');
                //if there is a ship to address for the customer then set the first address as preferred
                if (addressAccMap.get(acc.Id) != null) {
                    addressTypeMap = addressAccMap.get(acc.Id);
                    if (addressTypeMap.get('Ship To') != null) {
                        adrList = addressTypeMap.get('Ship To');
                    } else if (addressTypeMap.get('Bill To') != null) {
                        adrList = addressTypeMap.get('Bill To');
                    } else if (addressTypeMap.get(null) != null) {
                        adrList = addressTypeMap.get(null);
                    }
                    
                    if (adrList.size() > 0) {
                        Address__c updAdr = new Address__c();
                        updAdr.Id = adrList[0].Id;
                        updAdr.Preferred__c = true;
                        updateAdrList.add(updAdr);
                    }
                }
            }
        }
        
        if(updateAdrList.size() > 0) {
            Database.SaveResult[] srList = Database.update(updateAdrList, false);
            List<string> errMsgList = new List<string>();
            for(Database.SaveResult sr : srList) {
                if(!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        system.debug(err.getStatusCode() + ': ' + err.getMessage());
                        errMsgList.add(err.getStatusCode() + ': ' + err.getMessage());
                    }
                }
            }
            if(errMsgList.size() > 0) {
                //insert in ErrorLog__c object
                ErrorLog__c errLog = new ErrorLog__c(Name = 'State Preferred Update Batch Error', ApexClass__c = 'AddPreferredUpdBatch', Method__c = 'execute', Message__c = string.join(errMsgList,' | '));
                insert errLog;
            }
        }
    }
    global void finish(Database.BatchableContext bc) {
        
    }
}