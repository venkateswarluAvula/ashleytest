@isTest
public class SalesOrderControl_Test {
    @isTest
    public static void Usecase(){
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
        mock.setStaticResource('soRoutingDatamockResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        HotOrders__c opt = new HotOrders__c();
        opt.Market_Id__c = '8888300-164';
        opt.Name = 'Kingswere - Atlanta - 8888300';
        insert opt;
        Test.startTest();
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        SalesOrderControl.SalesOrderHotStatus('01/31/2019', '01/31/2019', '8888300-164');
        SalesOrderControl.searchAccountsSOQL('200446970:001q000000raI3CAAU');
        SalesOrderControl.marketvalues();
        Test.stopTest();
    }
    
    @isTest
    public static void searchAccountsSOQL() {
        Test.startTest();
        SalesOrder__x salesOrder = new SalesOrder__x(fulfillerID__c = '8888300-164',
                                                 ExternalId = '17331400:001q000000raDkvAAE',
                                                 phhProfitcenter__c = 1234567,
                                                 Phhcustomerid__c = '784584585',
                                                 phhSalesOrder__c = '88845758'
                                                );
        system.debug('order' + salesOrder);
        //SalesOrderControl.marketvalues();
        SalesOrderControl.searchAccountsSOQL(salesOrder.ExternalId);
        Test.stopTest();

    }
}