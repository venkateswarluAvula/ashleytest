@isTest
public class SendNotificationBatch_Test {
    @isTest
    static void test(){
        Account acc = new Account();
        acc.Name = 'Ashley';
        insert acc;
        System.debug('acc---->'+acc.id);
        
        Contact con = new Contact();
        con.AccountId = acc.id;
        con.LastName = 'Test';
        con.Email = 'test@gmail.com';
        insert con;
        System.debug('con---->'+con.id);
        
        SendNotificationBatch sch = new SendNotificationBatch();
        Test.startTest();
        sch.ScheduleDelete(acc.id);
        sch.execute(null);
        Test.stopTest();     
    }
}