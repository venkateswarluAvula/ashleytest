@isTest
public class CaseHistoryControllerTest {
    static testMethod void TestData() {
        
        List<Account> accList = new List<Account>();
        accList = TestDataFactory.initializeAccounts(1);
        insert accList;
        
        List<Contact> contList = new List<Contact>();
        contList = TestDataFactory.initializeContacts(accList[0].Id, 1);
        insert contList;
        
        // Insert Case
        Case caseObj = new Case(Sales_Order__c = '17331400:001q000000raDkvAAE', Status = 'New', Origin = 'Phone',
                                Type = 'Open Order Inquiry', Subject = 'Test',
                                Description = 'Test', AccountId = accList[0].Id, ContactId = contList[0].Id);
        insert caseObj;
        system.debug(caseObj);
        
        caseHistory ch = new caseHistory();
        ch.CaseId = caseObj.id;
        ch.Field = 'Created';
        Insert ch;
        
        caseObj.Description = 'test1';
        update caseObj;
        caseObj.Description = 'test2';
        update caseObj;
        caseObj.Description = 'test3';
        update caseObj;
        
        Test.startTest();
        CaseHistoryController.getcaseHistoryrecords(caseObj.id);
        Test.stopTest();
    }
}