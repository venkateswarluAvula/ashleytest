@isTest
public class AccountExportCSV_Test {
    public static testMethod void testaccountExport() {
        Id cId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Account acc = new Account();
        //acc.name = 'Acc Name';
        acc.LastName = 'Account Name';
        acc.BillingCity = 'testcity';
        acc.BillingPostalCode = '67872';
        acc.BillingState = 'teststate';
        acc.BillingStreet = 'teststreet';
        acc.PersonEmail = 'test@test.com';
        acc.Phone = '9876543210';
        acc.RecordTypeId = cId;
        insert acc;
        Opportunity opp = new Opportunity();
        
        opp.AccountId = acc.Id;
        opp.StageName = 'CLOSED WON';
        opp.Name = 'Testing opp';
        opp.CloseDate = System.today() + 5;
        insert opp;
        
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/AccountExportCSV';  //Request URL
        req.httpMethod = 'GET';//HTTP Request Type
        RestContext.request = req;
        RestContext.response= res;
        List<String> stas = AccountExportCSV.getAccountDetails();
        Test.stopTest();
    }
    public static testMethod void testaccountExport1() {
        Id cId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Account acc = new Account();
        //acc.name = 'Acc Name';
        acc.LastName = 'Account Name';
        acc.BillingCity = 'testcity';
        acc.BillingPostalCode = '67872';
        acc.BillingState = 'teststate';
        acc.BillingStreet = null;
        acc.PersonEmail = 'test@test.com';
        acc.Phone = '9876543210';
        acc.RecordTypeId = cId;
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.StageName = 'CLOSED WON';
        opp.Name = 'Testing opp';
        opp.CloseDate = System.today() + 5;
        insert opp;
        
        Opportunity opp1 = new Opportunity();
        opp1.AccountId = acc.Id;
        opp1.StageName = 'CLOSED WON';
        opp1.Name = 'Testing opp';
        opp1.CloseDate = System.today() + 5;
        insert opp1;
        
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/AccountExportCSV';  //Request URL
        req.httpMethod = 'GET';//HTTP Request Type
        RestContext.request = req;
        RestContext.response= res;
        List<String> stas = AccountExportCSV.getAccountDetails();
        Test.stopTest();
    }
    
    public static testMethod void testaccountExport2() {
        Id cId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
       Account acc = new Account();
        //acc.name = 'Acc Name';
        acc.LastName = 'Account Name';
        acc.BillingCity = 'testcity';
        acc.BillingPostalCode = '67872';
        acc.BillingState = 'teststate';
        acc.BillingStreet = null;
        acc.PersonEmail = 'test@test.com';
        acc.Phone = '9876543210';
        //acc.RecordTypeId = cId;
        insert acc;
        
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/AccountExportCSV';  //Request URL
        req.httpMethod = 'GET';//HTTP Request Type
        RestContext.request = req;
        RestContext.response= res;
        List<String> stas = AccountExportCSV.getAccountDetails();
        Test.stopTest();
    }
}