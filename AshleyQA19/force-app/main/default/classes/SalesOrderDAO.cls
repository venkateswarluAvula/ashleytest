/**
* This is a Data Access class that provides methods to access Sales Orders and Sales Order Line Items by Ids and External Ids. 
* External Objects can't be created in test context and the class provides a way to mock data that will be returned if the call is
* in test context. All methods in this class will identify the context of the call and return mocked results in test context or results
* from actual queries outside test context.
*/
public with sharing class SalesOrderDAO {
    @TestVisible 
    private static List<SalesOrder__x> mockedSalesOrders = new List<SalesOrder__x>();
    
    @TestVisible 
    private static List<SalesOrderItem__x> mockedSalesOrderLineItems = new List<SalesOrderItem__x>();
    
    public static List<SalesOrder__x> getOrdersByIds(List<Id> orderSalesforceIds) {
        SalesOrder__x so = new SalesOrder__x();
        if(Test.isRunningTest())
        {
          so = new SalesOrder__x( id ='x010n000000CuZuAAK',
            ExternalId = '17331400:001q000000raDkvAAE',
            phhHot__c = true,
            phhOrder_Notes__c = 'Test Type',
            phhDesiredDate__c = system.today(),
            fulfillerID__c = '1-',
            phhStoreID__c = '23-12345',
            phhDeliveryType__c = 'TW',
            phhProfitcenter__c  = 23,
            phhSaleType__c = 'POS',
            phhSalesOrder__c = '200460320',
            phhSOSource__c = 'POS9',
            phhCustomerName__c = 'DAVID HESSE',
            phhStoreLocation__c = 'TAMPA',
            phhReasonCode__c = 'Invalid delivery date',
            phhGuestID__c = '001e000001FinfrAAB',
            phhDatePromised__c = system.today(),
            phhWindowBegin__c = '07:00',
            phhWindowEnd__c = '19:00',
            phhASAP__c = false,
            phhServiceLevel__c = 'THD',
            VIPFlag__c = true,
            LIBFlag__c = true,
			IsMarketActive__c = true
                                  );
            mockedSalesOrders.add(so);
            return mockedSalesOrders;
        }
        
        List<SalesOrder__x> orders = [Select Id, ExternalId, phhHot__c, phhOrder_Notes__c, phhDesiredDate__c, fulfillerID__c, phhDeliveryComments__c, 
                                      phhStoreID__c, phhDeliveryType__c, phhProfitcenter__c, phhSaleType__c, phhSalesOrder__c,
                                      phhCustomerName__c, phhStoreLocation__c, phhReasonCode__c, phhGuestID__c, phhDatePromised__c,
                                      phhWindowBegin__c, phhWindowEnd__c, phhASAP__c, phhSOSource__c, IsMarketActive__c                        
                                      from SalesOrder__x  
                                      where Id in: orderSalesforceIds];
        
        return orders;
    }
    public static SalesOrder__x getOrderById(Id orderSalesforceId) {
        SalesOrder__x so = new SalesOrder__x();
        if(Test.isRunningTest())
        {
            so = new SalesOrder__x( id ='x010n000000CuZuAAK',
            ExternalId = '17331400:001q000000raDkvAAE',
            phhHot__c = true,
            phhOrder_Notes__c = 'Test Type',
            phhDesiredDate__c = system.today(),
            fulfillerID__c = '1-5678',
            phhStoreID__c = '23-12345',
            phhDeliveryType__c = 'TW',
            phhProfitcenter__c  = 23,
            phhSaleType__c = 'POS',
            phhSalesOrder__c = '200460320',
            phhSOSource__c = 'POS9',
            phhCustomerName__c = 'DAVID HESSE',
            phhStoreLocation__c = 'TAMPA',
            phhReasonCode__c = 'Invalid delivery date',
            phhGuestID__c = '001e000001FinfrAAB',
            phhDatePromised__c = system.today(),
            phhWindowBegin__c = '07:00',
            phhWindowEnd__c = '19:00',
            phhASAP__c = false,
            phhServiceLevel__c = 'THD',
            VIPFlag__c = true,
            LIBFlag__c = true,
            IsMarketActive__c = true
                                  );
            
            return so;
        }
        
        List<SalesOrder__x> orders = [Select Id, ExternalId, phhHot__c, phhOrder_Notes__c, phhDesiredDate__c, fulfillerID__c, phhDeliveryComments__c,
                                      phhStoreID__c, phhDeliveryType__c, phhProfitcenter__c, phhSaleType__c, phhSalesOrder__c,
                                      phhCustomerName__c, phhStoreLocation__c, phhReasonCode__c, phhGuestID__c, phhDatePromised__c, phhWindowBegin__c,
                                      phhWindowEnd__c, phhASAP__c, phhServiceLevel__c, IsMarketActive__c,
                                      VIPFlag__c, LIBFlag__c, phhSOSource__c, phhERPAccounShipTo__c, phhContactStatus__c
                                      from SalesOrder__x  
                                      where Id=: orderSalesforceId];
        
       // String cusname = orders[0].phhCustomerName__c;        
        //orders[0].phhCustomerName__c = cusname.replace('','\'');
        //system.debug('Testing ' + cusname);
        return (orders.size() > 0) ? orders[0] : null;
    }
    
    public static SalesOrder__x getOrderByExternalId(string externalId) {
        SalesOrder__x so = new SalesOrder__x();
        if(Test.isRunningTest())
        {
            so = new SalesOrder__x( id ='x010n000000CuZuAAK',
            ExternalId = '17331400:001q000000raDkvAAE',
            phhHot__c = true,
            phhOrder_Notes__c = 'Test Type',
            phhDesiredDate__c = system.today(),
            fulfillerID__c = '1-',
            phhStoreID__c = '23-12345',
            phhDeliveryType__c = 'TW',
            phhProfitcenter__c  = 23,
            phhSaleType__c = 'POS',
            phhSalesOrder__c = '200460320',
            phhSOSource__c = 'POS9',
            phhCustomerName__c = 'DAVID HESSE',
            phhStoreLocation__c = 'TAMPA',
            phhReasonCode__c = 'Invalid delivery date',
            phhGuestID__c = '001e000001FinfrAAB',
            phhDatePromised__c = system.today(),
            phhWindowBegin__c = '07:00',
            phhWindowEnd__c = '19:00',
            phhASAP__c = false,
            phhServiceLevel__c = 'THD',
            VIPFlag__c = true,
            LIBFlag__c = true,
			IsMarketActive__c = true
                                  );
            
            return so;
        }
        
        List<SalesOrder__x> orders = [Select Id, ExternalId, phhHot__c, phhOrder_Notes__c, phhDesiredDate__c, fulfillerID__c, 
                                      phhStoreID__c, phhDeliveryType__c, phhProfitcenter__c, phhSaleType__c, phhSalesOrder__c,
                                      phhCustomerName__c, phhStoreLocation__c, phhReasonCode__c, phhGuestID__c, phhDatePromised__c,
                                      phhWindowBegin__c, phhWindowEnd__c, phhASAP__c, phhSOSource__c, phhERPAccounShipTo__c, IsMarketActive__c
                                      from SalesOrder__x
                                      where ExternalId=: externalId];
        
        return (orders.size() > 0) ? orders[0] : null;
    }
    
    public static List<SalesOrderItem__x> getOrderLineItemsByOrderExternalId(string orderExternalId) {
        SalesOrderItem__x so = new SalesOrderItem__x();
        if(Test.isRunningTest())
        {
            so = new SalesOrderItem__x( Id = 'x00q00000000DMoAAM',
            ExternalId = '21111310:001q000000raI3DAAU',
            phdItemSKU__c = 'B502-87',
            phdItemDesc__c = 'Full Panel Headboard / Kaslyn / White',
            phdItemDesc2__c = 'Full Panel Headboard / Kaslyn / White',
            phdQuantity__c = 1,
            phdSalesOrder__c = '21111310',
            phdIsFPP__c = false,
            phdShipAddress1__c = '250 AMAL DR',
            phdShipAddress2__c = 'apt 102',
            phdShipCity__c = 'ATLANTA',
            phdShipState__c = 'GA',
            phdShipZip__c = '30315',
            phdInStoreQty__c = 0,
            phdInWarehouseQty__c = 0,
            phdWarrantyDaysLeft__c = 365,
            phdDeliveryType__c = 'G02',
            phdDeliveryTypeDesc__c = 'Take With',
            phdSaleType__c = 'POS',
            phdItemSeq__c = 10,
            phdOrderType__c = 'ORD(Exch)',
            phdPaymentType__c = 'Payment Test',
            phdRSA__c = 'HOU',
            phdAmount__c = 199.99,
            Ship_Customer_Name__c = 'HESSE',
            phdWholeSalePrice__c = 99.01,
            phdVendorName__c='vendor'
                                      );
            mockedSalesOrderLineItems.add(so);
            return mockedSalesOrderLineItems;
        }
        
        List<SalesOrderItem__x> lineItems = [Select Id, ExternalId, phdItemSKU__c, phdItemDesc__c, phdItemDesc2__c, phdQuantity__c, phdReturnedReason__c, 
                                             phdDeliveryDueDate__c, phdSalesOrder__c, phdIsFPP__c, phdItemStatus__c, phdWarrantyExpiredOn__c, 
                                             phdShipAddress1__c, phdShipAddress2__c, phdShipCity__c, phdShipState__c, phdShipZip__c,
                                             phdLOC_PO__c, phdInStoreQty__c, phdInWarehouseQty__c, phdWarrantyDaysLeft__c, phdDeliveryType__c,
                                             phdDeliveryTypeDesc__c, phdAckNo__c, phdSaleType__c, phdItemSeq__c, phdOrderType__c,
                                             phdPurchaseDate__c, phdPaymentType__c, phdInvoiceNo__c, phdRSA__c, phdCustomerPo__c,
                                             phdDiscountReasonCode__c,phdAmount__c,dtea__c,Ship_Customer_Name__c,phdWholeSalePrice__c,
                                             phdRSAName__c,phdStoreID__c,phdVendorName__c from SalesOrderItem__x 
                                             where phdSalesOrder__r.ExternalId =: orderExternalId];
        return lineItems;
    }
    
    public static SalesOrderItem__x getOrderLineItemByExternalId(string externalId) {
        SalesOrderItem__x so = new SalesOrderItem__x();
        if(Test.isRunningTest())
        {
            so = new SalesOrderItem__x( Id = 'x00q00000000DMoAAM',
            ExternalId = '21111310:001q000000raI3DAAU',
            phdItemSKU__c = 'B502-87',
            phdItemDesc__c = 'Full Panel Headboard / Kaslyn / White',
            phdItemDesc2__c = 'Full Panel Headboard / Kaslyn / White',
            phdQuantity__c = 1,
            phdSalesOrder__c = '21111310',
            phdIsFPP__c = false,
            phdShipAddress1__c = '250 AMAL DR',
            phdShipAddress2__c = 'apt 102',
            phdShipCity__c = 'ATLANTA',
            phdShipState__c = 'GA',
            phdShipZip__c = '30315',
            phdInStoreQty__c = 0,
            phdInWarehouseQty__c = 0,
            phdWarrantyDaysLeft__c = 365,
            phdDeliveryType__c = 'G02',
            phdDeliveryTypeDesc__c = 'GA TRUCK 02',
            phdSaleType__c = 'Open',
            phdItemSeq__c = 10,
            phdOrderType__c = 'ORD(Exch)',
            phdPaymentType__c = 'Payment Test',
            phdRSA__c = 'HOU',
            phdAmount__c = 199.99,
            Ship_Customer_Name__c = 'HESSE',
            phdWholeSalePrice__c = 99.01

                                      );
           
            return so;
        }
        
        List<SalesOrderItem__x> lineItems = [Select Id, ExternalId, phdItemSKU__c, phdItemDesc__c, phdItemDesc2__c, phdQuantity__c, phdReturnedReason__c, 
                                             phdDeliveryDueDate__c, phdSalesOrder__c, phdIsFPP__c, phdItemStatus__c, phdWarrantyExpiredOn__c, 
                                             phdShipAddress1__c, phdShipAddress2__c, phdShipCity__c, phdShipState__c, phdShipZip__c,
                                             phdLOC_PO__c, phdInStoreQty__c, phdInWarehouseQty__c, phdWarrantyDaysLeft__c, phdDeliveryType__c,
                                             phdDeliveryTypeDesc__c, phdAckNo__c, phdSaleType__c, phdItemSeq__c, phdOrderType__c,
                                             phdPurchaseDate__c, phdPaymentType__c, phdInvoiceNo__c, phdRSA__c, phdCustomerPo__c,
                                             phdDiscountReasonCode__c,phdAmount__c,Ship_Customer_Name__c,phdWholeSalePrice__c,phdRSAName__c,
                                             phdStoreID__c
                                             from SalesOrderItem__x 
                                             where ExternalId =: externalId];
        List<SalesOrderItem__x> filteredLineItems = new List<SalesOrderItem__x>();
        for(SalesOrderItem__x lineItem: lineItems){
            if(lineItem.ExternalId == externalId){
                filteredLineItems.add(lineItem);
                break;
            }
        }
        
        return (filteredLineItems.size() > 0) ? filteredLineItems[0] : null;
    }
    
}