@iSTest
public class ADSCommitmentRefreshConTest {
	
    static testMethod void insrtCommitment(){
        string InsertedRecId = 'a0q4F000000bjphQAA';
            
        Account acc = new Account(FirstName = 'Test', LastName = 'PersonAccount', PersonEmail = 'testemail@sfdctestclass.com', phone='2222200000');
        insert acc;
        
        opportunity opp = new opportunity();
        opp.name='test opp';
        opp.Account = acc;
        opp.RecordTypeId= (Id)Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ADS').getRecordTypeId();
        opp.Opportunity_Type__c = 'ADS';
        opp.StageName = 'Open';
        opp.CloseDate = system.today();
        insert opp;
        
        List<ADS_Zip__c> lstADSZip = new List<ADS_Zip__c>();
        ADS_Zip__c adszip = new ADS_Zip__c();
        adszip.Market__c = 'Winchester Mkt';
        lstADSZip.add(adszip);  
        insert lstADSZip;
        
        ADS_Lane__c adsl = new ADS_Lane__c();
        adsl.of_Trips_Per_Week__c = 10;
        adsl.ADS_Lane_Origin_Market__c = 'Billings Mkt';
        adsl.ADS_Lane_Destination_Market__c = 'Twin Falls Mkt';
        adsl.Opportunity__c = opp.id;
        adsl.ADS_Lane_Origin_State__c = 'NC';
        adsl.ADS_Lane_Origin_City__c = 'A M F GREENSBORO';
        adsl.ADS_Lane_Destination_City__c = 'ABSAROKEE';
        adsl.ADS_Lane_Destination_State__c = 'MT';
        insert adsl;
        
        Commitment__c com = new Commitment__c();
        com.Origin_Market__c = 'Advance Mkt';
        com.Destination_Market__c = 'Billings Mkt';
        com.Number_Trips_Required__c = 10;
        com.Status__c = 'Working';
        insert com;
        //ADSCommitmentController.InsertedRecId = com.id;
        
        List<Segment__c> SegmentList = new List<Segment__c>();
        Segment__c objSegment = new Segment__c();
        objSegment.Origin__c = com.Origin_Market__c;
        objSegment.Destination__c = com.Destination_Market__c; 
        objSegment.Stop__c = 'Advance Mkt';
        SegmentList.add(objSegment);
        
        Segment__c objSegment1 = new Segment__c();
        objSegment1.Origin__c = com.Origin_Market__c;
        objSegment1.Destination__c = com.Destination_Market__c; 
        objSegment1.Stop__c = 'Advance Mkt';
        SegmentList.add(objSegment1);
        
        insert SegmentList;
        

        Commitment__c comm = [select id,Origin_Market__c,Destination_Market__c,Number_Trips_Required__c from Commitment__c 
                              where Id =: com.Id limit 1];
        Decimal trips = 0;
        trips = trips + comm.Number_Trips_Required__c;
        
        List<Commitment_Opportunities__c> ComOpplist = new List<Commitment_Opportunities__c>();
        Commitment_Opportunities__c ComOpp = new Commitment_Opportunities__c();
        ComOpp.Lanes__c = 2;
        ComOpp.Trips_Per_Week__c =comm.Number_Trips_Required__c;
        ComOpp.Commitment__c =comm.Id;
        ComOpp.Opportunity__c =opp.Id;
        ComOpplist.add(ComOpp);
        insert ComOpplist;
        
        
        
        Test.startTest();
        ADSCommitmentRefreshCon ads = new ADSCommitmentRefreshCon();
        ads.onLoad();
        //ads.trips = comm.Number_Trips_Required__c;
        ADSCommitmentRefreshCon.UpdateRecord(com.Id);
        
        Test.stopTest();
        
    }
}