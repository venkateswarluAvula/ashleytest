@isTest
public class LRRTriggerhandlerTest {
    @isTest static void testCases1() {
        
        Account acc = new Account(Name='Test Account');
        Insert acc;
        
        Legal_Review_Request__c lrr = new Legal_Review_Request__c();
        lrr.Approver_By__c = 'Operations Review';
        lrr.Status__c = 'Rejected';
        lrr.Account__c = acc.Id;
        lrr.Submitted_for_Approval__c = true;
        insert lrr;
        
        Test.startTest();
        Approval.ProcessSubmitRequest reqs = new Approval.ProcessSubmitRequest();
        reqs.setComments('Submitting request for approval.');
        reqs.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        reqs.setObjectId(lrr.Id);
        
        Approval.ProcessResult resu = Approval.process(reqs);
        LRRTriggerhandler next = new LRRTriggerhandler();
        
        set<Id> LRRId = new set<Id>();
        LRRId.add(lrr.Id);
        
        Attachment attachment = new Attachment();
        attachment.ParentId = lrr.Id;
        attachment.Name = 'Test Attachment for Parent';
        attachment.Body = Blob.valueOf('Test Data');
        insert(attachment);
        
        LRRTriggerhandler.LRRApprovalProcess(LRRId);
        Test.stopTest();
    }
    
    @isTest static void testCases2() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='karthik.valakonda@d4insight.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, Phone='+911234567890',MobilePhone ='+911234567890',
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
        
        Account acc = new Account(Name='Test Account');
        Insert acc;
        
        Legal_Review_Request__c lrr = new Legal_Review_Request__c();
        lrr.Approver_By__c = 'Legal Review';
        lrr.Status__c = 'Rejected';
        lrr.Account__c = acc.Id;
        lrr.Submitted_for_Approval__c = true;
        lrr.CreatedById = u.Id;
        insert lrr;
        set<Id> LRRId = new set<Id>();
        LRRId.add(lrr.Id);
        
        Blob b = Blob.valueOf('Test Data');
        
        Attachment attachment = new Attachment();
        attachment.ParentId = lrr.Id;
        attachment.Name = 'Test Attachment for Parent';
        attachment.Body = b;
        insert(attachment);
        
        Test.startTest();
        LRRTriggerhandler.LRRApprovalProcess(LRRId);
        Test.stopTest();
    }
    
    @isTest static void testCases3() {
        
        Account acc = new Account(Name='Test Account');
        Insert acc;
        
        Legal_Review_Request__c lrr = new Legal_Review_Request__c();
        lrr.Approver_By__c = 'Legal Review';
        lrr.Status__c = 'Approved';
        lrr.Account__c = acc.Id;
        lrr.Submitted_for_Approval__c = true;
        insert lrr;
        set<Id> LRRId = new set<Id>();
        LRRId.add(lrr.Id);
        
        //Create Document
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        Insert cv;
        
        //Get Content Documents
        List<ContentDocument> docList = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        //Create ContentDocumentLink 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = lrr.Id;
        cdl.ContentDocumentId = docList[0].Id;
        cdl.shareType = 'V';
        Insert cdl;
        
        Attachment attachment = new Attachment();
        attachment.ParentId = lrr.Id;
        attachment.Name = 'Test Attachment for Parent';
        attachment.Body = Blob.valueOf('Test Data');
        insert(attachment);
        
        EmailMessage em = new EmailMessage();
        em.Subject = 'Test';
        em.HtmlBody = 'plaintext';
        em.ToAddress = 'example@email.com';
        em.FromName = 'DisplayName';
        em.Subject = 'Test';
        em.HtmlBody ='Html body';   
        em.Incoming= True;
        em.TextBody = 'Plaintext';  
        em.status = '3';
        em.RelatedToId = lrr.Id;
        insert em;
        
        Test.startTest();
        LRRTriggerhandler.LRRApprovalProcess(LRRId);
        Test.stopTest();
    }
    
    @isTest static void SendingSMS() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='karthik.valakonda@d4insight.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, Phone='+911234567890',MobilePhone ='+911234567890',
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
        List<user> userlist = new List<user>();
        userlist.add(u);
         
        Account acc = new Account(Name='Test Account');
        Insert acc;
        
        Legal_Review_Request__c lrr = new Legal_Review_Request__c();
        lrr.Approver_By__c = 'Operations Review';
        lrr.Status__c = '';
        lrr.Account__c = acc.Id;
        insert lrr;
        
        set<Id> LRRId = new set<Id>();
        LRRId.add(lrr.Id);
        
        LRRTriggerhandler.SendingSMS(LRRId);
    }
}