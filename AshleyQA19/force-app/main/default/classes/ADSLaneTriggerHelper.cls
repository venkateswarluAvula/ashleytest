public class ADSLaneTriggerHelper{
    
    public static void LoadZipMarket(List<ADS_Lane__c> ADSlist){  
        system.debug('ADSlist------'+ADSlist.size()+'--'+ ADSlist);
        
        Set<string> OriginCity = new Set<string>();
        Set<string> OriginState = new Set<string>();
        Set<string> OriginRegion = new Set<string>();
        Set<string> DestinationCity = new Set<string>();
        Set<string> DestinationState = new Set<string>();
        Set<string> DestinationRegion = new Set<string>();
        
        for(ADS_Lane__c ADSval : ADSlist){
            OriginCity.add(ADSval.ADS_Lane_Origin_City__c );
            OriginState.add(ADSval.ADS_Lane_Origin_State__c);
            OriginRegion.add(ADSval.Origin_Region__c);
            DestinationCity.add(ADSval.ADS_Lane_Destination_City__c);
            DestinationState.add(ADSval.ADS_Lane_Destination_State__c);
            DestinationRegion.add(ADSval.Destination_Region__c);
        }
        
        //--- code for Origin ---
        List<ADS_Zip__c> ADSval = [select id,name,City__c,State_Code__c,Market__c,Zip_Code__c,Region__c 
                                     from ADS_Zip__c where City__c =:OriginCity AND State_Code__c =: OriginState];
        
        Map<string,string> getZipVal = new Map<string,string>();
        Map<string,string> getMarketVal = new Map<string,string>();
        Map<string,string> getRegionVal = new Map<string,string>();
        
        For(ADS_Zip__c ADS : ADSval){
            String ADSValue = (ADS.City__c+','+ADS.State_Code__c).touppercase();
            getZipVal.put(ADSValue,ADS.Zip_Code__c);
            getMarketVal.put(ADSValue,ADS.Market__c);
            getRegionVal.put(ADSValue,ADS.Region__c);
        }
        
        
        //---- code for Destination----
        List<ADS_Zip__c> ADSDestval = [select id,name,City__c,State_Code__c,Market__c,Zip_Code__c,Region__c 
                                         from ADS_Zip__c where City__c =:DestinationCity AND State_Code__c =: DestinationState];
        
        Map<string,string> getDestZipVal = new Map<string,string>();
        Map<string,string> getDestMarketVal = new Map<string,string>();
        Map<string,string> getDestRegionVal = new Map<string,string>();
        
        For(ADS_Zip__c ADSD : ADSDestval){
            String Destval = (ADSD.City__c+','+ADSD.State_Code__c).touppercase();
            getDestZipVal.put(Destval,ADSD.Zip_Code__c);
            getDestMarketVal.put(Destval,ADSD.Market__c);
            getDestRegionVal.put(Destval,ADSD.Region__c);
        }
        
        
        //--- Assigning values to respective fields ---
        for(ADS_Lane__c ADSL : ADSlist){
            
            string OriginVal =  (ADSL.ADS_Lane_Origin_City__c+','+ADSL.ADS_Lane_Origin_State__c).touppercase();
            string DestinationVal =  (ADSL.ADS_Lane_Destination_City__c+','+ADSL.ADS_Lane_Destination_State__c).touppercase();
            
            ADSL.ADS_Lane_Origin_Zip__c= getZipVal.get(OriginVal);
            ADSL.ADS_Lane_Origin_Market__c = getMarketVal.get(OriginVal);
            ADSL.Origin_Region__c = getRegionVal.get(OriginVal);
            ADSL.ADS_Lane_Destination_Zip__c= getDestZipVal.get(DestinationVal);
            ADSL.ADS_Lane_Destination_Market__c= getDestMarketVal.get(DestinationVal);
            ADSL.Destination_Region__c = getDestRegionVal.get(DestinationVal);
        }
        
    }
}