@isTest
public class DeliveryWindowLookupserviceCtrl_Test {
    
    @testSetup
    public static void prepareAPICustomSetting(){
        TestDataFactory.prepareAPICustomSetting();
                
    }
	
	@isTest 
	static void testWithOutAPIMock() {
		//set some test data
		SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
		SalesOrderItem__x testLineItem = TestDataFactory.initializeSalesOrderLineItems(1)[0];

		SalesOrderDAO.mockedSalesOrders.add(testSalesOrder);
		SalesOrderDAO.mockedSalesOrderLineItems.add(testLineItem);

		DeliveryWindowLookupserviceController.DeliveryCalendarResponse response = DeliveryWindowLookupserviceController.getDeliveryCalendarDays('2017-01-01', 'ord-001', 'ordline-001', '8888300', '00154', '118', '400346240', 25, '2049-12-12','00x000000000000');
		System.assert(response.isSuccess == false);
	}

	@IsTest 
	static void testWithAPIMockAndAvailableDates() {
		//set some test data
		SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
		SalesOrderItem__x testLineItem = TestDataFactory.initializeSalesOrderLineItems(1)[0];

		SalesOrderDAO.mockedSalesOrders.add(testSalesOrder);
		SalesOrderDAO.mockedSalesOrderLineItems.add(testLineItem);

		String sampleResponse = '[{"DeliverDate": "Sunday, November 25, 2018","DeliverDateTime": "2018-11-25T00:00:00","ResourceGroup": "","ResourceGroupType": "Deliver","AllowableOverage": "2%","Capacity": 0,"AllowbalePromisedCapacity": 0,"Promised": 3,"PromisedActual": "0%","PromisedAllowable": "0%","Confirmed": 0,"ConfirmedActual": "0%", "IsBzEnabledZipcode": false, "BzNameDescription": null,"BzNameValue": null,"BzOtherCapacity": null, "BzOtherPromisedCapacity": null, "BzOtherAvailableCapacity": null, "IsNddCapacityEnabled": false, "NddCapacity": null,"BackColor": "DarkGray","ServiceLevel": "Premium Delivery", "IsNddSetupEnabled": false, "IsClosedDate": true}]';
        
		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'OK', sampleResponse,new Map<String, String>()));
		DeliveryWindowLookupserviceController.DeliveryCalendarResponse response = DeliveryWindowLookupserviceController.getDeliveryCalendarDays('2017-01-01', 'ord-001', 'ordline-001', '8888300', '00154', '118', '400346240', 25, '2049-12-12','00x000000000000');
		Test.stopTest();
		
		System.assert(response.isSuccess == true);
		System.assert(response.calendar != null, 'A calendar should have been returned');
	}	

	@IsTest 
	static void testWithAPIMockAndNoAvailableDates() {
		//set some test data
		SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
		SalesOrderItem__x testLineItem = TestDataFactory.initializeSalesOrderLineItems(1)[0];

		SalesOrderDAO.mockedSalesOrders.add(testSalesOrder);
		SalesOrderDAO.mockedSalesOrderLineItems.add(testLineItem);

        String sampleResponse = '[{"DeliverDate": "Wednesday, December 1, 2049", "DeliverDateTime": "2049-12-01T00:00:00","ResourceGroup": "Atlanta Wednesday","ResourceGroupType": "Deliver","AllowableOverage": "10%","Capacity": 45,"AllowbalePromisedCapacity": 50,"Promised": 13,"PromisedActual": "29%","PromisedAllowable": "26%","Confirmed": 0,"ConfirmedActual": "0%","IsBzEnabledZipcode": false,"BzNameDescription": null,"BzNameValue": null,"BzOtherCapacity": null,"BzOtherPromisedCapacity": null,"BzOtherAvailableCapacity": null,"IsNddCapacityEnabled": false,"NddCapacity": null,"ServiceLevel": "Threshold Delivery","BackColor": "DarkGray","DateColor": "Black","BackgroundImage": null,"IsNddSetupEnabled": false, "IsClosedDate": true}]';

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new MockCallout(500, 'internal server error', sampleResponse,new Map<String, String>()));
		DeliveryWindowLookupserviceController.DeliveryCalendarResponse response = DeliveryWindowLookupserviceController.getDeliveryCalendarDays('2017-01-01', 'ord-001', 'ordline-001', '8888300', '00154', '118', '400346240', 25, '2049-12-12','00x000000000000');
		Test.stopTest();
		
		System.assert(response.isSuccess == false);
		//System.assert(response.calendar == null);
		System.assert(response.calendar == null, response.message == 'No Delivery Dates available');
	}
	
    @IsTest 
	static void testWithAPIMockAndAvailableDeliveryDates() {
		//set some test data
		SalesOrder__x testSalesOrder = TestDataFactory.initializeSalesOrders(1)[0];
		SalesOrderItem__x testLineItem = TestDataFactory.initializeSalesOrderLineItems(1)[0];

		SalesOrderDAO.mockedSalesOrders.add(testSalesOrder);
		SalesOrderDAO.mockedSalesOrderLineItems.add(testLineItem);

		String sampleResponse = '[{"DeliverDate": "Wednesday, December 1, 2049", "DeliverDateTime": "2049-12-01T00:00:00","ResourceGroup": "Atlanta Wednesday","ResourceGroupType": "Deliver","AllowableOverage": "10%","Capacity": 45,"AllowbalePromisedCapacity": 50,"Promised": 13,"PromisedActual": "29%","PromisedAllowable": "26%","Confirmed": 0,"ConfirmedActual": "0%","IsBzEnabledZipcode": false,"BzNameDescription": null,"BzNameValue": null,"BzOtherCapacity": null,"BzOtherPromisedCapacity": null,"BzOtherAvailableCapacity": null,"IsNddCapacityEnabled": false,"NddCapacity": null,"ServiceLevel": "Threshold Delivery","BackColor": "DarkGray","DateColor": "Black","BackgroundImage": null,"IsNddSetupEnabled": false, "IsClosedDate": true}]';
        
		Test.startTest();

        Test.setMock(HttpCalloutMock.class, new MockCallout(200, 'OK', sampleResponse,new Map<String, String>()));
        Account acc =  prepareCustomerInfo();
        Opportunity opp =  TestDataFactory.prepareShoppingCart(acc);
        List<Shopping_cart_line_item__c> lineItems = TestDataFactory.prepareShoppingCartLineItems(opp, true);
       Address__c addr = new Address__c();
        addr.AccountId__c = acc.Id;
        addr.Address_Line_1__c = 'Sample address 1';
        addr.Address_Line_2__c = 'Sample address 2';
        addr.City__c = 'Sample city';
        addr.State__c = 'Sample state';
        addr.Country__c = 'Sample country';
        addr.Zip_Code__c = '123456';
        
        insert addr;
		DeliveryWindowLookupserviceController.DeliveryCalendarResponse response = DeliveryWindowLookupserviceController.getDeliveryCalendarDays('2017-01-01', 'ord-001', 'ordline-001', '8888300', '00154', '118', '400346240', 25, '2049-12-12','00x000000000000');
        API_ATCClientUpdateDeliveryDate.ATCPayload payload = new API_ATCClientUpdateDeliveryDate.ATCPayload ();
        DeliveryWindowLookupserviceController.updatePayloadWithOpenLineItemShipToAddress(payload, 'shipto'); 
        //DeliveryWindowLookupserviceController.updatePayloadWithOpenLineItemShipToAddress updPay = new DeliveryWindowLookupserviceController.updatePayloadWithOpenLineItemShipToAddress (payload, String shipToAddr);
		Test.stopTest();
		System.assert(response.isSuccess == true);
		System.assert(response.calendar != null);
	}	
    
     public static Account prepareCustomerInfo(){
        
        RecordType customerAccRT = [Select Id from recordType where SObjectType='Account' and Name='Customer'];
        Account acc =  new Account();
        acc.FirstName = 'testAccF';
        acc.LastName = 'testAccL';
        acc.RecordTypeId = customerAccRT.Id;
        acc.Phone ='(0)888-998-766';
        insert acc;
        
        return acc;
    }
    
    public static Shopping_cart_line_item__c shopCart(){
        
        Shopping_cart_line_item__c shpCrt = new Shopping_cart_line_item__c();
        List<Shopping_cart_line_item__c> lineItems = new List<Shopping_cart_line_item__c>();
        shpCrt.Id = '09dewgfbbfk';
        shpCrt.Product_SKU__c = '';
        shpCrt.Discount__c = 7;
        //shpCrt.Discount_Price__c = 346;
        shpCrt.DiscountType__c = '';
        shpCrt.Flat_Discounted_Amount__c = 67;
        //shpCrt.Last_Price__c = 98;
        shpCrt.List_Price__c = 75;
        shpCrt.Average_Cost__c = 23;
        shpCrt.Quantity__c = 2;
        shpCrt.Opportunity__c = '';
        shpCrt.Delivery_Mode__c = 'THD';
        shpCrt.Discount_Reason_Code__c = 'hdk';
        shpCrt.Discount_Status__c = '';
        shpCrt.Estimated_Tax__c = 8;
        insert shpCrt;
        return shpCrt;
    }
  

}