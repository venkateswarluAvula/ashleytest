@isTest
private class TechWorkLoadWrapTest {
    @isTest
    static void test1(){
        Test.startTest();
        TechWorkLoadWrap twlObj = new TechWorkLoadWrap();
        twlObj.TechId = 32;
        twlObj.FirstName = 'T Th S skill level';
        twlObj.LastName = 'ASHLEY A';
        twlObj.VendorId = 'ASHLEY';
        twlObj.CompanyName = 'ASHLEY';
        twlObj.ZipCode = '30002';
        twlObj.MaxHoursPerDay = 3;
        twlObj.MaxRepairPerDay = 6;
        twlObj.MaxPiecesPerDay = 5;
        twlObj.CasegoodsSkillLevel = 0;
        twlObj.UpholsterySkillLevel = 0;
        twlObj.LeatherSkillLevel = 0;
        twlObj.WeeklySchedule = 'NYYYYYY';
        twlObj.StreetAddress = null;
        twlObj.City = null;
        twlObj.StateCode = 'GA';
        twlObj.HomePhone = '';
        twlObj.CellPhone = '';
        twlObj.EmailID = '';
        twlObj.FaxNumber = '';

        TechWorkLoadWrap.LeaveDateWrap twlwLeaveObj = new TechWorkLoadWrap.LeaveDateWrap();
        twlwLeaveObj.LeaveDate = '2019-06-25T00:00:00';
        twlwLeaveObj.CreatedTime = '2015-08-28T15:03:18';
        twlwLeaveObj.CreatedUserID = 1299;
        twlObj.LeaveDates = new list<TechWorkLoadWrap.LeaveDateWrap>{twlwLeaveObj};

        Test.stopTest();
    }
}