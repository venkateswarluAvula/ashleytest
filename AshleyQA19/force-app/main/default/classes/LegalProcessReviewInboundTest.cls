@isTest
public class LegalProcessReviewInboundTest {
    @isTest static void testCases1() {
        
        string Recid;
        Account acc = new Account(Name='Test');
        insert acc;
        Legal_Review_Request__c LRR = new Legal_Review_Request__c();
        LRR.Account__c= acc.Id;
        LRR.CreatedById = userinfo.getUserId();
        insert LRR;
        
        Approval.ProcessSubmitRequest reqs = new Approval.ProcessSubmitRequest();
        reqs.setComments('Submitting request for approval.');
        reqs.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        reqs.setObjectId(lrr.Id);
        
        Approval.ProcessResult resu = Approval.process(reqs);
        
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.plainTextBody = 'APPROVED Here is my plainText body of the email';
        email.fromAddress = 'noreply@gmail.com';
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[1];  
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.Filename = 'test.txt';
        string[] tA = new string[]{'abc@abc.com'};  
            email.toAddresses = tA;
        email.subject = 'RE: Sandbox: Email From Salesforce ADS Team RecId:' + LRR.Id;
        string sub = email.subject;
        String algorithmName = 'HMacSHA1';
        Blob b = Crypto.generateMac(algorithmName, Blob.valueOf('test'),
                                    Blob.valueOf('test_key'));
        binaryAttachment.Body = b;
        binaryAttachments[0] =  binaryAttachment ;
        email.binaryAttachments = binaryAttachments ;
        env.fromAddress = 'noreply@gmail.com';
        
        email.plainTextBody = 'Here is my plainText body for Rejected email';
        email.fromAddress = 'balakrishna.n@d4insight.com';
        List<String> tA1 = new List<String>();
        tA.add('qw@gm.com');
        email.toAddresses = tA1;
        
        user u = [select id,email from user where Email=:email.fromAddress];
        
        LegalProcessReviewInbound adsemail = new LegalProcessReviewInbound();
        adsemail.handleInboundEmail(email, env);
        adsemail.getWorkItemId(LRR.Id,u.Id);
        
        
    }
    
    @isTest static void testCases2(){
        string Recid;
        Account acc = new Account(Name='Test');
        insert acc;
        
        Legal_Review_Request__c LRR = new Legal_Review_Request__c();
        LRR.Account__c= acc.Id;
        LRR.CreatedById = userinfo.getUserId();
        insert LRR;
        
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.plainTextBody = 'Rejected Here is my plainText body of the email';
        email.fromAddress = 'noreply@gmail.com';
        Messaging.InboundEmail.TextAttachment[] textAttachments = new Messaging.InboundEmail.TextAttachment[1];  
        Messaging.InboundEmail.TextAttachment textAttachment = new Messaging.InboundEmail.TextAttachment();
        textAttachment.Filename = 'test.txt';
        string[] tA = new string[]{'abc@abc.com'};  
            email.toAddresses = tA;
        email.subject = 'RE: Sandbox: Email From Salesforce ADS Team RecId:' + LRR.Id;
        string sub = email.subject;
        String algorithmName = 'HMacSHA1';
        Blob b = Crypto.generateMac(algorithmName, Blob.valueOf('test'),
                                    Blob.valueOf('test_key'));
        textAttachment.Body = 'b';
        textAttachments[0] =  textAttachment ;
        email.textAttachments = textAttachments ;
        env.fromAddress = 'noreply@email.com';
        
        email.plainTextBody = 'Here is my plainText body of the email';
        email.fromAddress = 'balakrishna.n@d4insight.com';
        List<String> tA1 = new List<String>();
        tA.add('qw@gm.com');
        email.toAddresses = tA1;
        
        user u = [select id,email from user where Email=:email.fromAddress];
        
        LegalProcessReviewInbound adsemail = new LegalProcessReviewInbound();
        adsemail.handleInboundEmail(email, env);
        
    }
    
    @isTest static void testCases3() {
        
        string Recid;
        Account acc = new Account(Name='Test');
        insert acc;
        Legal_Review_Request__c LRR = new Legal_Review_Request__c();
        LRR.Account__c= acc.Id;
        LRR.CreatedById = userinfo.getUserId();
        insert LRR;
        
        Approval.ProcessSubmitRequest reqs = new Approval.ProcessSubmitRequest();
        reqs.setComments('Submitting request for approval.');
        reqs.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        reqs.setObjectId(lrr.Id);
        
        Approval.ProcessResult resu = Approval.process(reqs);
        
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.plainTextBody = 'APPROVED Here is my plainText body of the email';
        email.fromAddress = 'noreply@gmail.com';
        
        string[] tA = new string[]{'abc@abc.com'};  
            email.toAddresses = tA;
        email.subject = 'RE: Sandbox: Email From Salesforce ADS Team RecId:' + LRR.Id;
        env.fromAddress = 'noreply@gmail.com';
        
        email.plainTextBody = 'Here is my plainText body for Rejected email';
        email.fromAddress = 'balakrishna.n@d4insight.com';
        List<String> tA1 = new List<String>();
        tA.add('qw@gm.com');
        email.toAddresses = tA1;
        
        user u = [select id,email from user where Email=:email.fromAddress];
        
        LegalProcessReviewInbound adsemail = new LegalProcessReviewInbound();
        adsemail.handleInboundEmail(email, env);
        adsemail.getWorkItemId(LRR.Id,u.Id);
        
    }
    
     @isTest static void testCases4(){
        string Recid;
        Account acc = new Account(Name='Test');
        insert acc;
        
        Legal_Review_Request__c LRR = new Legal_Review_Request__c();
        LRR.Account__c= acc.Id;
        LRR.CreatedById = userinfo.getUserId();
        insert LRR;
        
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.plainTextBody = 'Approved Here is my plainText body of the email';
        email.fromAddress = 'noreply@gmail.com';
        Messaging.InboundEmail.TextAttachment[] textAttachments = new Messaging.InboundEmail.TextAttachment[1];  
        Messaging.InboundEmail.TextAttachment textAttachment = new Messaging.InboundEmail.TextAttachment();
        textAttachment.Filename = 'test.txt';
        string[] tA = new string[]{'abc@abc.com'};  
            email.toAddresses = tA;
        email.subject = 'RE: Sandbox: Email From Salesforce ADS Team RecId:' + LRR.Id;
        string sub = email.subject;
        String algorithmName = 'HMacSHA1';
        Blob b = Crypto.generateMac(algorithmName, Blob.valueOf('test'),
                                    Blob.valueOf('test_key'));
        textAttachment.Body = 'b';
        textAttachments[0] =  textAttachment ;
        email.textAttachments = textAttachments ;
        env.fromAddress = 'noreply@email.com';
        
        email.plainTextBody = 'Here is my plainText body of the email';
        email.fromAddress = 'balakrishna.n@d4insight.com';
        List<String> tA1 = new List<String>();
        tA.add('qw@gm.com');
        email.toAddresses = tA1;
        
        user u = [select id,email from user where Email=:email.fromAddress];
        
        LegalProcessReviewInbound adsemail = new LegalProcessReviewInbound();
        adsemail.handleInboundEmail(email, env);
        
    }
    
}