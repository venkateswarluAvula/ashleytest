@isTest
public class techschedulingcancellationTest {

    @testSetup
    static void setup() {
        List<Account> accList = new List<Account>();
        accList = TestDataFactory.initializeAccounts(1);
        insert accList;

        List<Contact> contList = new List<Contact>();
        contList = TestDataFactory.initializeContacts(accList[0].Id, 1);
        insert contList;

        // Insert Sales Order
        Test.startTest();
        SalesOrder__x salesOrder = new SalesOrder__x(fulfillerID__c = '8888300-164',
                                                 ExternalId = '17331400:001q000000raDkvAAE',
                                                 phhProfitcenter__c = 1234567,
                                                 Phhcustomerid__c = '784584585',
                                                 phhSalesOrder__c = '88845758'
                                                );
        system.debug('order' + salesOrder);
        SalesOrderDAO.mockedSalesOrders.add(salesOrder);
        SalesOrder__x salesOrderObj = SalesOrderDAO.getOrderById(salesOrder.Id);
        SalesOrderItem__x salesOrderItem = new SalesOrderItem__x(ExternalId = '17331400:001q000000raDkvAAE',
                                                             phdShipZip__c = '30548');
        SalesOrderDAO.mockedSalesOrderLineItems.add(salesOrderItem);
        SalesOrderItem__x salesOrderItemObj = SalesOrderDAO.getOrderLineItemByExternalId(salesOrderItem.ExternalId);
        system.debug('orderitem' + salesOrderItemObj);

        // Insert Case
        Case caseObj = new Case(Sales_Order__c = '17331400:001q000000raDkvAAE', Status = 'New', Origin = 'Phone', Type = 'Open Order Inquiry', Subject = 'Test', Technician_ServiceReqId__c = '91506',
                            Description = 'Test', ContactId = contList[0].Id );
        insert caseObj;
        Test.stopTest();
    }

    @isTest static void getRelatedServiceRequestsforcancellingAllow () {
    	Case caseObj = [Select Id, Sales_Order__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
    	caseObj.Technician_Schedule_Date__c = system.today();
    	update caseObj;
    	Test.startTest();
    	techschedulingcancellation.getRelatedServiceRequestsforcancelling(caseObj.Id);
    	Test.stopTest();
    }

    @isTest static void getRelatedServiceRequestsforcancellingConfirm () {
    	Case caseObj = [Select Id, Sales_Order__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
        caseObj.Legacy_Service_Request_ID__c = '12345';
        caseObj.Tech_Scheduled_Date__c = system.today().adddays(-2);
        update caseObj;
    	Test.startTest();
    	techschedulingcancellation.getRelatedServiceRequestsforcancelling(caseObj.Id);
    	Test.stopTest();
    }

    @isTest static void getRelatedServiceRequestsforcancellingUnScheduled () {
    	Case caseObj = [Select Id, Sales_Order__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
        caseObj.Technician_Schedule_Date__c = null;
        caseObj.Tech_Scheduled_Date__c = null;
        update caseObj;
    	Test.startTest();
    	techschedulingcancellation.getRelatedServiceRequestsforcancelling(caseObj.Id);
    	Test.stopTest();
    }

    @isTest static void getRelatedServiceRequestsforcancellingClosed () {
    	Case caseObj = [Select Id, Sales_Order__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
        caseObj.Type_of_Resolution__c = 'type';
        caseObj.Resolution_Notes__c = 'notes';
        caseObj.status = 'Closed';
        update caseObj;
    	Test.startTest();
    	techschedulingcancellation.getRelatedServiceRequestsforcancelling(caseObj.Id);
    	Test.stopTest();
    }

    @isTest static void getApiResponse200 () {
        Case caseObj = [Select Id, Sales_Order__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('TechSchedulePost');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, mock);
        techschedulingcancellation.getApiResponse(caseObj.Id, true, null);
        Test.stopTest();
    }

    @isTest static void getApiResponse400 () {
        Case caseObj = [Select Id, Sales_Order__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('TechSchedulePost');
        mock.setStatusCode(400);
        mock.setHeader('Content-Type', 'application/json');

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, mock);
        techschedulingcancellation.getApiResponse(caseObj.Id, true, null);
        Test.stopTest();
    }

    @isTest static void getApiResponse404 () {
        Case caseObj = [Select Id, Sales_Order__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('TechSchedulePost');
        mock.setStatusCode(404);
        mock.setHeader('Content-Type', 'application/json');

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, mock);
        techschedulingcancellation.getApiResponse(caseObj.Id, true, null);
        Test.stopTest();
    }

    @isTest static void getApiResponse2 () {
        Case caseObj = [Select Id, Sales_Order__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
        Test.startTest();
        techschedulingcancellation.getApiResponse(caseObj.Id, false, null);
        Test.stopTest();
    }
}