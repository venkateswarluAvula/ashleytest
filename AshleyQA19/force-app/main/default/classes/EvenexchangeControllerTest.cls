@isTest
public class EvenexchangeControllerTest {

    static testMethod void getProductLineItemtest() {
        List<Account> accList = new List<Account>();
        accList = TestDataFactory.initializeAccounts(1);
        insert accList;

        List<Contact> contList = new List<Contact>();
        contList = TestDataFactory.initializeContacts(accList[0].Id, 1);
        insert contList;
        
        address__c adr = new address__c();
        adr.AccountId__c = accList[0].Id;
        adr.Address_Line_1__c = 'adr line 1';
        adr.Address_Line_2__c = 'adr line 2';
        adr.City__c = 'city';
        adr.StateList__c = 'CA';
        adr.Zip_Code__c = '30548';
        adr.Address_Type__c = 'Ship To';
        adr.Preferred__c = true;
        insert adr;
        //SalesOrderDAO.mockedSalesOrders.add(new SalesOrder__x());

        //SalesOrderDAO.mockedSalesOrderLineItems.add(new SalesOrderItem__x());
        Schema.DescribeSObjectResult describeResult = SalesOrder__x.SobjectType.getDescribe();
        string salesOrderIdPrefix = describeResult.getKeyPrefix();
        string smapleSalesOrderId = salesOrderIdPrefix + '2F0000004eetAAA';
        String Items = 'B219-31,B502-87';

        List<SalesOrder__x> ordersById = SalesOrderDAO.getOrdersByIds(new List<Id>{smapleSalesOrderId});
        List<SalesOrderItem__x> lineItemsByOrderExternalId = SalesOrderDAO.getOrderLineItemsByOrderExternalId('xyz'); 
        String externalId = '17331400:001q000000raDkvAAE';
        Case caseObj = new Case(Sales_Order__c = '17331400:001q000000raDkvAAE', Status = 'New', Origin = 'Phone', Type = 'Open Order Inquiry', Subject = 'Test', Technician_ServiceReqId__c = '91506',
                            Description = 'Test', Type_of_Resolution__c = 'testing',ContactId = contList[0].Id, Address__c = adr.Id, Technician_Schedule_Date__c = Date.today()+1, Legacy_Account_Ship_To__c = '8888000-130');
        insert caseObj;
        
        List<Case> casess = new List<Case>();    
        List<ProductLineItem__c> prlines = new List<ProductLineItem__c>();
        casess.add(caseObj);
        ProductLineItem__c pli = new ProductLineItem__c(Sales_Order_Number__c = '200470100', Case__c = caseObj.Id, Item_SKU__c = 'B502-87', Item_Serial_Number__c = '99999', Item_Seq_Number__c = '100', Fulfiller_ID__c = '8888300-164');
        insert pli;
        prlines.add(pli);
        test.startTest();
        List<String> addStars1 = new List<String>();   
        addStars1.add('ItemID: "* DISCOUNT", ItemDescription: "Keyed Discount"');
        //EvenexchangeController excon = new EvenexchangeController();
        EvenexchangeController.getStarFlags(externalId);
        EvenexchangeController.getDiscontinuedItem(Items, externalId);
        EvenexchangeController.getOrderDetails(externalId);
        EvenexchangeController.getProductLineItem(caseObj.Id);
        EvenexchangeController.sendEvenexchangeValue('2019-08-16', casess, lineItemsByOrderExternalId,prlines, ordersById[0],'salesordercomment', 'AddressType','Phone1', 'Phone2', 'Phone3', 'emailinfo', caseObj.Id , 'vipdes', 'hccdes', addStars1);
        //EvenexchangeController.sendEvenexchangeValue('2019-08-16', casess, lineItemsByOrderExternalId,ordersById[0],'salesordercomment', 'Case Address','Phone1', 'Phone2', 'Phone3', 'emailinfo', caseObj.Id , 'vipdes', 'hccdes', addStars1);
        test.stopTest();
    }
    static testMethod void getProductLineItemtest1() {
        List<Account> accList = new List<Account>();
        accList = TestDataFactory.initializeAccounts(1);
        insert accList;

        List<Contact> contList = new List<Contact>();
        contList = TestDataFactory.initializeContacts(accList[0].Id, 1);
        insert contList;
        
        address__c adr = new address__c();
        adr.AccountId__c = accList[0].Id;
        adr.Address_Line_1__c = 'adr line 1';
        adr.Address_Line_2__c = 'adr line 2';
        adr.City__c = 'city';
        adr.StateList__c = 'CA';
        adr.Zip_Code__c = '30548';
        adr.Address_Type__c = 'Ship To';
        adr.Preferred__c = true;
        insert adr;
        //SalesOrderDAO.mockedSalesOrders.add(new SalesOrder__x());

        //SalesOrderDAO.mockedSalesOrderLineItems.add(new SalesOrderItem__x());
        Schema.DescribeSObjectResult describeResult = SalesOrder__x.SobjectType.getDescribe();
        string salesOrderIdPrefix = describeResult.getKeyPrefix();
        string smapleSalesOrderId = salesOrderIdPrefix + '2F0000004eetAAA';
        String Items = 'B219-31,B502-87';

        List<SalesOrder__x> ordersById = SalesOrderDAO.getOrdersByIds(new List<Id>{smapleSalesOrderId});
        List<SalesOrderItem__x> lineItemsByOrderExternalId = SalesOrderDAO.getOrderLineItemsByOrderExternalId('xyz'); 
        String externalId = '17331400:001q000000raDkvAAE';
        Case caseObj = new Case(Sales_Order__c = '17331400:001q000000raDkvAAE', Status = 'New', Origin = 'Phone', Type = 'Open Order Inquiry', Subject = 'Test', Technician_ServiceReqId__c = '91506',
                            Description = 'Test', Type_of_Resolution__c = 'testing',ContactId = contList[0].Id, Address__c = adr.Id, Technician_Schedule_Date__c = Date.today()+1, Legacy_Account_Ship_To__c = '8888000-130');
        insert caseObj;
        
        List<Case> casess = new List<Case>();    
        List<ProductLineItem__c> prlines = new List<ProductLineItem__c>();
        casess.add(caseObj);
        ProductLineItem__c pli = new ProductLineItem__c(Sales_Order_Number__c = '200470100', Case__c = caseObj.Id, Item_SKU__c = 'B502-87', Item_Serial_Number__c = '99999', Item_Seq_Number__c = '100', Fulfiller_ID__c = '8888300-164');
        insert pli;
        prlines.add(pli);
        test.startTest();
        List<String> addStars1 = new List<String>();   
        addStars1.add('ItemID: "* DISCOUNT", ItemDescription: "Keyed Discount"');
        //EvenexchangeController excon = new EvenexchangeController();
        EvenexchangeController.getStarFlags(externalId);
        EvenexchangeController.getDiscontinuedItem(Items, externalId);
        EvenexchangeController.getOrderDetails(externalId);
        EvenexchangeController.getProductLineItem(caseObj.Id);
        //EvenexchangeController.sendEvenexchangeValue('2019-08-16', casess, lineItemsByOrderExternalId,prlines, ordersById[0],'salesordercomment', 'AddressType','Phone1', 'Phone2', 'Phone3', 'emailinfo', caseObj.Id , 'vipdes', 'hccdes', addStars1);
        EvenexchangeController.sendEvenexchangeValue('2019-08-16', casess, lineItemsByOrderExternalId,prlines,ordersById[0],'salesordercomment', 'Case Address','Phone1', 'Phone2', 'Phone3', 'emailinfo', caseObj.Id , 'vipdes', 'hccdes', addStars1);
        test.stopTest();
    }
}