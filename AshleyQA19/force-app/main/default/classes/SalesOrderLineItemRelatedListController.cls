/* This is a controller clas for SalesOrderLineItemRelatedList lightning component. The Component is used to display all line items 
associated to a given order. */
public with sharing class SalesOrderLineItemRelatedListController {
    @AuraEnabled
    public static List<SalesOrderItem__x> getLineItems(Id salesforceOrderId) {
        List<SalesOrderItem__x> lineItems = new List<SalesOrderItem__x>();
        system.debug('salesforceOrderId------'+salesforceOrderId);
        SalesOrder__x order = SalesorderDAO.getOrderById(salesforceOrderId);
        List<SalesOrderItem__x> finallineItems = new List<SalesOrderItem__x>();
        system.debug('order------'+order);
        system.debug('finallineItems------'+finallineItems);
        if(order != null){
            lineItems = SalesOrderDAO.getOrderLineItemsByOrderExternalId(order.ExternalId);
            system.debug('lineItems-----'+lineItems);
        }
        String orderId = order.Id;
        for(SalesOrderItem__x oli: lineItems){
            system.debug('oli---1---'+oli.phdSaleType__c+'-----'+order.phhSaleType__c);
            if(order.phhSaleType__c == oli.phdSaleType__c)//Added by Venkat on 10292018
            {
                system.debug('oli---1---'+oli);
                //if(!oli.phdItemSKU__c.startsWith('*')){
                    finallineItems.add(oli);
                //}
            }
        }
        system.debug('finallineItems---> ' +finallineItems);
        return lineItems;
    }
    
    
    
    
    @AuraEnabled
    public static List<SalesOrderItem__x> getSalesOrderLineItem (id salesOrderItemId, id salesOrderId){
        system.debug('salesOrderItemId value is '+salesOrderItemId);
        system.debug('salesOrderId value is '+salesOrderId);
        List < SalesOrderItem__x > lstOfSalesOrdLineItem = new List < SalesOrderItem__x >();
        lstOfSalesOrdLineItem = [SELECT Id,phdShipAddress1__c,phdShipAddress2__c,phdShipCity__c,phdShipState__c,phdShipZip__c,phdSalesOrder__c From SalesOrderItem__x where id =: salesOrderItemId];
        system.debug('lstOfSalesOrdLineItem value is '+lstOfSalesOrdLineItem);
        List < SalesOrderItem__x > SOLAddrsList = new List < SalesOrderItem__x >();
        
        SalesOrder__x order = SalesorderDAO.getOrderById(salesOrderId);
        SalesOrderWrapper1 orderWrapper = new SalesOrderWrapper1();
        lstOfSalesOrdLineItem = SalesOrderDAO.getOrderLineItemsByOrderExternalId(order.ExternalId);
        
        	orderWrapper.shipToStreet1 = lstOfSalesOrdLineItem[0].phdShipAddress1__c;
            orderWrapper.shipToStreet2 = lstOfSalesOrdLineItem[0].phdShipAddress2__c;
            orderWrapper.shipToCity = lstOfSalesOrdLineItem[0].phdShipCity__c;
            orderWrapper.shipToState = lstOfSalesOrdLineItem[0].phdShipState__c;
            orderWrapper.shipToPostalcode = lstOfSalesOrdLineItem[0].phdShipZip__c;
        
        	if (orderWrapper.shipToStreet1.Contains('+')) {
                orderWrapper.shipToStreet1 = orderWrapper.shipToStreet1.replace('+',' ');
                system.debug('after update'+orderWrapper.shipToStreet1);
            }
            if (orderWrapper.shipToStreet2 != null){
            if (orderWrapper.shipToStreet2.Contains('+')) {
                orderWrapper.shipToStreet2 = orderWrapper.shipToStreet2.replace('+',' ');
            }
            }
            if (orderWrapper.shipToCity.Contains('+')) {
                orderWrapper.shipToCity = orderWrapper.shipToCity.replace('+',' ');
            }
        
        if(lstOfSalesOrdLineItem.size()>0){
            for(SalesOrderItem__x soi:lstOfSalesOrdLineItem){
                if(soi.id == salesOrderItemId){
                    SOLAddrsList.add(soi);
                    break;
                }
            }
        }
        system.debug('orderWrapper'+orderWrapper);
        system.debug('SOLAddrsList'+SOLAddrsList);
       
        return SOLAddrsList;
        
    }

    @AuraEnabled
    public static List<String> getval(Id salesforceOrderId) {
        List<SalesOrderItem__x> lineItems = new List<SalesOrderItem__x>();
        List<String> EstArrival = new List<String>();
        List<SalesOrderItem__x> finallineItems = new List<SalesOrderItem__x>();
        system.debug('salesforceOrderId------'+salesforceOrderId);
        SalesOrder__x order = SalesorderDAO.getOrderById(salesforceOrderId);
        system.debug('order------'+order);
        if(order != null){
            lineItems = SalesOrderDAO.getOrderLineItemsByOrderExternalId(order.ExternalId);
        }
        for(SalesOrderItem__x oli: lineItems){
            //if(order.phhSaleType__c == oli.phdSaleType__c)//Added by Venkat on 10292018
            //{
                if(!oli.phdItemSKU__c.startsWith('*')){
                    finallineItems.add(oli);
                }
            //}
        }
        system.debug('finallineItems---> ' +finallineItems);
        
        String orderId = order.Id;
        if(!(lineItems.isEmpty())){
            for(integer i=0; i < lineItems.size(); i++)
            {
                String String1;
                  if(!lineItems[i].phdItemSKU__c.startsWith('*')){ 
                        System.debug('line item phditemsku-->'+lineItems[i].phdItemSKU__c);
                        String getJSON = SalesOrderLineItemDetailController.getLOC(lineItems[i].ExternalId, orderId);
                        system.debug('Expected is ' + getJSON.substringAfter('"EstimatedArrival":"'));
                        String str1 = getJSON.substringAfter('"EstimatedArrival":"');
                        String1 = str1.substringBefore('","AcknowledgementNumber"');
                        system.debug('String1-->'+String1);
                        String str2 = getJSON.substringAfter('"LocationPo":"');
                      system.debug('str2-->'+str2);
                        String String2 = str2.substringBefore('","EstimatedArrival"');
                      system.debug('String2-->'+String2);
                        system.debug('EST. Arrival Date ' + String1);
                        system.debug('LOC/PO ' + String2);
                        system.debug('my JSON for Line Item' +getJSON);   
                        if((String.isBlank(String1)) || (String1 == '0')){
                            system.debug('LOC/PO---->' + String2);
                            String1 = String2;   
                        }
                    } else {
                        String1 = '';
                    }
                
                EstArrival.add(String1);
            }
        }
        System.debug('lineItems.size-->'+lineItems.size());
        return EstArrival;
    }
    
    public class SalesOrderWrapper1 implements Attributable{
        @AuraEnabled
        public string  shipToStreet1 {get;set;}
        
        @AuraEnabled
        public string  shipToStreet2 {get;set;}
        
        @AuraEnabled
        public string  shipToCity {get;set;}
        
        @AuraEnabled
        public string  shipToState {get;set;}
        
        @AuraEnabled
        public string  shipToPostalcode {get;set;}
        
        @AuraEnabled
        public string  shipToCountry {get;set;}
    }
    
    @AuraEnabled
    public static List<imageWrapper> getLineItemswithImage(Id salesforceOrderId) {
        List<SalesOrderItem__x> lineItems = new List<SalesOrderItem__x>();
        system.debug('salesforceOrderId------'+salesforceOrderId);
        SalesOrder__x order = SalesorderDAO.getOrderById(salesforceOrderId);
        List<SalesOrderItem__x> finallineItems = new List<SalesOrderItem__x>();
        system.debug('order------'+order);
        system.debug('finallineItems------'+finallineItems);
        if(order != null){
            lineItems = SalesOrderDAO.getOrderLineItemsByOrderExternalId(order.ExternalId);
            system.debug('lineItems-----'+lineItems);
        }
        String orderId = order.Id;
        List<imageWrapper> imgwappers = new List<imageWrapper>();
        ProductDetailAPIHelper prodDetailAPIHelper = new ProductDetailAPIHelper();
        for(SalesOrderItem__x oli: lineItems){
            system.debug('oli---1---'+oli.phdSaleType__c+'-----'+order.phhSaleType__c);
            //if(order.phhSaleType__c == oli.phdSaleType__c)//Added by Venkat on 10292018
            //{
                system.debug('oli---1---'+oli);
                //if(!oli.phdItemSKU__c.startsWith('*')){
                 //finallineItems.add(oli);
                 imageWrapper imgwapper = new imageWrapper();
                 imgwapper.savalue = oli;
                 String sku = formatSku(oli.phdItemSKU__c);
                 /*if(oli.phdItemSKU__c.isNumeric()){
                    sku = String.valueOf(Integer.ValueOf(oli.phdItemSKU__c));
                 }else{
                    sku = oli.phdItemSKU__c;        
                 }*/
                 List<Shopping_cart_line_item__c> snoplines =[SELECT eComm_Small_Image__c,Product_SKU__c FROM Shopping_cart_line_item__c WHERE Product_SKU__c =:sku And eComm_Small_Image__c != null limit 1];
                if(snoplines.size() > 0){
                    imgwapper.imageurl = snoplines[0].eComm_Small_Image__c;
                }else{
                    imgwapper.imageurl = '';
                }                 
                 imgwappers.add(imgwapper);
                //}
            //}
        }
        
        
        system.debug('finallineItems---> ' +finallineItems);
        return imgwappers;
    }

    public static string formatSku(String ipSku){
    	string opSku;
		if(ipSku != null)
        {
            string[] ItemNum1 = ipSku.split('[^0]*');
            integer i=0;
            for(i=0; i< ItemNum1.size(); i++){
                if(ItemNum1[i]=='') break;
                system.debug('Num1-->'+ItemNum1[i]);
                system.debug('\n');
            }
            system.debug('i-->'+i);
            opSku = ipSku.substring(i);
            opSku.trim();
            system.debug('opSku-->'+opSku);
		}
    	return opSku;
    }

    public class imageWrapper{
        @AuraEnabled
        public SalesOrderItem__x savalue {get;set;}  
        @AuraEnabled
        public String imageurl {get;set;}        
    }
}