/* This is a test class for helper class.*/
@isTest
public class CaseHelperTest {

    @testSetup
    static void setup() {
        List<Account> accList = new List<Account>();
        accList = TestDataFactory.initializeAccounts(1);
        insert accList;

        List<Contact> contList = new List<Contact>();
        contList = TestDataFactory.initializeContacts(accList[0].Id, 1);
        insert contList;

        // Insert Sales Order
        Test.startTest();
        SalesOrder__x salesOrder = new SalesOrder__x(fulfillerID__c = '8888300-164',
                                                 ExternalId = '17331400:001q000000raDkvAAE',
                                                 phhProfitcenter__c = 1234567,
                                                 Phhcustomerid__c = '784584585',
                                                 phhSalesOrder__c = '88845758'
                                                );
        system.debug('order' + salesOrder);
        SalesOrderDAO.mockedSalesOrders.add(salesOrder);
        SalesOrder__x salesOrderObj = SalesOrderDAO.getOrderById(salesOrder.Id);
        SalesOrderItem__x salesOrderItem = new SalesOrderItem__x(ExternalId = '17331400:001q000000raDkvAAE',
                                                             phdShipZip__c = '30548');
        SalesOrderDAO.mockedSalesOrderLineItems.add(salesOrderItem);
        SalesOrderItem__x salesOrderItemObj = SalesOrderDAO.getOrderLineItemByExternalId(salesOrderItem.ExternalId);
        system.debug('orderitem' + salesOrderItemObj);

		address__c adr = new address__c();
        adr.AccountId__c = accList[0].Id;
        adr.Address_Line_1__c = 'adr line 1';
        adr.Address_Line_2__c = 'adr line 2';
        adr.City__c = 'city';
        adr.StateList__c = 'CA';
        adr.Zip_Code__c = '30548';
        adr.Address_Type__c = 'Ship To';
        adr.Preferred__c = true;
        insert adr;
        
        // Insert Case
        Case caseObj = new Case(Sales_Order__c = '17331400:001q000000raDkvAAE', Status = 'New', Origin = 'Phone', Type = 'Open Order Inquiry', Subject = 'Test', Technician_ServiceReqId__c = '91506',
                            Description = 'Test', ContactId = contList[0].Id, Address__c = adr.Id);
        insert caseObj;
        Test.stopTest();
    }

    @isTest static void getCaseClosedValuesTest() {
        Test.startTest();
       	list<string> closeValList = CaseHelper.getCaseClosedValues();
       	CaseHelper.getMarketConfig();
        CaseHelper.caseIsClosed(closeValList[0]);
       	CaseHelper.getSalesOrderLineInfo('17331400:001q000000raDkvAAE');
        CaseHelper.getSalesOrderLineList('17331400:001q000000raDkvAAE');
        Test.stopTest();
    }

    @isTest static void closeCaseInHomesTest() {
        Test.startTest();
		SingleRequestMock fakeResponse = new SingleRequestMock(200, 'OK', 'Service request was closed successfully', null);
		Test.setMock(HttpCalloutMock.class, fakeResponse);
		try {
        	CaseHelper.closeCaseInHomes('17331400:001q000000raDkvAAE', '90597', null);
		} catch (Exception e) {}
        Test.stopTest();
    }

    @isTest static void closeCaseInHomesTest2() {
        Test.startTest();
		SingleRequestMock fakeResponse = new SingleRequestMock(200, 'OK', 'Service request was closed successfully', null);
		Test.setMock(HttpCalloutMock.class, fakeResponse);
		try {
        	CaseHelper.closeCaseInHomes('17331400:001q000000raDkvAAE', '90597', '8888300-164');
		} catch (Exception e) {}
        Test.stopTest();
    }

    @isTest static void servReqCommentInHomesSchedule() {
        Test.startTest();
        Case caseObj = [Select Id, Sales_Order__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
        caseObj.Subject = '';
        for (integer i = 0;i <= 30;i++) {
            caseObj.Subject += 'Subject ';
        }
        caseObj.Description = '';
        for (integer i = 0;i <= 100;i++) {
            caseObj.Description += 'Description ';
        }
        update caseObj;
        map<string, string> strMap = new map<string, string>();
        map<string, integer> intMap = new map<string, integer>();
        map<string, id> idMap = new map<string, id>();
        idMap.put('caseId', caseObj.Id);
        strMap.put('section', 'schedule');
        strMap.put('scheduledDate', '2019-01-01');
        intMap.put('totLineItem', 2);
        intMap.put('RequestId', 12345);
        strMap.put('fulfillerId', '8888300-164');
		SingleRequestMock fakeResponse = new SingleRequestMock(200, 'OK', '[12345]', null);
		Test.setMock(HttpCalloutMock.class, fakeResponse);
		try {
        	CaseHelper.servReqCommentInHomes(strMap, intMap, idMap);
		} catch (Exception e) {}
        Test.stopTest();
    }

    @isTest static void servReqCommentInHomesUnSchedule() {
        Test.startTest();
        Case caseObj = [Select Id, Sales_Order__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
        map<string, string> strMap = new map<string, string>();
        map<string, integer> intMap = new map<string, integer>();
        map<string, id> idMap = new map<string, id>();
        idMap.put('caseId', caseObj.Id);
        strMap.put('section', 'unschedule');
        intMap.put('RequestId', 12345);
        strMap.put('fulfillerId', '8888300-164');
		SingleRequestMock fakeResponse = new SingleRequestMock(200, 'OK', '[12345]', null);
		Test.setMock(HttpCalloutMock.class, fakeResponse);
		try {
        	CaseHelper.servReqCommentInHomes(strMap, intMap, idMap);
		} catch (Exception e) {}
        Test.stopTest();
    }

    @isTest static void servReqCommentInHomesShipto() {
        Test.startTest();
        Case caseObj = [Select Id, Sales_Order__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
        map<string, string> strMap = new map<string, string>();
        map<string, integer> intMap = new map<string, integer>();
        map<string, id> idMap = new map<string, id>();
        idMap.put('caseId', caseObj.Id);
        strMap.put('section', 'shipto');
        strMap.put('oldAdr', 'Old Address');
        strMap.put('newAdr', 'New Address');
        intMap.put('RequestId', 12345);
        strMap.put('fulfillerId', '8888300-164');
		SingleRequestMock fakeResponse = new SingleRequestMock(200, 'OK', '[12345]', null);
		Test.setMock(HttpCalloutMock.class, fakeResponse);
		try {
        	CaseHelper.servReqCommentInHomes(strMap, intMap, idMap);
		} catch (Exception e) {}
        Test.stopTest();
    }

    @isTest static void databaseErrorLogTest1() {
        Test.startTest();
		try {
			Account accObj = new Account();
			Database.SaveResult sr = Database.insert(accObj, false);
	        CaseHelper.databaseErrorLog(sr, 'Account Insert Error', 'CaseHelperTest', 'databaseErrorLogTest1', JSON.serialize(accObj));
		} catch (Exception e) {}
        Test.stopTest();
    }

    @isTest static void databaseErrorLogTest2() {
        Test.startTest();
		try {
			List<Account> accObjList = new List<Account>();
            accObjList.add(new Account());
			Database.SaveResult[] sr = Database.insert(accObjList, false);
	        CaseHelper.databaseErrorLog(sr, 'Account Insert Error', 'CaseHelperTest', 'databaseErrorLogTest1', JSON.serialize(accObjList));
		} catch (Exception e) {}
        Test.stopTest();
    }

    @isTest static void callApiTestErr1() {
        Test.startTest();
        HttpRequest req = new HttpRequest();
        req.setHeader('apikey', 'bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713');
        req.setHeader('Accept', 'application/json');
        req.setEndpoint('https://ashley-preprod-qa.apigee.net/homestores/8888300-164/customer-service/service-request-assignees');
        req.setMethod('GET');
        try{
            HttpResponse res = CaseHelper.callApi(req, 'callout error', 'CaseHelperTest', 'callApiTestErr1', 'Test');
        } catch(exception e) {}
        Test.stopTest();
    }

    @isTest static void callApiTestErr2() {
        Test.startTest();
        try{
            Test.setMock(HttpCalloutMock.class, new MockCallout(400, 'OK', '', new Map<String, String>()));
            HttpResponse res = CaseHelper.callApi(new HttpRequest(), 'callout error', 'CaseHelperTest', 'callApiTestErr2', 'Test');
        } catch(exception e) {}
        Test.stopTest();
    }

    @isTest static void exceptionErrorTest() {
        Test.startTest();
        Account cs = new Account();
        try{
            insert cs;
        } catch(Exception e) {
            CaseHelper.exceptionError(e, 'Case insert Error', 'CaseHelperTest', 'exceptionErrorTest', JSON.serialize(cs));
        }
        Test.stopTest();
    }

    @isTest static void dmlExceptionErrorTest() {
        Test.startTest();
        Account cs = new Account();
        try{
            insert cs;
        } catch(DmlException de) {
            CaseHelper.dmlExceptionError(de, 'Case insert Error', 'CaseHelperTest', 'dmlExceptionErrorTest', JSON.serialize(cs));
        }
        Test.stopTest();
    }

    @isTest static void returnCalloutResponseTestErr1() {
        Test.startTest();
        HttpRequest req = new HttpRequest();
        req.setHeader('apikey', 'bGna5ABiJwsJyqV9DDtQmTBEH2Kpz713');
        req.setHeader('Accept', 'application/json');
        req.setEndpoint('https://ashley-preprod-qa.apigee.net/homestores/8888300-164/customer-service/service-request-assignees');
        req.setMethod('GET');
        try{
            CaseHelper.CalloutResponse res = CaseHelper.returnCalloutResponse(req, 'callout error', 'CaseHelperTest', 'callApiTestErr1', 'Test');
			if (res.InsErrorLogWrapList.size() > 0) {
				CaseHelper.insertError(res.InsErrorLogWrapList);
			}
        } catch(exception e) {}
        Test.stopTest();
    }

    @isTest static void returnCalloutResponseTestErr2() {
        Test.startTest();
        try{
            Test.setMock(HttpCalloutMock.class, new MockCallout(400, 'OK', '', new Map<String, String>()));
            CaseHelper.CalloutResponse res = CaseHelper.returnCalloutResponse(new HttpRequest(), 'callout error', 'CaseHelperTest', 'callApiTestErr2', 'Test');
        } catch(exception e) {}
        Test.stopTest();
    }

    @isTest static void getCaseFulfillerIdTest() {
        Test.startTest();
        try{
            CaseHelper.getCaseFulfillerId('8888300-164', '');
            CaseHelper.getCaseFulfillerId('', '17331400:001q000000raDkvAAE');
        } catch(exception e) {}
        Test.stopTest();
    }

    @isTest static void updateCaseAdrSchTest() {
        Test.startTest();
        Case caseObj = [Select Id, Sales_Order__c, Address__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE'];
        try{
            CaseHelper.updateCaseAdrSch(caseObj.Address__c, caseObj.Id, '8888300-164', 12345);
        } catch(exception e) {}
        Test.stopTest();
    }
}