global class SendEmailControllerInbound implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        String myPlainText= '';
        // Add the email plain text into the local variable 
        myPlainText = email.plainTextBody;        
        string sub = email.subject;
        string [] a = new string[]{};
        string [] bb = new string[]{};
        if(String.isNotEmpty(sub) && String.isNotBlank(sub)){
            a = sub.split('\\(');
            bb = a[1].split('\\)');
            string subrecid = bb[0] ;
            Case mycase = [SELECT Id,CaseNumber,ContactEmail,ContactId FROM Case WHERE CaseNumber =:subrecid];
            // For storing Relation Id
            string oppcid = mycase.ContactId;
            //Add Inbound Email Message for Case
            EmailMessage caseEmailMessage = new EmailMessage();
            caseEmailMessage.ToAddress =  String.join(email.toAddresses, ',');
            caseEmailMessage.FromAddress = email.FromAddress;
            caseEmailMessage.FromName = email.FromName;
            caseEmailMessage.Subject = sub;
            caseEmailMessage.status = '2';
            caseEmailMessage.HtmlBody = email.htmlBody;
            caseEmailMessage.Incoming= True;
            caseEmailMessage.TextBody = myPlainText;
            caseEmailMessage.ParentId = mycase.Id;
            insert caseEmailMessage;
            // Add Email Message Relation for id of the sender
            EmailMessageRelation emr = new EmailMessageRelation();
            emr.EmailMessageId = caseEmailMessage.Id;
            emr.RelationId = oppcid;// user id of the sender
            emr.RelationAddress = email.FromAddress;
            emr.RelationType = 'FromAddress';
            insert emr; 
        }
        else{
             //Send an Email to Queue
            QueueSobject Qso = new QueueSobject();
            Qso = [SELECT Id,QueueId,SobjectType,Queue.Name,Queue.Email FROM QueueSobject WHERE Queue.Name = 'Cases without Accounts'];
            Qso.Queue.Email = System.label.Queue_Email_for_Email_to_others;
            String us = Qso.Queue.Email;
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {us};
            mail.setToAddresses(toAddresses );
            mail.setSubject('CaseReplyEmail: Without Subject');
            mail.setHtmlBody(email.htmlBody);
            mail.setPlainTextBody(myPlainText);
            Messaging.SendEmail(new Messaging.SingleEmailMessage[] {mail});   
        }
        return result;
    }
    
}