@isTest
public class ProductLineItemRefreshTest {
    @testSetup
    static void setup() {
        List<Account> accList = new List<Account>();
        accList = TestDataFactory.initializeAccounts(1);
        insert accList;

        List<Contact> contList = new List<Contact>();
        contList = TestDataFactory.initializeContacts(accList[0].Id, 1);
        insert contList;

        // Insert Sales Order
        Test.startTest();
        SalesOrder__x salesOrder = new SalesOrder__x(fulfillerID__c = '8888300-164',
                                                 ExternalId = '17331400:001q000000raDkvAAE',
                                                 phhProfitcenter__c = 1234567,
                                                 Phhcustomerid__c = '784584585',
                                                 phhSalesOrder__c = '88845758'
                                                );
        system.debug('order' + salesOrder);
        SalesOrderDAO.mockedSalesOrders.add(salesOrder);
        SalesOrder__x salesOrderObj = SalesOrderDAO.getOrderById(salesOrder.Id);
        SalesOrderItem__x salesOrderItem = new SalesOrderItem__x(ExternalId = '17331400:001q000000raDkvAAE',
                                                             phdShipZip__c = '30548');
        SalesOrderDAO.mockedSalesOrderLineItems.add(salesOrderItem);
        SalesOrderItem__x salesOrderItemObj = SalesOrderDAO.getOrderLineItemByExternalId(salesOrderItem.ExternalId);
        system.debug('orderitem' + salesOrderItemObj);

        Group testGroup = new Group(Name='Parts_Ordered_Cases', Type='Queue');
        insert testGroup;
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
        }
        
        Group testGroup2 = new Group(Name='East_Tech_Schedule', Type='Queue');
        insert testGroup2;
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup2.id, SObjectType = 'Case');
            insert testQueue;
        }
        Group testGroup3 = new Group(Name='West_Tech_Schedule', Type='Queue');
        insert testGroup3;
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup3.id, SObjectType = 'Case');
            insert testQueue;
        }
                
        address__c adr = new address__c();
        adr.AccountId__c = accList[0].Id;
        adr.Address_Line_1__c = 'adr line 1';
        adr.Address_Line_2__c = 'adr line 2';
        adr.City__c = 'city';
        adr.StateList__c = 'CA';
        adr.Zip_Code__c = '30548';
        adr.Address_Type__c = 'Ship To';
        adr.Preferred__c = true;
        insert adr;

        // Insert Case
        Case caseObj = new Case(Sales_Order__c = '17331400:001q000000raDkvAAE', Status = 'New', Origin = 'Phone', Type = 'Open Order Inquiry', Subject = 'Test', Technician_ServiceReqId__c = '91506',
                            Description = 'Test', ContactId = contList[0].Id, Address__c = adr.Id, Technician_Schedule_Date__c = Date.today()+1, Legacy_Account_Ship_To__c = '8888000-130',  Type_of_Resolution__c = 'No', Resolution_Notes__c = 'Test');
        insert caseObj;
        Case caseObj2 = new Case(Sales_Order__c = '17331400:001q000000raDkvAAE', Status = 'New', Origin = 'Phone', Type = 'Open Order Inquiry', Subject = 'Test', Technician_ServiceReqId__c = '91506',
                            Description = 'Test', ContactId = contList[0].Id, Address__c = adr.Id, Tech_Scheduled_Date__c = Date.today()+1, Legacy_Account_Ship_To__c = '8888300-164',  Type_of_Resolution__c = 'No', Resolution_Notes__c = 'Test');
        insert caseObj2;
        Case caseObj3 = new Case(Sales_Order__c = '17331400:001q000000raDkvAAE', Status = 'New', Origin = 'Phone', Type = 'Open Order Inquiry', Subject = 'Test', Technician_ServiceReqId__c = '91506',
                            Description = 'Test', ContactId = contList[0].Id, Address__c = adr.Id, Legacy_Account_Ship_To__c = '8888300-164',  Type_of_Resolution__c = 'No', Resolution_Notes__c = 'Test');
        insert caseObj3;
        Test.stopTest();
    }

    @isTest static void testTechScheduled() {

        Case caseObj = [Select Id, Sales_Order__c, Technician_Schedule_Date__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE' AND Technician_Schedule_Date__c!= null];
        List<ProductLineItem__c> pliList = new List<ProductLineItem__c>();
        pliList.add(new ProductLineItem__c(Sales_Order_Number__c = '17331400', Case__c = caseObj.Id, Item_SKU__c = 'B219-31', Item_Serial_Number__c = '99999', Item_Seq_Number__c = '100', Fulfiller_ID__c = '8888300-164'));
        insert pliList;
        ProductLineItem__c pliObj = [SELECT Id, Fulfiller_ID__c, Ashley_Direct_Link_ID__c, Part_Order_Number__c FROM ProductLineItem__c WHERE Id = :pliList[0].Id];
		system.debug('PLI: ' + pliObj);
        string tokenRespBody = '{'
			+'    "token_type": "Bearer",'
			+'    "scope": "user_impersonation",'
			+'    "expires_in": "3600",'
			+'    "ext_expires_in": "3600",'
			+'    "expires_on": "1545292956",'
			+'    "not_before": "1545289056",'
			+'    "resource": "https://MasterAshley.onmicrosoft.com/Ashley.ReplacementParts.Api",'
			+'    "access_token": "eyJ0eXAi.eyJhdWQiOiJodHRw.hQF-yp-_X_Z-L-a-T-K-Q-Vq_S-3E_Xe",'
			+'    "refresh_token": "AQABAAAAAAC-19_F-6-l-a-W-t--i-W-hE_LR_4d_kt-f-N__u-pf-bI"'
			+'}';

        String strTest = pliObj.Fulfiller_ID__c;
        List<String> arrTest = strTest.split('\\-');
        String customerNumber = arrTest[0];
        String shipTo = arrTest[1];

        string orderStatusEndPoint = system.label.ReplacementPartSerialNumber+'/orders/status?ponumber='+pliObj.Ashley_Direct_Link_ID__c+'&customerNumber='+customerNumber+'&shipTo='+shipTo+'&apikey=NvcXrflst6O2sQurPOzOxytA1bUVvW8t';

        string orderStatusRespBody = '{'
			+'    "ExtDetail": {'
			+'        "RPKey": "2416767",'
			+'        "ShipVia": "UPS",'
			+'        "Customer": "'+customerNumber+'",'
			+'        "ShipTo": "'+shipTo+'",'
			+'        "PONumber": "'+pliList[0].Ashley_Direct_Link_ID__c+'",'
			+'        "Address1": "DBA HELPING HAND",'
			+'        "Address2": "2100 WHITMAN AVENUE",'
			+'        "Address3": "CHICO",'
			+'        "Street": "CA",'
			+'        "ZipCode": "95928",'
			+'        "ShipDate": "20190823",'
			+'        "UPSTracking": "92748901790654513057233781",'
			+'        "DefectCode": "DW",'
			+'        "Defect": "DRILLED WRONG",'
			+'        "DefectLocationCode": "CC",'
			+'        "DefectLocation": "CUSHION COVER",'
			+'        "Model": "B219-31"'
			+'    }'
			+'}';

        SingleRequestMock tokenResp = new SingleRequestMock(200, 'OK', tokenRespBody, null);
        SingleRequestMock orderStatusResp = new SingleRequestMock(200, 'OK', orderStatusRespBody, null);

        Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put(Label.Ashley_TokenGenerationURL, tokenResp);
        endpoint2TestResp.put(orderStatusEndPoint, orderStatusResp);
		system.debug('ep: ' + orderStatusEndPoint);
        HttpCalloutMock multiCalloutMock = new MultiRequestMock(endpoint2TestResp);
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);

        Test.startTest();
        Database.executeBatch(new ProductLineItemRefresh());
        Test.stopTest();   
    }
	@isTest static void testLegacyTechScheduled() {

        Case caseObj = [Select Id, Sales_Order__c, Tech_Scheduled_Date__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE' AND Tech_Scheduled_Date__c!= null ];
        List<ProductLineItem__c> pliList = new List<ProductLineItem__c>();
        pliList.add(new ProductLineItem__c(Sales_Order_Number__c = '17331400', Case__c = caseObj.Id, Item_SKU__c = 'B219-31', Item_Serial_Number__c = '99999', Item_Seq_Number__c = '100', Fulfiller_ID__c = '8888300-164'));
        insert pliList;
        ProductLineItem__c pliObj = [SELECT Id, Fulfiller_ID__c, Ashley_Direct_Link_ID__c, Part_Order_Number__c FROM ProductLineItem__c WHERE Id = :pliList[0].Id];
		system.debug('PLI: ' + pliObj);
        string tokenRespBody = '{'
			+'    "token_type": "Bearer",'
			+'    "scope": "user_impersonation",'
			+'    "expires_in": "3600",'
			+'    "ext_expires_in": "3600",'
			+'    "expires_on": "1545292956",'
			+'    "not_before": "1545289056",'
			+'    "resource": "https://MasterAshley.onmicrosoft.com/Ashley.ReplacementParts.Api",'
			+'    "access_token": "eyJ0eXAi.eyJhdWQiOiJodHRw.hQF-yp-_X_Z-L-a-T-K-Q-Vq_S-3E_Xe",'
			+'    "refresh_token": "AQABAAAAAAC-19_F-6-l-a-W-t--i-W-hE_LR_4d_kt-f-N__u-pf-bI"'
			+'}';

        String strTest = pliObj.Fulfiller_ID__c;
        List<String> arrTest = strTest.split('\\-');
        String customerNumber = arrTest[0];
        String shipTo = arrTest[1];

        string orderStatusEndPoint = system.label.ReplacementPartSerialNumber+'/orders/status?ponumber='+pliObj.Ashley_Direct_Link_ID__c+'&customerNumber='+customerNumber+'&shipTo='+shipTo+'&apikey=NvcXrflst6O2sQurPOzOxytA1bUVvW8t';

        string orderStatusRespBody = '{'
			+'    "ExtDetail": {'
			+'        "RPKey": "2416767",'
			+'        "ShipVia": "UPS",'
			+'        "Customer": "'+customerNumber+'",'
			+'        "ShipTo": "'+shipTo+'",'
			+'        "PONumber": "'+pliList[0].Ashley_Direct_Link_ID__c+'",'
			+'        "Address1": "DBA HELPING HAND",'
			+'        "Address2": "2100 WHITMAN AVENUE",'
			+'        "Address3": "CHICO",'
			+'        "Street": "CA",'
			+'        "ZipCode": "95928",'
			+'        "ShipDate": "20190823",'
			+'        "UPSTracking": "92748901790654513057233781",'
			+'        "DefectCode": "DW",'
			+'        "Defect": "DRILLED WRONG",'
			+'        "DefectLocationCode": "CC",'
			+'        "DefectLocation": "CUSHION COVER",'
			+'        "Model": "B219-31"'
			+'    }'
			+'}';

        SingleRequestMock tokenResp = new SingleRequestMock(200, 'OK', tokenRespBody, null);
        SingleRequestMock orderStatusResp = new SingleRequestMock(200, 'OK', orderStatusRespBody, null);

        Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put(Label.Ashley_TokenGenerationURL, tokenResp);
        endpoint2TestResp.put(orderStatusEndPoint, orderStatusResp);
		system.debug('ep: ' + orderStatusEndPoint);
        HttpCalloutMock multiCalloutMock = new MultiRequestMock(endpoint2TestResp);
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);

        Test.startTest();
        Database.executeBatch(new ProductLineItemRefresh());
        Test.stopTest();   
    }
    @isTest static void testWithoutTechScheduled() {

        Case caseObj = [Select Id, Sales_Order__c, Technician_Schedule_Date__c from Case Where Sales_Order__c = '17331400:001q000000raDkvAAE' AND Technician_Schedule_Date__c = Null AND Tech_Scheduled_Date__c = null];
        List<ProductLineItem__c> pliList = new List<ProductLineItem__c>();
        pliList.add(new ProductLineItem__c(Sales_Order_Number__c = '17331400', Case__c = caseObj.Id, Item_SKU__c = 'B219-31', Item_Serial_Number__c = '99999', Item_Seq_Number__c = '100', Fulfiller_ID__c = '8888300-164'));
        insert pliList;
        ProductLineItem__c pliObj = [SELECT Id, Fulfiller_ID__c, Ashley_Direct_Link_ID__c, Part_Order_Number__c FROM ProductLineItem__c WHERE Id = :pliList[0].Id];
		system.debug('PLI: ' + pliObj);
        string tokenRespBody = '{'
			+'    "token_type": "Bearer",'
			+'    "scope": "user_impersonation",'
			+'    "expires_in": "3600",'
			+'    "ext_expires_in": "3600",'
			+'    "expires_on": "1545292956",'
			+'    "not_before": "1545289056",'
			+'    "resource": "https://MasterAshley.onmicrosoft.com/Ashley.ReplacementParts.Api",'
			+'    "access_token": "eyJ0eXAi.eyJhdWQiOiJodHRw.hQF-yp-_X_Z-L-a-T-K-Q-Vq_S-3E_Xe",'
			+'    "refresh_token": "AQABAAAAAAC-19_F-6-l-a-W-t--i-W-hE_LR_4d_kt-f-N__u-pf-bI"'
			+'}';

        String strTest = pliObj.Fulfiller_ID__c;
        List<String> arrTest = strTest.split('\\-');
        String customerNumber = arrTest[0];
        String shipTo = arrTest[1];

        string orderStatusEndPoint = system.label.ReplacementPartSerialNumber+'/orders/status?ponumber='+pliObj.Ashley_Direct_Link_ID__c+'&customerNumber='+customerNumber+'&shipTo='+shipTo+'&apikey=NvcXrflst6O2sQurPOzOxytA1bUVvW8t';

        string orderStatusRespBody = '{'
			+'    "ExtDetail": {'
			+'        "RPKey": "2416767",'
			+'        "ShipVia": "UPS",'
			+'        "Customer": "'+customerNumber+'",'
			+'        "ShipTo": "'+shipTo+'",'
			+'        "PONumber": "'+pliList[0].Ashley_Direct_Link_ID__c+'",'
			+'        "Address1": "DBA HELPING HAND",'
			+'        "Address2": "2100 WHITMAN AVENUE",'
			+'        "Address3": "CHICO",'
			+'        "Street": "CA",'
			+'        "ZipCode": "95928",'
			+'        "ShipDate": "20190823",'
			+'        "UPSTracking": "92748901790654513057233781",'
			+'        "DefectCode": "DW",'
			+'        "Defect": "DRILLED WRONG",'
			+'        "DefectLocationCode": "CC",'
			+'        "DefectLocation": "CUSHION COVER",'
			+'        "Model": "B219-31"'
			+'    }'
			+'}';

        SingleRequestMock tokenResp = new SingleRequestMock(200, 'OK', tokenRespBody, null);
        SingleRequestMock orderStatusResp = new SingleRequestMock(200, 'OK', orderStatusRespBody, null);

        Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put(Label.Ashley_TokenGenerationURL, tokenResp);
        endpoint2TestResp.put(orderStatusEndPoint, orderStatusResp);
		system.debug('ep: ' + orderStatusEndPoint);
        HttpCalloutMock multiCalloutMock = new MultiRequestMock(endpoint2TestResp);
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);

        Test.startTest();
        Database.executeBatch(new ProductLineItemRefresh());
        Test.stopTest();   
    }
}