global class caseAttachmentBatch implements Database.Batchable<sObject> {
	public String query;   
   global Database.QueryLocator start(Database.BatchableContext BC){
      if(query != null){
            return Database.getQueryLocator(query);
      }else{
        return Database.getQueryLocator([select id, Body, Name, ParentId, OwnerId from Attachment where ParentId != null]);
      }      
   }
   global void execute(Database.BatchableContext BC, List<Attachment> scope){
       list<Attachment> tobeadd = new list<Attachment>();
       list<attachment> tobedelete = new list<Attachment>();
       Attachment att; 
       List<Id> tids = new List<Id>();
       for(Attachment attfile : scope){            
          if (String.valueOf(attfile.ParentId).substring(0, 3) == '00T'){
              tids.add(attfile.id);
              List<Task> tasks = [SELECT id, WhatId, whoId, type, status FROM Task where Id =: attfile.ParentId];
              for(Task tk : tasks){
                  if(String.valueOf(tk.WhatId).substring(0, 3) == '500'){
                      Attachment b = New Attachment(Name = attfile.Name, Body = attfile.Body, OwnerId = attfile.OwnerId);
                      b.parentID = tk.WhatId;
                      //attfile.ParentId = tk.WhatId;
                      tobeadd.add(b);
                      tobedelete.add(attfile);
                  }
              }
          }
      }       
        Database.SaveResult [] saveResults;
        Database.deleteresult[] saveResults1;
        if(tobeadd.size()> 0){
           saveResults = Database.insert(tobeadd, false);
     	}   
       if(tobedelete.size()> 0){
           saveResults1 = Database.delete(tobedelete, false);           
       }
   }
   global void finish(Database.BatchableContext BC){
   }
}