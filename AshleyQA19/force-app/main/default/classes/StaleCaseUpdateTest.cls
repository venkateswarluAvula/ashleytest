@isTest
public class StaleCaseUpdateTest {
    @isTest
    static void testMethod1(){
        Account acc = new Account(Name='test');
        insert acc;
        Address__c Add = new Address__c();
        Add.Address_Line_1__c = '91 Andrew And Vincent Abate Way';
        Add.City__c = 'London';
        Add.Zip_Code__c = '411058';
        Add.Address_Type__c = 'Ship To';
        Add.Country__c = 'UK';
        Add.AccountId__c = acc.id;
        Add.Preferred__c = true;
        Add.State__c = 'MH';
        insert Add;
        
        List<Case> Caselist = new List<Case>();
        Case mycase = new Case();
        mycase.AccountId = acc.id;
        mycase.Status = 'New';
        mycase.Type= 'Delivery order Inquery';
        mycase.Sub_Type__c = 'test ';
        mycase.CreatedDate = date.today()-65;
        mycase.LastModifiedDate = Date.today()-64;
        mycase.Tech_Scheduled_Date__c = date.today()-70;
        mycase.Technician_Schedule_Date__c = date.today()-80;
        mycase.Address__c = Add.id;
        mycase.Type_of_Resolution__c = 'No';
        mycase.Resolution_Notes__c = 'Test';
        insert mycase;
        
        
        Test.startTest();
        Database.executeBatch(new StaleCaseUpdate());
        Test.stopTest();
    } 
    
}