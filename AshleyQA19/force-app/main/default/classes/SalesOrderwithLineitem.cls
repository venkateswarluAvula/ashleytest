@RestResource(urlMapping = '/SalesOrderLineItemExportCSV/*')
global with sharing class SalesOrderwithLineitem {
    
    @HttpGet
    global static List<String> getOrderValues(){
        
        RestRequest request = RestContext.request;        
        String sId = request.requestURI.substring(request.requestURI.lastIndexOf('/') + 1);        
        List<String> salesorders = new List<String>();        
        List<Opportunity> opps = [SELECT ID,STAGENAME, ACCOUNT.OWNER.NAME, ACCOUNT.OWNER.Email,ACCOUNT.PersonEmail,Account.FirstName,Account.LastName, (SELECT ID,PRODUCT_SKU__C,Product_Title__c, DELIVERYDATE__C,QUANTITY__C,LAST_PRICE__C FROM SHOPPING_CART_LINE_ITEMS__R) FROM OPPORTUNITY WHERE STAGENAME != 'CLOSED WON' And ACCOUNT.OWNER.NAME != 'Integration User' And ACCOUNT.CREATEDDATE = LAST_N_DAYS:7];
        //String headervalues = 'Order ID' + ',' + 'Email Address' + ',' + 'Total Spent' + ',' + 'Date Delivered' + ',' + 'Product IDs' + ',' + 'Store ID' + ',' + 'Store Location';        
        String headervalues = 'Account Owner: Full Name' + ',' + 'Stage ' + ',' + 'Last Price ' + ',' + 'Person Account: Full Name' + ',' + 'Product SKU' + ',' + 'Product Title' + ',' + 'Person Account: Email';        
        salesorders.add(headervalues);
        String strStoreItem = '';
        
        for(Opportunity opp: opps){
            strStoreItem = opp.ACCOUNT.OWNER.NAME + ','+ opp.StageName + ',';
			String pipestring = '';
            String pipetitle = '';
            Double total = 0.00;
			Date deliveryDate;
            
            for(SHOPPING_CART_LINE_ITEM__C scli: opp.SHOPPING_CART_LINE_ITEMS__R){                
                pipestring += scli.PRODUCT_SKU__C + '|';
                pipetitle += scli.Product_Title__c + '|';
                total += (scli.LAST_PRICE__C);
                deliveryDate = scli.DeliveryDate__c;                
            }
            
            strStoreItem += String.valueOf(total) + ',' + opp.ACCOUNT.FirstName + ' ' + opp.ACCOUNT.LastName + ',' + pipestring.removeEnd('|') + ',' + pipetitle.removeEnd('|') + ','; 
            strStoreItem += opp.ACCOUNT.PersonEmail;
            salesorders.add(strStoreItem);
        }
        return salesorders;
    }
}