/* *******************************************************************************************************************
* Class Name   : ProductReviewAndRatingCallOutMock
* Description  : Http Call Out Mock test class for Asheley Product Reviews and Rating API .
* Author       : JoJo Zhao (Perficient, Inc.)
* Created On   : 12/20/2017
* Modification Log:  
* --------------------------------------------------------------------------------------------------------------------------------------
* Developer                          Date                   Modification ID      Description 
* ---------------------------------------------------------------------------------------------------------------------------------------
*Shiva paila						26/03/19										Replacing Bazar voice with TurnTo
*************************************************************************************************************************************///
@isTest
global class ProductReviewAndRatingCallOutMock implements HttpCalloutMock {
    private String fetchType;
    /**
    * @description <Constructor for apiType> 
    **/
    global ProductReviewAndRatingCallOutMock(String apiType){
        this.fetchType = apiType;
    }
    
    /**
    * @description <Implement the interface HttpCalloutMock's respond method to return mock response>
    * @return <return is the Mocked JSON string or throw a CalloutException>
    **/
	global HTTPResponse respond(HTTPRequest req) {
        if(fetchType == 'CalloutExceptionTest'){
            CalloutException e = (CalloutException)CalloutException.class.newInstance();
            e.setMessage('Throw CalloutException Test. Unauthorized endpoint, please check Setup->Security->Remote site settings.');
            throw e;
        }else{
            system.debug(req.getEndpoint());
            System.assertEquals('GET', req.getMethod());
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            
            ProductRatingsWrapper productRating;
           // ProductReviewsWrapper productReviews;
            
            if(fetchType == 'JSONExceptionTest'){
                res.setBody('Non JSON format String to make it throw JSONException when do the JSON Parse');
            }else if(fetchType == 'RatingTest'){
                productRating = prepareProductRating();
                String bodyContent = JSON.serialize(productRating);
                res.setBody(bodyContent);
            }
            res.setStatusCode(200);
            return res;
        }
    }


     /**
    * @description <Simulate Product Rating info>                                                       
    * @return <Asserts return is the Mocked ProductRatingsWrapper object>
    **/    
    public static ProductRatingsWrapper prepareProductRating(){
        ProductRatingStatisticWrapper prsw = new ProductRatingStatisticWrapper();
        prsw.averageOverallRating = 5.0;
        prsw.overalRatingRange = 5;
        prsw.totalReviewCount = 1;

        ProductStatisticsWrapper psw = new ProductStatisticsWrapper();
        psw.productStatistics = new ProductStatisticWrapper();
        psw.productStatistics.productId ='1200014';
        psw.productStatistics.reviewStatistics = prsw;
        
       	ProductRatingsWrapper productRating = new ProductRatingsWrapper();
        productRating.totalResults = 1;
        productRating.results = new List<ProductStatisticsWrapper>{psw};
         
        return productRating;
    }
     /**
    * @description <Simulate Product Reviews info>                                                     
    * @return <Asserts return is the Mocked ProductRatingsWrapper object>
    **/    
 /*  public static ProductReviewsWrapper prepareProductReviews(){
        ProductReviewsWrapper prsW = new ProductReviewsWrapper();
        ProductReviewWrapper productReview = new ProductReviewWrapper();
       	ProductReviewcatalogIt prdtusr= new ProductReviewcatalogIt();
		prdtusr.reviewCount =1; 
        prdtusr.averageRating =2.4;
        productReview.catalogItems = new List<ProductReviewcatalogIt>{prdtusr};
        ProductUserReviewWrap prdur = new ProductUserReviewWrap();
        prdur.nickName = 'test';
       	productReview.id= 1200014; 
       	productReview.ProductId= '1200014';        
        productReview.rating=5;
        productReview.text= '1200014';
        prsW.reviews = new List<ProductReviewWrapper>{productReview};
            
        return prsW;
    }*/
    
}