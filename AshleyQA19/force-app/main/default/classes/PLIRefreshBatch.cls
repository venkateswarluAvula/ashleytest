global class PLIRefreshBatch implements Schedulable {
    global void execute(SchedulableContext sc) {
        ProductLineItemRefresh pl = new ProductLineItemRefresh();
        Database.executeBatch(pl,50);
    }
 //   public static String CRON_EXP = '14 12 0 6 12 ? 2019';
/*    GLOBAL List<ProductLineItem__c> lstPLI =new List<ProductLineItem__c>();
    
    global void execute(SchedulableContext ctx) {
        lstPLI = [SELECT Id,Part_Order_Number__c,Fulfiller_ID__c,
                                         Part_Order_Num__c,Ashley_Direct_Link_ID__c FROM ProductLineItem__c];
        if(!lstPLI.isEmpty()){
            ID jobID = System.enqueueJob(new ProductLineItemRefresh(lstPLI));
        }
    }   */
}