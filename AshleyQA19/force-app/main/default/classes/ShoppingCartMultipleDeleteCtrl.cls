public class ShoppingCartMultipleDeleteCtrl {
    
    @AuraEnabled
    public static String multipleDleteFromCart(String[] lineItemIdList,string AccountId){
        try{
            List<Shopping_cart_line_item__c> DeletedItems = new  List<Shopping_cart_line_item__c>();
            List<Shopping_cart_line_item__c> lineItem = [Select Id from Shopping_cart_line_item__c where id IN:lineItemIdList];
            for(Shopping_cart_line_item__c itemId:lineItem){
                
                DeletedItems.add(itemId);
            }
            
            delete DeletedItems;
            List<Opportunity> opp = [SELECT Id FROM Opportunity WHERE AccountId =:AccountId AND StageName = 'saved shopping cart'];
            for(Opportunity oppid:opp){
                List<AggregateResult> agr = [SELECT count(Id)numCartLineItems 
                                             FROM Shopping_cart_line_item__c 
                                             WHERE Opportunity__c = :oppid.Id];
                if(agr[0].get('numCartLineItems') == 0){
                    Opportunity opty = new Opportunity(Id = oppid.Id, Cart_Grand_Total__c = 0);
                    update opty;
                }
            }
        }
        catch (Exception ex){
            System.debug(LoggingLevel.ERROR, 'Failed to Delete Items: '+ ex.getMessage());
            ErrorLogController.createLogFuture('ShoppingCartMultipleDeleteCtrl', 'multipleDleteFromCart', 'Failed to Delete Items: ' + ex.getMessage() +  ' Stack Trace: ' + ex.getStackTraceString() );         
            throw new AuraHandledException(ex.getMessage());
        }    
        
        return 'Success';
    }
    
}