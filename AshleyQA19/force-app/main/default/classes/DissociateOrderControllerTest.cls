@isTest
public class DissociateOrderControllerTest {
    @isTest static void testGetMethod(){
        //Create Account
        Account delacct = new Account();
        delacct.LastName = 'last' + Math.random();
        delacct.FirstName = 'first' + Math.random();
        string phone = '888' + Integer.valueOf(Math.random() * 10000);
        Insert delacct;
        System.debug('delacct-->'+delacct.Id);
        
        //Create Case
        Case caseObj = new Case(Sales_Order__c = '17331400:001q000000raDkvAAE', Status = 'New', Origin = 'Phone', 
                                Type = 'Open Order Inquiry',sub_Type__c = 'Finance' , Subject = 'Test',
                                Description = 'Test', AccountId = delacct.Id );
        insert caseObj;
        String caseId = caseObj.Id;
        
        Test.startTest();
        Boolean resmsg = DissociateOrder.getCase(caseId);
        System.debug(resmsg);
   		Test.stopTest();
    }
    @isTest static void testGetMethod2(){
        //Create Account
        Account delacct = new Account();
        delacct.LastName = 'last' + Math.random();
        delacct.FirstName = 'first' + Math.random();
        string phone = '888' + Integer.valueOf(Math.random() * 10000);
        Insert delacct;
        System.debug('delacct-->'+delacct.Id);
        
        //Create Case
        Case caseObj = new Case(Status = 'New', Origin = 'Phone', 
                                Type = 'Open Order Inquiry',sub_Type__c = 'Finance' , Subject = 'Test',
                                Description = 'Test', AccountId = delacct.Id );
        insert caseObj;
        String caseId = caseObj.Id;
        
        Test.startTest();
        Boolean resmsg = DissociateOrder.getCase(caseId);
        System.debug(resmsg);
   		Test.stopTest();
    }
    @isTest static void testGetMethod3(){
        Test.startTest();
        try{
        Boolean resmsg = DissociateOrder.getCase('5003D0000036y7NQAQ');
        System.debug(resmsg);
        }
        catch(exception ex){
            
        }
   		Test.stopTest();
    }

}