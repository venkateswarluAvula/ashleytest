/****** v1 | Description: Methods for viewing customer associated records | 12/5/2017 | L OMeara */
/****** v2 | Description: Updated for person accounts | 2/1/2018 | L OMeara */
/****** v3 | Description: Updated for Preferred filed,Duplicate Address and checking address related to any case| 01/04/2019 | Ajay Sagar */
public with sharing class ViewCustomerController {
    @AuraEnabled
    public static List<Address__c> getCustomerAddresses(Id customerId){      
        return [SELECT Id,Address_Line_1__c,Address_Line_2__c,City__c,StateList__c,Zip_Code__c, Preferred__c,Address_Type__c,Address_Validation_Status__c, Address_Validation_Timestamp__c
                           FROM Address__c WHERE AccountId__c=:customerId ORDER BY Preferred__c DESC, LastModifiedDate DESC LIMIT 5];
    }
    
    @AuraEnabled
    public static String saveAddress(Address__c toSave) {
        string validMessage;
    	try {
            Map<string, id> accAdrMap = AddressHelper.getCustomerAddressWithType(toSave.AccountId__c, toSave.Id);
            string dupStr = AddressHelper.getAddressStr(toSave.Address_Line_1__c, toSave.Address_Line_2__c, toSave.City__c, toSave.StateList__c, toSave.Zip_Code__c, toSave.Address_Type__c);
            if ((dupStr != null) && (accAdrMap.get(dupStr) != null)) {
                //duplicate address
                validMessage = 'duplicate address';
            }else{
                List<Case> Caselist = new List<Case>();
                if(toSave.Id != null) {
                     Caselist = [Select Id from Case where Address__c =:toSave.Id];  
                }
                if(Caselist.size() > 0 ){
                 toSave.id = null;
                 insert toSave;
                }else{
                 upsert toSave;   
                }
                if(toSave.Preferred__c){
                    AddressHelper.setCustomerPreferredAddress(toSave.AccountId__c, toSave.Id);    
                }  
                validMessage = 'Success'; 
            }
        }catch(Exception e){
            system.debug(e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
        return validMessage;
    }

    @AuraEnabled
    public static String SearchAddress(string searchTerm, string country, Integer take) {
        return EDQService.SearchAddress(searchTerm, country, take);
    }

    @AuraEnabled
    public static String FormatAddress(string formatUrl) {
        return EDQService.FormatAddress(formatUrl);
    }

    @AuraEnabled
    public static void updateOwner(String Id) {
        List<Account> acc=[Select Id,OwnerId from Account Where id=:Id];
        if(!acc.isEmpty()){
            acc[0].OwnerId=userInfo.getUserId();
            update acc[0];
        }
        List<Opportunity> cartLst= new List<Opportunity>();
        String oppExpireLimit=System.Label.Cart_limit;
        String query='Select Id,Name, AccountId, Account.Name,CreatedDate,StageName,Cart_Grand_Total__c '+
            'From Opportunity '+
            'Where  Account.RecordType.DeveloperName=\'Customer\' and  AccountId =:Id and StageName != \'Closed Won\' and StageName != \'Sale Suspended\' and StageName != \'Closed Lost\' and CreatedDate = LAST_N_DAYS:'+oppExpireLimit;


        cartLst=Database.query(query);
         if(!cartLst.isEmpty()){
            cartLst[0].OwnerId=userInfo.getUserId();
            update cartLst[0];
        }
    }
}