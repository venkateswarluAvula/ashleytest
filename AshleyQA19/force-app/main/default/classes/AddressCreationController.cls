public class AddressCreationController {
    @AuraEnabled
    public static Account getAccount(Id accId) {
    	if (accId != null) {
	    	List<Account> accList = [SELECT Id, Name FROM Account WHERE Id = :accId];
	    	if (accList.size() > 0) {
	    		return accList[0]; 
	    	}
    	}
        return null;
    }

    @AuraEnabled
    public static Address__c getAddress(Id adrId) {
    	if (adrId != null) {
	    	List<Address__c> adrList = new List<Address__c>();
	        adrList = [SELECT Id, Name, AccountId__c, Preferred__c, Address_Line_1__c, Address_Line_2__c, Address_Type__c, Address_Validation_Status__c, City__c, StateList__c, Zip_Code__c,
						Country__c, Address_Validation_Timestamp__c, Geocode__Latitude__s, Geocode__Longitude__s FROM Address__c WHERE Id = :adrId];
	        if (adrList.size() > 0) {
    	    	return adrList[0];
    		}
        }
        return null;
    }

    @AuraEnabled
    public static AddressValidationWrap validateAddress(Address__c Address, Id accId) {
		string validMessage;
		List<case> csList = new List<case>();

        Address.AccountId__r = null;
		Address.Country__c = 'USA';
        system.debug('*** Address: ' + Address);

        Map<string, id> accAdrMap = AddressHelper.getCustomerAddressWithType(accId, Address.Id);
        string dupStr = AddressHelper.getAddressStr(Address.Address_Line_1__c, Address.Address_Line_2__c, Address.City__c, Address.StateList__c, Address.Zip_Code__c, Address.Address_Type__c);
		if ((dupStr != null) && (accAdrMap.get(dupStr) != null)) {
			//duplicate address
			validMessage = 'duplicate_address';
		}

		if ((validMessage == null) && (Address.Id != null)) {
			// then verify tech scheduled case and show pop up
			// if Technician_Schedule_Date__c field is not empty then assign it to 'Technician Schedule Date' else check Tech_Scheduled_Date__c field for not empty
			List<Case> caseList = [SELECT Id, caseNumber, Technician_Schedule_Date__c, Tech_Scheduled_Date__c, Address__r.Address_Line_1__c, Address__r.Address_Line_2__c, Address__r.City__c, Address__r.StateList__c, Address__r.Zip_Code__c, 
									Legacy_Account_Ship_To__c, Sales_Order__c, Technician_Company__c, Company__c, Technician_Id__c, Legacy_Service_Request_ID__c
									FROM Case WHERE (Technician_Schedule_Date__c >= TODAY OR Tech_Scheduled_Date__c >= TODAY) AND Legacy_Service_Request_ID__c != null AND address__c = :Address.Id AND IsClosed = false];
			if (caseList.size() > 0) {
				case firstCase = caseList[0];

				string oldAdr = AddressHelper.getAddressStr(firstCase.Address__r.Address_Line_1__c, firstCase.Address__r.Address_Line_2__c, firstCase.Address__r.City__c, firstCase.Address__r.StateList__c, firstCase.Address__r.Zip_Code__c);
				string newAdr = AddressHelper.getAddressStr(Address.Address_Line_1__c, Address.Address_Line_2__c, Address.City__c, Address.StateList__c, Address.Zip_Code__c);
				if (oldAdr != newAdr) {
					// case associated address is modified
					if (caseList.size() == 1) {
						// Use Case 3 & 4 - VSTS-269828
						// a case is schedule in future date for the same address
						string marketFulfillerId = CaseHelper.getCaseFulfillerId(firstCase.Legacy_Account_Ship_To__c, firstCase.Sales_Order__c);

				    	string companyValue;
				    	if (string.isNotBlank(firstCase.Technician_Company__c)) {
				    		companyValue = firstCase.Technician_Company__c;
				    	} else if (string.isNotBlank(firstCase.Company__c)) {
				    		companyValue = firstCase.Company__c;
				    	}

						if (firstCase.Address__r.Zip_Code__c == Address.Zip_Code__c) {
							// but the zip code is same, so the same technician is available
							validMessage = 'tech_available';
							csList = caseList;
						} else {
							// need to make a call out to see whether the same technician is available
							validMessage = 'tech_not_available';
							csList = caseList;

							if (string.isNotBlank(marketFulfillerId) && string.isNotBlank(companyValue) && (firstCase.Technician_Id__c > 0) && string.isNotBlank(Address.Zip_Code__c)) { 
								string techRespStr = TechSchedulingController.callServiceTechnicians(marketFulfillerId, companyValue, integer.valueOf(firstCase.Technician_Id__c), Address.Zip_Code__c);
								if (string.isNotBlank(techRespStr) && (techRespStr == 'Tech Available')) {
									validMessage = 'tech_available';
									csList = caseList;
								}
							}
						}
					} else {
						// Use Case 5 - VSTS-269828
						// multiple cases are schedule in future date for the address to update
						validMessage = 'more_schedule';
						csList = caseList;
					}
				}
			} else {
				//if the address is associated to any of the case then return to insert address
				List<Case> caseList1 = [SELECT Id FROM Case WHERE address__c = :Address.Id LIMIT 1];
				if (caseList1.size() > 0) {
					validMessage = 'other_cases';
				}
			}
		}

		AddressValidationWrap adrValidObj = new AddressValidationWrap(validMessage, csList);
		return adrValidObj;
	}

    @AuraEnabled
    public static string saveAddress(Address__c Address, id accId, string pType) {
    	string returnStr;
        Address.AccountId__r = null;
		Address.Country__c = 'USA';
        system.debug('*** Address: ' + Address);

		try {
			if ((Address.Id != null) && (pType == 'update')){
				if(!Test.isRunningTest()){
					update Address;
				}
			} else {
				if (Address.Id != null) {
					Address.Id = null;
				}
				Address.AccountId__c = accId;
				if(!Test.isRunningTest()){
					insert Address;
				}
			}

			if (Address.Id != null) {
				returnStr = 'success|' + string.ValueOf(Address.Id);
				if (Address.Preferred__c) {
					//if the insert/update address is set as preferred then make the other addresses of that customer as not preferred
					AddressHelper.setCustomerPreferredAddress(accId, Address.Id);
				}
			}
		} catch (DmlException de) {
        	CaseHelper.dmlExceptionError(de, 'Save Address Error', 'AddressCreationController', 'saveAddress', JSON.serialize(Address));
		}
        return returnStr;
	}

    @AuraEnabled
    public static string schTechAddress(Id addressId, Id caseId, string salesorder, integer reqId, string accShipTo) {
    	//get the fulfiller id based on sales order then call the tech schedule fn
    	string marketFulfillerId = CaseHelper.getCaseFulfillerId(accShipTo, salesorder);

    	return CaseHelper.updateCaseAdrSch(addressId, caseId, marketFulfillerId, reqId);
	}

    @AuraEnabled
    public static string unSchTechAddress(Id addressId, Id caseId) {
    	//get the fulfiller id based on sales order then call the tech un schedule fn
    	return techschedulingcancellation.getApiResponse(caseId, true, addressId);
    }

    // EDQ added
    @AuraEnabled
    public static String SearchAddress(string searchTerm, string country, Integer take) {
        return EDQService.SearchAddress(searchTerm, country, take);
    }

    // EDQ added
    @AuraEnabled
    public static String FormatAddress(string formatUrl) {
        return EDQService.FormatAddress(formatUrl);
    }

    public class AddressValidationWrap {
    	@AuraEnabled
    	public string vMessage { get; set; }
    	@AuraEnabled
    	public List<case> vCaseList { get; set; }

		public AddressValidationWrap(string validMessage, List<case> csList) {
			this.vMessage = validMessage;
			this.vCaseList = csList;
		}
    }
}