@iSTest
public class createSegmentControllerTest {
	
    static testMethod void insrtSegment(){
        
        List<ADS_Zip__c> lstADSZip = new List<ADS_Zip__c>();
        ADS_Zip__c adszip = new ADS_Zip__c();
        adszip.Market__c = 'Winchester Mkt';
        lstADSZip.add(adszip);  
        insert lstADSZip;
        
        Segment__c objSegment = new Segment__c();
        objSegment.Origin__c = 'Winchester Mkt';
        objSegment.Destination__c = 'Twin Falls Mkt'; 
        objSegment.Stop__c = 'testStop';        
        
        Segment__c objSegment1 = new Segment__c();
        objSegment1.Origin__c = 'Advance Mkt';
        objSegment1.Destination__c = 'Billings Mkt';
        objSegment1.Stop__c = 'testStop2';
        insert objSegment1; 
        
        objSegment1.Origin__c = 'Syracuse Mkt';
        objSegment1.Destination__c = 'Toledo Mkt';
        objSegment1.Stop__c='teststop22';
                
     Test.startTest();    
      createSegmentController.createRecord(objSegment);
      createSegmentController.updtRecord(objSegment1);
      createSegmentController.getListViews();
      createSegmentController.getMarketValues(); 
     Test.stopTest();        
    }    

}