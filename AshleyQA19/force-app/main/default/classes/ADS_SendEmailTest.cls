@isTest
public class ADS_SendEmailTest {
    @isTest static void testCases1() { 
        Account newAcc = new Account();
        newAcc.name='test';
        insert newAcc;

        Legal_Review_Request__c LRR = new Legal_Review_Request__c();
        LRR.Account__c = newAcc.Id;
        Insert LRR;

        /*
        ContentDocument documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument limit 1];
        
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = newAcc.id;
        cdl.ContentDocumentId = documents.Id;
        cdl.shareType = 'V';
        insert cdl;
        */

        ADS_SendEmail.getrecords(LRR.Id);
        ADS_SendEmail.sendEmail(LRR.Id, null);
    }
}