public class ImportLanesInOpportunities {
    @AuraEnabled
    public static string insertData(String strfromlex, Id oppId)
    {
        String returnresponse ='';
        List<ADS_Lane__c> LaneListtoInsert = new List<ADS_Lane__c>();
        system.debug('strfromlex = ' + strfromlex);
        system.debug('oppId = ' + oppId);
        strfromlex = strfromlex.replaceall('__c','');
        List<fieldWrapper> datalist = (List<fieldWrapper>) JSON.deserialize(strfromlex, List<fieldWrapper>.class);
        system.debug('datalist = ' + datalist);
        system.debug('datalistSize = ' + datalist.size());
        if(datalist.size() > 0){
            for(fieldWrapper wrapper: datalist)
            {
                system.debug('wrapper = ' + wrapper);
                ADS_Lane__c lane =new  ADS_Lane__c();
                if(wrapper.of_Trips_Per_Week != '' && wrapper.of_Trips_Per_Week != null && wrapper.of_Trips_Per_Week != 'undefined'){
                    system.debug('of_Trips_Per_Week__c = ' + wrapper.of_Trips_Per_Week);
                    lane.of_Trips_Per_Week__c = Decimal.valueOf(wrapper.of_Trips_Per_Week);
                    system.debug('Lane Trips_Per_Week__c = ' + lane.of_Trips_Per_Week__c);
                }
                //lane.Name = wrapper.Name;
                if(wrapper.Awarded != '' && wrapper.Awarded != null && wrapper.Awarded != 'undefined'){
                    lane.Awarded__c = Boolean.valueOf(wrapper.Awarded);
                }
                lane.ADS_Lane_Destination_City__c = wrapper.ADS_Lane_Destination_City;
                lane.Destination_Drop_Type__c = wrapper.Destination_Drop_Type;
                lane.ADS_Lane_Destination_Market__c = wrapper.ADS_Lane_Destination_Market;
                lane.Destination_Region__c = wrapper.Destination_Region;
                lane.ADS_Lane_Destination_State__c = wrapper.ADS_Lane_Destination_State;
                if(wrapper.ADS_Lane_Destination_Zip != 'undefined')
                    lane.ADS_Lane_Destination_Zip__c = wrapper.ADS_Lane_Destination_Zip;
                else
                    lane.ADS_Lane_Destination_Zip__c = null;
                if(wrapper.Minimum_Charge != '' && wrapper.Minimum_Charge != null && wrapper.Minimum_Charge != 'undefined'){
                    lane.Minimum_Charge__c = Decimal.valueOf(wrapper.Minimum_Charge);
                }
                lane.Opportunity__c = oppId;
                lane.ADS_Lane_Origin_City__c = wrapper.ADS_Lane_Origin_City;
                lane.Origin_Drop_Type__c = wrapper.Origin_Drop_Type;
                lane.ADS_Lane_Origin_Market__c = wrapper.ADS_Lane_Origin_Market;
                lane.Origin_Region__c = wrapper.Origin_Region;
                lane.ADS_Lane_Origin_State__c = wrapper.ADS_Lane_Origin_State;
                if(wrapper.ADS_Lane_Origin_Zip != 'undefined')
                    lane.ADS_Lane_Origin_Zip__c = wrapper.ADS_Lane_Origin_Zip;
                else
                    lane.ADS_Lane_Origin_Zip__c = null;
                if(wrapper.Id != null && wrapper.Id != '' && wrapper.Id != 'undefined'){
                    lane.Id = Id.valueOf(wrapper.Id);
                }
                System.debug('lane : '+lane);
                System.debug('lane of_Trips_Per_Week__c : '+lane.of_Trips_Per_Week__c);
                System.debug('lane Opportunity__c : '+lane.Opportunity__c);
                LaneListtoInsert.add(lane);
            }
        }
        else{
            returnresponse = 'ERROR';
        }
        if(LaneListtoInsert.size() > 0)
        {
            try {
                System.debug('Lane_List_to_Insert : '+LaneListtoInsert[0]);
                upsert LaneListtoInsert;
                System.debug('LaneListtoInsert : '+LaneListtoInsert[0].id);
                returnresponse = 'SUCCESS';
            }
            catch(Exception ex)
            {
                returnresponse = 'ERROR';
            }
        }
        else{
            returnresponse = 'ERROR';
        }
        return returnresponse;
    }
    @AuraEnabled
    public static list<ADS_Lane__c> fetchLane(String oppId){
        System.debug('oppId-->'+oppId);
        List <ADS_Lane__c> returnlaneList = new List < ADS_Lane__c > ();
        for(ADS_Lane__c lane: [SELECT ADS_Lane_Destination_City__c,ADS_Lane_Destination_Market__c,ADS_Lane_Destination_State__c,
                          ADS_Lane_Destination_Zip__c,ADS_Lane_Origin_City__c,ADS_Lane_Origin_Market__c,
                          ADS_Lane_Origin_State__c,ADS_Lane_Origin_Zip__c,Awarded__c,Destination_Drop_Type__c,
                          Destination_Region__c,Id,Minimum_Charge__c,Name,of_Trips_Per_Week__c,Opportunity__c,
                          Origin_Drop_Type__c,Origin_Region__c FROM ADS_Lane__c WHERE Opportunity__c = :oppId]) {
            returnlaneList.add(lane);
        }
        System.debug('returnlaneList-->'+returnlaneList);
        return returnlaneList;
    }
    public class fieldWrapper {
        public String of_Trips_Per_Week;
        public String Id;
        public String Name;
        public String Awarded;
        public String ADS_Lane_Destination_City;
        public String Destination_Drop_Type;
        public String ADS_Lane_Destination_Market;
        public String Destination_Region;
        public String ADS_Lane_Destination_State;
        public String ADS_Lane_Destination_Zip;
        public String Minimum_Charge;
        public String Opportunity;
        public String ADS_Lane_Origin_City;
        public String Origin_Drop_Type;
        public String ADS_Lane_Origin_Market;
        public String Origin_Region;
        public String ADS_Lane_Origin_State;
        public String ADS_Lane_Origin_Zip;
        
    } 
}