public class ProductLineItemTriggerHandler {
    public static void handleAfterUpdate(Map<Id,ProductLineItem__c> newPLIMap, Map<Id,ProductLineItem__c> oldPLIMap) {
        afterupdate(newPLIMap,oldPLIMap);
        if(ProductLineItemHelper.isFirstTime){
            System.debug('ProductLineItemHelper.isFirstTime-->'+ProductLineItemHelper.isFirstTime);
            ProductLineItemHelper.isFirstTime = false;
            checkPartsShipped(newPLIMap.values(),oldPLIMap);
        }
    }
    public static void afterupdate(Map<Id, ProductLineItem__c> newMap, Map<Id, ProductLineItem__c> oldMap)
    {
        List<Case> listtoupdate = new List<Case>();
        list<string> caseIdList = new list<string>();
        for(Id plid : newMap.keySet())
        {
            ProductLineItem__c plc = newMap.get(plid);
            if (!caseIdList.contains(plc.Case__c)) {
                case mycase = new case();
                mycase.Id = plc.Case__c;
                mycase.Last_Action_By__c = 'Product line item updated';
                listtoupdate.add(mycase);
                caseIdList.add(plc.Case__c);
            }
        }
        if(listtoupdate.size() > 0 ){
            update listtoupdate;
        }
    }
    public static void checkPartsShipped(List<ProductLineItem__c> newPLI, Map<Id, ProductLineItem__c> oldPLIMap){
        for(ProductLineItem__c PLI :newPLI){
            ProductLineItem__c oldPLI = oldPLIMap.get(PLI.Id);
            if((PLI.Part_Order_Tracking_Number__c != ' ' && PLI.Part_Order_Tracking_Number__c != NULL) && (oldPLI.Part_Order_Tracking_Number__c == ' ' || oldPLI.Part_Order_Tracking_Number__c == NULL)){
                ProductLineItemHelper.sendEmail(PLI);
            }
        }
    }
}