public class ReplacementPartsInfoCtrl {
    public static blob cryptoKey;
    public static blob cryptoIV;
    public static String algrType;
    public static final string MY_KEY = 'abcdefghijabcdef'; 
    public static final string MY_IV = '1234567890123456'; //it always will be 16 bytes
    
    @AuraEnabled
    public static String OpenAshleyDirect(String ProductLineItemId)
    {
        String strAddress1;
        String strAddress2;
        String strCity;
        String strState;
        String strZip;
        String strSOrder;
        String strAName;
        String strAPhone;
        String strISku;
        String strADLinkId;
        String strFFid;
        String strAckno;
        String strCtry;
        
        ProductLineItem__c PLI = new ProductLineItem__c();
        PLI = [SELECT Id, Name, Sales_Order_Number__c, AckNo__c, Item_SKU__c, Ashley_Direct_Link_ID__c, Fulfiller_ID__c,(SELECT Name,Salesforce_AS400_Reference_Number_and_PO__c FROM PartOrderInfo__r Order by Createddate DESC LIMIT 1), 
                   Address_Line1_Case__c, Address_Line2_Case__c, City_Case__c, State_Case__c, Zip_Case__c, Country_Case__c, 
                   Case__r.AccountId, Case__r.Account.Name, Case__r.Account.phone
                   FROM ProductLineItem__c WHERE Id=:ProductLineItemId];
        for(PartOrderInfo__c POI:PLI.PartOrderInfo__r){
            System.debug('PLI Name:'+PLI.Name+'   POI Name:'+POI.Name+'  POI AS400:'+POI.Salesforce_AS400_Reference_Number_and_PO__c);
            strADLinkId = encryptValue(POI.Salesforce_AS400_Reference_Number_and_PO__c);

        }    
        if(PLI.Address_Line1_Case__c!=null)
            strAddress1 = encryptValue(PLI.Address_Line1_Case__c);
        if(PLI.Address_Line2_Case__c!=null)
            strAddress2 = encryptValue(PLI.Address_Line2_Case__c);
        if(PLI.City_Case__c!=null)
            strCity = encryptValue(PLI.City_Case__c);
        if(PLI.State_Case__c!=null)
            strState = encryptValue(PLI.State_Case__c);
        if(PLI.Zip_Case__c!=null)
            strZip = encryptValue(PLI.Zip_Case__c);
        if(PLI.Sales_Order_Number__c!= null)
            strSOrder = encryptValue(PLI.Sales_Order_Number__c);
        if(PLI.Case__r.Account.Name!= null)
            strAName = encryptValue(PLI.Case__r.Account.Name);
        if(PLI.Case__r.Account.phone!=null)
            strAPhone = encryptValue(PLI.Case__r.Account.phone);
        if(PLI.Item_SKU__c!= null)
            strISku = encryptValue(formatSku(PLI.Item_SKU__c));
        /*if(PLI.Ashley_Direct_Link_ID__c != null)
            strADLinkId = encryptValue(PLI.Ashley_Direct_Link_ID__c);*/
        if(PLI.Fulfiller_ID__c != null)
            strFFid = encryptValue(PLI.Fulfiller_ID__c);
        if(PLI.AckNo__c != null)
            strAckno = encryptValue(PLI.AckNo__c);
        // String adURL = 'https://stage.ashleydirect.com/Login/SignIn?url=/ReplacementsPartsOrderEntry_NET/Order.aspx?CN%3D{!URLENCODE(LEFT('+strFFid+', FIND("-", '+strFFid+') - 1))}%26SN%3D{!IF(CONTAINS('+strFFid+',"-"),URLENCODE(RIGHT('+strFFid+', LEN('+strFFid+') - FIND("-", '+strFFid+'))),"")}%26PN%3D{!URLENCODE('+strADLinkId+')}%26AC%3Dnew%26IT%3D{!URLENCODE('+strISku+')}%26NM%3D{!URLENCODE('+strAName+')}%26A1%3D{!URLENCODE('+strAddress2+')}%26A2%3D{!URLENCODE('+strAddress1+')}%26CY%3D{!URLENCODE('+strCity+')}%26ST%3D{!URLENCODE('+strState+')}%26ZP%3D{!URLENCODE('+strZip+')}%26PH%3D{!URLENCODE('+strAPhone+')}%26CT%3D{!URLENCODE('+strCtry+')}%26AK%3D{!URLENCODE('+strAckno+')}%26SARO%3D1{!URLENCODE('+strSOrder+')}%26decrypt%3Dtrue';
        String adURL = system.label.Ashley_Direct+ 'url=/ReplacementsPartsOrderEntry_NET/Order.aspx?CN%3D{!URLENCODE('+strFFid+')}%26PN%3D{!URLENCODE('+strADLinkId+')}%26AC%3Dnew%26IT%3D{!URLENCODE('+strISku+')}%26NM%3D{!URLENCODE('+strAName+')}%26A1%3D{!URLENCODE('+strAddress1+')}%26A2%3D{!URLENCODE('+strAddress2+')}%26CY%3D{!URLENCODE('+strCity+')}%26ST%3D{!URLENCODE('+strState+')}%26ZP%3D{!URLENCODE('+strZip+')}%26PH%3D{!URLENCODE('+strAPhone+')}%26CT%3DUSA%26AK%3D{!URLENCODE('+strAckno+')}%26SARO%3D{!URLENCODE('+strSOrder+')}%26decrypt%3Dtrue';
        PageReference pr = new PageReference(adURL);
        System.debug('Values in URL  '+adURL);
        return adURL;
    }
    @AuraEnabled
    public static String encryptValue(String valueforEncrypt){
        Encrypt_Data__c setting = Encrypt_Data__c.getInstance();//getValues('crypto__c');
        cryptoKey = blob.valueOf(setting.crypto__c);
        cryptoIV = blob.valueOf(setting.cryptoIV__c);
        algrType= setting.AlgorthemType__c;
        System.debug('setting-----'+setting.crypto__c);
        blob newcryptoKey256 = EncodingUtil.base64Decode(setting.crypto__c);
        System.debug('cryptoKey-----'+cryptoKey);
        System.debug('valueforEncrypt-----'+valueforEncrypt);
        blob data = Blob.valueOf(valueforEncrypt);
        blob encryptedData = Crypto.encryptWithManagedIV(algrType, newcryptoKey256, data );
        String encryptedValue = EncodingUtil.base64Encode(encryptedData);
        System.debug('encryptedValue: ' + encryptedValue);
        System.debug('iv: ' + EncodingUtil.base64Encode(Blob.valueOf(ReplacementPartsInfoCtrl.MY_IV)));
        System.debug('key: ' + EncodingUtil.base64Encode(Blob.valueOf(ReplacementPartsInfoCtrl.MY_KEY)));
        return encryptedValue;
    }
    @AuraEnabled
    public static String formatSku(String ipSku){
        String opSku;
        if(ipSku != null)
        {
            String[] ItemNum1 = ipSku.split('[^0]*');
            Integer i=0;
            for(i=0; i< ItemNum1.size(); i++){
                if(ItemNum1[i]=='') break;
                System.debug('Num1-->'+ItemNum1[i]);
                System.debug('\n');
            }
            System.debug('i-->'+i);
            opSku = ipSku.substring(i);
            opSku.trim();
            System.debug('opSku-->'+opSku);
        }
        return opSku;
    }
    @AuraEnabled
    public static Boolean NewPartInsert(String PLIid){
        System.debug('PLIid-->'+PLIid);
        Boolean Insertstatus;
        ProductLineItem__c PLI = new ProductLineItem__c();
        PLI = getPLI(PLIid);
        System.debug('PLI-->'+PLI);
        PartOrderInfo__c POI = new PartOrderInfo__c();
        POI.Fulfiller_ID__c = PLI.Fulfiller_ID__c;
        POI.Product_Line_Item__c = PLIid;
        POI.Item_SKU__c = PLI.Item_SKU__c;
        POI.Case__c = PLI.Case__c;
        insert POI;
        System.debug('POI-->'+POI);
        Insertstatus = true;
        return Insertstatus;
    }
    @AuraEnabled
    public static List<PartOrderInfo__c> getPOI(String PLIid){
        List<PartOrderInfo__c> POI = new List<PartOrderInfo__c>();
        POI = [SELECT Id,Product_Line_Item__c,Item_Defect__c,Defect_Location__c,Address__c,Manufacturing_Defect__c,Defect_Details__c,Salesforce_AS400_Reference_Num_and_PO__c,
               Salesforce_AS400_Reference_Number_and_PO__c,Part_Order_Shipping_Date__c,Replacement_Part_Item_Number__c,Part_Order_Status__c,
               Fulfiller_ID__c,Part_Order_Tracking_Number__c,Part_Order_Number__c FROM PartOrderInfo__c WHERE Product_Line_Item__c =:PLIid Order by Createddate DESC ];
        return POI;
    }
    @AuraEnabled
    public static PartOrderInfo__c getPOIrec(String poid){
        System.debug('poid-->'+poid);
        PartOrderInfo__c POI = new PartOrderInfo__c();
        POI = [SELECT Id,Product_Line_Item__c,Salesforce_AS400_Reference_Number_and_PO__c,Address__c,Part_Order_Shipping_Date__c,Salesforce_AS400_Reference_Num_and_PO__c,
               Replacement_Part_Item_Number__c,Part_Order_Status__c,Fulfiller_ID__c,Part_Order_Number__c,Part_Order_Tracking_Number__c FROM PartOrderInfo__c WHERE Id =:poid];
       System.debug('POI-->'+POI);
        return POI;
    }
    @AuraEnabled
    public static ProductLineItem__c getPLI(String PLIid){
        ProductLineItem__c PLI = new ProductLineItem__c();
        PLI = [SELECT Id, Name, Sales_Order_Number__c, AckNo__c, Item_SKU__c, Ashley_Direct_Link_ID__c, Fulfiller_ID__c,
                   Address_Line1_Case__c, Address_Line2_Case__c, City_Case__c, State_Case__c, Zip_Case__c, Country_Case__c, 
                   Case__r.AccountId, Case__r.Account.Name, Case__r.Account.phone, Case__c
                   FROM ProductLineItem__c WHERE Id=:PLIid];
        return PLI;
    }
    @AuraEnabled
   // @future(callout=true)
    public static boolean getTrackingNumber(ID mynum, String FulfillerId, String PoNum){
        boolean isUTFailed = false;
        boolean isUTSuccessfull = false;
    	System.debug('My record ID' +mynum);
        String strTest = FulfillerId;
        System.debug(strTest);
        List<String> arrTest = strTest.split('\\-');
        String customerNumber = arrTest[0];
        String shipTo = arrTest[1];
        String PONumber = PoNum;
        System.debug(customerNumber);
        System.debug(shipTo);
        if((strTest != null || strTest != '') && (PONumber != null || PONumber != '' )){
            // get Access token call
            ServiceReplacementPartsAuthorization replParts= new ServiceReplacementPartsAuthorization();
            //  String response;
            String accessTkn = replParts.accessTokenData();
            system.debug('accesstokenresponse' + accessTkn);
            //Send API the request
            HttpRequest req = new HttpRequest();
            req.setEndpoint(System.label.ReplacementPartSerialNumber+'/orders/status?ponumber='+PONumber+'&customerNumber='+customerNumber+'&shipTo='+shipTo+'&apikey=NvcXrflst6O2sQurPOzOxytA1bUVvW8t');
            //https://stageapigw.ashleyfurniture.com/replacement-parts/v1/orders/status?ponumber=2622632&customerNumber=700&shipTo=01&apikey=NvcXrflst6O2sQurPOzOxytA1bUVvW8t
            req.setMethod('GET');
            req.setHeader('Content-Type' ,'application/json;charset=utf-8');
            req.setHeader('Authorization', 'Bearer '+accessTkn);
            //req.setHeader('apikey', System.label.TechSchedulingApiKey);
            system.debug('req--->' + req);
            string errMsg;
            Http http = new Http();
            HttpResponse res = http.send(req);
            // Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
            System.debug('***Tracking Number Response Status: '+ res.getStatus());
            System.debug('***Tracking Number Response Body: '+ res.getBody());
            System.debug('***Tracking Number Response Status Code: '+ res.getStatusCode());
            Integer statusCode = res.getStatusCode();
            //Check the response
            System.debug('My PO NUM entered--' +PoNum);
            System.debug('My FulfillerId entered--' +FulfillerId);
            system.debug('body ' + res.getBody()); 
            //POJSON.ExtDetail data = (POJSON.ExtDetail)JSON.deserialize(res.getBody(), POJSON.ExtDetail.class); 
            Type wrapperType = Type.forName('POJSON'); 
            POJSON data = (POJSON)JSON.deserialize(res.getBody(),wrapperType);
            //System.debug('SO--->'+data.Detail.Street);
            if(statusCode == 200){
                System.debug('Success');
                boolean isRes = updateTrackingNumber(mynum,data);
                if(isRes == true){
                    return True;
                }
            }
            else{
                isUTFailed = true;
                System.debug('FAILED');
                return false;
            }
        }
        return false;
    }
     @AuraEnabled
    public static boolean updateTrackingNumber(ID POIID, POJSON data){ 
        system.debug('POIID'+POIID);
        system.debug('data-->'+data);
        PartOrderInfo__c PLITrackingNumUpdate = new PartOrderInfo__c();
        try{
            PLITrackingNumUpdate.id = POIID;
            if(data.ExtDetail.UPSTracking!='' && data.ExtDetail.UPSTracking != null){
                PLITrackingNumUpdate.Part_Order_Tracking_Number__c = data.ExtDetail.UPSTracking;
            }
            if(data.ExtDetail.DefectLocation!='' && data.ExtDetail.DefectLocation != null){
                PLITrackingNumUpdate.Defect_Location__c = data.ExtDetail.DefectLocation;
            }
            if(data.ExtDetail.DefectCode!='' && data.ExtDetail.DefectCode != null){
                PLITrackingNumUpdate.Item_Defect__c = data.ExtDetail.Defect;
            }
            if(data.ExtDetail.RPKey!='' && data.ExtDetail.RPKey != null){
                PLITrackingNumUpdate.Part_Order_Number__c = data.ExtDetail.RPKey;
            }
            if(data.ExtDetail.Model!='' && data.ExtDetail.Model != null){
                PLITrackingNumUpdate.Replacement_Part_Item_Number__c = data.ExtDetail.Model;
            }
            
            if(data.ExtDetail.Address1!='' && data.ExtDetail.Address1 != null){
                PLITrackingNumUpdate.Address__c = data.ExtDetail.Address1 + ' ';
            }
            if(data.ExtDetail.Address2!='' && data.ExtDetail.Address2 != null){
                PLITrackingNumUpdate.Address__c = PLITrackingNumUpdate.Address__c + data.ExtDetail.Address2 + ' ';
            }
            if(data.ExtDetail.Address3!='' && data.ExtDetail.Address3 != null){
                PLITrackingNumUpdate.Address__c = PLITrackingNumUpdate.Address__c + data.ExtDetail.Address3 + ' ';
            }
            if(data.ExtDetail.Street!='' && data.ExtDetail.Street != null){
                PLITrackingNumUpdate.Address__c = PLITrackingNumUpdate.Address__c + data.ExtDetail.Street + ' ';
            }
            if(data.ExtDetail.ZipCode!='' && data.ExtDetail.ZipCode != null){
                PLITrackingNumUpdate.Address__c = PLITrackingNumUpdate.Address__c + data.ExtDetail.ZipCode + ' ';
            }
            
            
            system.debug('ShipDate-->'+data.ExtDetail.UPSTracking);
            if(data.ExtDetail.ShipDate!='' && data.ExtDetail.ShipDate != null && data.ExtDetail.ShipDate.length() > 7){
                String String1 = data.ExtDetail.ShipDate;
                String String2 = '-';
                String newString = String1.substring(0, 4) + '-';
                newString = newString.substring(0,newString.length()) + 
                    String1.substring(4,6);
                newString = newString.substring(0,newString.length()) + '-';
                newString = newString.substring(0,newString.length()) +
                    String1.substring(6,String1.length());
                System.debug('newString-->'+newString);
                PLITrackingNumUpdate.Part_Order_Shipping_Date__c = Date.valueOf(newString);
            }
           // PLITrackingNumUpdate.Part_Order_Shipping_Date__c = Date.valueOf(data.ExtDetail.ShipDate);
            system.debug('PLITrackingNumUpdate-->'+PLITrackingNumUpdate);
            Update PLITrackingNumUpdate;
            system.debug('status-->'+true);
            return true;
        }
        catch(Exception ex){
            
            return false;
        }
    } 
}