@IsTest(SeeAllData=true)
public class ImportEmailOpportunity_Test {
    static String str = 'Name,Awarded,Opportunity,Id,ADS_Lane_Destination_City\n Esha Patharabe,10001,Prospect,Test,Banking\n Trupti Nimje,10002,Prospect,Test,Banking';       
    
    public static String[] csvFileLines;
    public static String[] csvFileHeaders;
    public static Blob csvFileBody;
    static testMethod void testCreateContactFrmEmail() {
        //Create Account
        Account acc = new Account();
        acc.LastName = 'last' + Math.random();
        acc.FirstName = 'first' + Math.random();
        acc.PersonEmail = acc.FirstName + '.' + acc.LastName + '@test.com';
        acc.Primary_Language__pc = 'English';
        string phone = '888' + Integer.valueOf(Math.random() * 10000);
        while (phone.length() < 10){
            phone = phone + '0';
        }
        acc.phone = phone;
        Insert acc;
        //Create Opportunity
        Opportunity op = new Opportunity();
        op.Name = 'TestOp';
        op.CloseDate = Date.today();
        op.AccountId = acc.Id;
        op.StageName = 'Closed Won';
        Insert op;
        String JsonMsg;
        JsonMsg = '(ADS_Lane_Destination_City__c,ADS_Lane_Destination_Market__c,ADS_Lane_Destination_State__c,ADS_Lane_Destination_Zip__c,ADS_Lane_Origin_City__c,ADS_Lane_Origin_Market__c,ADS_Lane_Origin_State__c,ADS_Lane_Origin_Zip__c,Awarded__c,Destination_Drop_Type__c,Destination_Region__c,Id,Minimum_Charge__c,Name,of_Trips_Per_Week__c,Opportunity__c,Origin_Drop_Type__c,Origin_Region__c,\n100 PALMS,Redlands Mkt,CA,22225,100 PALMS,Redlands Mkt,CA,22225,FALSE,Driver Assist,WEST,,,LN-00002,0,,Driver Assist,WEST)';
        
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
        email.subject = 'Create Opportunity';
        email.plainTextBody = 'FromEmail';
        env.fromAddress = 'example@gmail.com';
        // List<SObject> lstLanes = Test.loadData(ADS_Lane__c.sObjectType,'LaneLoad_Test');
        // add an Binary attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf(JsonMsg);
        attachment.fileName = 'textfileone.csv';
        attachment.mimeTypeSubType = 'text/csv';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
        Test.startTest();
        // call the email service class and test it with the data in the testMethod
        ImportEmailOpportunity CreateOpp = new ImportEmailOpportunity();
        CreateOpp.handleInboundEmail(email, env);
        ImportEmailOpportunity.CreateNewAcc();
        ImportEmailOpportunity.CreateNewOpp();
        Test.stopTest();  
        
    }
}