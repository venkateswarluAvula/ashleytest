public class ADSCommitmentRefreshCon {
    
    public String currentRecordId {get; set;}
    
    public void onLoad(){
        currentRecordId =  ApexPages.CurrentPage().getparameters().get('id');
        system.debug('currentRecordId--'+currentRecordId);
    }
    
    
    @AuraEnabled
    Public Static Commitment__c UpdateRecord(id currentrecId){
        
        system.debug('currentrecId--'+currentrecId);
        
        Commitment__c comm = [select id,Origin_Market__c,Destination_Market__c,Number_Trips_Required__c,Complete__c,
                              Number_Opportunities__c,Status__c,Number_of_In_Progress_Opportunities__c 
                              from Commitment__c where id=:currentrecId];
        System.debug('comm ----'+comm);
        
        list<Segment__c> segmantrec = [SELECT Id, Destination__c, Origin__c, Stop__c FROM Segment__c 
                                       WHERE Destination__c = :comm.Destination_Market__c 
                                       AND Origin__c = :comm.Origin_Market__c];
        System.debug('segmantrec ----'+segmantrec.size() +'--'+segmantrec);
        
        set<string> originSet = new set<string>();
        set<string> DestinationSet = new set<string>();
        set<string> StopSet = new set<string>();
        for(Segment__c sec : segmantrec){
            originSet.add(sec.Origin__c);
            DestinationSet.add(sec.Destination__c);
            StopSet.add(sec.Stop__c);
        }
        
        System.debug('originSet----'+originSet.size() +'--'+originSet);
        System.debug('DestinationSet----'+DestinationSet.size() +'--'+DestinationSet);
        System.debug('StopSet----'+StopSet.size() +'--'+StopSet);
        
        //----- goruping the lanes records for calculation-------------
        List<AggregateResult> adsLaneRec = [SELECT Opportunity__c,Opportunity__r.name oppname,Opportunity__r.Account.Name AccountName, sum(of_Trips_Per_Week__c) trps, 
                                            count(of_Trips_Per_Week__c) cnt FROM ADS_Lane__c 
                                            WHERE Opportunity__r.StageName != 'Closed Lost'  AND Opportunity__r.Type = 'New Business'
                                            AND (Opportunity__r.RecordType.Name = 'ADS' OR Opportunity__r.RecordType.Name = 'ADS Detail Page') AND 
                                            Opportunity__r.Opportunity_Type__c = 'ADS' AND 
                                            ((ADS_Lane_Origin_Market__c =: originSet OR ADS_Lane_Origin_Market__c =: StopSet )
                                              AND 
                                             (ADS_Lane_Destination_Market__c =: DestinationSet OR ADS_Lane_Destination_Market__c=: StopSet)
                                            )
                                            group by Opportunity__c,Opportunity__r.name,Opportunity__r.Account.Name];
        System.debug('adsLaneRec----'+adsLaneRec.size() +'--'+adsLaneRec);
        
        //-------concatinating the lane names-----------------
        set<Id> oppIdset = new set<Id>();
        for(AggregateResult opId :adsLaneRec){ oppIdset.add((Id)opId.get('Opportunity__c'));  }
        
        Map<Id,string> adsnamesMap = new Map<Id,string>();
        Map<string,string> stgNameMap = new Map<string,string>();
        for (Id key : oppIdset) {  adsnamesMap.put(key,'');   stgNameMap.put(key,''); }
        
        list<ADS_Lane__c> adsnames = [select id,name,Opportunity__c,Opportunity__r.StageName from ADS_Lane__c where Opportunity__c=:oppIdset Order by Opportunity__c];
        System.debug('adsnames----'+adsnames);
        string str1 = '';
        string stgName = '';
        
        
        for(ADS_Lane__c adsmap: adsnames){
            System.debug('stage----'+adsmap.Opportunity__r.StageName);
            str1 = adsnamesMap.get(adsmap.Opportunity__c);  stgName = stgNameMap.get(adsmap.Opportunity__r.StageName);
            
            if(String.isEmpty(str1)){
                str1 = adsmap.Name;    stgName = adsmap.Opportunity__r.StageName;
            }else{
                str1 = str1 + ', ' + adsmap.Name;   stgName = adsmap.Opportunity__r.StageName;
            }
            adsnamesMap.put(adsmap.Opportunity__c,str1);
            stgNameMap.put(adsmap.Opportunity__c,stgName); 
            //System.debug('stgName----'+stgName);
        }
        
        System.debug('stgNameMap----'+stgNameMap);
        
        //----- deleting the exisitng Commitment Opportunities record--------------
        List<Commitment_Opportunities__c> ComDelList = [select id,Commitment__c from Commitment_Opportunities__c where Commitment__c =: currentrecId];
        System.debug('ComDelList----'+ ComDelList.size()+'--->'+ComDelList);
        if(comm.Status__c != 'Complete'){
        	delete ComDelList; 
        }
        //----- creating the Commitment Opportunities records--------
        List<Commitment_Opportunities__c> ComOpplist = new List<Commitment_Opportunities__c>();
        
        //------ using collectios allocating values for commitment object--------
        Decimal trips = 0;
        integer wonoppcnt = 0;
        integer oppcnt = 0;
        for(AggregateResult exp: adsLaneRec){
            System.debug('stage----'+stgNameMap.get((Id)exp.get('Opportunity__c')));
            System.debug('exp----'+exp); System.debug('trps----'+((Decimal)exp.get('trps')));
            string oppstage = stgNameMap.get((Id)exp.get('Opportunity__c'));
            if(oppstage == 'Closed Won'){ trips = trips + ((Decimal)exp.get('trps')); wonoppcnt = wonoppcnt+1; }else{ oppcnt = oppcnt + 1; }
            
            Commitment_Opportunities__c ComOpp = new Commitment_Opportunities__c();
            ComOpp.Lanes__c = ((Decimal)exp.get('cnt')); ComOpp.Trips_Per_Week__c =((Decimal)exp.get('trps'));
            ComOpp.Commitment__c =currentrecId; ComOpp.OppLanes__c = adsnamesMap.get((Id)exp.get('Opportunity__c'));
            ComOpp.Opportunity__c =((Id)exp.get('Opportunity__c')); ComOpplist.add(ComOpp);
        }
        if(comm.Status__c != 'Complete'){
        	insert ComOpplist;
        }
        
        System.debug('ComOpplist----'+ComOpplist.size()+'--ComOpplist--'+ComOpplist);
        System.debug('trips----'+trips); System.debug('wonoppcnt----'+wonoppcnt);   System.debug('oppcnt----'+oppcnt);
        
        //------ updateing the commitment record values ------------
        Decimal calculation = ((trips/comm.Number_Trips_Required__c)*100);
        System.debug('calculation----'+calculation);
        
        comm.Number_Opportunities__c = wonoppcnt;
        comm.Number_of_In_Progress_Opportunities__c = oppcnt;
        comm.Complete__c = calculation;
        if(comm.Status__c != 'Complete'){
            update comm;
        }
        
        
        return comm;
    } 
    
    
}