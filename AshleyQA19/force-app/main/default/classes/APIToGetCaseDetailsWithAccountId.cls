/*******************************************************************************************
* Description – Apex REST service with GET method
* Author – Sudeshna Saha

* Get Case Details with Account ID
********************************************************************************************/
@RestResource(urlMapping='/v1/GetCase/*')
global class APIToGetCaseDetailsWithAccountId {
    @HttpGet
    global static CaseWrapper doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        CaseWrapper response = new CaseWrapper();
        System.debug('someval-->'+RestContext.request.requestUri);
        String someval = RestContext.request.requestUri.substringAfterLast('/');
        System.debug('someval-->'+someval);
        List<Account> myAcc = new List<Account>();
        List<getCaseDetail> myCase = new List<getCaseDetail>();
        ///services/apexrest/v1/GetCase/AccountID=001q000000raIIAAA2
        someval = getVal(someval);
        List<Account> accountList = new List<Account>();
        List<Case> caseList = new List<Case>();
        Boolean finalCaseFlag = false;
        If(someval !=null || someval != ''){
            caseList = getCaseList(someval);
            myCase = getCase(caseList);
        }
        System.debug('case--->'+myCase);
        System.debug('case size--->'+myCase.size());
        if(myCase.size()>0){
            finalCaseFlag = true;
        }
        if(finalCaseFlag == false){
            response.status = 'Success';
            response.message = 'No Cases Found';
            res.statusCode = 200;
        }
        else{
            response.caseList = myCase ;
            response.status = 'Success';
            response.message = 'Successful';
            res.statusCode = 200;
        }
        return response;
    }
    
    //If the request came to /v1/accounts, then we want to execute a search
    private static String getVal(String someval) {
        someval = someval.substringAfter('=');
        return someval;
    }
    private static List<Case> getCaseList(String accountId) {
        System.debug('accountList-->'+accountId);
        List<Case> caseList = new List<Case>();
        if(accountId != null && accountId != ''){
            caseList = [SELECT AccountId,CaseNumber,Id,Status,Subject,Sub_Type__c,Type,IsEscalated
                        FROM Case 
                        WHERE AccountId =:accountId];
        }
        System.debug('caseList-->'+caseList);
        return caseList; 
    }
    private static List<getCaseDetail> getCase(List<Case> caseList) {
        List<getCaseDetail> mycase = new List<getCaseDetail>();
        System.debug('getCase-->'+caseList);
        if(caseList.size() > 0)
            mycase = searchCase(caseList);
        return mycase; 
    }
    private static List<getCaseDetail> searchCase(List<Case> caseList) {
        List<getCaseDetail> mycase = new List<getCaseDetail>();
        Boolean flag = false;
        Boolean flagCase = false;
        getCaseDetail C2 = new getCaseDetail();
        System.debug('searchCase-->'+caseList);
        if(caseList.size() > 0)
        {
            for(Case c1:caseList){
                System.debug('URL-->'+System.URL.getSalesforceBaseUrl());
                String lnk = System.URL.getSalesforceBaseUrl()+'/lightning/r/Case/'+ c1.Id + '/view';
                lnk = lnk.substringAfter('=');
                lnk = lnk.replaceAll( ']', '');
                C2.caseLink = lnk;
                C2.cases = c1;
                mycase.add(C2);
                System.debug('C2-->'+C2);
                System.debug('mycase-->'+mycase);
            }
        }
        System.debug('getCase-->'+mycase);
        return mycase;
    }
    global class CaseWrapper {
        public getCaseDetail[] caseList;
        public String status;
        public String message;
        public CaseWrapper(){
            caseList = new List<getCaseDetail>();
        }
    }
    global class getCaseDetail {
        public String caseLink;
        public case cases;
    }
}