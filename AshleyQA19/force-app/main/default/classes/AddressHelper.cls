/* This is a helper class which contains address related method that can be invoked where ever it is required.*/
public class AddressHelper {

    /*
     * Method to set Preferred address for the customer
     */
	public static void setCustomerPreferredAddress(Id accId, Id addressId) {
    	List<Address__c> addressList = [SELECT Id, AccountId__c, Preferred__c FROM Address__c WHERE AccountId__c = :accId and Id != :addressId];
    	for(Address__c address : addressList) {
    		address.Preferred__c = false;
    	}
    	update addressList;
	}

    /*
     * Method to get all the address associate to customer with address type
     */
	public static Map<string, id> getCustomerAddressWithType(Id accId, Id addressId) {
    	Map<string, id> addressMap = new Map<string, id>();
    	List<Address__c> addressList = new List<Address__c>(); 
    	if (addressId != null) {
    		addressList = [SELECT Id, AccountId__c, Address_Line_1__c, Address_Line_2__c, City__c, StateList__c, Zip_Code__c, Country__c, Address_Type__c FROM Address__c WHERE AccountId__c = :accId and Id != :addressId];
    	} else {
    		addressList = [SELECT Id, AccountId__c, Address_Line_1__c, Address_Line_2__c, City__c, StateList__c, Zip_Code__c, Country__c, Address_Type__c FROM Address__c WHERE AccountId__c = :accId];
    	}

    	for(Address__c address : addressList) {
    		string adrStr = '';
    		if(!string.isBlank(address.Address_Line_1__c)) {
    			adrStr += address.Address_Line_1__c + '-';
    		}
    		if(!string.isBlank(address.Address_Line_2__c)) {
    			adrStr += address.Address_Line_2__c + '-';
    		}
    		if(!string.isBlank(address.City__c)) {
    			adrStr += address.City__c + '-';
    		}
    		if(!string.isBlank(address.StateList__c)) {
    			adrStr += address.StateList__c + '-';
    		}
    		if(!string.isBlank(address.Zip_Code__c)) {
    			adrStr += address.Zip_Code__c + '-';
    		}
    		if(!string.isBlank(address.Address_Type__c)) {
    			adrStr += address.Address_Type__c;
    		}
    		addressMap.put(adrStr, address.Id);
    	}
    	addressList.clear();
    	return addressMap;
	}

    /*
     * Method to get all the address associate to customer
     */
	public static Map<string, id> getCustomAddress(Id accId) {
    	Map<string, id> addressMap = new Map<string, id>();
    	List<Address__c> addressList = [SELECT Id, AccountId__c, Address_Line_1__c, Address_Line_2__c, City__c, StateList__c, Zip_Code__c, Country__c FROM Address__c WHERE AccountId__c = :accId];
    	for(Address__c address : addressList) {
    		string adrStr = '';
    		if(!string.isBlank(address.Address_Line_1__c)) {
    			adrStr += address.Address_Line_1__c + '-';
    		}
    		if(!string.isBlank(address.Address_Line_2__c)) {
    			adrStr += address.Address_Line_2__c + '-';
    		}
    		if(!string.isBlank(address.City__c)) {
    			adrStr += address.City__c + '-';
    		}
    		if(!string.isBlank(address.StateList__c)) {
    			adrStr += address.StateList__c + '-';
    		}
    		if(!string.isBlank(address.Zip_Code__c)) {
    			adrStr += address.Zip_Code__c;
    		}
    		addressMap.put(adrStr, address.Id);
    	}
    	addressList.clear();
    	return addressMap;
	}

    /*
     * Method to get the address with address type in string format for validation
     */
	public static string getAddressStr(string sAdd1, string sAdd2, string sCity, string sState, string sZip, string sType) {
		string dupStr = '';
		if(!string.isBlank(sAdd1)) {
			dupStr += sAdd1 + '-';
		}
		if(!string.isBlank(sAdd2)) {
			dupStr += sAdd2 + '-';
		}
		if(!string.isBlank(sCity)) {
			dupStr += sCity + '-';
		}
		if(!string.isBlank(sState)) {
			dupStr += sState + '-';
		}
		if(!string.isBlank(sZip)) {
			dupStr += sZip + '-';
		}
		if(!string.isBlank(sType)) {
			dupStr += sType;
		}
		//system.debug('****** dupStr: ' + dupStr);
		return dupStr;
	}

    /*
     * Method to get the address in string format for validation
     */
	public static string getAddressStr(string sAdd1, string sAdd2, string sCity, string sState, string sZip) {
		string dupStr = '';
		if(!string.isBlank(sAdd1)) {
			dupStr += sAdd1 + '-';
		}
		if(!string.isBlank(sAdd2)) {
			dupStr += sAdd2 + '-';
		}
		if(!string.isBlank(sCity)) {
			dupStr += sCity + '-';
		}
		if(!string.isBlank(sState)) {
			dupStr += sState + '-';
		}
		if(!string.isBlank(sZip)) {
			dupStr += sZip;
		}
		//system.debug('****** dupStr: ' + dupStr);
		return dupStr;
	}

    /*
     * Method to get the address in comma separated string format
     */
	public static string getCommaAddressStr(string sAdd1, string sAdd2, string sCity, string sState, string sZip) {
		string adrStr = '';
		if(!string.isBlank(sAdd1)) {
			adrStr += sAdd1 + ',';
		}
		if(!string.isBlank(sAdd2)) {
			adrStr += sAdd2 + ',';
		}
		if(!string.isBlank(sCity)) {
			adrStr += sCity + ',';
		}
		if(!string.isBlank(sState)) {
			adrStr += sState + ',';
		}
		if(!string.isBlank(sZip)) {
			adrStr += sZip;
		}
		//system.debug('****** adrStr: ' + adrStr);
		return adrStr;
	}

    /*
     * Method to get address based on EDA customer account and pay load address
     *
	 *	if there is any address associated to the customer then
	 *		if address is available in the pay load
	 *			if any of the customer address matches with pay load address then take it
	 *				GET THE MATCHING ADDRESS ID
	 *			else
	 *				INSERT PAY LOAD ADDRESS
	 *		else if there is no address in the pay load
	 *			OPTION 1: Take customer preferred address
	 *			OPTION 2: Take last modified address
	 *	else if there is no address available for the customer then insert it from the pay load if available 
	 *		INSERT PAY LOAD ADDRESS AND SET IT AS PREFERRED
	 *	else if not in the pay load
	 *		NULL
     */
    public static Id getCustomerEDAAddress(string accId, string addLine1, string addLine2, string addCity, string addState, string addZip, List<Address__c> custAddressList, string calledFrom) {
    	id addressId;
    	string insertAdr;
    	if (custAddressList.size() > 0) {
    		//if there is any address associated to the customer then
    		if ( string.isNotBlank(addLine1) && string.isNotBlank(addCity) && string.isNotBlank(addState) && string.isNotBlank(addZip) ) {
				//if address is available in the pay load
				for (Address__c custAdr : custAddressList) {
					//if any of the customer address matches with pay load address then take it
					string adrStr = '', cwStr = '';
					adrStr = getCommaAddressStr(custAdr.Address_Line_1__c, custAdr.Address_Line_2__c, custAdr.City__c, custAdr.StateList__c, custAdr.Zip_Code__c);
					cwStr = getCommaAddressStr(addLine1, addLine2, addCity, addState, addZip);
					system.debug('****** Adr: ' + adrStr + ' : ' + cwStr);
					if (adrStr == cwStr) {
						//GET THE MATCHING ADDRESS ID
						addressId = custAdr.Id;
						break;
					}
				}

				if (addressId == null) {
					//INSERT PAY LOAD ADDRESS
					insertAdr = 'Preferred';
				}
			} else {
				//else if there is no address in the pay load
				//OPTION 1: Take customer preferred address
				for (Address__c custPAdr : custAddressList) {
					if (custPAdr.Preferred__c) {
						addressId = custPAdr.Id;
						break;
					}
				}

				if (addressId == null) {
					//OPTION 2: Take last modified address
					addressId = custAddressList[0].Id;
				}
			}
    	} else if (string.isNotBlank(addLine1)) {
    		//else if there is no address available for the customer then insert it from the pay load if available
    		insertAdr = 'Preferred';
    	} else {
    		//else if not in the pay load
    	}

    	if (string.isNotBlank(insertAdr)) {
	        Address__c insAdr = new Address__c();
    	    insAdr.AccountId__c = accId;
    	    insAdr.Address_Line_1__c = addLine1;
        	insAdr.Address_Line_2__c = addLine2;
        	insAdr.City__c = addCity;
        	insAdr.StateList__c = addState;
        	insAdr.Zip_Code__c = addZip;
        	insAdr.Address_Type__c = 'Ship To';
        	insAdr.Country__c = 'USA';
    		if (insertAdr == 'Preferred') {
    			insAdr.Preferred__c = true;
    		}

            try {
            	if (!Test.isRunningTest()) {
                  	insert insAdr;
					addressId = insAdr.Id;
            	}
				system.debug('****** Address insert: ' + insAdr);
            } catch (DmlException de) {
            	CaseHelper.dmlExceptionError(de, 'Payload Address insert Error', 'CaseHelper', 'getCustomerEDAAddress: ' + calledFrom, JSON.serialize(insAdr));
			}
    	}
    	return addressId;
    }

    public static Map<Id, List<Address__c>> getAddresses(Set<string> AccId) {
    	Map<Id, List<Address__c>> addressMap = new Map<Id, List<Address__c>>();
        List<Address__c> AddressList = [SELECT Id, AccountId__c, Address_Line_1__c, Address_Line_2__c, City__c, StateList__c, Zip_Code__c, Country__c, Address_Type__c, Preferred__c, LastModifiedDate 
                       FROM Address__c WHERE AccountId__c IN :AccId ORDER BY LastModifiedDate DESC];
		if (AddressList.size() > 0) {
			for (Address__c adr:AddressList) {
    			if (addressMap.get(adr.AccountId__c) != null) {
    				addressMap.get(adr.AccountId__c).add(adr);
    			} else {
    				addressMap.put(adr.AccountId__c, new list<Address__c>{adr});
    			}
			}
		}
        return addressMap;
    }

    /*
     * Method to get the address id, if the address exist for the customer or create a new address and return the new id
     */
	public static id getAddressId(string sAdd1, string sAdd2, string sCity, string sState, string sZip, Id accId, Map<string, id> addressMap, string source) {
		id addressId;

		string dupStr = getAddressStr(sAdd1, sAdd2, sCity, sState, sZip);
		//system.debug('****** dupStr: ' + dupStr);

		if ((dupStr != null) && (addressMap.get(dupStr) != null)) {
			addressId = addressMap.get(dupStr);
		} else {
            Address__c addrObj = new Address__c();
            addrObj.Address_Line_1__c = sAdd1;
            addrObj.Address_Line_2__c = sAdd2;
            addrObj.City__c = sCity;
            addrObj.StateList__c = sState;
            addrObj.Zip_Code__c = sZip;
            //addrObj.Preferred__c = true;
            addrObj.AccountId__c = accId;
            addrObj.Country__c = 'USA';
            addrObj.Address_Type__c = 'Ship To';

			try {
				if(!Test.isRunningTest()){
		        	Database.SaveResult sr = Database.insert(addrObj, false);
				    if (sr.isSuccess()) {
						addressId = sr.getId();
				    } else {
				    	CaseHelper.databaseErrorLog(sr, '(' + source + ') Address insert try Error', 'AddressHelper', 'getAddressId', JSON.serialize(addrObj));
				    }
				}
    		} catch (DmlException de) {
            	CaseHelper.dmlExceptionError(de, '(' + source + ') Address insert DML catch Error', 'AddressHelper', 'getAddressId', JSON.serialize(addrObj));
			} catch (exception e) {
            	CaseHelper.exceptionError(e, '(' + source + ') Address insert catch Error', 'AddressHelper', 'getAddressId', JSON.serialize(addrObj));
        	}
		}
		addressMap.clear();
		return addressId;
	}

    /*
     * Method to get the address id for the given account based on the Preferrence
     */
	public static id getAccountAddressId(id accId) {
		//2) Preferred Address
		//3) Newest Preferred address (if multiple)
		//4) Newest Non-Preferred address (if no preferred addresses)

		//return address id
		id returnId;
		if(accId != null) {
			//get the last modified Preferred address id
			id preferredAdrId;
			//get the last modified Non-Preferred address id
			id adrId;
			List<Address__c> myAddress = [SELECT Id, AccountId__c, LastModifiedDate, Preferred__c FROM Address__c WHERE AccountId__c =:accId ORDER BY LastModifiedDate DESC];
			for(Address__c adr:myAddress) {
				if (adr.Preferred__c) {
					//if the address is Preferred and if the variable is not set
					if (preferredAdrId == null) preferredAdrId = adr.Id; 
				} else {
					//if the address is Non-Preferred and if the variable is not set
					if (adrId == null) adrId = adr.Id; 
				}

				if (preferredAdrId != null) {
					//if the Preferred address is set then break the loop
					break;
				}
			}

			if (preferredAdrId != null) {
				//2 or 3 criteria
				returnId = preferredAdrId;
			} else if (adrId != null) {
				//4 criteria
				returnId = adrId;
			}
		}
		return returnId;
	}
}