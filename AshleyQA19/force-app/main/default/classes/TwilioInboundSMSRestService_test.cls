@isTest
public class TwilioInboundSMSRestService_test {

    @isTest 
    static void testdoPost() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        TwilioConfig__c tconfig = new TwilioConfig__c();
        tconfig.Name = Utilities_SalesAndService.isSandbox ? 'TWILIO_Sandbox' : 'TWILIO_Prod';
        tconfig.Messaging_Service_Id__c = 'test service id';
        tconfig.AccountSid__c = 'AC03c2fcd60e144e7cbeee413fcbf812a3';
        tconfig.AuthToken__c = '12345678901234567890123456789012';
        insert tconfig;

        req.requestURI = '/services/apexrest/TwilioInboundSMS';
        req.httpMethod = 'POST';

        req.addParameter('From','+61458883222');
        req.addParameter('Body','This is a test');
        req.addParameter('To','+61437879336');
        req.addParameter('ToCountry','AU');
        req.addParameter('ToState','');
        req.addParameter('SmsMessageSid','SMS MESSAGE ID HERE ');
        req.addParameter('NumMedia','0');
        req.addParameter('ToCity','');
        req.addParameter('FromZip','');
        req.addParameter('SmsSid','SMS ID HERE');
        req.addParameter('SmsStatus','received');
        req.addParameter('FromCity','');
        req.addParameter('FromCountry','AU');
        req.addParameter('ToZip','');
        req.addParameter('MessageSid','MESSAGE SID HERE');
        req.addParameter('AccountSid','ACCOUNT SID HERE');
        req.addParameter('ApiVersion','2010-04-01');

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        TwilioInboundSMSRestService.doPost();    
        Map<String,String> params = new Map<String,String> {
             'To'   => '+17602444355',
             'From' => '13176664444',               
             'Body' => 'test message',
             'NumMedia' => '4',
             'MediaUrl1' => 'https://www.url1.com',
             'MediaContentType1' =>'jpeg',
             'MediaUrl2' => 'https://www.url1.com',
             'MediaContentType2' =>'jpeg'
         };
        Account ac = new Account();
        ac.Name = 'New test';
        insert ac;
        Case ca = new Case();
        ca.status = 'New Customer Reply';
        insert ca;
        Lead l = new Lead(Company='Test',LastName='Test');
		insert l;
        Task t = new Task(WhoId=l.Id,Subject='Test',ActivityDate=Date.Today());
    	insert t;        
        Contact con = new Contact(AccountId = ac.id,LastName = 'testaccount');
        insert con;
        
        Task tk = new task();
        tk.Description = 'Sms replay from customer 13176664444';
        tk.WhoId = con.id;
        tk.WhatId = ca.id;
        tk.Twilio_Sent_To_Phone_Number__c = '+13176664444';
        tk.Status = 'Completed';
       
        insert tk;
        
        TwilioInboundSMSRestService.SMSInfo si = new TwilioInboundSMSRestService.SMSInfo();
        TwilioInboundSMSRestService.ImageInfo iinfo = new TwilioInboundSMSRestService.ImageInfo();
        String myString = 'StringToBlob';
		Blob myBlob = Blob.valueof(myString);
        iinfo.fileName = 'test';
        iinfo.imageBlob = myBlob;
        iinfo.mediaType = 'pdf';
        iinfo.imageURL = 'http://www.test.com';
        
        List<TwilioInboundSMSRestService.ImageInfo> imlist = new List<TwilioInboundSMSRestService.ImageInfo>();
        imlist.add(iinfo);
        imlist.add(iinfo);
        User u = [Select id, name, email from User where id=: UserInfo.getUserId()];
        Id OwnerId = u.id;
        Id WhatId = ca.id;
        Id whoID = con.id;
        si.fromMobile = '13176664444';
        si.msgBody = 'Replay test sms';
        si.toDetails = 'Case update for image';
        si.imageList = imlist;        
        TwilioInboundSMSRestService.getTargetTask('987654321');
        TwilioInboundSMSRestService.getAccountId('9876543210');
        TwilioInboundSMSRestService.saveTask(si, whoId, whatId, ownerId);
        //TwilioInboundSMSRestService.getOneImage('http://www.image.com');        
        TwilioInboundSMSRestService.saveSMS(params);        
    }
    @isTest
    static void testdoPost1() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        TwilioConfig__c tconfig = new TwilioConfig__c();
        tconfig.Name = Utilities_SalesAndService.isSandbox ? 'TWILIO_Sandbox' : 'TWILIO_Prod';
        tconfig.Messaging_Service_Id__c = 'test service id';
        tconfig.AccountSid__c = 'AC03c2fcd60e144e7cbeee413fcbf812a3';
        tconfig.AuthToken__c = '12345678901234567890123456789012';
        insert tconfig;

        req.requestURI = '/services/apexrest/TwilioInboundSMS';
        req.httpMethod = 'POST';

        req.addParameter('From','+61458883222');
        req.addParameter('Body','This is a test');
        req.addParameter('To','+61437879336');
        req.addParameter('ToCountry','AU');
        req.addParameter('ToState','');
        req.addParameter('SmsMessageSid','SMS MESSAGE ID HERE ');
        req.addParameter('NumMedia','0');
        req.addParameter('ToCity','');
        req.addParameter('FromZip','');
        req.addParameter('SmsSid','SMS ID HERE');
        req.addParameter('SmsStatus','received');
        req.addParameter('FromCity','');
        req.addParameter('FromCountry','AU');
        req.addParameter('ToZip','');
        req.addParameter('MessageSid','MESSAGE SID HERE');
        req.addParameter('AccountSid','ACCOUNT SID HERE');
        req.addParameter('ApiVersion','2010-04-01');

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        TwilioInboundSMSRestService.doPost();    
        Map<String,String> params = new Map<String,String> {
             'To'   => '+17602444355',
             'From' => '13176664444',               
             'Body' => 'test message',
             'NumMedia' => '4',
             'MediaUrl1' => 'https://www.url1.com',
             'MediaContentType1' =>'jpeg',
             'MediaUrl2' => 'https://www.url1.com',
             'MediaContentType2' =>'jpeg'
         };
        Account ac = new Account();
        ac.Name = 'New test';
        insert ac;
        Case ca = new Case();
        ca.status = 'New Customer Reply';
        insert ca;
        Lead l = new Lead(Company='Test',LastName='Test');
		insert l;
        Task t = new Task(WhoId=l.Id,Subject='Test',ActivityDate=Date.Today());
    	insert t;        
        Contact con = new Contact(AccountId = ac.id,LastName = 'testaccount');
        insert con;
        
        Task tk = new task();
        tk.Description = 'Sms replay from customer 13176664444';
        tk.WhoId = con.id;
        tk.WhatId = ac.id;
        tk.Twilio_Sent_To_Phone_Number__c = '+13176664444';
        tk.Status = 'Completed';
       
        insert tk;
        
        TwilioInboundSMSRestService.SMSInfo si = new TwilioInboundSMSRestService.SMSInfo();
        TwilioInboundSMSRestService.ImageInfo iinfo = new TwilioInboundSMSRestService.ImageInfo();
        String myString = 'StringToBlob';
		Blob myBlob = Blob.valueof(myString);
        iinfo.fileName = 'test';
        iinfo.imageBlob = myBlob;
        iinfo.mediaType = 'pdf';
        iinfo.imageURL = 'http://www.test.com';
        
        List<TwilioInboundSMSRestService.ImageInfo> imlist = new List<TwilioInboundSMSRestService.ImageInfo>();
        imlist.add(iinfo);
        imlist.add(iinfo);
        User u = [Select id, name, email from User where id=: UserInfo.getUserId()];
        Id OwnerId = u.id;
        Id WhatId = ac.id;
        Id whoID = con.id;
        si.fromMobile = '13176664444';
        si.msgBody = 'Replay test sms';
        si.toDetails = 'Case update for image';
        si.imageList = imlist;        
        TwilioInboundSMSRestService.getTargetTask('987654321');
        TwilioInboundSMSRestService.getAccountId('9876543210');
        TwilioInboundSMSRestService.saveTask(si, whoId, whatId, ownerId);
        //TwilioInboundSMSRestService.getOneImage('http://www.image.com');        
        TwilioInboundSMSRestService.saveSMS(params);        
    }
    @isTest
    static void testdoPost2() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        TwilioConfig__c tconfig = new TwilioConfig__c();
        tconfig.Name = Utilities_SalesAndService.isSandbox ? 'TWILIO_Sandbox' : 'TWILIO_Prod';
        tconfig.Messaging_Service_Id__c = 'test service id';
        tconfig.AccountSid__c = 'AC03c2fcd60e144e7cbeee413fcbf812a3';
        tconfig.AuthToken__c = '12345678901234567890123456789012';
        insert tconfig;

        req.requestURI = '/services/apexrest/TwilioInboundSMS';
        req.httpMethod = 'POST';
        

        req.addParameter('From','+61458883222');
        req.addParameter('Body','This is a test');
        req.addParameter('To','+61437879336');
        req.addParameter('ToCountry','AU');
        req.addParameter('ToState','');
        req.addParameter('SmsMessageSid','SMS MESSAGE ID HERE ');
        req.addParameter('NumMedia','0');
        req.addParameter('ToCity','');
        req.addParameter('FromZip','');
        req.addParameter('SmsSid','SMS ID HERE');
        req.addParameter('SmsStatus','received');
        req.addParameter('FromCity','');
        req.addParameter('FromCountry','AU');
        req.addParameter('ToZip','');
        req.addParameter('MessageSid','MESSAGE SID HERE');
        req.addParameter('AccountSid','ACCOUNT SID HERE');
        req.addParameter('ApiVersion','2010-04-01');

        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        TwilioInboundSMSRestService.doPost();    
        Map<String,String> params = new Map<String,String> {
             'To'   => '+17602444355',
             'From' => '13176664444',               
             'Body' => 'test message',
             'NumMedia' => '4',
             'MediaUrl1' => 'https://www.url1.com',
             'MediaContentType1' =>'jpeg',
             'MediaUrl2' => 'https://www.url1.com',
             'MediaContentType2' =>'jpeg'
         };
        Account ac = new Account();
        ac.Name = 'New test1';
        insert ac;
        Case ca = new Case();
        ca.status = 'Closed';
        ca.Type_of_Resolution__c = 'Reselect';
        ca.Resolution_Notes__c = 'Resolution test';
        insert ca;
        Lead l = new Lead(Company='Test',LastName='Test');
		insert l;
        Task t = new Task(WhoId=l.Id,Subject='Test',ActivityDate=Date.Today());
    	insert t;        
        Contact con = new Contact(AccountId = ac.id,LastName = 'testaccount');
        insert con;
        
        Task tk = new task();
        tk.Description = 'Sms replay from customer 13176664444';
        tk.WhoId = con.id;
        tk.WhatId = ca.id;
        tk.Twilio_Sent_To_Phone_Number__c = '+13176664444';
        tk.Status = 'Completed';
       
        insert tk;
        
        TwilioInboundSMSRestService.SMSInfo si = new TwilioInboundSMSRestService.SMSInfo();
        TwilioInboundSMSRestService.ImageInfo iinfo = new TwilioInboundSMSRestService.ImageInfo();
        String myString = 'StringToBlob';
		Blob myBlob = Blob.valueof(myString);
        iinfo.fileName = 'test';
        iinfo.imageBlob = myBlob;
        iinfo.mediaType = 'pdf';
        iinfo.imageURL = 'http://www.test.com';
        
        List<TwilioInboundSMSRestService.ImageInfo> imlist = new List<TwilioInboundSMSRestService.ImageInfo>();
        imlist.add(iinfo);
        imlist.add(iinfo);
        User u = [Select id, name, email from User where id=: UserInfo.getUserId()];
        Id OwnerId = u.id;
        Id WhatId = ac.id;
        Id whoID = con.id;
        si.fromMobile = '13176664444';
        si.msgBody = 'Replay test sms';
        si.toDetails = 'Case update for image';
        si.imageList = imlist;        
        TwilioInboundSMSRestService.getTargetTask('987654321');
        TwilioInboundSMSRestService.getAccountId('9876543210');
        TwilioInboundSMSRestService.saveTask(si, whoId, whatId, ownerId);
        //TwilioInboundSMSRestService.getOneImage('http://www.image.com');        
        TwilioInboundSMSRestService.saveSMS(params);        
    }
}