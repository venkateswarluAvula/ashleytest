public class supRepWrapper{
    @AuraEnabled public string EmployeeId {get;set;}
    @AuraEnabled public string ManagerName {get;set;}
    @AuraEnabled public string ManagerId {get;set;}
    @AuraEnabled public string EmployeeName {get;set;}
    @AuraEnabled public string inboundcalltype {get;set;}
    @AuraEnabled public string outboundcalltype {get;set;}
    @AuraEnabled public decimal inboundcallduration {get;set;}
    @AuraEnabled public decimal outboundcallduration {get;set;}
    @AuraEnabled public integer inboundrecordcount {get;set;}
    @AuraEnabled public integer outboundrecordcount {get;set;}
    //Added for case types and their record counts
    
    //for New case
    @AuraEnabled public integer Newcaseibduration {get;set;}
    @AuraEnabled public integer Newibcount {get;set;}
    @AuraEnabled public integer Newcaseobduration {get;set;}
    @AuraEnabled public integer Newobcount {get;set;}
    
    //for Working case
    @AuraEnabled public integer Workingcaseibduration {get;set;}
    @AuraEnabled public integer Workingibcount {get;set;}
    @AuraEnabled public integer Workingcaseobduration {get;set;}
    @AuraEnabled public integer Workingobcount {get;set;}
    
    //for Awaiting Customer Reply case
    @AuraEnabled public integer AwaitCustRespcaseibduration {get;set;}
    @AuraEnabled public integer AwaitCustRespibcount {get;set;}
    @AuraEnabled public integer AwaitCustRespcaseobduration {get;set;}
    @AuraEnabled public integer AwaitCustRespobcount {get;set;}
    
    //for New Customer Reply case
    @AuraEnabled public integer NewCustReplycaseibduration {get;set;}
    @AuraEnabled public integer NewCustReplyibcount {get;set;}
    @AuraEnabled public integer NewCustReplycaseobduration {get;set;}
    @AuraEnabled public integer NewCustReplyobcount {get;set;}
    
    //For Sent to Store case
    @AuraEnabled public integer SentToStorecaseibduration {get;set;}
    @AuraEnabled public integer SentToStoreibcount {get;set;}
    @AuraEnabled public integer SentToStorecaseobduration {get;set;}
    @AuraEnabled public integer SentToStoreobcount {get;set;}
    
    //For Ready for Review case
    @AuraEnabled public integer ReadyForReviewcaseibduration {get;set;}
    @AuraEnabled public integer ReadyForReviewibcount {get;set;}
    @AuraEnabled public integer ReadyForReviewcaseobduration {get;set;}
    @AuraEnabled public integer ReadyForReviewobcount {get;set;}
    
    //For Closed case 
    @AuraEnabled public integer Closedcaseibduration {get;set;}
    @AuraEnabled public integer Closedibcount {get;set;}
    @AuraEnabled public integer Closedcaseobduration {get;set;}
    @AuraEnabled public integer Closedobcount {get;set;}
    
    //For Closed in Salesforce case
    @AuraEnabled public integer ClosedInSFcaseibduration {get;set;}
    @AuraEnabled public integer ClosedInSFibcount {get;set;}
    @AuraEnabled public integer ClosedInSFcaseobduration {get;set;}
    @AuraEnabled public integer ClosedInSFobcount {get;set;}
    
    //For Open case
    @AuraEnabled public integer Opencaseibduration {get;set;}
    @AuraEnabled public integer Openibcount {get;set;}
    @AuraEnabled public integer Opencaseobduration {get;set;}
    @AuraEnabled public integer Openobcount {get;set;}
}